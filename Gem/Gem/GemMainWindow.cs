﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Tools;
using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Animation;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Gem
{
    public partial class GemMainWindow : Form
    {
        GemApplication app;
        SceneService ss;
        ContentService cs;

        bool loaded;

        public GemMainWindow()
        {
            InitializeComponent();
        }

        private void GemMainWindow_Load(object sender, EventArgs e)
        {
            EngineConfiguration configuration = new EngineConfiguration();
            configuration.Handle = xnaTarget.Handle;
            configuration.TargetWidth = xnaTarget.Width;
            configuration.TargetHeight = xnaTarget.Height;
            configuration.Fullscreen = false;
            configuration.VSync = false;
            configuration.Name = "Gem Model Viewer";

            app = GemApplication.App;
            app.Initialize(configuration);

            Application.Idle += delegate { Invalidate(); };
            Application.ApplicationExit += delegate { app.Dispose(); };

            ss = ServiceProvider.GetService<SceneService>();
            cs = ServiceProvider.GetService<ContentService>();

            Reset();
        }

        private void Reset()
        {
            // reset state
            GemEditorState state = app.GetCurrentState() as GemEditorState;
            state.Reset();

            // reset controls and options here
            cmbAnimations.Items.Clear();
            cmbLoopType.SelectedIndex = 0;

            nudBlendMs.Value = 100;
            nudSpeed.Value = (decimal)1.0;

            textBox1.Text = "";
            loaded = false;
            UpdateControls();
        }

        private void GemMainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            app.ExitRequested = true;
        }

        private void GemMainWindow_Paint(object sender, PaintEventArgs e)
        {
            if (app.ExitRequested)
                Application.Exit();

            if (app.IsInitialized)
                app.OneFrame();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            app.ExitRequested = true;
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void LoadModel(string filename)
        {
            SceneNode modelNode = ss.AddSceneNode("model", true);
            modelNode.Model = cs["game"].Load<Model>(filename);
        }

        private void UpdateControls()
        {            
            SceneNode modelNode = ss["model"];
            if (modelNode == null)
            {
                // disable controls
                cmbAnimations.Enabled = false;
                cmbLoopType.Enabled = false;
                nudBlendMs.Enabled = false;
                nudSpeed.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
            }
            else
            {
                // update animation list
                if (modelNode.Animation != null)
                {
                    Dictionary<string, AnimationClip> clips = modelNode.Animation.AnimationClips;
                    if (clips != null)
                    {
                        foreach (AnimationClip clip in clips.Values)
                        {
                            cmbAnimations.Items.Add(clip.Name);
                        }
                        cmbAnimations.SelectedIndex = 0;

                        cmbAnimations.Enabled = true;
                        cmbLoopType.Enabled = true;
                        nudBlendMs.Enabled = true;
                        nudSpeed.Enabled = true;
                        button1.Enabled = true;
                        button2.Enabled = true;
                        button3.Enabled = true;
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SceneNode modelNode = ss["model"];
            modelNode.Animation.Stop();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog(this);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            //Reset();

            LoadModel(textBox1.Text);
            UpdateControls();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SceneNode modelNode = ss["model"];
            string animationName = cmbAnimations.SelectedItem.ToString();
            AnimationPlay loop = AnimationPlay.Once;
            if (cmbLoopType.SelectedIndex == 1)
                loop = AnimationPlay.Loop;

            AnimationClip clip = modelNode.Animation[animationName];
            clip.Speed = (float)nudSpeed.Value;

            modelNode.Animation.StartClip(animationName, loop);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            SceneNode modelNode = ss["model"];
            string animationName = cmbAnimations.SelectedItem.ToString();
            AnimationPlay loop = AnimationPlay.Once;
            if (cmbLoopType.SelectedIndex == 1)
                loop = AnimationPlay.Loop;

            AnimationClip clip = modelNode.Animation[animationName];
            clip.Speed = (float)nudSpeed.Value;

            modelNode.Animation.BlendInto(animationName, loop, (float)nudBlendMs.Value);
        }
    }
}
