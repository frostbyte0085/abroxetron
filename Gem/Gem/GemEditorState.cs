﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;

namespace Gem
{
    public class GemEditorState : State
    {
        SceneService ss;
        GraphicsService gs;
        ContentService cs;
        DebugRendererService debug;

        SceneNode cameraNode;
        SceneNode modelNode;

        public GemEditorState()
        {
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();
            debug = ServiceProvider.GetService<DebugRendererService>();
        }

        public void Reset()
        {
            ss.Clear(true);

            cameraNode = ss.AddSceneNode("camera");
            cameraNode.Camera = new Camera();
            cameraNode.Camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.1f, 1000.0f);
            cameraNode.Translation = new Vector3(0, 0, 10);
            ss.ActiveCameraNode = cameraNode;

            modelNode = null;            

            // add a light so we can see the beauty of our model :)
            SceneNode light = ss.AddSceneNode("light");
            light.Light = new Engine.Graphics.DirectionalLight(new Vector4(1, 1, 1, 1), 1.0f);
            light.Orientation = Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), MathHelper.ToRadians(-75));
        }

        public override bool OnEnter()
        {
            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        private void UpdateInput()
        {
            KeyboardState ks = Keyboard.GetState();
            // camera movement
            if (ks.IsKeyDown(Keys.Up))
            {
                cameraNode.Translate(new Vector3(0, 1, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.Down))
            {
                cameraNode.Translate(new Vector3(0, -1, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.Left))
            {
                cameraNode.Translate(new Vector3(-1, 0, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.Right))
            {
                cameraNode.Translate(new Vector3(1, 0, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.W))
            {
                cameraNode.Translate(new Vector3(0, 0, -1) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.S))
            {
                cameraNode.Translate(new Vector3(0, 0, 1) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.C))
            {
                cameraNode.Translation = new Vector3(0, 0, 10);
            }

            // model movement and rotation
            if (ks.IsKeyDown(Keys.Y))
            {
                modelNode.Translate(new Vector3(0, 1, 0) * GameTiming.TimeElapsed * 7);
            }
            if (ks.IsKeyDown(Keys.H))
            {
                modelNode.Translate(new Vector3(0, -1, 0) * GameTiming.TimeElapsed * 7);
            }
            if (ks.IsKeyDown(Keys.G))
            {
                modelNode.Translate(new Vector3(-1, 0, 0) * GameTiming.TimeElapsed * 7);
            }
            if (ks.IsKeyDown(Keys.J))
            {
                modelNode.Translate(new Vector3(1, 0, 0) * GameTiming.TimeElapsed * 7);
            }
        }

        public override bool OnUpdate()
        {
            debug.AddLine3D(new Vector3(-1, 0, 0), new Vector3(1, 0, 0), Color.Red);
            debug.AddLine3D(new Vector3(0, -1, 0), new Vector3(0, 1, 0), Color.Red);
            debug.AddLine3D(new Vector3(0, 0, -1), new Vector3(0, 0, 1), Color.Red);

            modelNode = ss["model"];
            if (modelNode != null)
                UpdateInput();

            return base.OnUpdate();
        }
    }
}
