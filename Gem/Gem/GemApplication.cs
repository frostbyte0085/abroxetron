﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine;
using Engine.Core;
using Engine.Diagnostics;
using Engine.Tools;

namespace Gem
{
    public class GemApplication : EngineApplication
    {
        static GemApplication _app;

        public GemApplication()
            : base()
        {

        }

        public static GemApplication App
        {
            get
            {
                if (_app == null)
                    _app = new GemApplication();
                return _app;
            }
        }

        public override void Initialize(EngineConfiguration configuration)
        {
            base.Initialize(configuration);
        }

        public override void SetupStates()
        {
            AddState("GemEditor", new GemEditorState());
            SetState("GemEditor");

            base.SetupStates();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
