/**
 *  Our custom XML exporter. Export specific 3ds max nodes into the
 *  an XML file format which is specific to our game.
 *
 * @author Olivier "SanitySlayer" Legat
 */
include "WkXmlLibrary.ms"

/* ----------------------------------------------------------------	*
 * 							FILE HANDLING							*
 * ----------------------------------------------------------------	*/
 -- Where to export the file:
OUTFILE = "../../Game2/GameContent/" + (getFileNameFile maxFileName) + ".xml";
 -- to avoid "already exists" error, delete this file.
deletefile OUTFILE

/* ----------------------------------------------------------------	*
 * 							GLOBAL VARIABLES						*
 * ----------------------------------------------------------------	*/
-- XML Document File
myNewXmlDoc = tXMLDocument()
myNewXmlDoc.mfCreateXML rXmlFilePath:OUTFILE

-- XML root node struct
myRootNode = myNewXmlDoc.mfGetRootNode()
myRootNode = myNewXmlDoc.mfCreateRootNode "scene"

/* ----------------------------------------------------------------	*
 * 							SHARED FUNCTIONS						*
 * ----------------------------------------------------------------	*/

/**
 * input 
 *    > Chan:int - value of a color channel [0,255]
 * output
 *    > :string - the value of the color normalized between [0,1] 
 *      as a string
 */
fn NormalizedColorChannel Chan =
(
	return ((Chan / 255) as string)
)
/**
 * input 
 *    > Chan:color - a color with values for R, G and B.
 * output
 *    > :string - the value of the 3 color channels normalized 
 *      between [0,1] seperated by a ','
 */
fn GetColorAsText COL =
(
	local colStr = ""
	colStr = colStr + (NormalizedColorChannel COL.R) + ","
	colStr = colStr + (NormalizedColorChannel COL.G) + ","
	colStr = colStr + (NormalizedColorChannel COL.B)
	return colStr
)

/**
 * input
 *    > nodeType:object - any 3ds node object
 *    > name:string - value for the "name" attribute
 * output
 *    > :tXMLNode a new XML node of the form: <$nodeType name=$name>
 */
fn NewSceneNode nodeType OBJ =
(
	local objectNode = myNewXmlDoc.mfGetNewNode nodeType
	objectNode.mfAddAttribute "name" OBJ.name
	
	local propertiesFile = (getUserProp OBJ "propertiesClass")
	if ( not (propertiesFile == undefined) ) then
	(
		objectNode.mfAddAttribute "properties" propertiesFile
	)
	
	local behaviourFile = (getUserProp OBJ "behaviourClass")
	if ( not (behaviourFile == undefined) ) then
	(
		objectNode.mfAddAttribute "behaviour" behaviourFile
	)
	
	return objectNode
) 

/**
 * input
 *    > objectNode:tXMLNode - node to add the sub-node to.
 *    > tagName:string - Text to use for the tag.
 *    > text:string - Text to use in the between the <> and </> tags
 * output
 *    > n/a
 * effect
 *    > adds a tXMLNode of the form <$tagName>$text</$tagName> to
 *      objectNode
 */
fn AddSceneNodeProperty objectNode tagName text =
(
	local propNode = myNewXmlDoc.mfGetNewNode tagName
	propNode.mfSetText text;
	
	objectNode.mfAppendChild propNode
)

/**
 * input
 *    > objectNode:tXMLNode - node to add translation to
 *    > pos:Point3 - a vector with x,z,y coordinates
 * output
 *    > n/a
 * effect
 *    > adds a <translation> tag to the object. Spawns y and z.
 *      Adds negative sign to y.
 */
fn AddTranslation objectNode pos = 
(
	local translation = (pos.x as string) +","+ (pos.z as string) +","+ ((-pos.y) as string)
	AddSceneNodeProperty objectNode "translation" translation
)

/**
 * input
 *    > objectNode:tXMLNode - node to add rotation to
 *    > rot:quaternion - a quat with x,z,y,w values
 * output
 *    > n/a
 * effect
 *    > adds a <rotation> tag to the object. Spawns y and z.
 *      Adds negative sign to y.
 */
fn AddRotation objectNode rot =
(
	local rotation = (rot.x as string)
	rotation = rotation +","+ (rot.z as string)
	rotation = rotation +","+ ((-rot.y) as string)
	rotation = rotation +","+ (rot.w as string)
	AddSceneNodeProperty objectNode "rotation" rotation
)

/**
 * input
 *    > objectNode:tXMLNode - node to add volume to
 *    > vol:point3 - a vector with x,z,y values representing the widths
 * output
 *    > n/a
 * effect
 *    > adds a <volume> tag to the object. Spawns y and z.
 */
fn AddVolume objectNode vol =
(
	local volume = (vol.x as string) +","+ (vol.z as string) +","+ (vol.y as string)
	AddSceneNodeProperty objectNode "volume" volume
)

/* ----------------------------------------------------------------	*
 * 						NODE PROCESSING FUNCTIONS					*
 * ----------------------------------------------------------------	*/

 /**
  * input
  *    > OBJ:Geometry - any object of the geometry class
  * output
  *    > n/a
  * effect
  *    > adds an <object> node to the root node
  */
fn ProcessGeometry OBJ =
(
	local objectNode = NewSceneNode "object" OBJ
	AddTranslation objectNode OBJ.pos
	AddRotation objectNode OBJ.rotation
	myRootNode.mfAppendChild objectNode
)

 /**
  * input
  *    > OBJ:Omnilight - an omnilight node
  * output
  *    > n/a
  * effect
  *    > adds a <light> node to the root node
  */
fn ProcessOmniLight OBJ =
(
	local objectNode = NewSceneNode "light" OBJ
	
	AddTranslation objectNode OBJ.pos
	AddSceneNodeProperty objectNode "intensity" (OBJ.multiplier as string)
	AddSceneNodeProperty objectNode "color" (GetColorAsText OBJ.rgb)
	AddSceneNodeProperty objectNode "radius" (OBJ.DecayRadius as string)
	AddSceneNodeProperty objectNode "shadow" (GetColorAsText OBJ.ShadowColor)
	AddSceneNodeProperty objectNode "castshadows" (OBJ.castShadows as string)
	AddSceneNodeProperty objectNode "enabled" (OBJ.enabled as string)
	
	myRootNode.mfAppendChild objectNode
)

 /**
  * input
  *    > OBJ:Dummy - an dummy node
  * output
  *    > n/a
  * effect
  *    > adds a <trigger> node to the root node
  */
fn ProcessDummy OBJ =
(
	local objectNode = NewSceneNode "trigger" OBJ
	
	AddTranslation objectNode OBJ.pos
	AddRotation objectNode OBJ.rotation
	AddVolume objectNode (OBJ.boxsize * OBJ.scale)
	-- TODO enabled
	
	myRootNode.mfAppendChild objectNode
)

 /**
  * input
  *    > OBJ:Point - a spawn point node
  * output
  *    > n/a
  * effect
  *    > adds a <trigger> node to the root node
  */
fn ProcessSpawnPoint OBJ =
(
	local objectNode = NewSceneNode "spawn" OBJ
	
	AddTranslation objectNode OBJ.pos
	
	myRootNode.mfAppendChild objectNode
)

/**
 * input
 *   > OBJ:camera - a Target camera
 * output
 *   > n/a
 * For doc on cameras :
 * http://docs.autodesk.com/3DSMAX/14/ENU/MAXScript%20Help%202012/
 */
fn ProcessCamera OBJ =
(
	local objectNode = NewSceneNode "camera"
	if object.parent != undefined then
	(
		objectNode.mfAddAttribute "parent" OBJ.parent.name
	)
	
	AddTranslation objectNode OBJ.pos
	AddRotation objectNode OBJ.rotation
)

/* ----------------------------------------------------------------	*
 * 				 NODE ENUMERATION / FUNCTION MAPPING				*
 * ----------------------------------------------------------------	*/

/* Create children */
for OBJ in $* do (
	-- Point Light:
	if classOf OBJ == Omnilight then
	(
		ProcessOmniLight OBJ
	)
	
	-- Prop objects:
	else if superClassOf OBJ == GeometryClass  then
	(
		ProcessGeometry OBJ
	)
	
	-- Trigger:
	else if classOf OBJ == Dummy then 
	(
		ProcessDummy OBJ
	)
	
	-- spawn point: 
	else if classOf OBJ == Point then
	(
		ProcessSpawnPoint OBJ
	)
	
	-- Camera details:
	else if classOf OBJ == camera then
	(
		
	)
	else
	(
		string s = "Warning:: Ditched node: "+OBJ.name
		print s
	)
)

/* Save and close */
myNewXmlDoc.mfSaveXML()
myNewXmlDoc.mfReleaseXML()