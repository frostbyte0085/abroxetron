for OBJ in $* do (
	print "--------------------------"
	print OBJ.name
	print (classOf OBJ) as string
	print (superClassOf OBJ) as string
)