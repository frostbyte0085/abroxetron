﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using System.IO;
using System.ComponentModel;

using TInput = System.String;
using TOutput = System.String;

namespace ExtendedEnginePipeline
{
    public class DeferredProcessor : ModelProcessor
    {

        [Browsable(false)]
        public override bool ColorKeyEnabled
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        [Browsable(false)]
        public override bool ResizeTexturesToPowerOfTwo
        {
            get
            {
                return false;
            }
            set
            {
            }

        }

        [Browsable(false)]
        public override Color ColorKeyColor
        {
            get
            {
                return base.ColorKeyColor;
            }
            set
            {
            }
        }

        [Browsable(false)]
        public override bool PremultiplyTextureAlpha
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        [Browsable(false)]
        public override bool PremultiplyVertexColors
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        [Browsable(false)]
        public override bool GenerateTangentFrames
        {
            get { return true; }
            set { }
        }

        protected String directory;

        [DisplayName("Normal Map Texture")]
        [Description("If set, this file will be used as the normal map on the model, " +
        "unless a material in the model is found.")]
        [DefaultValue("")]
        public string NormalMapTexture
        {
            get { return normalMapTexture; }
            set { normalMapTexture = value; }
        }
        protected string normalMapTexture;
        [DisplayName("Normal Map Key")]
        [Description("This will be the key that will be used to search the normal map in the opaque data of the model")]
        [DefaultValue("Bump")]
        public string NormalMapKey
        {
            get { return normalMapKey; }
            set { normalMapKey = value; }
        }
        protected string normalMapKey = "Bump";

        [DisplayName("Texture Map Texture")]
        [Description("If set, this file will be used as the texture map on the model, " +
        "unless a material in the model is found.")]
        [DefaultValue("")]
        public string TextureMapTexture
        {
            get { return textureMapTexture; }
            set { textureMapTexture = value; }
        }
        protected string textureMapTexture;
        [DisplayName("Texture Map Key")]
        [Description("This will be the key that will be used to search the texture map in the opaque data of the model")]
        [DefaultValue("Texture")]
        public string TextureMapKey
        {
            get { return textureMapKey; }
            set { textureMapKey = value; }
        }
        protected string textureMapKey = "Texture";

        [DisplayName("Specular Map Texture")]
        [Description("If set, this file will be used as the specular map on the model, " +
        "unless a material in the model is found.")]
        [DefaultValue("")]
        public string SpecularMapTexture
        {
            get { return specularMapTexture; }
            set { specularMapTexture = value; }
        }
        protected string specularMapTexture;
        [DisplayName("Specular Map Key")]
        [Description("This will be the key that will be used to search the specular map in the opaque data of the model")]
        [DefaultValue("Specular")]
        public string SpecularMapKey
        {
            get { return specularMapKey; }
            set { specularMapKey = value; }
        }
        protected string specularMapKey = "Specular";



        [DisplayName("Emissive Map Texture")]
        [Description("If set, this file will be used as the emissive map on the model, " +
        "unless a material in the model is found.")]
        [DefaultValue("")]
        public string EmissiveMapTexture
        {
            get { return emissiveMapTexture; }
            set { emissiveMapTexture = value; }
        }
        protected string emissiveMapTexture;
        [DisplayName("Emissive Map Key")]
        [Description("This will be the key that will be used to search the emissive map in the opaque data of the model")]
        [DefaultValue("Emissive")]
        public string EmissiveMapKey
        {
            get { return emissiveMapKey; }
            set { emissiveMapKey = value; }
        }
        protected string emissiveMapKey = "Emissive";


        [DisplayName("Parallax Map Texture")]
        [Description("If set, this file will be used as the parallax map on the model, " +
        "unless a material in the model is found.")]
        [DefaultValue("")]
        public string ParallaxMapTexture
        {
            get { return parallaxMapTexture; }
            set { parallaxMapTexture = value; }
        }
        protected string parallaxMapTexture;
        [DisplayName("Parallax Map Key")]
        [Description("This will be the key that will be used to search the parallax map in the opaque data of the model")]
        [DefaultValue("Parallax")]
        public string ParallaxMapKey
        {
            get { return parallaxMapKey; }
            set { parallaxMapKey = value; }
        }
        protected string parallaxMapKey = "Parallax";

        [DisplayName("Parallax scale")]
        [DefaultValue(0.04)]
        public float ParallaxScale
        {
            set;
            get;
        }

        [DisplayName("Parallax bias")]
        [DefaultValue(-0.03)]
        public float ParallaxBias
        {
            set;
            get;
        }

        protected void LookUpNormalMaps(MeshContent mesh)
        {
            //If the NormalMapTexture property is set, we use that normal map for all meshes in the model.
            //This overrides anything else
            if (!String.IsNullOrEmpty(NormalMapTexture))
            {
                normalMapPath = NormalMapTexture;
            }
            else
            {
                //If NormalMapTexture is not set, we look into the opaque data of the model, 
                //and search for a texture with the key equal to NormalMapKey
                normalMapPath = mesh.OpaqueData.GetValue<string>(NormalMapKey, null);
            }
            //if the NormalMapTexture Property was not used, and the key was not found in the model, than normalMapPath would have the value null.
            if (normalMapPath == null)
            {
                //If a key with the required name is not found, we make a final attempt, 
                //and search, in the same directory as the model, for a texture named 
                //meshname_n.tga, where meshname is the name of a mesh inside the model.
                normalMapPath = Path.Combine(directory, mesh.Name + "_n.png");
                
                if (!File.Exists(normalMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_normal.tga
                    //normalMapPath = "../EngineContent/Textures/null_normal.tga";
                    normalMapPath = "";
                }
            }
            else
            {
                normalMapPath = Path.Combine(directory, normalMapPath);
                if (!File.Exists(normalMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_normal.tga
                    //normalMapPath = "../EngineContent/Textures/null_normal.tga";
                    normalMapPath = "";
                }
            }
        }

        protected void LookUpTextureMaps(MeshContent mesh)
        {
            //If the SpecularMapTexture property is set, we use it
            if (!String.IsNullOrEmpty(TextureMapTexture))
            {
                textureMapPath = TextureMapTexture;
            }
            else
            {
                //If SpecularMapTexture is not set, we look into the opaque data of the model, 
                //and search for a texture with the key equal to specularMapKey
                textureMapPath = mesh.OpaqueData.GetValue<string>(textureMapKey, null);
                

            }
            if (textureMapPath == null)
            {
                //we search, in the same directory as the model, for a texture named 
                //meshname_s.tga
                textureMapPath = Path.Combine(directory, mesh.Name + "_d.png");
                if (!File.Exists(textureMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_diffuse.tga
                    //textureMapPath = "../EngineContent/Textures/null_diffuse.tga";
                    textureMapPath = "";
                }
            }
            else
            {
                textureMapPath = Path.Combine(directory, textureMapPath);
                if (!File.Exists(textureMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_diffuse.tga
                    //textureMapPath = "../EngineContent/Textures/null_diffuse.tga";
                    textureMapPath = "";
                }
            }
        }

        protected void LookUpSpecularMaps(MeshContent mesh)
        {
            //If the SpecularMapTexture property is set, we use it
            if (!String.IsNullOrEmpty(SpecularMapTexture))
            {
                specularMapPath = SpecularMapTexture;
            }
            else
            {
                //If SpecularMapTexture is not set, we look into the opaque data of the model, 
                //and search for a texture with the key equal to specularMapKey
                specularMapPath = mesh.OpaqueData.GetValue<string>(specularMapKey, null);
            }
            if (specularMapPath == null)
            {
                //we search, in the same directory as the model, for a texture named 
                //meshname_s.tga
                specularMapPath = Path.Combine(directory, mesh.Name + "_s.png");
                if (!File.Exists(specularMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_specular.tga
                    //specularMapPath = "../EngineContent/Textures/null_specular.tga";
                    specularMapPath = "";
                }
            }
            else
            {
                specularMapPath = Path.Combine(directory, specularMapPath);
                if (!File.Exists(specularMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_specular.tga
                    //specularMapPath = "../EngineContent/Textures/null_specular.tga";
                    specularMapPath = "";
                }
            }
        }

        protected void LookUpEmissiveMaps(MeshContent mesh)
        {
            //If the SpecularMapTexture property is set, we use it
            if (!String.IsNullOrEmpty(EmissiveMapTexture))
            {
                emissiveMapPath = EmissiveMapTexture;
            }
            else
            {
                //If EmissiveMapTexture is not set, we look into the opaque data of the model, 
                //and search for a texture with the key equal to emissiveMapKey
                emissiveMapPath = mesh.OpaqueData.GetValue<string>(emissiveMapKey, null);
            }
            if (emissiveMapPath == null)
            {
                //we search, in the same directory as the model, for a texture named 
                //meshname_s.tga
                emissiveMapPath = Path.Combine(directory, mesh.Name + "_e.png");
                if (!File.Exists(emissiveMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_specular.tga
                    //specularMapPath = "../EngineContent/Textures/null_specular.tga";
                    emissiveMapPath = "";
                }
            }
            else
            {
                emissiveMapPath = Path.Combine(directory, emissiveMapPath);
                if (!File.Exists(emissiveMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_specular.tga
                    //specularMapPath = "../EngineContent/Textures/null_specular.tga";
                    emissiveMapPath = "";
                }
            }
        }

        protected void LookUpParallaxMaps(MeshContent mesh)
        {
            //If the ParallaxMapTexture property is set, we use it
            if (!String.IsNullOrEmpty(ParallaxMapTexture))
            {
                parallaxMapPath = ParallaxMapTexture;
            }
            else
            {
                //If EmissiveMapTexture is not set, we look into the opaque data of the model, 
                //and search for a texture with the key equal to parallaxMapKey
                parallaxMapPath = mesh.OpaqueData.GetValue<string>(parallaxMapKey, null);
            }
            if (parallaxMapPath == null)
            {
                //we search, in the same directory as the model, for a texture named 
                //meshname_s.tga
                parallaxMapPath = Path.Combine(directory, mesh.Name + "_p.png");
                if (!File.Exists(parallaxMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_specular.tga
                    //specularMapPath = "../EngineContent/Textures/null_specular.tga";
                    parallaxMapPath = "";
                }
            }
            else
            {
                parallaxMapPath = Path.Combine(directory, parallaxMapPath);
                if (!File.Exists(parallaxMapPath))
                {
                    //if this fails also (that texture does not exist), 
                    //then we use a default texture, named null_specular.tga
                    //specularMapPath = "../EngineContent/Textures/null_specular.tga";
                    parallaxMapPath = "";
                }
            }
        }

        protected ContentProcessorContext context;

        protected ModelContent model;

        protected ModelContent ModelContent
        {
            get
            {
                return model;
            }
        }

        public override ModelContent Process(NodeContent input, ContentProcessorContext context)
        {
            this.context = context;
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }

            directory = Path.GetDirectoryName(input.Identity.SourceFilename);
            LookUpTextures(input);

            model = base.Process(input, context);
            Optimize(model);
            
            Dictionary<string, object> tagData = new Dictionary<string, object>();
            BoundingSphere bs = CalculateBoundingSphere();
            BoundingBox bb = CalculateBoundingBox();
                
            tagData.Add("BoundingSphere", bs);
            tagData.Add("BoundingBox", bb);
            tagData.Add("ContentScale", Scale);

            model.Tag = tagData;
            return model;
        }
        
        protected void Optimize(ModelContent model)
        {
            foreach (ModelMeshContent mesh in model.Meshes)
            {
                MeshHelper.OptimizeForCache(mesh.SourceMesh);
            }
        }

        private BoundingSphere CalculateBoundingSphere()
        {
            return BoundingSphere.CreateFromBoundingBox (CalculateBoundingBox());
        }

        private BoundingBox CalculateBoundingBox()
        {
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            List<Vector3> points = new List<Vector3>();

            foreach (ModelMeshContent mesh in model.Meshes)
            {
                MeshContent mc = mesh.SourceMesh;
                if (mc != null)
                {
                    Matrix absTransform = mc.AbsoluteTransform;

                    foreach (GeometryContent gc in mc.Geometry)
                    {
                        foreach (int index in gc.Indices)
                        {
                            // Look up the position of this vertex.
                            Vector3 vertex = gc.Vertices.Positions[index];
                            Vector3 transformedVertex = Vector3.Transform(vertex, absTransform);

                            points.Add(transformedVertex);
                        } 
                    }
                }
            }
            return BoundingBox.CreateFromPoints(points);
        }

        protected string normalMapPath;
        protected string specularMapPath;
        protected string textureMapPath;
        protected string emissiveMapPath;
        protected string parallaxMapPath;

        protected void FixTexRef(ref ExternalReference<TextureContent> texRef)
        {
            int filenameLength = texRef.Filename.Length;
            TOutput[] newFilename = texRef.Filename.Split('\\');

            texRef.Filename = Path.Combine(directory, newFilename[newFilename.Length - 1]);
        }

        protected void LookUpTextures(NodeContent node)
        {
            MeshContent mesh = node as MeshContent;
            if (mesh != null)
            {
                LookUpTextureMaps(mesh);
                LookUpNormalMaps(mesh);
                LookUpSpecularMaps(mesh);
                LookUpEmissiveMaps(mesh);
                LookUpParallaxMaps(mesh);
                
                //add the keys to the material, so they can be used by the shader
                foreach (GeometryContent geometry in mesh.Geometry)
                {
                    //in some .fbx files, the key might be found in the textures collection, but not
                    //in the mesh, as we checked above. If this is the case, we need to get it out, and
                    //add it with the "NormalMap" key
                    if (geometry.Material == null)
                        geometry.Material = new MaterialContent();

                    if (geometry.Material.Textures.ContainsKey(textureMapKey))
                    {
                        ExternalReference<TextureContent> texRef = geometry.Material.Textures[textureMapKey];
                        
                        FixTexRef(ref texRef);

                        geometry.Material.Textures.Remove(textureMapKey);
                        if (File.Exists(texRef.Filename))
                        {                            
                            geometry.Material.Textures.Add(textureMapKey, texRef);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(textureMapPath))
                                geometry.Material.Textures.Add(textureMapKey,
                                        new ExternalReference<TextureContent>(textureMapPath));
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty( textureMapPath))
                            geometry.Material.Textures.Add(textureMapKey,
                                        new ExternalReference<TextureContent>(textureMapPath));
                    }

                    if (geometry.Material.Textures.ContainsKey(specularMapKey))
                    {
                        ExternalReference<TextureContent> texRef = geometry.Material.Textures[specularMapKey];

                        FixTexRef(ref texRef);

                        geometry.Material.Textures.Remove(specularMapKey);
                        if (File.Exists(texRef.Filename))
                        {
                            geometry.Material.Textures.Add(specularMapKey, texRef);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(specularMapPath))
                                geometry.Material.Textures.Add(specularMapKey,
                                    new ExternalReference<TextureContent>(specularMapPath));
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(specularMapPath))
                            geometry.Material.Textures.Add(specularMapKey,
                                    new ExternalReference<TextureContent>(specularMapPath));
                    }

                    if (geometry.Material.Textures.ContainsKey(normalMapKey))
                    {
                        ExternalReference<TextureContent> texRef = geometry.Material.Textures[normalMapKey];

                        FixTexRef(ref texRef);

                        geometry.Material.Textures.Remove(normalMapKey);
                        if (File.Exists(texRef.Filename))
                        {
                            geometry.Material.Textures.Add(normalMapKey, texRef);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(normalMapPath))
                            {
                                geometry.Material.Textures.Add(normalMapKey,
                                    new ExternalReference<TextureContent>(normalMapPath));
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(normalMapPath))
                            geometry.Material.Textures.Add(normalMapKey,
                                    new ExternalReference<TextureContent>(normalMapPath));
                    }

                    if (geometry.Material.Textures.ContainsKey(emissiveMapKey))
                    {
                        ExternalReference<TextureContent> texRef = geometry.Material.Textures[emissiveMapKey];

                        FixTexRef(ref texRef);

                        geometry.Material.Textures.Remove(emissiveMapKey);
                        if (File.Exists(texRef.Filename))
                        {
                            geometry.Material.Textures.Add(emissiveMapKey, texRef);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(emissiveMapPath))
                                geometry.Material.Textures.Add(emissiveMapKey,
                                    new ExternalReference<TextureContent>(emissiveMapPath));
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(emissiveMapPath))
                            geometry.Material.Textures.Add(emissiveMapKey,
                                    new ExternalReference<TextureContent>(emissiveMapPath));
                    }

                    if (geometry.Material.Textures.ContainsKey(parallaxMapKey))
                    {
                        ExternalReference<TextureContent> texRef = geometry.Material.Textures[parallaxMapKey];

                        FixTexRef(ref texRef);

                        geometry.Material.Textures.Remove(parallaxMapKey);
                        if (File.Exists(texRef.Filename))
                        {
                            geometry.Material.Textures.Add(parallaxMapKey, texRef);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(parallaxMapPath))
                                geometry.Material.Textures.Add(parallaxMapKey,
                                    new ExternalReference<TextureContent>(parallaxMapPath));
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(parallaxMapPath))
                            geometry.Material.Textures.Add(parallaxMapKey,
                                    new ExternalReference<TextureContent>(parallaxMapPath));
                    }
                }

            }
            // go through all children and apply LookUpTextures recursively
            foreach (NodeContent child in node.Children)
            {
                LookUpTextures(child);
            }
        }
    }
}
