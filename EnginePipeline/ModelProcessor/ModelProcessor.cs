using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using System.IO;
using System.ComponentModel;

using TInput = System.String;
using TOutput = System.String;

namespace ExtendedEnginePipeline
{
    [ContentProcessor(DisplayName = "Extended Model - UEA XNA")]
    public class ExtendedModel : DeferredProcessor
    {
        protected static IList<string> acceptableVertexChannelNames = new string[]
            {
                VertexChannelNames.TextureCoordinate(0),
                VertexChannelNames.Normal(0),
                VertexChannelNames.Binormal(0),
                VertexChannelNames.Tangent(0)
            };

        protected override void ProcessVertexChannel(GeometryContent geometry,
                                            int vertexChannelIndex, ContentProcessorContext context)
        {
            String vertexChannelName = geometry.Vertices.Channels[vertexChannelIndex].Name;
            // if this vertex channel has an acceptable names, process it as normal.
            if (acceptableVertexChannelNames.Contains(vertexChannelName))
            {
                base.ProcessVertexChannel(geometry, vertexChannelIndex, context);
            }
            // otherwise, remove it from the vertex channels; it's just extra data
            // we don't need.
            else
            {
                geometry.Vertices.Channels.Remove(vertexChannelName);
            }
        }

        [Browsable(false)]
        [DefaultValue(MaterialProcessorDefaultEffect.BasicEffect)]
        public override MaterialProcessorDefaultEffect DefaultEffect
        {
            get { return MaterialProcessorDefaultEffect.BasicEffect; }
            set { }
        }

        [Browsable(true)]
        [DefaultValue(false)]
        public bool IsLiquid
        {
            set;
            get;
        }

        public override ModelContent Process(NodeContent input, ContentProcessorContext context)
        {
            model = base.Process(input, context);

            Dictionary<string, object> tags = model.Tag as Dictionary<string, object>;
            tags["isLiquid"] = IsLiquid;
            model.Tag = tags;

            return model;
        }

        

        protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
        {
            EffectMaterialContent deferredShadingMaterial = new EffectMaterialContent();
            deferredShadingMaterial.Effect = new ExternalReference<EffectContent>("../EngineContent/Effects/Deferred/RenderGBuffer.fx");
            // copy the textures in the original material to the new normal mapping
            // material, if they are relevant to our renderer. The
            // LookUpTextures function has added the normal map and specular map
            // textures to the Textures collection, so that will be copied as well.
            foreach (KeyValuePair<String, ExternalReference<TextureContent>> texture in material.Textures)
            {
                if ((texture.Key == textureMapKey) ||
                        (texture.Key == normalMapKey) ||
                        (texture.Key == specularMapKey ||
                        (texture.Key == emissiveMapKey) ||
                        (texture.Key == parallaxMapKey))) 
                {
                    deferredShadingMaterial.Textures.Add(texture.Key, texture.Value);
                }
            }

            return context.Convert<MaterialContent, MaterialContent>(deferredShadingMaterial, typeof(MaterialProcessor).Name);
        }

    }
}