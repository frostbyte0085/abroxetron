// this is used for rendering XNA Sprite using VS/PS 3.0

sampler Texture : register(s0);
float2  TextureSize;

float4x4 MatrixTransform;

void Sprite_DefaultVS(inout float4 color    : COLOR0,
                        inout float2 texCoord : TEXCOORD0,
                        inout float4 position : SV_Position)
{
    position = mul(position, MatrixTransform);
}

float4 Sprite_DefaultPS(float4 Color: COLOR0, float2 TexCoord: TEXCOORD0):COLOR0
{    
	return tex2D(Texture, TexCoord) * Color;
}