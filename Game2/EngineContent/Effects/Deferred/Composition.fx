#include "../packing.h"

float2 GBufferTextureSize;
sampler Albedo : register(s0);
sampler LightMap : register(s1);
sampler DepthMap: register(s2);

//Vertex Input Structure
struct VSI
{
	float3 Position : POSITION0;
	float2 UV : TEXCOORD0;
};

//Vertex Output Structure
struct VSO
{
	float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
};

//Vertex Shader
VSO VS(VSI input)
{
	VSO output;
	output.Position = float4(input.Position, 1);
	output.UV = input.UV + float2(0.5/GBufferTextureSize.xy);

	return output;
}

//Pixel Output Structure
struct PSO
{
	float4 Color: COLOR0;
	float Depth: DEPTH;
};

//Pixel Shader
PSO PS(VSO input)
{
	PSO output;

	// to create the accumulated image, we combine the base albedo colour and the calculated lightmap
	float4 Color = tex2D(Albedo, input.UV);
	float4 Lighting = tex2D(LightMap, input.UV);

	// add the emissive map color
	float realEmissive = Color.w*10;
	float blend = min(1,realEmissive);
	float4 finalColor = saturate(Color * float4(1,1,1,1)*0.035 + float4(Color.xyz * Lighting.xyz + Lighting.w, 1.0) * (1-blend) + Color * realEmissive);

	output.Color = finalColor;

	// write back the depth so we can reuse it in forward-pass rendering, such as the particle system
	output.Depth = tex2D(DepthMap, input.UV).r;
	
	return output;
}

//Technique
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}