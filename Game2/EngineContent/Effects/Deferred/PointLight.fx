#include "../packing.h"

float4x4 World;
float4x4 View;
float4x4 inverseView;
float4x4 Projection;
float4x4 InverseViewProjection;
float3 CameraPosition;
float3 LightPosition;
float LightRadius;
float4 LightColor;
float LightIntensity;
float2 GBufferTextureSize;
sampler GBuffer0 : register(s0); // albedo
sampler GBuffer1 : register(s1); // normal 
sampler GBuffer2 : register(s2); // depth

//Vertex Input Structure
struct VSI
{
	float4 Position : POSITION0;
};
//Vertex Output Structure
struct VSO
{
	float4 Position : POSITION0;
	float4 ScreenPosition : TEXCOORD0;
};

//Vertex Shader
VSO VS(VSI input)
{
	VSO output;
	float4 worldPosition = mul(input.Position, World);
	float4 viewPosition = mul(worldPosition, View);
	output.Position = mul(viewPosition, Projection);
	
	output.ScreenPosition = output.Position;
	
	return output;
}

//Phong Shader
float4 Phong(float3 Position, float3 N, float SpecularIntensity, float SpecularPower)
{
	float3 L = LightPosition - Position;
	float Distance = length(L);

	// simple linear attenuation
	float Factor = max(.01f, Distance) / LightRadius;
	float Attenuation = saturate(1 - Factor);

	L = normalize(L);

	float3 R = normalize(reflect(-L, N));
	float3 E = normalize(CameraPosition - Position.xyz);
	float NL = dot(N, L);
	float3 Diffuse = NL * LightColor.xyz;
	float Specular = SpecularIntensity * pow(saturate(dot(R, E)), SpecularPower);

	return Attenuation * LightIntensity * float4(Diffuse.rgb, Specular);
}

//Pixel Shader
float4 PS(VSO input) : COLOR0
{
	float SpecularIntensity = 0;
	float SpecularPower = 0;

	input.ScreenPosition.xy /= input.ScreenPosition.w;
	float2 UV = 0.5f * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1) + float2(0.5 / GBufferTextureSize.xy);

	float4 diffuseMap = tex2D(GBuffer0, UV);
	//Get All Data from Normal part of the GBuffer
	float4 encodedNormal = tex2D(GBuffer1, UV);
	//Decode Normal
	float3 Normal = UnpackNIEfromRGB10A2(encodedNormal, SpecularIntensity, SpecularPower);
	SpecularPower *= 255.0;
		
	Normal = mul(Normal, inverseView);
	
	//Get Depth from GBuffer
	float Depth = tex2D(GBuffer2, UV).x;
	
	//Make Position in Homogenous Space using current ScreenSpace coordinates and the Depth from the GBuffer
	float4 Position = 1.0f;
	Position.xy = input.ScreenPosition.xy;
	Position.z = Depth;
	//Transform Position from Homogenous Space to World Space
	Position = mul(Position, InverseViewProjection);
	Position /= Position.w;

	float4 finalColor = Phong(Position.xyz, Normal, SpecularIntensity, SpecularPower);
	return finalColor;
}
//Technique
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}