#include "../packing.h"

//Vertex Shader
float4 VS(float3 Position : POSITION0) : POSITION0
{
	return float4(Position, 1);
}

//Pixel Shader Out
struct PSO
{
	float4 Albedo : COLOR0;
	float4 Normals : COLOR1;
	float4 Depth : COLOR2;
};

//Pixel Shader
PSO PS()
{
	// clear the MRT to some initial values.
	PSO output;
	output.Albedo = 0.0f;
	output.Normals.xyz = 0.5f;
	output.Normals.w = 0.0f;
	output.Depth = 1.0f;

	return output;
}

//Technique
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}