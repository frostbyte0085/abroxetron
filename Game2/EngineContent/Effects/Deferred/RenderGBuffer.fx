#include "../packing.h"

float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 WorldViewIT;

//Color Texture
texture Texture;
bool hasTextureMap;

//Normal Texture
texture Bump;
bool hasNormalMap;

//Specular Texture
texture Specular;
bool hasSpecularMap;

// Emissive Texture
texture Emissive;
bool hasEmissiveMap;
float EmissiveAdditive;

// Parallax Texture
texture Parallax;
bool hasParallaxMap;
float parallaxScale;
float parallaxBias;

float3 colorOverride;
bool isLiquid;
float elapsedTime;
float currentTime;

float3 tint;
float tintTime;

//Albedo Sampler
sampler AlbedoSampler = sampler_state
{
	texture = <Texture>;
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
	MAXANISOTROPY = 16;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
//NormalMap Sampler
sampler NormalSampler = sampler_state
{
	texture = <Bump>;
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC;
	MAXANISOTROPY = 16;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
//SpecularMap Sampler
sampler SpecularSampler = sampler_state
{
	texture = <Specular>;
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC;
	MAXANISOTROPY = 16;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

//SpecularMap Sampler
sampler EmissiveSampler = sampler_state
{
	texture = <Emissive>;
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC;
	MAXANISOTROPY = 16;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

//ParallaxMap Sampler
sampler ParallaxSampler = sampler_state
{
	texture = <Parallax>;
	MINFILTER = ANISOTROPIC;
	MAGFILTER = ANISOTROPIC;
	MAXANISOTROPY = 16;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

//Vertex Input Structure
struct VSI
{
	float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 UV : TEXCOORD0;
	float3 Tangent : TANGENT0;
	float3 BiTangent : BINORMAL0;
};
//Vertex Output Structure
struct VSO
{
	float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float3 Depth : TEXCOORD1;
	float3x3 TBN : TEXCOORD2;
};

// a small function to simulate crappy water :)
// DrGoggler lives again!
float2 liquidVS(float2 uv) : TEXCOORD0
{
	return uv * 40 + currentTime;
}

//Vertex Shader
VSO VS(VSI input)
{
	//Initialize Output
	VSO output;

	float4 worldPosition = mul(input.Position, World);
	float4 viewPosition = mul(worldPosition, View);
	output.Position = mul(viewPosition, Projection);
	//Pass Depth
	output.Depth.x = output.Position.z;
	output.Depth.y = output.Position.w;
	output.Depth.z = viewPosition.z;

	//Build TBN Matrix
	output.TBN[0] = normalize(mul(input.Tangent, (float3x3)WorldViewIT));
	output.TBN[1] = normalize(mul(input.BiTangent, (float3x3)WorldViewIT));
	output.TBN[2] = normalize(mul(input.Normal, (float3x3)WorldViewIT));

	//Pass UV	
	if (isLiquid)
		output.UV = liquidVS(input.UV);
	else
		output.UV = input.UV;

	//Return Output
	return output;
}
//Pixel Output Structure
struct PSO
{
	float4 Albedo : COLOR0;
	float4 Normals : COLOR1;
	float4 Depth : COLOR2;
};

void liquidPS()
{
}

float3 ToGreyscale(float3 original)
{
	return dot(original, float3(0.3, 0.59, 0.11));
}

//Pixel Shader
PSO PS(VSO input)
{
	PSO output;
	float2 uv = input.UV;
	
	if (isLiquid)
		liquidPS();

	float4 albedo = float4(1,1,1,0);
	float specular_i = 0;
	float specular_e = 0;
	float emissiveIntensity = 0;
	float3 normal = input.TBN[2];

	// this is not really used. I didn't have time to implement it sadly :(
	float parallaxFactor = 0;
	if (hasParallaxMap)
	{
		// parallax factor
		parallaxFactor = tex2D(ParallaxSampler, uv).r;
		parallaxFactor *= 0.01;//parallaxScale + parallaxBias;

		uv += parallaxFactor;
	}

	float4 newAlbedo;
	//Pass Albedo from Texture
	// Especially for the wizards, if the alpha is less than 0.8, then we can replace the colour
	// with an override. That is how we change the wizard robe main colour
	if (hasTextureMap)
	{
		albedo = tex2D(AlbedoSampler, input.UV);
		if (albedo.a <= 0.8)
			albedo.rgb += colorOverride;

		newAlbedo = albedo;
	}

	albedo.rgb = lerp(newAlbedo, tint * ToGreyscale(newAlbedo), tintTime);

	if (hasSpecularMap)
	{
		specular_i = tex2D(SpecularSampler, input.UV).x;
		specular_e = tex2D(SpecularSampler, input.UV).y;
	}

	if (hasNormalMap)
	{
		normal = tex2D(NormalSampler, uv).xyz * 2.0f - 1.0f;
		normal = normalize(mul(normal, input.TBN));
	}

	if (hasEmissiveMap)
	{
		// emissive intensity
		float4 emissiveColor = tex2D(EmissiveSampler, input.UV);
		emissiveIntensity = (emissiveColor.r + emissiveColor.g + emissiveColor.b)/3;
	}
	emissiveIntensity += EmissiveAdditive;
	emissiveIntensity = clamp(emissiveIntensity, 0, 1);

	output.Normals = PackNIEtoRGB10A2(normal, specular_i, specular_e);
	output.Albedo = float4(albedo.rgb, emissiveIntensity);

	// screen space depth
	output.Depth = input.Depth.x / input.Depth.y;
	// view space depth
	output.Depth.g = input.Depth.z;

	//Return Output
	return output;
}
//Technique
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}