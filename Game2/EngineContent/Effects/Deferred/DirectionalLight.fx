#include "../packing.h"

float4x4 InverseViewProjection;
float4x4 inverseView;
float3 CameraPosition;
float3 L;
float4 LightColor;

float LightIntensity;
float2 GBufferTextureSize;
sampler GBuffer0 : register(s0); // albedo
sampler GBuffer1 : register(s1); // normal
sampler GBuffer2 : register(s2); // depth

//Vertex Input Structure
struct VSI
{
	float3 Position : POSITION0;
	float2 UV : TEXCOORD0;
};
//Vertex Output Structure
struct VSO
{
	float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
};
//Vertex Shader
VSO VS(VSI input)
{
	VSO output;

	output.Position = float4(input.Position, 1);
	output.UV = input.UV + float2(0.5 / GBufferTextureSize.xy);

	return output;
}

//Phong Shader
float4 Phong(float3 Position, float3 N, float SpecularIntensity, float SpecularPower)
{
	float3 R = normalize(reflect(L, N));
	float3 E = normalize(CameraPosition - Position.xyz);
	float NL = dot(N, -L);
	float3 Diffuse = NL * LightColor.xyz;
	float Specular = SpecularIntensity * pow(saturate(dot(R, E)), SpecularPower);

	return LightIntensity * float4(Diffuse.rgb, Specular);
}

//Pixel Shader
float4 PS(VSO input) : COLOR0
{
	float SpecularIntensity = 0;
	float SpecularPower = 0;

	float4 diffuseMap = tex2D(GBuffer0, input.UV);

	//Get All Data from Normal part of the GBuffer
	float4 encodedNormal = tex2D(GBuffer1, input.UV);
	//Decode Normal
	float3 Normal = UnpackNIEfromRGB10A2(encodedNormal, SpecularIntensity, SpecularPower);
	SpecularPower *= 255.0;

	Normal = mul(Normal, inverseView);

	//Get Depth from GBuffer
	float Depth = tex2D(GBuffer2, input.UV).x;

	//Calculate Position in Homogenous Space
	float4 Position = 1.0f;
	float xPosition = input.UV.x * 2.0f - 1.0f;
	Position.x = xPosition;
	Position.y = -xPosition;
	Position.z = Depth;
	//Transform Position from Homogenous Space to World Space
	Position = mul(Position, InverseViewProjection);
	Position /= Position.w;
	
	float4 finalColor = Phong(Position.xyz, Normal, SpecularIntensity, SpecularPower);
	return finalColor;
}

//Technique
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}