const float Pi = 3.14159265;

float encodeFloat4in1(float4 channel)
{
	return dot( channel, float4(1.0, 1/255.0, 1/65025.0, 1/160581375.0) );
}

float4 decodeFloat1to4(float v)
{
	float4 enc = float4(1.0, 255.0, 65025.0, 160581375.0) * v;
	enc = frac(enc);
	enc -= enc.yzww * float4(1.0/255.0,1.0/255.0,1.0/255.0,0.0);
	return enc;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
float PackIEto10bit(in float I, in float E) {
    return (floor(I * 31) * 32 + E * 31) / 1023.0;
}
//function
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void UnpackIEfrom10bit(in float V, out float I, out float E) {

    float t1;
    float t2;
    
    t1  = V * 1023;
    t2  = floor(t1 / 32.0);
    I   = t2 / 31.0;
    E   = (t1 - t2 * 32) / 31.0;
    
}
//function
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
float4 PackNIEtoRGB10A2(in float3 N, in float I, in float E) {

    float4  color;
    float   phi;

    // Map E
    E           = saturate(log10(E) - 1);

    phi         = atan2(N.x, N.z); // Note! This gives NaN when x = z = 0
    
    color.r     = abs(phi) / Pi;
    color.g     = abs(N.y);
    color.b     = PackIEto10bit(I, E);
    
    float a1    = saturate(sign(phi)) * 0.3333;
    float a2    = saturate(sign(N.y)) * 0.6666;
    
    color.a     = a1 + a2;
    
    return color;
    
}
//function
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// R = abs(phi)
// G = abs(Y)
// B = IE
// A = sign(phi) + sign(Y)
float3 UnpackNIEfromRGB10A2(in float4 color, out float I, out float E) {

    UnpackIEfrom10bit(color.b, I, E);
    
    // Map E
    E = pow(10, E + 1);
    
    float signPhi   = sign(frac(color.a * 1.9) - 0.5);
    float signY     = sign(color.a - 0.5);
    float phi       = color.r * Pi * signPhi;
    float Y         = color.g * signY;
    
    float2 N;
    sincos(phi, N.x, N.y);
    
    const float threshold = 1021.0 / 1023.0;
    
    if (color.g > threshold) N = 0.0; // bodge up for singularity

    float L = sqrt(1.0 - Y * Y);
    
    return float3(N * L, Y).xzy;
    
}//function
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
