float4x4 World;
float4x4 View;
float4x4 Projection;

// ------------------------------------------------------------------
//   Structs
// ------------------------------------------------------------------

//Vertex Input/Output Structure
struct vpos
{
	float4 Position : POSITION;
	float4 Color : COLOR0;
};
//Pixel Output Structure
struct pso
{
	float4 color : COLOR0;
};

// ------------------------------------------------------------------
//   Shaders
// ------------------------------------------------------------------

//Vertex Shader
vpos vshader(vpos p)
{
	// Transpose 3D point into 2D point:
	float4 worldPosition = mul(p.Position, World);
	float4 viewPosition = mul(worldPosition, View);
	p.Position = mul(viewPosition, Projection);

	return p;
}

//Pixel Shader
pso pshader(vpos input)
{
	pso output = (pso)0;
	
	output.color = input.Color;
	
	return output;
}

// ------------------------------------------------------------------
//   Technique / pass
// ------------------------------------------------------------------

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 vshader();
		PixelShader = compile ps_3_0 pshader();
	}
}