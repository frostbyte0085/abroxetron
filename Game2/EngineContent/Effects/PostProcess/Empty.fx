#include "../sprite_ps3.h"

technique Default
{
    pass p0
    {
		VertexShader = compile vs_3_0 Sprite_DefaultVS();
        PixelShader = compile ps_3_0 Sprite_DefaultPS();
    }
}
