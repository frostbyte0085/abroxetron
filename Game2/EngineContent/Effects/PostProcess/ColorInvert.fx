#include "../sprite_ps3.h"


float4 PS(float2 TexCoord:TEXCOORD0): COLOR0
{
	float4 color = tex2D(Texture, TexCoord);

	return 1-color;
}

technique Default
{
    pass p0
    {
		VertexShader = compile vs_3_0 Sprite_DefaultVS();
        PixelShader = compile ps_3_0 PS();
    }
}
