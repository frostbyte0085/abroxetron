#include "../sprite_ps3.h"
#include "../packing.h"

// Pixel shader combines the glow image with the original scene

sampler GlowSampler : register(s0);
sampler BaseSampler : register(s1);
sampler GBufferAlbedoSampler : register(s2);

float4 PS(float2 TexCoord : TEXCOORD0) : COLOR0
{
    // Look up the bloom and original base image colors.
    float4 glow = tex2D(GlowSampler, TexCoord);
    float4 base = tex2D(BaseSampler, TexCoord);
    
	return saturate(base + glow * 15);
}


technique GlowCombine
{
    pass Pass1
    {
		VertexShader = compile vs_3_0 Sprite_DefaultVS();
        PixelShader = compile ps_3_0 PS();
    }
}
