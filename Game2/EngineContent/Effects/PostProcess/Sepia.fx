#include "../sprite_ps3.h"

float4 PS(float2 TexCoord: TEXCOORD0): COLOR0
{
	float4 color = tex2D(Texture, TexCoord);
 
    float4 outputColor = color;
    outputColor.r = (color.r * 0.393) + (color.g * 0.769) + (color.b * 0.189);
    outputColor.g = (color.r * 0.349) + (color.g * 0.686) + (color.b * 0.168);
    outputColor.b = (color.r * 0.272) + (color.g * 0.534) + (color.b * 0.131);

	return outputColor;
}

technique Default
{
    pass p0
    {
		VertexShader = compile vs_3_0 Sprite_DefaultVS();
        PixelShader = compile ps_3_0 PS();
    }
}
