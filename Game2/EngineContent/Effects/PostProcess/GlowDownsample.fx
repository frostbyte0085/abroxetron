#include "../sprite_ps3.h"
#include "../packing.h"

sampler TextureSampler : register(s0);
sampler GBufferAlbedoSampler: register(s2);


float4 PS(float2 TexCoord : TEXCOORD0) : COLOR0
{
	float2 texelSize = float2(1.0 / TextureSize.xy);

	float2 texcoord = TexCoord;

	float4 albedo=0;
	
	for (int i=0; i<4; i++)
	{
		texcoord.x = TexCoord.x;
		for (int j=0; j<4; j++)
		{
			albedo += tex2D(GBufferAlbedoSampler, texcoord);
			texcoord.x += texelSize.x;
		}
		texcoord.y += texelSize.y;
	}
	albedo /= 16;

	return saturate(float4(albedo.rgb * albedo.w, 1));

}


technique GlowDownsample
{
    pass Pass1
    {
		VertexShader = compile vs_3_0 Sprite_DefaultVS();
        PixelShader = compile ps_3_0 PS();
    }
}
