//------------------------------------------------------
//  code from www.riemers.net  
//------------------------------------------------------

struct VertexData
{
    float3 Position   	: POSITION0;
    float2 TextureCoords: TEXCOORD0;
	float4 Color		: COLOR0;
	float2 Transform	: TEXCOORD1;
};

struct VertexToPixel
{
    float4 Position   	: POSITION0;
    float2 TextureCoords: TEXCOORD0; 
	float4 Color		: COLOR0;
};

struct PixelToFrame
{
    float4 Color : COLOR0;
};

//------- Constants --------
float4x4 xView;
float4x4 xProjection;
float4x4 xWorld;
float3 xCamPos;
float3 xCamUp;
float2 xDepthBufferSize;

//------- Texture Samplers --------

Texture xTexture;
sampler TextureSampler = sampler_state 
{ 
	texture = <xTexture>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter=LINEAR; 
	AddressU = mirror; 
	AddressV = mirror;
};

Texture xDepth;
sampler DepthSampler = sampler_state 
{ 
	texture = <xDepth>; 
	magfilter = POINT; 
	minfilter = POINT; 
	mipfilter = POINT; 
	AddressU = CLAMP; 
	AddressV = CLAMP;
};
//------- Helper functions --------

// v - Vector to rotate
// u - Axis
// angle - Angle in radians
//
// refer to http://en.wikipedia.org/wiki/Rotation_matrix
float3 RotateAroundAxis(float3 v, float3 u, float angle) 
{
	float cosa = cos(angle);
	float sina = sin(angle);
	float cosa1 = 1 - cosa;

	float3x3 R = 0;
	R[0][0] = u.x*u.x*cosa1 + cosa;
	R[0][1] = u.x*u.y*cosa1 + u.z*sina;
	R[0][2] = u.x*u.z*cosa1 - u.y*sina;

	R[1][0] = u.y*u.x*cosa1 - u.z*sina;
	R[1][1] = u.y*u.y*cosa1 + cosa;
	R[1][2] = u.y*u.z*cosa1 + u.x*sina;

	R[2][0] = u.z*u.x*cosa1 + u.y*sina;
	R[2][1] = u.z*u.y*cosa1 - u.x*sina;
	R[2][2] = u.z*u.z*cosa1 + cosa;

	return mul(v, R);
}

//------- Technique: PointSprites --------

VertexToPixel PointSpriteVS(VertexData data)
{
	// Get Variables
	float3 inPos = data.Position;
	float2 inTexCoord = data.TextureCoords;
	float scale = data.Transform.x; 
	float angle = data.Transform.y;
	
	// Init ouput var.
    VertexToPixel Output = (VertexToPixel)0;

    float3 center = mul(inPos, xWorld);
    float3 eyeVector = center - xCamPos;

	// Calculate local axis of the billboard:
    float3 sideVector = cross(eyeVector,xCamUp);
    sideVector = normalize(sideVector);
    float3 upVector = cross(sideVector,eyeVector);
    upVector = normalize(upVector);
	float3 forwardVector = normalize(eyeVector);

	// Calculate real position of this vertex:
    float3 finalPosition = (0,0,0);
    finalPosition += (inTexCoord.x-0.5f)*sideVector*0.5f*scale;
    finalPosition += (0.5f-inTexCoord.y)*upVector*0.5f*scale;
	//finalPosition += (inTexCoord.x-0.5f)*sideVector*0.5f*3.0f;
    //finalPosition += (0.5f-inTexCoord.y)*upVector*0.5f*3.0f;

	// Rotate the vertex:
	finalPosition = RotateAroundAxis (finalPosition, forwardVector, angle);

	// Translate it so that it's not centered at 0,0,0
	finalPosition += center;
    float4 finalPosition4 = float4(finalPosition, 1);

    float4x4 preViewProjection = mul (xView, xProjection);

    Output.Position = mul(finalPosition4, preViewProjection);
    Output.TextureCoords = inTexCoord;
	Output.Color = data.Color;
	
    return Output;
}



PixelToFrame PointSpritePS(VertexToPixel PSIn)
{
    PixelToFrame Output = (PixelToFrame)0;

	float4 textCol = tex2D(TextureSampler, PSIn.TextureCoords);

	//Output.Color = (textCol + PSIn.Color) / 2.0f;
	//Output.Color.a = textCol.a * PSIn.Color.a;
	Output.Color = textCol * PSIn.Color;
	
	// sample depth buffer
	//float2 screenUV = PSIn.Position.xy / xDepthBufferSize;
	// get the view space depth that we stored in the GBuffer construction
	//float sceneDepth = tex2D(DepthSampler, screenUV.xy).g;
	//float particleFragDepth = 0;//PSIn.Position.z;
	//float depthDiff = sceneDepth - particleFragDepth;

	// soft particle
	//if (depthDiff < 0.5)
	//{
	//	Output.Color.a = 0.0f;
	//}

    return Output;
}

technique Default
{
	pass Pass0
	{   
		VertexShader = compile vs_3_0 PointSpriteVS();
		PixelShader  = compile ps_3_0 PointSpritePS();
	}
}