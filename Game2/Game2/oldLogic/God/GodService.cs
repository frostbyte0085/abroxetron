﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2.Logic.Command;
using Game2.Logic.Event;

namespace Game2.Logic.God
{
    /// <summary>
    /// Root of a Game logic... Essentially for the ingame State all that is
    /// need is GodService.Update()
    /// 
    /// God handles the interaction between the Commands and Events of the world.
    /// </summary>
    public sealed class GodService
    {
        private List<Commander> puppetMasters;
        private EventService eventService;
        
        public GodService(EventService eventService)
        {
            this.eventService = eventService;
        }

        public void Update()
        {
            // TODO
            // -> Handle game logic events
            //    - This will issue commands to certain objects and/or...
            //    - delete objects and/or...
            //    - create objects
            //
            // -> Update all running controllers (that is... user input and AI)
            //    - This will normally only issue (more) commands and/or...
            //
            // -> Update all commanders in puppetMaster
            //    - This will update the state of the game logic and/or
            //    - Create new events and send them to the EventHandler (which will be
            //      handled in the next frame).
            //    - Events are "created" by GameObjects... example: player dies, that object
            //      will trigger an event.
        }
    }
}
