﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

using Game2.Logic.Object;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Command
{
    public abstract class Commander : SceneNodeBehaviour
    {
        private Command cmd;
        private Animator animator;

        public Animator Animator
        {
            get { return animator; }
        }

        public Commander(Command cmd, GameObject node)
        {
            this.cmd = cmd;
            this.animator = new Animator();
            
        }

        public Command Cmd
        {
            set 
            { 
                //TODO if higher prioprity then:
                cmd = value; 
            }
            get { return this.cmd; }
        }

        public void IssueCommand(Command newCmd)
        {
            if (newCmd.PriorityLevel < this.cmd.PriorityLevel)
            {
                //this.cmd.Stop(time);
                this.cmd = newCmd;
                this.animator.PlayAnimation(cmd.AnimationName, cmd.AnimationPlay);
            }
            else
            {
                // Ignore this new cmd
            }
        }
    }
}
