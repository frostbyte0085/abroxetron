﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2.Logic.Object;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Command
{
    /// <summary>
    /// Describes the type of command issued
    /// 
    /// <value>RealTime</value> This command is one that can change value with respect
    ///   to time. Example: User input or physics.
    /// <value>Timed</value> This is command which is fixed and will stop after a specified
    ///   amount of time (or earlier if interrupted). Once this command is issued it will
    ///   never change. Example: "Swing your sword once."
    /// <value>Order</value> Similar to Timed, once issued this command never changes.
    ///   The difference to Timed is that the max. time for this command is unknown, so
    ///   it can potentially go on forever. Example: "Follow Player 3"
    /// </summary>
    enum CommandType
    {
        RealTime = 0,
        Timed = 1,
        Order = 2
    }

    public abstract class Command
    {
        private int priority;

        public int PriorityLevel
        {
            get { return priority; }
        }

        public Command(int prioLevel)
        {
            priority = prioLevel;
        }

        public string AnimationName { set; get; }

        public AnimationPlay AnimationPlay { set; get; }

        public abstract void Start(GameTime time);

        public abstract void Run(GameTime time);

        /// <summary>
        /// Do not confuse a command pause with game pause! A command can be used
        /// for a timed stunned for example.
        /// </summary>
        /// <param name="time"></param>
        public abstract void Pause(GameTime time);

        public abstract void Resume(GameTime time);

        public abstract void Stop(GameTime time);
    }
}
