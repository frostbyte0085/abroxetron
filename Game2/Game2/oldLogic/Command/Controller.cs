﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

namespace Game2.Logic.Command
{
    /// <summary>
    /// External scene node behaviour. (Typically user input or AI)
    /// </summary>
    public abstract class Controller : SceneNodeBehaviour
    {
    }

    /*
    public partial class PlayerInput : Controller {}

    public partial class AI : Controller { }
     */
}
