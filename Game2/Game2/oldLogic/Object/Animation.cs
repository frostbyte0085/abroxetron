﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Object
{
    public enum AnimationPlay
    {
        ONCE = 0,
        LOOP = 1,
        BLEND = 2
    }

    public interface IAnimation
    {
        void PlayAnimation(AnimationPlay play);

        void Update();
    }

    public class MeshAnimation : IAnimation
    {
        private Model model;
        private string animName;

        public Model Model
        {
            set { model = value; }
            get { return model; }
        }

        public string AnimationName
        {
            set { animName = value; }
            get { return animName; }
        }

        public MeshAnimation(string animName, Model model)
        {
            this.model = model;
            this.animName = animName;
        }

        public void PlayAnimation(AnimationPlay play) { /* TODO */ }

        public void Update() {/* TODO */}
    }

    public class ParticleAnimation : IAnimation
    {
        public void PlayAnimation(AnimationPlay play) { /* TODO */ }

        public void Update() { /* TODO */ }
    }

    public class LightAnimation : IAnimation
    {
        public void PlayAnimation(AnimationPlay play) { /* TODO */}

        public void Update() {/* TODO */}
    }

    public abstract class SceneNodeAnimation : IAnimation
    {
        public SceneNode Node { set; get; }

        public SceneNodeAnimation(SceneNode node)
        {
            Node = node;
        }

        public abstract void PlayAnimation(AnimationPlay play);

        public abstract void Update();
    }
}
