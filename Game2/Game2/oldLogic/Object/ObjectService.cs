﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Object
{
    public sealed class ObjectService
    {
        public ObjectService()
        {
        }

        public GameObject NewCharacter(/* data */) { return null; }

        public GameObject NewMissile(/* data */) { return null; }

        public GameObject NewProp(/* data */) { return null; }

        public GameObject NewEnvironmentObject(/* data */) { return null; }

        public List<PlayerChar> GetPlayers() { return null; }
    }
}
