﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

using Game2.Logic.Command;

namespace Game2.Logic.Object
{
    public class GameObject : SceneNode
    {
        private Commander cmder = null;

        public Commander Commander
        {
            get { return cmder; }
        }

        internal GameObject()
            : base()
        {
            //cmder = new Commander(null, this);
            //private
        }
    }

    public partial class GameCharacter : GameObject { }

    public partial class PlayerChar : GameCharacter { }

    public partial class FoeChar : GameCharacter { }

    public partial class Missile : GameObject { }

    public partial class Prop : GameObject { }

    public partial class Pickup : Prop { }

    public partial class Doodad : Prop { }

    public partial class EnvironmentObject : GameObject { }

    public interface Interactable 
    {
        void InteractWith(GameCharacter c);
    }
}
