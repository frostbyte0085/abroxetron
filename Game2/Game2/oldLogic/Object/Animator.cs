﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Object
{
    public sealed class Animator
    {
        private Dictionary<string, IAnimation> Animations { set; get; }
        private IAnimation currentAnimation = null;

        public Animator()
        {
            Animations = new Dictionary<string, IAnimation>();
        }

        public void AddAnimation(string key, IAnimation anim)
        {
            Animations.Add(key, anim);
        }
        public void RemoveAnimation(string key)
        {
            Animations.Remove(key);
        }

        public void PlayAnimation(string key, AnimationPlay play)
        {
            currentAnimation = Animations[key];

            /* TODO Enter blending here. */

            currentAnimation.PlayAnimation(play);
        }

        public void Update()
        {
            /* TODO Exit blending here. */
            if (currentAnimation != null)  currentAnimation.Update();
        }
    }
}
