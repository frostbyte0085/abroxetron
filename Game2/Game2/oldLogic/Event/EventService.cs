﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Event
{
    public class EventService
    {
        List<GameEvent> events;

        public EventService()
        {
        }

        public void AddEvent(GameEvent e) { events.Add(e);  }

        public void Update()
        {
            foreach (GameEvent e in events)
                HandleEvent(e);
            events.Clear();
        }

        private void HandleEvent(GameEvent e) { /* TODO */ }
    }
}
