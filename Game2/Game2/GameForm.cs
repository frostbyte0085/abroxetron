﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Engine;
using Engine.Tools;
using Engine.Core;
using Engine.Scene;

namespace Game2
{
    public partial class XNAWindowForm : Form
    {
        GameApplication app;

        public XNAWindowForm()
        {
            InitializeComponent();
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        private void XNAWindowForm_Load(object sender, EventArgs e)
        {
            EngineConfiguration configuration = new EngineConfiguration();
            configuration.Handle = Handle;

#if DEBUG
            configuration.TargetWidth = Width;
            configuration.TargetHeight = Height;
            configuration.Fullscreen = false;

#else
            configuration.TargetWidth = 1280;
            configuration.TargetHeight = 720;
            configuration.Fullscreen = true;
#endif
            configuration.VSync = true;
            configuration.Name = "Abroxetron";

            app = GameApplication.App;
            app.Initialize(configuration);

            Application.Idle += delegate { Invalidate(); };
            Application.ApplicationExit += delegate { app.Dispose(); };
        }

        private void XNAWindowForm_Paint(object sender, PaintEventArgs e)
        {
            if (app.ExitRequested)
                Application.Exit();

            app.OneFrame();
        }

        private void XNAWindowForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            app.ExitRequested = true;
        }
    }
}
