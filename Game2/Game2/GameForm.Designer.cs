﻿namespace Game2
{
    partial class XNAWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // XNAWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "XNAWindowForm";
            this.Text = "Game [Windows]";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.XNAWindowForm_FormClosing);
            this.Load += new System.EventHandler(this.XNAWindowForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.XNAWindowForm_Paint);
            this.ResumeLayout(false);

        }

        #endregion
    }
}