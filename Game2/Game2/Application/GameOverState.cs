﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Audio;
using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using Game2.Level;
using Game2.GameManager;
using Game2.UI;

using Engine.UI;

namespace Game2
{
    public sealed class GameOverState : State
    {
        SceneService ss;
        ContentService cs;
        GraphicsService gs;
        InputService input;
        InputDevice device;

        SceneNode sunLightNode;
        SceneNode cameraNode;

        SceneNode deadNode;
        SceneNode swordNode;

        float currentYaw = 0.0f;
        const float distance = 8.0f;
        const float pitch = -23.0f;
        const float height = 3.0f;
        const float turnspeed = 0.2f;

        float timeElapsed;
        bool timeToGo;

        UIBasicLayer layer;
        UITexture gameOver;
        UILabel whyGameOverLabel;

        public GameOverState()
        {
            ss = ServiceProvider.GetService<SceneService>();
            cs = ServiceProvider.GetService<ContentService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            input = ServiceProvider.GetService<InputService>();
        }

        public override bool OnEnter()
        {
            Texture2D texture =
                cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            UIAtlas atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            layer = new UIBasicLayer(atlas);

            UIRendererService urs = ServiceProvider.GetService<UIRendererService>();

            gameOver = new UITexture();
            gameOver.Texture = cs["game"].Load<Texture2D>("UI/GameOver");
            gameOver.Rectangle = new Rectangle(urs.VirtualResolution.X / 2 - gameOver.Texture.Width / 2, 50, gameOver.Texture.Width, gameOver.Texture.Height);

            string whyGameOverText = "";
            if(this.Parameters.Count > 0 && this.Parameters[0] is string)
                whyGameOverText = (string) this.Parameters[0];
            Vector2 dim = font.MeasureString(whyGameOverText);
            whyGameOverLabel = new UILabel(whyGameOverText, font, Color.White);
            whyGameOverLabel.Rectangle = new Rectangle(urs.VirtualResolution.X / 2 - (int)(dim.X / 2 + 0.5f), 550, (int)dim.X, (int)dim.Y);

            layer.Add(gameOver);
            layer.Add(whyGameOverLabel);
            urs.CurrentLayer = layer;

            timeElapsed = 0.0f;
            timeToGo = false;

            sunLightNode = ss.AddSceneNode("gameover_sunlight");
            sunLightNode.Light = new Engine.Graphics.DirectionalLight(new Vector4(1, 1, 1, 1), 0.0f);
            sunLightNode.Rotate(new Vector3(1, 0, 0), -75);

            Camera mainCamera = new Camera();
            mainCamera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.1f, 1000.0f);
            cameraNode = ss.AddSceneNode("MenuCamera");
            cameraNode.Camera = mainCamera;
            cameraNode.Translation = new Vector3(0, height, distance);
            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(pitch), 0);
            ss.ActiveCameraNode = cameraNode;

            deadNode = ss.AddSceneNode("dead");
            deadNode.Model = cs["game"].Load<Model>("Models/Wizard/WizardDead_Static");
            deadNode.MeshColor = new Vector3(1, 0, 0);

            // why doesn't this show up?
            swordNode = ss.AddSceneNode("sword");
            swordNode.Model = cs["game"].Load<Model>("Models/Wizard/Sword");
            swordNode.Translate(2, 0, 0);

            device = input.GetDevice(0);

            MediaPlayerService mps = ServiceProvider.GetService<MediaPlayerService>();
            mps.Play(cs["game"].Load<Song>("Music/gloom"));

            return base.OnEnter();
        }

        public override bool OnUpdate()
        {
            if (!timeToGo)
            {
                if (sunLightNode.Light.Intensity < 1.0f)
                    sunLightNode.Light.Intensity += GameTiming.TimeElapsed * 0.4f;
            }
            else
            {
                sunLightNode.Light.Intensity -= GameTiming.TimeElapsed * 0.5f;
                if (sunLightNode.Light.Intensity <= 0.0f)
                    GameApplication.App.SetState("MainMenuState");
            }

            if (sunLightNode.Light.Intensity >= 1.0f)
            {
                timeElapsed += GameTiming.TimeElapsed;
                if (timeElapsed > 20.0f
                    || device.IsClicked("A") || device.IsClicked("B") || device.IsClicked("X") || device.IsClicked("Y"))
                {
                    timeToGo = true;
                }
            }

            currentYaw += GameTiming.TimeElapsed * turnspeed;

            cameraNode.Translation = new Vector3(
                distance * (float)Math.Sin(currentYaw),
                height,
                distance * (float)Math.Cos(currentYaw));

            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(
                currentYaw,
                MathHelper.ToRadians(pitch),
                0);
            
            return base.OnUpdate();
        }

        public override bool OnExit()
        {
            ss.Clear();

            return base.OnExit();
        }
    }
}
