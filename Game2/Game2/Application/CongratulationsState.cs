﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Audio;
using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;
using Engine.Animation;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using Game2.Level;
using Game2.GameManager;
using Game2.UI;

using Engine.UI;

namespace Game2
{
    public sealed class CongratulationsState : State
    {
        SceneService ss;
        ContentService cs;
        GraphicsService gs;
        InputService input;
        InputDevice device;

        SceneNode sunLightNode;
        SceneNode cameraNode;
        SceneNode guardianDancer;
        float guardianDancer_emission;

        float currentYaw = 0.0f;
        const float distance = 10.0f;
        const float pitch = -25.0f;
        const float height = 5.0f;
        const float turnspeed = 0.4f;

        float timeElapsed;
        bool timeToGo;

        UIBasicLayer layer;
        UITexture gameOver;
        UILabel victoryText;

        public CongratulationsState()
        {
            ss = ServiceProvider.GetService<SceneService>();
            cs = ServiceProvider.GetService<ContentService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            input = ServiceProvider.GetService<InputService>();
        }

        public override bool OnEnter()
        {
            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            UIAtlas atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            layer = new UIBasicLayer(atlas);

            UIRendererService urs = ServiceProvider.GetService<UIRendererService>();

            gameOver = new UITexture();
            gameOver.Texture = cs["game"].Load<Texture2D>("UI/GameOver");
            gameOver.Rectangle = new Rectangle(urs.VirtualResolution.X / 2 - gameOver.Texture.Width / 2, 50, gameOver.Texture.Width, gameOver.Texture.Height);

            string youwin = "Congratulations! You have defeated the forces of evil!";
            Vector2 dim = font.MeasureString(youwin);
            victoryText = new UILabel(youwin, font, Color.White);
            victoryText.Rectangle = new Rectangle(urs.VirtualResolution.X / 2 - (int)(dim.X / 2 + 0.5f), 550, (int)dim.X, (int)dim.Y);

            layer.Add(gameOver);
            layer.Add(victoryText);
            urs.CurrentLayer = layer;

            timeElapsed = 0.0f;
            timeToGo = false;

            sunLightNode = ss.AddSceneNode("gameover_sunlight");
            sunLightNode.Light = new Engine.Graphics.DirectionalLight(new Vector4(1, 1, 1, 1), 0.0f);
            sunLightNode.Rotate(new Vector3(1, 0, 0), -75);

            Camera mainCamera = new Camera();
            mainCamera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.1f, 1000.0f);
            cameraNode = ss.AddSceneNode("MenuCamera");
            cameraNode.Camera = mainCamera;
            cameraNode.Translation = new Vector3(0, height, distance);
            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(pitch), 0);
            ss.ActiveCameraNode = cameraNode;

            LoadWizards();

            device = input.GetDevice(0);

            MediaPlayerService mps = ServiceProvider.GetService<MediaPlayerService>();
            mps.Play (cs["game"].Load<Song>("Music/Victory"));
            
            return base.OnEnter();
        }

        private void LoadWizards()
        {
            int[] ints = (int[])(this.Parameters[0]);

            int nWizards = ints.Length;

            SceneNode[] dancers = new SceneNode[1 + nWizards];

            dancers[0] = LoadGuardianDancer();
            for (int i = 0; i < nWizards; i++)
            {
                
                dancers[i + 1] = LoadWizardDancer(ints[i]);
            }

            float sep = 1.5f;
            float x = - (sep * nWizards) / 2.0f;

            for (int i = 0; i < dancers.Length; i++)
            {
                dancers[i].Translation = new Vector3(x, 0, 0);
                x += sep;
            }
        }

        private SceneNode LoadGuardianDancer()
        {
            float emission = 0.02f;

            SceneNode dancer = ss.AddSceneNode("dancing guardian");
            dancer.Model = cs["game"].Load<Model>("Models/Wizard/Wizard");
            dancer.TintColor = Color.White.ToVector3();
            dancer.TintAmount = 1f;
            dancer.EmissiveAdditive = emission;
            dancer.Animation.StartClip("dance", AnimationPlay.Loop);

            guardianDancer = dancer;
            guardianDancer_emission = emission;

            return dancer;
        }

        private SceneNode LoadWizardDancer(int i)
        {
            SceneNode dancer = ss.AddSceneNode("wizard guardian "+i);
            dancer.Model = cs["game"].Load<Model>("Models/Wizard/Wizard");
            dancer.Animation.StartClip("dance", AnimationPlay.Loop);

            switch (i)
            {
                case 0: dancer.MeshColor = Color.Red.ToVector3(); break;
                case 1: dancer.MeshColor = Color.Blue.ToVector3(); break;
                case 2: dancer.MeshColor = Color.Green.ToVector3(); break;
                case 3: dancer.MeshColor = Color.Yellow.ToVector3(); break;
                default: break;
            }

            return dancer;
        }

        public override bool OnUpdate()
        {
            if (!timeToGo)
            {
                if (sunLightNode.Light.Intensity < 1.0f)
                    sunLightNode.Light.Intensity += GameTiming.TimeElapsed * 0.4f;
            }
            else
            {
                sunLightNode.Light.Intensity -= GameTiming.TimeElapsed * 0.5f;
                if (sunLightNode.Light.Intensity <= 0.0f)
                    GameApplication.App.SetState("MainMenuState");

                guardianDancer.EmissiveAdditive = sunLightNode.Light.Intensity * guardianDancer_emission;
                if (guardianDancer.EmissiveAdditive < 0)
                    guardianDancer.EmissiveAdditive = 0;
            }

            if (sunLightNode.Light.Intensity >= 1.0f)
            {
                timeElapsed += GameTiming.TimeElapsed;
                if (timeElapsed > 20.0f
                    || device.IsClicked("A") || device.IsClicked("B") || device.IsClicked("X") || device.IsClicked("Y"))
                {
                    timeToGo = true;
                }
            }

            currentYaw += GameTiming.TimeElapsed * turnspeed;

            cameraNode.Translation = new Vector3(
                distance * (float)Math.Sin(currentYaw),
                height,
                distance * (float)Math.Cos(currentYaw));

            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(
                currentYaw,
                MathHelper.ToRadians(pitch),
                0);
            
            return base.OnUpdate();
        }

        public override bool OnExit()
        {
            ss.Clear();

            return base.OnExit();
        }
    }
}
