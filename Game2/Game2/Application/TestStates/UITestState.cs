﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Input;
using Engine.Core;
using Engine.Graphics;
using Engine.UI;

using Game2.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2
{
    public sealed class UITestState : State
    {
        UIAtlas atlas;
        UIRendererService urs;

        public UITestState()
        {
        }

        public void test3()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");

            urs = ServiceProvider.GetService<UIRendererService>();
            urs.CurrentLayer = new SpellAssignUI(
                atlas,
                new PlayerIndex[] { PlayerIndex.One, PlayerIndex.Two, PlayerIndex.Three, PlayerIndex.Four },
                new Color[] { Color.Red, Color.Blue, Color.Green, Color.Yellow });
        }

        public void test2() 
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();

            // Load textures, atlas, font and tiledata
            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            Texture2D flyingCude = cs["game"].Load<Texture2D>("UI/PNG");
            atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            UITileData myTiles = UITileData.LoadFromXml("GameContent/UI/PNG.xml");

            // Set up layer 1
            UITreeLayer layerA = new UITreeLayer(atlas);
            UILabel title = new UILabel("YOU PRESS A!!!", font, Color.Green, new Rectangle(200, 50, 0, 0));
            layerA.Selector = new UISelector();
            layerA.Add(title);
            layerA.BackKey = "B";

            // Set up layer 2
            UITreeLayer layerY = new UITreeLayer(atlas);
            title = new UILabel("YOU PRESS Y!!!", font, Color.Yellow, new Rectangle(200, 50, 0, 0));
            layerY.Selector = new UISelector();
            layerY.Add(title);

            // Set up root layer
            UITreeLayer root = new UITreeLayer(atlas);
            title = new UILabel("root", font, Color.Red, new Rectangle(0, 0, 200, 50));
            UIDialog diag = new UIDialog(DialogAdjustmentMode.Automatic, new Rectangle(0, 50, 200, 300));
            UITiledTexture tiled = new UITiledTexture(flyingCude, new Rectangle(350, 100, 100, 100), myTiles);
            root.Add(tiled);
            root.Selector = new UISelector();
            root.Add(title);
            root.Add(diag);
            UIButton b1 = new UIButton(font, "press A", Color.Green);
            UIButton b2 = new UIButton(font, "press Y", Color.Pink);
            diag.Add(b1);
            diag.Add(b2);
            root.Selector.AddLink(b1, b2, InputAxis.LS_Y);
            root.SetInvokerTarget(b1, layerA, "A");
            root.SetInvokerTarget(b2, layerY, "Y");

            urs = ServiceProvider.GetService<UIRendererService>();
            urs.CurrentLayer = root;

            // Build hierarchy:
            layerA.Parent = root;
            layerY.Parent = root;
        }

        public void test1()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();
            //gs.ResetDevice(800, 600, false, true);
            urs = ServiceProvider.GetService<UIRendererService>();

            // Load atlas and font
            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");

            // Set up layer
            UILayer layer = new UIBasicLayer(atlas);
            urs.CurrentLayer = layer;

            // Add content to the layer:
            UIDialog myDialog = new UIDialog(DialogAdjustmentMode.Automatic, new Rectangle(0, 0, 1240 / 2, 800 / 2));
            UIButton b1 = new UIButton(font, "my button 1");
            UIButton b2 = new UIButton(font, "my button 2");
            UIButton textless = new UIButton();
            myDialog.Add(b1);
            myDialog.Add(b2);
            myDialog.Add(textless);
            myDialog.Add(new UILabel("These are words", font, Color.Red));
            layer.Add(myDialog);

            // Set up selector:
            layer.Selector = new UISelector();
            layer.Selector.AddLink(b1, b2, InputAxis.LS_Y);
            layer.Selector.AddLink(b2, textless, InputAxis.LS_Y);
            layer.Selector.AddLink(textless, b1, InputAxis.LS_Y);

            layer.Selector.AddLink(b1, b2, InputAxis.RS_Y);
            layer.Selector.AddLink(b2, textless, InputAxis.RS_Y);
            layer.Selector.AddLink(textless, b1, InputAxis.RS_Y);
        }

        public override bool OnEnter()
        {
            test3();
            return base.OnEnter();
        }

        public override bool OnExit() { return base.OnExit(); }
        public override bool OnUpdate() { return true; }
    }
}
