﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;

using System.Diagnostics;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

using System.IO;

namespace Game2
{
    public sealed class TestState : State
    {
        public TestState()
        {

        }

        ContentService cs;
        SceneNode cameraNode;
        SceneNode modelNode;
        SceneNode lightNode;
        SceneNode groundNode;
        SceneNode lightNode2;

        public override bool OnEnter()
        {
            Random rnd = new Random();
            
            cs = ServiceProvider.GetService<ContentService>();
            SceneService scs = ServiceProvider.GetService<SceneService>();
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();

            lightNode = scs.AddSceneNode("sun");
            lightNode.Light = new Engine.Graphics.DirectionalLight(Color.White.ToVector4(),0.4f);
            //lightNode.Rotate(new Vector3(-80, 0, 0), Space.Local);

            lightNode2 = scs.AddSceneNode("lights");

            for (int i = 0; i < 40; i++)
            {
                /*
                SceneNode n = scs.AddSceneNode("point" + i, lightNode2);

                n.Light = new Engine.Graphics.PointLight(new Color((float)rnd.NextDouble(),
                                (float)rnd.NextDouble(),
                                (float)rnd.NextDouble()).ToVector4(), 1.0f, 5);
                */
                //n.GlobalTranslation = new Vector3(rnd.Next(-5, 5), rnd.Next(1, 5), rnd.Next(-5, 5));
            }

            /*
            animatedModel = scs.AddSceneNode("animated");
            animatedModel.Model = cs["game"].Load<Model>("Models/tiny");
            animatedModel.GlobalTranslation = new Vector3(0, 3, 0);
            animatedModel.Rotate(0, 0, 0, Space.Local);
            animatedModel.Scale(0.01f, 0.01f, 0.01f);
            animatedModel.AudioSource = new Engine.Audio.AudioSource(cs["game"].Load<SoundEffect>("Sounds/splash"));
            animatedModel.AudioSource.PositionalSource = true;
            */
            modelNode = scs.AddSceneNode("SmallBox");
            modelNode.Model = cs["game"].Load<Model>("Models/Physical/SmallBox");


            groundNode = scs.AddSceneNode("ground");
            groundNode.Model = cs["shared"].Load<Model>("Models/TestGround2");
            //groundNode.Scale(2, 1, 2);
            //groundNode.GlobalTranslation = new Vector3(0, 0, 0);

            cameraNode = scs.AddSceneNode("camera");
            Camera camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 300.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(0, 0, 20);
            //cameraNode.Rotate(new Vector3(1, 0, 0), 0);
            //cameraNode.Rotate(new Vector3(0, 1, 0), 0);

            scs.ActiveCameraNode = cameraNode;

            Song s = cs["game"].Load<Song>("Music/gloom");
            MediaPlayer.Play(s);

            return base.OnEnter();
        }


        SceneNode animatedModel = null;

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            //lightNode2.Rotate(new Vector3(0, 1, 0) * GameTiming.TimeElapsed * 20, Space.Local);

            GamePadState gp = GamePad.GetState(PlayerIndex.One);
            if (gp.IsButtonDown(Buttons.A))
            {
                animatedModel.AudioSource.Play();
            }

            if (gp.IsButtonDown(Buttons.B))
            {
                animatedModel.AudioSource.Paused = true;
            }

            if (gp.IsButtonDown(Buttons.Y))
            {
                animatedModel.AudioSource.Paused = false;
            }

            KeyboardState state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.Escape))
            {
                StorageService storage = ServiceProvider.GetService<StorageService>();

                storage.SaveDevice.SaveAsync("MyGame", "gameOptions", stream =>
                    {
                        using (StreamWriter writer = new StreamWriter(stream))
                        {
                            writer.WriteLine("test");
                        }
                    });
            }
            
            return base.OnUpdate();
        }

    }
}
