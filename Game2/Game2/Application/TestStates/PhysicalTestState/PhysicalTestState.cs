﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;

using BEPUphysics.CollisionRuleManagement;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Game2
{
    public sealed class PhysicalTestState : State
    {
        ConsoleLogService logger;
        InputDevice device;
        
        // services
        InputService input;
        SceneService scene;
        ContentService content;
        GraphicsService graphics;

        SceneNode levelNode;
        SceneNode boxNode;
        SceneNode cameraNode;

        SceneNode sunNode;

        public PhysicalTestState()
        {
        }

        public override bool OnEnter()
        {
            logger = ServiceProvider.GetService<ConsoleLogService>();
            
            // services
            input = ServiceProvider.GetService<InputService>();
            scene = ServiceProvider.GetService<SceneService>();
            content = ServiceProvider.GetService<ContentService>();
            graphics = ServiceProvider.GetService<GraphicsService>();

            device = input.GetDevice(0);

            device.AddAction("quit", InputButton.Back);

            device.AddAction("vertical_left", InputButton.LS_Y);
            device.AddAction("horizontal_left", InputButton.LS_X);

            device.AddAction("vertical_right", InputButton.RS_Y);
            device.AddAction("horizontal_right", InputButton.RS_X);

            device.AddAction("gravity", InputButton.A);
            device.AddAction("debugSpheres", InputButton.Y);
            device.AddAction("debugEntities", InputButton.X);


            // load the level mesh
            
            levelNode = scene.AddSceneNode("level");
            levelNode.Model = content["game"].Load<Model>("Models/Physical/Landscape");
            levelNode.PhysicalEntity = new PhysicalEntity(levelNode, new PhysicalStaticMeshEntityProperties(StaticMeshSidedness.DoubleSided), false);

            scene.ShowPhysicalEntities = true;
            //levelNode.Behaviour = new SimpleBehaviour();
            
            //levelNode = scene.AddSceneNode("level");
            /*
            levelNode.Model = content["shared"].Load<Model>("Models/ship2");
            levelNode.Translation = new Vector3(-40, 30, 0);
            levelNode.PhysicalEntity = new PhysicalEntity(levelNode, new PhysicalSphereEntityProperties(), false);
            levelNode.Behaviour = new SimpleBehaviour();
            levelNode.PhysicalEntity.IsAffectedByGravity = false;
            */
            //levelNode.Model = content["game"].Load<Model>("Models/DummyBoxes/StaticMaze");

            // load the box mesh
            
            boxNode = scene.AddSceneNode("box");
            boxNode.Model = content["game"].Load<Model>("Models/Props/Table");
            boxNode.Translation = new Vector3(10, 30, 0);
            boxNode.PhysicalEntity = new PhysicalEntity(boxNode, new PhysicalBoxEntityProperties(), 1.0f, false);
            //boxNode.Behaviour = new PhysicalEntityBehaviour();
            

            // our sun :)
            sunNode = scene.AddSceneNode("sun");
            sunNode.Light = new Engine.Graphics.DirectionalLight(Color.White.ToVector4(), 1.0f);
            sunNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-90), 0);

            // make a simep top-down camera
            cameraNode = scene.AddSceneNode("camera");
            cameraNode.Camera = new Camera();
            cameraNode.Camera.SetupPerspective(45.0f, graphics.GraphicsDevice.Viewport.AspectRatio, 1.0f, 3000.0f);
            cameraNode.Translation = new Vector3(0, 25, 0);
            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-90), 0);
            //cameraNode.LookAt = boxNode.Translation;

            scene.ActiveCameraNode = cameraNode;

            // collision group initialisation
            CustomCollisionGroups.Add("terrain");
            CustomCollisionGroups.Add("table");

            CustomCollisionGroupPairs.Add("terrain-table", CustomCollisionGroups.Get("terrain"), CustomCollisionGroups.Get("table"));

            boxNode.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("table");
            levelNode.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("terrain");

            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            if (device.IsClicked("quit"))
            {
                return false;
                //boxNode.PhysicalEntity.Mass = 0.0f;
            }

            if (device.IsClicked("debugSpheres"))
                scene.ShowModelBoundingSpheres = !scene.ShowModelBoundingSpheres;

            if (device.IsClicked("debugEntities"))
                scene.ShowPhysicalEntities = !scene.ShowPhysicalEntities;

            return base.OnUpdate();
        }
    }
}
