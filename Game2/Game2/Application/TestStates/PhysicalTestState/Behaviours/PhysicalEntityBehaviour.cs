﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


using Engine.Scene;
using Engine.Input;
using Engine.Core;
using Engine.Graphics;
using Engine;
using Engine.Physics;

using System.Diagnostics;

using BEPUphysics.Entities.Prefabs;
using BEPUphysics;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;

namespace Game2
{
    public sealed class PhysicalEntityBehaviour : SceneNodeBehaviour
    {
        SceneService scene;
        InputService input;
        InputDevice device;
        PhysicsService physics;

        Vector3 oldPosition;

        public override bool Start()
        {

            scene = ServiceProvider.GetService<SceneService>();
            input = ServiceProvider.GetService<InputService>();
            physics = ServiceProvider.GetService<PhysicsService>();
            device = input.GetDevice(0);

            oldPosition = SceneNode.Translation;

            SceneNode.PhysicalEntity.IsAffectedByGravity = false;
            
            return true;
        }

        public override bool Update()
        {
            //SceneNode.Rotate (Quaternion.CreateFromAxisAngle(new Vector3(0,1,0), GameTiming.TimeElapsed*0.3f));
            //SceneNode.Translate(new Vector3(device.GetValue("horizontal"), 0, -device.GetValue("vertical")) * GameTiming.TimeElapsed * 10);
            //SceneNode.Translate(new Vector3(0,0,-device.GetValue("vertical")) * GameTiming.TimeElapsed*5);

            //SceneNode.Translation = oldPosition;
            float xmov = device.GetValue("horizontal_left") * 25;
            float zmov = device.GetValue("vertical_left") * 25;

            float xrot = device.GetValue("horizontal_right") * 55;
            float zrot = device.GetValue("vertical_right") * 55;

            if (device.IsClicked("gravity"))
                SceneNode.PhysicalEntity.IsAffectedByGravity = !SceneNode.PhysicalEntity.IsAffectedByGravity;

            if (!SceneNode.PhysicalEntity.IsAffectedByGravity)
            {
                SceneNode.PhysicalEntity.LinearMomentum = new Vector3(xmov, 0.0f, -zmov);
                
            }
            //SceneNode.PhysicalEntity.AngularVelocity = new Vector3(zrot, 0.0f, xrot);
            
            SceneNode.PhysicalEntity.AngularMomentum = new Vector3(-zrot, 0.0f, -xrot);
            return true;
        }

        public override void Dispose()
        {
        }

        public override void OnCollideEnter(SceneNode otherNode)
        {
            Debug.WriteLine("collided with " + otherNode.Name);
            base.OnCollideEnter(otherNode);
        }

        public override void OnCollideStay(SceneNode otherNode)
        {
            base.OnCollideStay(otherNode);
        }

        public override void OnTriggerEnter(SceneNode otherNode)
        {
            Debug.WriteLine("triggered with " + otherNode.Name);
            base.OnTriggerEnter(otherNode);
        }
    }
}
