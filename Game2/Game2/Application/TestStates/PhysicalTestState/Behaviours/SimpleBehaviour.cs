﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


using Engine.Scene;
using Engine.Input;
using Engine.Core;
using Engine.Graphics;
using Engine;

using System.Diagnostics;
using Engine.Physics;

using BEPUphysics.Entities.Prefabs;
using BEPUphysics;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;

namespace Game2
{
    public sealed class SimpleBehaviour : SceneNodeBehaviour
    {
        SceneService scene;
        InputService input;
        InputDevice device;
        PhysicsService physics;

        Vector3 oldPosition;

        public override bool Start()
        {
            scene = ServiceProvider.GetService<SceneService>();
            input = ServiceProvider.GetService<InputService>();
            physics = ServiceProvider.GetService<PhysicsService>();
            device = input.GetDevice(0);

            oldPosition = SceneNode.Translation;

            return true;
        }

        public override bool Update()
        {
            //SceneNode.Rotate (Quaternion.CreateFromAxisAngle(new Vector3(0,1,0), GameTiming.TimeElapsed*0.3f));
            //SceneNode.Translate(new Vector3(device.GetValue("horizontal"), 0, -device.GetValue("vertical")) * GameTiming.TimeElapsed * 10);
            //SceneNode.Translate(new Vector3(0,0,-device.GetValue("vertical")) * GameTiming.TimeElapsed*5);

            //SceneNode.Translation = oldPosition;

            //SceneNode.Translate(new Vector3(1, 0, 0) * GameTiming.TimeElapsed);
            //SceneNode.PhysicalEntity.LinearVelocity = SceneNode.Right;
            //SceneNode.PhysicalEntity.Translation += SceneNode.Right * GameTiming.TimeElapsed;
            return true;
        }

        public override void Dispose()
        {
        }

        public override void OnCollideEnter(SceneNode otherNode)
        {
            Debug.WriteLine("collided with " + otherNode.Name);
            base.OnCollideEnter(otherNode);
        }

        public override void OnCollideStay(SceneNode otherNode)
        {
            base.OnCollideStay(otherNode);
        }

        public override void OnTriggerEnter(SceneNode otherNode)
        {
            Debug.WriteLine("triggered with " + otherNode.Name);
            base.OnTriggerEnter(otherNode);
        }
    }
}
