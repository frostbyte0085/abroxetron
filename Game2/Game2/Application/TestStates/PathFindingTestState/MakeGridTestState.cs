﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.AI;
using Engine.Graphics;
using Engine.Scene;

using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using System.IO;

namespace Game2
{
    public sealed class MakeGridTestState : State
    {
        #region variables
        ContentService cs;
        SceneService ss;
        SceneNode cameraNode;
        DebugRendererService debugRend;
        #endregion

        #region constructors
        public MakeGridTestState()
        {

        }
        #endregion

        public override bool OnEnter()
        {
            // Get services:
            cs = ServiceProvider.GetService<ContentService>();
            ss = ServiceProvider.GetService<SceneService>();
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();

            // Init debugger
            debugRend = ServiceProvider.GetService<DebugRendererService>();
            debugRend.RasterizerState.FillMode = FillMode.WireFrame;
            debugRend.RasterizerState.CullMode = CullMode.CullCounterClockwiseFace;
            debugRend.SphereDivisions = 10;

            // Init Camera:
            cameraNode = ss.AddSceneNode("camera");
            Camera camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 300.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(-10, 10, 10);
            cameraNode.Rotate(new Vector3(0, 1, 0), -45);
            cameraNode.Rotate(new Vector3(1, 0, 0), -45);

            ss.ActiveCameraNode = cameraNode;

            // Init Grid:
            Model walkable = cs["game"].Load<Model>("Models/Dungeon/Earth/Earth_walkable");
            ss.PathGrid = PathGridMaker.MakeGrid(walkable, Vector3.Zero, 1);
            ss.ShowPathGrid = true;

            return base.OnEnter();
        }

        public override bool OnExit()  { return base.OnExit(); }

        public override bool OnUpdate()
        {
            processInput();

            debugRend.AddMagicBox3D(new Vector3(0), new Vector3(1));

            return base.OnUpdate();
        }

        private void processInput()
        {
            KeyboardState keys = Keyboard.GetState();
            float dt = GameTiming.ElapsedTicks * 0.000004f;


            if (keys.IsKeyDown(Keys.Up))
            {
                cameraNode.Rotate(0, dt, 0);
            }
            if (keys.IsKeyDown(Keys.Down))
            {
                cameraNode.Rotate(0, -dt, 0);
            }
            if (keys.IsKeyDown(Keys.Right))
            {
                cameraNode.Rotate(-dt, 0, 0);
            }
            if (keys.IsKeyDown(Keys.Left))
            {
                cameraNode.Rotate(dt, 0, 0);
            }

            dt *= 0.5f;

            if (keys.IsKeyDown(Keys.W))
            {
                Vector3 v = new Vector3(0,0,-1) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
            if (keys.IsKeyDown(Keys.S))
            {
                Vector3 v = new Vector3(0, 0, 1) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
            if (keys.IsKeyDown(Keys.A))
            {
                Vector3 v = new Vector3(-1, 0, 0) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
            if (keys.IsKeyDown(Keys.D))
            {
                Vector3 v = new Vector3(1, 0, 0) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
        }
    }
}
