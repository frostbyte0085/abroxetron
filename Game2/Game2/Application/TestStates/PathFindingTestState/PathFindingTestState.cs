﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;
using Engine.AI;

using System.Diagnostics;
using System.ComponentModel;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2
{
    public sealed class PathFindingTestState : State
    {
        GraphicsService gs;
        SceneService ss;
        ParticleRendererService prs;
        DebugRendererService drs;
        ConsoleLogService logger;
        ContentService cs;
        PhysicsService ps;

        InputDevice device;
        InputService input;

        SceneNode cameraNode;
        Camera camera;

        PathGrid grid;
        SceneNode box1;
        SceneNode box2;

        public PathFindingTestState()
        {

        }

        public override bool OnEnter()
        {

            logger = ServiceProvider.GetService<ConsoleLogService>();
            input = ServiceProvider.GetService<InputService>();
            prs = ServiceProvider.GetService<ParticleRendererService>();
            drs = ServiceProvider.GetService<DebugRendererService>();
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();
            ps = ServiceProvider.GetService<PhysicsService>();

            device = input.GetDevice(4);

            device.AddAction("click", InputButton.Start);
            device.AddAction("esc",InputButton.Back);
            device.AddAction("zoomin", InputButton.B);
            device.AddAction("zoomout", InputButton.A);

            SceneNode sun = ss.AddSceneNode("sun");
            sun.Light = new Engine.Graphics.DirectionalLight(Vector4.One, 0.9f);
            sun.Orientation = Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), MathHelper.ToRadians(-45));

            // Init Camera:
            cameraNode = ss.AddSceneNode("camera");
            camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 3000.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(0, 950, 0);
            cameraNode.Rotate(new Vector3(1, 0, 0), -90);
            ss.ActiveCameraNode = cameraNode;

            grid = new PathGrid(800, 800, 4);
            ss.PathGrid = grid;

            Point previousPoint = new Point(-99999, -99999);

            // block some cells here
            for (int i = -400; i < 400; i++)
            {
                if (i == 0 || i == 1 || i == 2 || i == 3)
                    continue;

                Vector2 worldPos = new Vector2(0, i);
                Point gridPos = ss.PathGrid.WorldToGrid(worldPos);
                // we have processed this grid cell, just continue
                if (previousPoint == gridPos)
                    continue;

                ss.PathGrid.BlockPosition(worldPos, true);

                previousPoint = gridPos;
            }

            List<Vector2> free = new List<Vector2>();

            for (int i = -400; i < 400; i++)
            {
                if (ss.PathGrid.IsPositionFree(new Vector2(0, i)))
                {
                    free.Add(new Vector2(0, i));
                }
            }

            // load the 2 boxes
            // target
            box1 = ss.AddSceneNode("box1");
            box1.Model = cs["game"].Load<Model>("Models/Physical/SmallBox");
            box1.Translation = new Vector3(400, 0, 60);

            // agent
            box2 = ss.AddSceneNode("box2");
            box2.Model = cs["game"].Load<Model>("Models/Physical/SmallBox");
            box2.Translation = new Vector3(-400, 0, -250);
            box2.Behaviour = new PathBoxBehaviour(box1);

            drs.ShowFrameStatistics = true;
            
            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            if (device.IsClicked("esc"))
            {

            }

            if (device.IsPressed("zoomin"))
            {
                cameraNode.Translate(Vector3.Up * GameTiming.TimeElapsed*50);
                Console.WriteLine(cameraNode.Translation.ToString());
            }

            if (device.IsPressed("zoomout"))
            {
                cameraNode.Translate(-Vector3.Up * GameTiming.TimeElapsed*50);
                Console.WriteLine(cameraNode.Translation.ToString());
            }

            return base.OnUpdate();
        }
    }
}
