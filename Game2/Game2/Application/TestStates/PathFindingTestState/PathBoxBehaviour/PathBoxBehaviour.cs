﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


using Engine.Scene;
using Engine.Input;
using Engine.Core;
using Engine.Graphics;
using Engine;

using System.Diagnostics;

using Engine.AI;

namespace Game2
{
    public sealed class PathBoxBehaviour : SceneNodeBehaviour
    {
        SceneService scene;
        InputService input;
        InputDevice device;

        private SceneNode targetNode;

        public PathBoxBehaviour(SceneNode target)
        {
            targetNode = target;
        }

        public override bool Start()
        {
            scene = ServiceProvider.GetService<SceneService>();
            input = ServiceProvider.GetService<InputService>();
         
            device = input.GetDevice(4);

            

            return true;
        }

        public override bool Update()
        {
            if (device.IsClicked("esc"))
            {
                Vector2 start = new Vector2(SceneNode.Translation.X, SceneNode.Translation.Z);
                Vector2 end = new Vector2(targetNode.Translation.X, targetNode.Translation.Z);

                PathFind(start, end);
            }

            // lerp through the points
            if (positions != null)
            {
                float i = (GameTiming.TotalTimeElapsed - startTime) / walkDuration;

                SceneNode.Translation = Vector3.Lerp(startPoint, endPoint, i);

                if (i >= 1)
                {
                    startTime = GameTiming.TotalTimeElapsed;

                    targetPoint++;
                    // have we reached the end?
                    if (targetPoint == positions.Count)
                    {
                        positions.Clear();
                        positions = null;
                    }
                    // nah, keep moving
                    else
                    {
                        startPoint = endPoint;
                        endPoint = new Vector3(positions[targetPoint].X, 0, positions[targetPoint].Y);
                    }
                }
            }

            return true;
        }

        public override void Dispose()
        {
        }

        private List<Vector2> positions;
        private float walkDuration = 0.03f;
        private Vector3 startPoint;
        private Vector3 endPoint;
        private float startTime;

        private int targetPoint;

        public override void OnPathFindUpdated(List<Vector2> newPath)
        {
            positions = new List<Vector2>(newPath);

            targetPoint = 0;
            startTime = GameTiming.TotalTimeElapsed;
            startPoint = SceneNode.Translation;
            endPoint = new Vector3(positions[targetPoint].X, 0, positions[targetPoint].Y);           

            base.OnPathFindUpdated(newPath);
        }
        
    }
}
