﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Game2.Level;
using Game2.GameManager;

namespace Game2
{
    public sealed class SceneLoadingState : State
    {
        GraphicsService gs;
        SceneService ss;
        ConsoleLogService logger;
        ContentService cs;
        DebugRendererService drs;

        InputDevice device;
        InputService input;

        public SceneLoadingState()
        {
        }

        public override bool OnEnter()
        {
            logger = ServiceProvider.GetService<ConsoleLogService>();
            input = ServiceProvider.GetService<InputService>();
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();
            drs = ServiceProvider.GetService<DebugRendererService>();

            drs.ShowFrameStatistics = false;

            device = input.GetDevice(0);

            device.AddAction("click", InputButton.Start);
            device.AddAction("esc",InputButton.Back);
            device.AddAction("zoomin", InputButton.B);
            device.AddAction("zoomout", InputButton.A);
            device.AddAction("change", InputButton.X);

            //ss.ShowModelBoundingSpheres = true;
            //ss.ShowPhysicalEntities = true;
            PlayerIndex[] players = { PlayerIndex.One};//, PlayerIndex.Two, PlayerIndex.Three };
            GameApplication.App.GameManager.Begin(players);

            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        Vector2 nextRes;

        public override bool OnUpdate()
        {
            BaseGameManager mgr = GameApplication.App.GameManager;

            nextRes = new Vector2(1280, 720);
            
            if (device.IsClicked("esc"))
            {
                //return false;
                if (mgr.CurrentLevelType == LevelType.MainHalls)
                {
                    mgr.Transition(LevelType.Fire);
                }
                else
                {
                    mgr.Transition(LevelType.MainHalls);
                }
            }

            if (device.IsClicked("change"))
            {
                gs.ResetDevice((int)nextRes.X, (int)nextRes.Y, !gs.PresentationParameters.IsFullScreen, true);
            }

            return base.OnUpdate();
        }
    }
}
