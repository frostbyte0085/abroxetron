﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2
{
    public sealed class ModelMaterialTestState : State
    {
        GraphicsService gs;
        SceneService ss;
        ParticleRendererService prs;
        DebugRendererService drs;
        ConsoleLogService logger;
        ContentService cs;
        PhysicsService ps;

        InputDevice device;
        InputService input;

        SceneNode cameraNode;
        Camera camera;
        SceneNode box;

        public ModelMaterialTestState()
        {
        }

        public override bool OnEnter()
        {
            logger = ServiceProvider.GetService<ConsoleLogService>();
            input = ServiceProvider.GetService<InputService>();
            prs = ServiceProvider.GetService<ParticleRendererService>();
            drs = ServiceProvider.GetService<DebugRendererService>();
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();
            ps = ServiceProvider.GetService<PhysicsService>();

            device = input.GetDevice(0);

            device.AddAction("click", InputButton.Start);
            device.AddAction("esc",InputButton.Back);
            device.AddAction("zoomin", InputButton.B);
            device.AddAction("zoomout", InputButton.A);

            SceneNode sun = ss.AddSceneNode("sun");
            sun.Light = new Engine.Graphics.DirectionalLight(Vector4.One, 0.9f);
            sun.Orientation = Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), MathHelper.ToRadians(-45));

            // Init Camera:
            cameraNode = ss.AddSceneNode("camera");
            camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 3000.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(0, 20, 70);
            ss.ActiveCameraNode = cameraNode;
            
            drs.RasterizerState.CullMode = CullMode.CullCounterClockwiseFace;

            /*
            SceneNode ll = ss.AddSceneNode("light1");
            ll.Light = new PointLight(Color.LightBlue.ToVector4(), 1.0f, 25);
            ll.Translation = new Vector3(-5, 5, 0);

            SceneNode ll2 = ss.AddSceneNode("light2");
            ll2.Light = new PointLight(Color.DarkRed.ToVector4(), 3.0f, 15);
            ll2.Translation = new Vector3(5, 5, 0);
            */
            //ss.ShowPhysicalEntities = true;

            box = ss.AddSceneNode("box");
            box.Model = cs["game"].Load<Model>("Models/CharTest/PlayerCharacter");
            box.Translation = new Vector3(0, 15, 0);

            //box.PhysicalEntity = new PhysicalEntity(box, new PhysicalBoxEntityProperties(), 0.0f, false);
            
            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            box.Rotate(new Vector3(0, 1, 0), GameTiming.TimeElapsed * 20);
            //if (device.IsClicked("click"))
            //{
                
                
            //}

            if (device.IsClicked("esc"))
            {
                RaycastHit hit;

                if (ps.Raycast(new Ray(new Vector3(-5, 50, -5), new Vector3(0, -1, 0)), 1000.0f, out hit))
                {
                    Console.WriteLine("hit: " + hit.Position + "normal: " + hit.Normal);
                }
                //return false;
            }

            if (device.IsClicked("zoomin"))
            {
                cameraNode.Translate(Vector3.Backward);
                Console.WriteLine(cameraNode.Translation.ToString());
            }

            if (device.IsClicked("zoomout"))
            {
                cameraNode.Translate(Vector3.Forward);
                Console.WriteLine(cameraNode.Translation.ToString());
            }

            return base.OnUpdate();
        }
    }
}
