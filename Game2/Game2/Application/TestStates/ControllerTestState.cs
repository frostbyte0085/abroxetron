﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;

namespace Game2
{
    public sealed class ControllerTestState : State
    {
        ConsoleLogService logger;
        InputDevice device;
        InputService input;

        public ControllerTestState()
        {
        }

        public override bool OnEnter()
        {
            logger = ServiceProvider.GetService<ConsoleLogService>();
            input = ServiceProvider.GetService<InputService>();

            device = input.GetDevice(4);

            device.AddAction("a", InputButton.A);
            device.AddAction("ls", InputButton.LS_Y);

            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {

            Console.WriteLine("---------------------------");

            Console.WriteLine("A:" + device.GetValue("a") + "  " + device.IsPressed("a") + "  " + device.IsClicked("a"));
            Console.WriteLine("LS_Y:" + device.GetValue("ls") + "  " + device.IsPressed("ls") + "  "+ device.IsClicked("ls"));

            return base.OnUpdate();
        }
    }
}
