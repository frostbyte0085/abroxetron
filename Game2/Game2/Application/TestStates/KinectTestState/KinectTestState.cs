﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Scene;
using Engine.Core;
using Engine.Input;
using Engine.UI;
using Engine.Graphics;
using Engine.Tools;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Kinect;

namespace Game2
{
    public sealed class KinectTestState : State
    {
        KinectDevice kinect;
        UITexture myStream;
        UITiledTexture theOooooooorb;
        UILabel myLabel;
        UILabel myLabel2;
        UILabel whichGesture;

        UILabel myTestLabel;

        DebugRendererService debug;

        public KinectTestState()
        {
        }

        public override bool OnEnter()
        {
            debug = ServiceProvider.GetService<DebugRendererService>();
            debug.ShowFrameStatistics = true;
            debug.ShowKinectGestures = true;

            InputService input = ServiceProvider.GetService<InputService>();
            kinect = input.GetKinect();
            kinect.ClearAll();
            KinectGesture down = new KinectGesture(
                new JointMovement[] 
                {
                    new JointMovement() 
                    { 
                        Direction = JointMovement.South,
                        JointType = JointType.HandRight
                    }
                });
            kinect.AddAction("down", down);

            KinectGesture up = new KinectGesture(
                new JointMovement[] 
                {
                    new JointMovement() 
                    { 
                        Direction = JointMovement.North,
                        JointType = JointType.HandRight
                    },
                    new JointMovement() 
                    { 
                        Direction = JointMovement.South,
                        JointType = JointType.HandRight
                    }
                });
            kinect.AddAction("up", up);

            KinectGesture right = new KinectGesture(
                new JointMovement[] 
                {
                    new JointMovement() 
                    { 
                        Direction = JointMovement.East,
                        JointType = JointType.HandRight
                    }
                });
            kinect.AddAction("right", right);

            KinectGesture leftright = new KinectGesture(
                new JointMovement[] 
                {
                    new JointMovement() 
                    { 
                        Direction = JointMovement.West,
                        JointType = JointType.HandRight
                    },
                    new JointMovement() 
                    { 
                        Direction = JointMovement.East,
                        JointType = JointType.HandRight
                    }
                });
            kinect.AddAction("leftRight", leftright);

            // Set up camera (for debug renderer);
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode cameraNode = ss.AddSceneNode("camera");
            cameraNode.Camera = new Camera();
            cameraNode.Camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 1.0f, 3000.0f);
            cameraNode.Translation = new Vector3(0, 0, 200);
            ss.ActiveCameraNode = cameraNode;

            InitUI();

            return base.OnEnter();
        }

        private void InitUI()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();

            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            Texture2D orbTexture = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Fire");
            UITileData tilingData = UITileData.LoadFromXml("GameContent/UI/Ingame/BasicTiling.xml");
            UIAtlas atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            UIBasicLayer layer = new UIBasicLayer(atlas);

            myStream = new UITexture();
            myStream.Rectangle = new Rectangle(0, 0, 1240, 800);//hard code v-res... i don't care
            layer.Add(myStream);

            theOooooooorb = new UITiledTexture(orbTexture, new Rectangle(0, 0, 90, 90), tilingData);
            layer.Add(theOooooooorb);

            myLabel = new UILabel("", font, Color.White, new Rectangle(100, 570, 100, 100));
            layer.Add(myLabel);

            myLabel2 = new UILabel("", font, Color.White, new Rectangle(100, 600, 100, 100));
            layer.Add(myLabel2);

            myTestLabel = new UILabel("", font, Color.White, new Rectangle(600, 630, 100, 100));
            layer.Add(myTestLabel);

            whichGesture = new UILabel("", font, Color.White, new Rectangle(100, 660, 100, 100));
            layer.Add(whichGesture);

            layer.SetAsCurrent();
        }

        private float duration = 0.8f;
        private float timestamp = -0.4f;
        public override bool OnUpdate()
        {
            myLabel.Text = kinect.StatusAsString();
            myStream.Texture = kinect.RGBVideoStream;
            Skeleton skel = kinect.PlayerSkeleton;

            if (skel == null)
            {
                myLabel2.Color = Color.Red;
                myLabel2.Text = "NO SKELETON FOUND";
            }
            else
            {
                myLabel2.Color = Color.Green;
                myLabel2.Text = "SKELETON FOUND";

                Joint joint = kinect.PlayerSkeleton.Joints[JointType.HandRight];
                Point pos = KinectDevice.GetScreenPoint(joint, 1240, 800);
                theOooooooorb.X = pos.X;
                theOooooooorb.Y = pos.Y;

                debug.SetSkeleton(skel, 10, false, Color.Red, Color.White);
            }

            List<MovementDetection> l = kinect.GetMovementDetection();
            MovementDetection m = l[0];

            string posstr =
                String.Format("{0,9:0.00000}", m.Position.X) + "," +
                String.Format("{0,9:0.00000}", m.Position.Y) + "," +
                String.Format("{0,9:0.00000}", m.Position.Z) + ",";
            string velstr =
                String.Format("{0,9:0.00000}", m.Velocity.X) + "," +
                String.Format("{0,9:0.00000}", m.Velocity.Y);
            string spdstr =
                String.Format("{0,9:0.00000}", m.Velocity.Length());

            if (m.Velocity.X < 0 || m.Velocity.Y < 0)
                while (false) ;

            myTestLabel.Text = 
                "Vel: "+ velstr
                +"      Speed: " + spdstr
                +"      Pos: "+ posstr;

            if (kinect.IsTriggered("down"))
            {
                timestamp = GameTiming.TotalTimeElapsed;
                whichGesture.Text = "Down";
                whichGesture.Color = Color.Red;
            }
            else if (kinect.IsTriggered("up"))
            {
                timestamp = GameTiming.TotalTimeElapsed;
                whichGesture.Text = "Up-Down";
                whichGesture.Color = Color.Red;
            }
            else if (kinect.IsTriggered("leftRight"))
            {
                timestamp = GameTiming.TotalTimeElapsed;
                whichGesture.Text = "Left-Right";
                whichGesture.Color = Color.Red;
                Console.WriteLine("leftRight");
            }
            else if (kinect.IsTriggered("right"))
            {
                timestamp = GameTiming.TotalTimeElapsed;
                whichGesture.Text = "Right";
                whichGesture.Color = Color.Red;
            }
            else if(timestamp + duration <= GameTiming.TotalTimeElapsed)
            {
                whichGesture.Text = "No Gesture...";
                whichGesture.Color = Color.White;
            }

            return base.OnUpdate();
        }
    }
}
