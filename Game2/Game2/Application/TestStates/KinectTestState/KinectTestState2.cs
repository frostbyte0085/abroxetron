﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Scene;
using Engine.Core;
using Engine.Input;
using Engine.UI;
using Engine.Graphics;
using Engine.Tools;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Kinect;

namespace Game2
{
    public sealed class KinectTestState2 : State
    {
        GraphicsService gs;
        SceneService ss;
        ContentService cs;

        KinectDevice kinect;
        UILabel myLabel;
        UILabel myLabel2;

        SceneNode wizard;
        JointStringMapping jointMap;

        DebugRendererService debug;

        public KinectTestState2()
        {
        }

        public override bool OnEnter()
        {
            cs = ServiceProvider.GetService<ContentService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            ss = ServiceProvider.GetService<SceneService>();
            debug = ServiceProvider.GetService<DebugRendererService>();

            debug.ShowFrameStatistics = true;

            InputService input = ServiceProvider.GetService<InputService>();
            kinect = input.GetKinect();

            // Set up camera:
            SceneNode cameraNode = ss.AddSceneNode("camera");
            cameraNode.Camera = new Camera();
            cameraNode.Camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 1.0f, 3000.0f);
            cameraNode.Translation = new Vector3(0, 1, 3f);
            ss.ActiveCameraNode = cameraNode;

            // Add light
            SceneNode sunNode = ss.AddSceneNode("sun");
            sunNode.Light = new Engine.Graphics.DirectionalLight(Color.White.ToVector4(), 1.0f);
            sunNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-75), 0);

            InitUI();
            InitDummy();

            return base.OnEnter();
        }

        private void InitUI()
        {
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            UIAtlas atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            UIBasicLayer layer = new UIBasicLayer(atlas);

            myLabel = new UILabel("", font, Color.White, new Rectangle(100, 600, 100, 100));
            layer.Add(myLabel);

            myLabel2 = new UILabel("", font, Color.White, new Rectangle(100, 630, 100, 100));
            layer.Add(myLabel2);

            layer.SetAsCurrent();
        }

        private void InitDummy()
        {
            Model wizardModel = cs["game"].Load<Model>("Models/Wizard/Wizard");
            wizard = ss.AddSceneNode("wizard");
            wizard.Model = wizardModel;
            wizard.MeshColor = Color.Green.ToVector3();

            jointMap = new JointStringMapping();

            jointMap[JointType.ShoulderRight] = "RightArm";
            //jointMap[JointType.ElbowRight] = "RightForeArm";
            //jointMap[JointType.HipRight] = "RightUpLeg";
            //jointMap[JointType.KneeRight] = "RightLeg";

            jointMap[JointType.ShoulderLeft] = "LeftArm";
            //jointMap[JointType.ElbowLeft] = "LeftForeArm";
            //jointMap[JointType.HipLeft] = "LeftUpLeg";
            //jointMap[JointType.KneeLeft] = "LeftLeg";

            float PiHalf = (float) Math.PI / 2.0f;
            jointMap.SetNormal(JointType.ShoulderRight, new Vector3(0, -PiHalf, 0));
            jointMap.SetNormal(JointType.ShoulderLeft, new Vector3(0, PiHalf, 0));
        }

        public override bool OnUpdate()
        {
            myLabel.Text = kinect.StatusAsString();
            Skeleton skel = kinect.PlayerSkeleton;

            if (skel == null)
            {
                myLabel2.Color = Color.Red;
                myLabel2.Text = "NO SKELETON FOUND";
            }
            else
            {
                myLabel2.Color = Color.Green;
                myLabel2.Text = "SKELETON FOUND";

                wizard.Animation.PlayKinectStance(skel, jointMap);
                debug.SetSkeleton(skel, 10, false, Color.Red, Color.White);
            }

            return base.OnUpdate();
        }
    }
}
