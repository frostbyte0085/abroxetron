﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;

using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using System.IO;

namespace Game2
{
    public sealed class DebugRenderTestState : State
    {
        #region variables
        ContentService cs;
        SceneNode cameraNode;
        DebugRendererService debugRend;
        #endregion

        #region constructors
        public DebugRenderTestState()
        {

        }
        #endregion

        public override bool OnEnter()
        {
            // Get services:
            cs = ServiceProvider.GetService<ContentService>();
            SceneService scs = ServiceProvider.GetService<SceneService>();
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();

            // Init debugger
            debugRend = ServiceProvider.GetService<DebugRendererService>();
            debugRend.RasterizerState.FillMode = FillMode.WireFrame;
            debugRend.RasterizerState.CullMode = CullMode.CullCounterClockwiseFace;
            debugRend.SphereDivisions = 10;

            // Init Camera:
            cameraNode = scs.AddSceneNode("camera");
            Camera camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 300.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(-10, 10, 10);
            cameraNode.Rotate(new Vector3(0, 1, 0), -45);
            cameraNode.Rotate(new Vector3(1, 0, 0), -45);

            scs.ActiveCameraNode = cameraNode;

            scs.PathGrid = new Engine.AI.PathGrid(4, 4, 1);
            scs.ShowPathGrid = true;
            scs.PathGrid.BlockPosition(new Point(1, 1), true);
            scs.PathGrid.BlockPosition(new Point(2, 2), true);
            scs.PathGrid.BlockPosition(new Point(2, 1), true);
            scs.PathGrid.BlockPosition(new Point(1, 2), true);

            return base.OnEnter();
        }

        public override bool OnExit()  { return base.OnExit(); }

        private Color[] colors = {
                                 Color.Red, Color.DarkRed,
                                 Color.Blue, Color.DarkBlue,
                                 Color.Green, Color.DarkGreen,
                                 Color.White, Color.Gray};
        public override bool OnUpdate()
        {
            processInput();

            Vector3[] pts = new Vector3[4];
            pts[0] = new Vector3(0, 0, 1);
            pts[1] = new Vector3(0, 1, 1);
            pts[2] = new Vector3(-1, 1, 1);
            pts[3] = new Vector3(-1, 0, 0);

            debugRend.AddLinkedLines3D(pts, Color.Green);
            debugRend.AddMagicBox3D(new Vector3(2, 0, 0), new Vector3(3, 1, 1));
            debugRend.AddSphere(new Vector3(-3,0,1), 0.7f, Color.Red);

            debugRend.AddBox3D(new Vector3(4, 0, 0), new Vector3(5, 1, 1), colors, Quaternion.CreateFromYawPitchRoll(1, 1, 0));

            //debugRend.AddCapsule(new Vector3(4, -4, 0), 1.0f, 3.0f, 24, Color.White);
            // Reference cude:
            // refcude();

            return base.OnUpdate();
        }

        private void processInput()
        {
            KeyboardState keys = Keyboard.GetState();
            float dt = GameTiming.ElapsedTicks * 0.000004f;


            if (keys.IsKeyDown(Keys.Up))
            {
                cameraNode.Rotate(0, dt, 0);
            }
            if (keys.IsKeyDown(Keys.Down))
            {
                cameraNode.Rotate(0, -dt, 0);
            }
            if (keys.IsKeyDown(Keys.Right))
            {
                cameraNode.Rotate(-dt, 0, 0);
            }
            if (keys.IsKeyDown(Keys.Left))
            {
                cameraNode.Rotate(dt, 0, 0);
            }

            dt *= 0.5f;

            if (keys.IsKeyDown(Keys.W))
            {
                Vector3 v = new Vector3(0,0,-1) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
            if (keys.IsKeyDown(Keys.S))
            {
                Vector3 v = new Vector3(0, 0, 1) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
            if (keys.IsKeyDown(Keys.A))
            {
                Vector3 v = new Vector3(-1, 0, 0) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
            if (keys.IsKeyDown(Keys.D))
            {
                Vector3 v = new Vector3(1, 0, 0) * dt;
                v = Vector3.Transform(v, cameraNode.Orientation);

                cameraNode.Translate(v);
            }
        }

        private void refcude()
        {
            debugRend.AddLine3D(new Vector3(0, 0, 0), new Vector3(1, 0, 0), Color.White);
            debugRend.AddLine3D(new Vector3(1, 0, 0), new Vector3(1, 1, 0), Color.Blue);
            debugRend.AddLine3D(new Vector3(1, 1, 0), new Vector3(0, 1, 0), Color.White);
            debugRend.AddLine3D(new Vector3(0, 1, 0), new Vector3(0, 0, 0), Color.Blue);

            debugRend.AddLine3D(new Vector3(0, 0, 0), new Vector3(0, 0, -1), Color.Red);
            debugRend.AddLine3D(new Vector3(0, 1, 0), new Vector3(0, 1, -1), Color.Red);

            debugRend.AddLine3D(new Vector3(1, 0, 0), new Vector3(1, 0, -1), Color.Green);
            debugRend.AddLine3D(new Vector3(1, 1, 0), new Vector3(1, 1, -1), Color.Green);

            debugRend.AddLine3D(new Vector3(0, 0, -1), new Vector3(1, 0, -1), Color.Orange);
            debugRend.AddLine3D(new Vector3(1, 1, -1), new Vector3(0, 1, -1), Color.Orange);

            debugRend.AddLine3D(new Vector3(0, 0, -1), new Vector3(0, 1, -1), Color.Cyan);
            debugRend.AddLine3D(new Vector3(1, 0, -1), new Vector3(1, 1, -1), Color.Cyan);
        }
    }
}
