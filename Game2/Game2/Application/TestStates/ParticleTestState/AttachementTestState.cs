﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;

using System.Diagnostics;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

using System.IO;

namespace Game2
{
    public sealed class AttachementTestState : State
    {
        public AttachementTestState()
        {

        }

        SceneNode cameraNode;
        Camera camera;

        SceneNode candle;

        public override bool OnEnter()
        {
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();

            cameraNode = ss.AddSceneNode("camera");
            camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 300.0f);

            SceneNode directionalLightNode = ss.AddSceneNode("sunLight");
            directionalLightNode.Light = new Engine.Graphics.DirectionalLight(Color.White.ToVector4(), 0.5f);
            directionalLightNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-45), 0);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(0, 0, 3);
            ss.ActiveCameraNode = cameraNode;

            candle = ss.AddSceneNode("candeHolder");
            candle.Model = cs["game"].Load<Model>("Models/Props/CandleHolder3");
            candle.Behaviour = new TestParticleBehaviour();

            ParticleAnimation anim
                = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/CandleFire.xml");

            ParticleSystem cand1 = new ParticleSystem(anim);
            ParticleSystem cand2 = new ParticleSystem(anim);
            ParticleSystem cand3 = new ParticleSystem(anim);

            candle.Attach("Candle1", cand1);
            candle.Attach("Candle2", cand2);
            candle.Attach("Candle3", cand3);

            cand1.StartEmitting();
            cand2.StartEmitting();
            cand3.StartEmitting();

            return base.OnEnter();
        }

        public override bool OnExit() { return true; }
        public override bool OnUpdate() {
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();
            //drs.AddBox3D(new Vector3(-1, 0, 0), new Vector3(0, 1, 0), Color.Red);
            //drs.AddBox3D(new Vector3(0, 0, 0), new Vector3(1, 1, 0), Color.Blue);
            //drs.AddBox3D(new Vector3(-1, -1, 0), new Vector3(0, 0, 0), Color.Green);
            //drs.AddBox3D(new Vector3(0, -1, 0), new Vector3(1, 0, 0), Color.Magenta);

            return true;
        
        }
    }
}
