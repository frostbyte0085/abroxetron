﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2
{
    public sealed class ParticleCullingTest : State
    {
        GraphicsService gs;
        SceneService ss;
        ParticleRendererService prs;
        ContentService cs;
        DebugRendererService drs;

        InputDevice device;
        InputService input;

        SceneNode[] particleNode;
        ParticleAnimation animation;
        SceneNode cameraNode;
        Camera camera;

        public ParticleCullingTest()
        {
        }

        public override bool OnEnter()
        {
            input = ServiceProvider.GetService<InputService>();
            prs = ServiceProvider.GetService<ParticleRendererService>();
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();
            drs = ServiceProvider.GetService<DebugRendererService>();

            device = input.GetDevice(0);

            // Init Camera:
            cameraNode = ss.AddSceneNode("camera");
            camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 300.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(0, 0, 0);
            ss.ActiveCameraNode = cameraNode;

            animation = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/TestParts.xml");
            animation.Loop = true;
            animation.Interval = 0.1f;

            particleNode = new SceneNode[4];
            for (int i = 0; i < 4; i++) particleNode[i] = ss.AddSceneNode("particles " + i);

            particleNode[0].Attach("node", new ParticleSystem(animation));
            particleNode[1].Attach("node", new ParticleSystem(animation));
            particleNode[2].Attach("node", new ParticleSystem(animation));
            particleNode[3].Attach("node", new ParticleSystem(animation));

            particleNode[0].Translation = new Vector3(0, 0, 20);
            particleNode[1].Translation = new Vector3(0, 0, -20);
            particleNode[2].Translation = new Vector3(20, 0, 0);
            particleNode[3].Translation = new Vector3(-20, 0, 0);

            for (int i = 0; i < 4; i++)
            {
                ParticleSystem p = null;
                particleNode[i].GetAttachment("node", out p);
                if (p != null)
                    p.StartEmitting();
            }

            drs.RasterizerState.CullMode = CullMode.CullCounterClockwiseFace;

            return base.OnEnter();
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            for (int i = 0; i < 4; i++)
            {
                ParticleSystem p = null;
                particleNode[i].GetAttachment("node", out p);
                if (p != null)
                {
                    BoundingSphere sphere = p.GetBoundingSphere();
                    drs.AddSphere(sphere.Center, sphere.Radius, Color.Red);
                }
            }

            if (device.IsPressed("LS_Left"))
            {
                float val = device.GetValue("LS_Left");
                cameraNode.Rotate(GameTiming.TimeElapsed * val * 700, 0, 0);
            }
            if (device.IsPressed("LS_Right"))
            {
                float val = device.GetValue("LS_Right");
                cameraNode.Rotate(GameTiming.TimeElapsed * val * 700, 0, 0);
            }

            List<SceneNode> allnodes = ss.GetSceneNodesWithParticleSystems();
            List<SceneNode> nodes = new List<SceneNode>();
            foreach (SceneNode node in allnodes)
            {
                ParticleSystem p = null;
                node.GetAttachment("node", out p);
                if (p != null && !p.FrustumCulled)
                    nodes.Add(node);
            }
            // break here to test culling.

            return base.OnUpdate();
        }
    }
}
