﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Core;

using Microsoft.Xna.Framework;

namespace Game2
{
    public sealed class TestParticleBehaviour : SceneNodeBehaviour
    {
        public override bool Start()
        {
            return true;
        }

        public override bool Update()
        {
            float x = (float)Math.Sin(GameTiming.TotalTimeElapsed*0.5f)*2.0f;
            SceneNode.Translation = new Vector3(x, 0, 0);
            return true;
        }

        public override void Dispose()
        {
        }

        public override void OnCollideEnter(SceneNode otherNode)
        {
            base.OnCollideEnter(otherNode);
        }

        public override void OnCollideStay(SceneNode otherNode)
        {
            base.OnCollideStay(otherNode);
        }

        public override void OnTriggerEnter(SceneNode otherNode)
        {
            base.OnTriggerEnter(otherNode);
        }
    }
}
