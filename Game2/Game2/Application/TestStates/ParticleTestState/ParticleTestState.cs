﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2
{
    public sealed class ParticleTestState : State
    {
        GraphicsService gs;
        SceneService ss;
        ParticleRendererService prs;
        DebugRendererService drs;
        ConsoleLogService logger;
        ContentService cs;

        InputDevice device;
        InputService input;

        SceneNode particleNode;
        SceneNode cameraNode;
        Camera camera;

        public ParticleTestState()
        {
        }

        public override bool OnEnter()
        {
            logger = ServiceProvider.GetService<ConsoleLogService>();
            input = ServiceProvider.GetService<InputService>();
            prs = ServiceProvider.GetService<ParticleRendererService>();
            drs = ServiceProvider.GetService<DebugRendererService>();
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();

            drs.ShowFrameStatistics = true;

            device = input.GetDevice(4);

            device.AddAction("click", InputButton.Start);
            device.AddAction("esc",InputButton.Back);
            device.AddAction("zoomin", InputButton.B);
            device.AddAction("zoomout", InputButton.A);
            initTestPs();

            // Init Camera:
            cameraNode = ss.AddSceneNode("camera");
            camera = new Camera();
            camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.01f, 300.0f);

            cameraNode.Camera = camera;
            cameraNode.Translation = new Vector3(0, 10, 0);
            cameraNode.Rotate(Vector3.Right, -90);

            ss.ActiveCameraNode = cameraNode;
            
            particleNode = ss.AddSceneNode("particles");
            particleNode.ParticleAttachMode = ParticleAttachMode.Default;
            particleNode.Behaviour = new TestParticleBehaviour();

            drs.RasterizerState.CullMode = CullMode.CullCounterClockwiseFace;

            return base.OnEnter();
        }

        ParticleAnimation anim;
        private void initTestPs()
        {
            /*
            anim = new ParticleAnimation();
            anim.Colors = new Color[5];
            anim.Colors[0] = new Color(1.0f, 0.3f, 0.0f, 0.0f);
            anim.Colors[1] = new Color(1.0f, 0.3f, 0.0f, 0.8f);
            anim.Colors[2] = new Color(1.0f, 0.5f, 0.0f, 1.0f);
            anim.Colors[3] = new Color(1.0f, 0.7f, 0.1f, 0.5f);
            anim.Colors[4] = new Color(0.8f, 0.6f, 0.2f, 0.0f);

            anim.Texture = cs["game"].Load<Texture2D>("Particles/Textures/FlameD");

            anim.GrowSize = 1.0f;
            anim.MinSize = 2.0f;
            anim.MaxSize = 3.0f;

            anim.MinEnergy = 0.8f;
            anim.MaxEnergy = 1.2f;

            anim.MinAmount = 1;
            anim.MaxAmount = 1;

            anim.MinSpeed = 0.4f;
            anim.MaxSpeed = 0.5f;
            anim.Gravity = new Vector3(0, -5, 0);

            anim.Interval = 0.01f;
            anim.Loop = false;*/
            
            anim = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/FireballExplode.xml");
            //ParticleAnimation.SaveToXmlFile(anim, "MyTexture.jpeg", "hello.xml");
        }

        public override bool OnExit()
        {
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            //drs.AddBox3D(new Vector3(-1, -1, -1), new Vector3(1, 1, 1), Color.Red);

            if (device.IsClicked("click"))
            {
                ParticleSystem ps = new ParticleSystem(anim);
                ps.ParticleSystemSpaceMode = ParticleSystemSpaceMode.LocalSpace;
                particleNode.Detach("node");
                particleNode.Attach("node",ps);
                ps.StartEmitting();
            }

            if (device.IsClicked("esc"))
                return false;

            if (device.IsClicked("zoomin"))
            {
                cameraNode.Translate(Vector3.Backward);
                Console.WriteLine(cameraNode.Translation.ToString());
            }

            if (device.IsClicked("zoomout"))
            {
                cameraNode.Translate(Vector3.Forward);
                Console.WriteLine(cameraNode.Translation.ToString());
            }

            return base.OnUpdate();
        }
    }
}
