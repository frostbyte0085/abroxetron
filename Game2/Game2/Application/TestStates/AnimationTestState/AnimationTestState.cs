﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Game2.Logic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Engine.Animation;
using Engine.Input;

namespace Game2
{
    public sealed class AnimationTestState : State
    {
        InputDevice input;
        SceneNode levelNode;

        public AnimationTestState()
        {
        }

        public override bool OnEnter()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();
            input = ServiceProvider.GetService<InputService>().GetDevice(0);

            SceneNode cameraNode = ss.AddSceneNode("camera");
            cameraNode.Camera = new Camera();
            cameraNode.Camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 1.0f, 3000.0f);
            cameraNode.Translation = new Vector3(0, 1, 15);//new Vector3(0, 1, 5);
            //cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-90), 0);
            ss.ActiveCameraNode = cameraNode;

            // Add light
            SceneNode sunNode = ss.AddSceneNode("sun");
            sunNode.Light = new Engine.Graphics.DirectionalLight(Color.White.ToVector4(), 1.0f);
            sunNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-75), 0);

            // Add Level stuff
            levelNode = ss.AddSceneNode("level");
            levelNode.MeshColor = Color.Red.ToVector3();
            levelNode.Model = cs["game"].Load<Model>("Models/Drangonus/Dragon");
            levelNode.Translate(new Vector3(0, 0, 0));

            levelNode.Animation.StartClip(levelNode.Animation["attack"], AnimationPlay.Loop);
            
            drs.ShowFrameStatistics = true;

            return base.OnEnter();
        }

        public override bool OnUpdate()
        {
            //levelNode.Rotate(new Vector3(1, 1, 1), GameTiming.TimeElapsed*10);

            //node1.Translate(Vector3.Right * GameTiming.TimeElapsed);
            //node1.PhysicalEntity.IsAffectedByGravity = false;
            //levelNode.PhysicalEntity.IsAffectedByGravity = false;

            //node1.PhysicalEntity.LinearVelocity = Vector3.Up;
            if (input.IsClicked("A"))
            {
                levelNode.TintColor = Color.CornflowerBlue.ToVector3();
                levelNode.TintAmount = 1.0f;
                //levelNode.Animation.BlendInto("Soldier_StrafeWalkLeftAim", AnimationPlay.Loop, 600.0f);
            }
            else if (input.IsClicked("B"))
            {
                //levelNode.Animation.BlendInto("Soldier_WalkAim", AnimationPlay.Once, 100.0f);
                levelNode.TintAmount = 0.0f;
            }
            else if (input.IsClicked("Back"))
            {
                return false;
            }
            return base.OnUpdate();
        }
    }
}
