﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Core;
using Engine.Tools;
using Engine.Graphics;
using Engine.Input;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using System.Diagnostics;

namespace Game2
{
    public sealed class AnimationBehaviour : CharacterSceneNodeBehaviour
    {
        // Engine vars:
        private ContentService cs;
        private SceneService ss;

        public AnimationBehaviour()
        {
            this.cs = ServiceProvider.GetService<ContentService>();
            this.ss = ServiceProvider.GetService<SceneService>();
        }

        public override bool Start()
        {
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();

            //CreateCharacterController(new Vector3(2,2,0), 2, 0.8f, 0.0f, 0);
            CreateCharacterController(0.0f, 0);
            MaxSlope = MathHelper.ToRadians(45);

            return base.Start();
        }
    }
}
