﻿using Engine.Scene;
using Engine.Core;
using Engine.Graphics;
using Engine;

using Microsoft.Xna.Framework.Graphics;

namespace Game2
{
    public sealed class CandleHolder3Behaviour : SceneNodeBehaviour
    {
        public override bool Start()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();

            SceneNode.Model = cs["game"].Load<Model>("Models/Props/CandleHolder3");

            ParticleAnimation anim 
                = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/CandleFire.xml");

            ParticleSystem cand1 = new ParticleSystem(anim);
            ParticleSystem cand2 = new ParticleSystem(anim);
            ParticleSystem cand3 = new ParticleSystem(anim);

            SceneNode.Attach("Candle1", cand1);
            SceneNode.Attach("Candle2", cand2);
            SceneNode.Attach("Candle3", cand3);

            cand1.StartEmitting();
            cand2.StartEmitting();
            cand3.StartEmitting();

            return true;
        }

        public override bool Update() { return true; }
        public override void Dispose() { }
        public override void OnCollideEnter(SceneNode otherNode) { }
        public override void OnCollideStay(SceneNode otherNode) { }
        public override void OnTriggerEnter(SceneNode otherNode) { }
    }
}
