﻿#define TEST_STATES

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine;
using Engine.Core;
using Engine.Diagnostics;
using Engine.Tools;
using Engine.Audio;
using Engine.UI;

using Microsoft.Xna.Framework.Audio;

using Game2.GameManager;

namespace Game2
{
    public class GameApplication : EngineApplication
    {
        private static GameApplication _app;

        public GameApplication() : base()
        {
            
        }

        public static GameApplication App
        {
            get
            {
                if (_app == null)
                    _app = new GameApplication();
                return _app;
            }
        }

        public override void Initialize(EngineConfiguration configuration)
        {
            base.Initialize(configuration);


        }

        private BaseGameManager gameManager;

        public BaseGameManager GameManager
        {
            set
            {
                gameManager = null;
            }
            get
            {
                if (gameManager == null)
                {
#if WINDOWS
                    gameManager = new WindowsGameManager();
#elif XBOX
                    gameManager = new XBoxGameManager();
#endif
                }
                return gameManager;
            }
        }

        public override void SetupStates()
        {
#if TEST_STATES
            // Test states :
            AddState("TestState", new TestState());
            AddState("DebugRenderTestState", new DebugRenderTestState());
            AddState("ControllerTestState", new ControllerTestState());
            AddState("PhysicalTestState", new PhysicalTestState());
            AddState("ParticleTestState", new ParticleTestState());
            AddState("ParticleCullingTest", new ParticleCullingTest());
            AddState("SceneLoadingState", new SceneLoadingState());
            AddState("AttachementTestState", new AttachementTestState());
            AddState("ModelMaterialTestState", new ModelMaterialTestState());
            AddState("UITestState", new UITestState());
            AddState("AnimationTestState", new AnimationTestState());
            AddState("PathFindingTestState", new PathFindingTestState());
            AddState("MakeGridTestState", new MakeGridTestState());
            AddState("KinectTestState", new KinectTestState());
            AddState("KinectTestState2", new KinectTestState2());
#endif

            // setup the UI sounds
            ContentService content = ServiceProvider.GetService<ContentService>();

            UISounds.BackSound = content["game"].Load<SoundEffect>("Sounds/menuBack");
            UISounds.ClickSound = content["game"].Load<SoundEffect>("Sounds/menuSelect");
            UISounds.ItemChangedSound = content["game"].Load<SoundEffect>("Sounds/menuChange");

            // Actual states :
            // oh! We have two of them now :O :O
            AddState("MainMenuState", new MainMenuState());
            AddState("InGameState", new InGameState());
            AddState("GameOverState", new GameOverState());
            AddState("CongratulationsState", new CongratulationsState());

            SetState("MainMenuState");

            base.SetupStates();
        }

        public override void OnPreStateUpdate()
        {
            if (gameManager != null && gameManager.Active)
                gameManager.Update();

            base.OnPreStateUpdate();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
