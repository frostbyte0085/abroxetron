﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Audio;
using Engine.Core;
using Engine.Graphics;
using Engine.Scene;
using Engine.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

using Game2.Level;
using Game2.GameManager;
using Game2.UI;

namespace Game2
{
    public sealed class MainMenuState : State
    {
        SceneService ss;
        ContentService cs;
        GraphicsService gs;
        UIRendererService urs;

        SceneNode cameraNode;
        MainMenuUI ui;

        float currentYaw = 0.0f;
        const float distance = 8.0f;
        const float pitch = -13.0f;
        const float height = 1.0f;
        const float turnspeed = 0.2f;

        public MainMenuState()
        {
            // Get services:
            ss = ServiceProvider.GetService<SceneService>();
            cs = ServiceProvider.GetService<ContentService>();
            urs = ServiceProvider.GetService<UIRendererService>();
            gs = ServiceProvider.GetService<GraphicsService>();
        }

        public override bool OnEnter()
        {
            // Prepare the UI:
            Microsoft.Xna.Framework.Graphics.Texture2D texture =
                cs["game"].Load<Microsoft.Xna.Framework.Graphics.Texture2D>("UI/UITextureAtlas");
            UIAtlas atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            ui = new MainMenuUI(atlas);

            // Load the mesh
            SceneNode dungeon = ss.AddSceneNode("dungeon");
            dungeon.Model = cs["game"].Load<Microsoft.Xna.Framework.Graphics.Model>("Models/Dungeon/MainHalls/MainHalls");

            // Set up the lighting
            DirectionalLight sunLight = new DirectionalLight(Vector4.One, 0.3f);
            SceneNode sunLightNode = ss.AddSceneNode("sunLight");
            sunLightNode.Light = sunLight;
            sunLightNode.Rotate(new Vector3(1, 0, 0), -75);

            // Set up the camera
            Camera mainCamera = new Camera();
            mainCamera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.1f, 2000.0f);
            cameraNode = ss.AddSceneNode("MenuCamera");
            cameraNode.Camera = mainCamera;
            cameraNode.Translation = new Vector3(17.032f, height, distance);
            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(pitch), 0);
            ss.ActiveCameraNode = cameraNode;

            // Set up the UI
            urs.CurrentLayer = ui.HomeLayer;

            // Play a song :)
            Song s = cs["game"].Load<Song>("Music/legend");
            MediaPlayerService mps = ServiceProvider.GetService<MediaPlayerService>();
            mps.Play(s);

            return base.OnEnter();
        }

        public override bool OnExit()
        {
            urs.CurrentLayer = null;
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            currentYaw += GameTiming.TimeElapsed * turnspeed;

            cameraNode.Translation = new Vector3(
                distance * (float)Math.Sin(currentYaw),
                height,
                distance * (float)Math.Cos(currentYaw));

            cameraNode.Translate(17.032f, -2.0f, 0f);

            cameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(
                currentYaw,
                MathHelper.ToRadians(pitch),
                0);

            return ui.Update();
        }
    }
}
