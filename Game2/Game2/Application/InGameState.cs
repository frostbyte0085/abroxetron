﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Audio;
using Engine.Core;
using Engine.Diagnostics;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using Game2.Level;
using Game2.GameManager;
using Game2.UI;

using Engine.UI;

namespace Game2
{
    public sealed class InGameState : State
    {
        GraphicsService gs;
        DebugRendererService drs;

        InputDevice device;
        InputService input;

        PauseMenuUI pauseUI;

        UIAtlas atlas;

        UIRendererService urs;

        public InGameState()
        {
        }

        public override bool OnEnter()
        {
            input = ServiceProvider.GetService<InputService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            drs = ServiceProvider.GetService<DebugRendererService>();
            urs = ServiceProvider.GetService<UIRendererService>();
            ContentService cs = ServiceProvider.GetService<ContentService>();

            PrecacheResources();

            drs.ShowFrameStatistics = false;

            device = input.GetDevice(0);

            // collision groups and pairs
            InitCollisionGroups();

            PlayerIndex[] players = (PlayerIndex[])Parameters[0];
            GameApplication.App.GameManager.Begin(players);

            //SceneService ss = ServiceProvider.GetService<SceneService>();
            //ss.ShowPathGrid = true;

            Texture2D texture = cs["game"].Load<Texture2D>("UI/UITextureAtlas");
            atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");
            pauseUI = new PauseMenuUI(atlas);

            return base.OnEnter();
        }

        private void PrecacheResources()
        {
            // preload some resources such as models and particle systems so that delay when first spawning them
            // is eliminated
            // Unreal 2 did this :)
            ContentService cs = ServiceProvider.GetService<ContentService>();
            cs["game"].Load<Model>("Models/Skeleton/SkeletonWarrior");
            cs["game"].Load<Model>("Models/Skeleton/Scimitar");
            cs["game"].Load<Model>("Models/Props/RockMissile");
        }

        private void InitCollisionGroups()
        {
            CustomCollisionGroupPairs.Clear();
            CustomCollisionGroups.Clear();

            // Global Collision Groups
            CustomCollisionGroups.Add("Terrain");
            CustomCollisionGroups.Add("Skeleton");
            CustomCollisionGroups.Add("Wizard");
            CustomCollisionGroups.Add("Guardian");
            CustomCollisionGroups.Add("Missile");
            CustomCollisionGroups.Add("Dragon");

            // Can't hit a flesh-less being.
            CustomCollisionGroupPairs.Add("Guardian-Skeleton", CustomCollisionGroups.Get("Guardian"), CustomCollisionGroups.Get("Skeleton"));
            CustomCollisionGroupPairs.Add("Guardian-Wizard", CustomCollisionGroups.Get("Guardian"), CustomCollisionGroups.Get("Wizard"));
            CustomCollisionGroupPairs.Add("Guardian-Missile", CustomCollisionGroups.Get("Guardian"), CustomCollisionGroups.Get("Missile"));
            CustomCollisionGroupPairs.Add("Guardian-Dragon", CustomCollisionGroups.Get("Guardian"), CustomCollisionGroups.Get("Dragon"));
        }

        public override bool OnExit()
        {
            MediaPlayerService mps = ServiceProvider.GetService<MediaPlayerService>();
            mps.Stop();

            BaseGameManager mgr = GameApplication.App.GameManager;
            mgr.Clear();

            GameApplication.App.GameManager = null;

            
            return base.OnExit();
        }

        public override bool OnUpdate()
        {
            BaseGameManager mgr = GameApplication.App.GameManager;

            if (device.IsClicked("Start"))
            {
                pauseUI.Toggle();
            }

            if (pauseUI.IsOn)
            {
                PauseMenuUI.PauseItem selectedItem = pauseUI.Update();
                if (selectedItem == PauseMenuUI.PauseItem.Quit)
                {
                    // reset the pauseUI and set the new state to MainMenuState
                    urs.CurrentLayer = null;
                    GameTiming.Paused = false;
                    GameApplication.App.SetState("MainMenuState");
                }
                else if (selectedItem == PauseMenuUI.PauseItem.Resume)
                {
                    pauseUI.Off();
                }
            }
             
            return base.OnUpdate();
        }
    }
}
