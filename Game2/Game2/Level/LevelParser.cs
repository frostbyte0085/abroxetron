﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Linq;
using System.IO;

using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Game2.Level.LevelPropertyClasses;
using Game2.Level.LevelBehaviourClasses;

using Engine.Tools;

namespace Game2.Level
{
    public class LevelParser
    {
        SceneService sceneService;
        ContentService contentService;

        string currentSceneName;
        Dictionary<string, Vector3> spawnPoints;
        List<Vector3> skeletonSpawnPoints;

        List<SceneNode> triggers;

        public void MakeLevel(string sceneName)
        {
            currentSceneName = sceneName;

            this.triggers = new List<SceneNode>();

            sceneService = ServiceProvider.GetService<SceneService>();
            Debug.Assert(sceneService != null);

            contentService = ServiceProvider.GetService<ContentService>();
            Debug.Assert(contentService != null);

            spawnPoints = new Dictionary<string, Vector3>();
            skeletonSpawnPoints = new List<Vector3>();

            List<LevelLight> lights = new List<LevelLight>();
            List<LevelObject> objects = new List<LevelObject>();
            List<LevelTrigger> triggers = new List<LevelTrigger>();
            List<LevelSpawnPoint> spawns = new List<LevelSpawnPoint>();

            ParseLevelXml(ref lights, ref objects, ref triggers, ref spawns);
            GenerateLevelNodes(ref lights, ref objects, ref triggers, ref spawns);

            lights.Clear();
        }

        private void ParseLevelXml(ref List<LevelLight> lights, ref List<LevelObject> objects, ref List<LevelTrigger> triggers,
                ref List<LevelSpawnPoint> spawns)
        {
            Stream sceneXmlStream = TitleContainer.OpenStream("GameContent/" + currentSceneName + ".xml");
            Debug.Assert(sceneXmlStream != null);

            XDocument sceneXml = XDocument.Load(sceneXmlStream);
            Debug.Assert(sceneXml != null);

            lights = (from light in sceneXml.Descendants("light")
                      select new LevelLight()
                      {
                          Name = light.Attribute("name").Value,
                          Enabled = Convert.ToBoolean(light.Element("enabled").Value),
                          CastShadows = Convert.ToBoolean(light.Element("castshadows").Value),
                          Radius = Convert.ToSingle(light.Element("radius").Value),
                          Intensity = Convert.ToSingle(light.Element("intensity").Value),
                          ShadowColor = new Vector4(Parser.ParseVector3(light.Element("shadow").Value), 1.0f),
                          LightColor = new Vector4(Parser.ParseVector3(light.Element("color").Value), 1.0f),
                          Translation = Parser.ParseVector3(light.Element("translation").Value),
                          Properties = light.Attribute("properties") != null ? light.Attribute("properties").Value : "",
                          Behaviour = light.Attribute("behaviour") != null ? light.Attribute("behaviour").Value : ""

                      }).ToList();

            objects = (from obj in sceneXml.Descendants("object")
                      select new LevelObject()
                      {
                          Name = obj.Attribute("name").Value,
                          Translation = Parser.ParseVector3(obj.Element("translation").Value),
                          Orientation= Parser.ParseQuaternion(obj.Element("rotation").Value),
                          Properties = obj.Attribute("properties") != null ? obj.Attribute("properties").Value : "",
                          Behaviour = obj.Attribute("behaviour") != null ? obj.Attribute("behaviour").Value : ""

                      }).ToList();

            triggers = (from trig in sceneXml.Descendants("trigger")
                       select new LevelTrigger()
                       {
                           Name = trig.Attribute("name").Value,
                           Translation = Parser.ParseVector3(trig.Element("translation").Value),
                           Orientation = Parser.ParseQuaternion(trig.Element("rotation").Value),
                           Properties = trig.Attribute("properties") != null ? trig.Attribute("properties").Value : "",
                           Behaviour = trig.Attribute("behaviour") != null ? trig.Attribute("behaviour").Value : "",
                           Dimensions = Parser.ParseVector3(trig.Element("volume").Value)
                           
                       }).ToList();

            spawns = (from spn in sceneXml.Descendants("spawn")
                        select new LevelSpawnPoint()
                        {
                            Name = spn.Attribute("name").Value,
                            Translation = Parser.ParseVector3(spn.Element("translation").Value)

                        }).ToList();
        }
        
        private void GenerateLights(ref List<LevelLight> lights)
        {
            // make our light nodes
            foreach (LevelLight light in lights)
            {
                LevelLightProperties propertiesClassInstance = null;
                LevelLightBehaviour behaviourClassInstance = null;
                Type lightType = null;
                Type behaviourType = null;
                string fullTypeName = "";

                
                // create the properties class instance
                if (!string.IsNullOrEmpty(light.Properties))
                    fullTypeName = "Game2.Level.LevelPropertyClasses." + light.Properties;
                else
                    fullTypeName = "Game2.Level.LevelPropertyClasses." + light.Name + "Properties";

                lightType = Type.GetType(fullTypeName);
                if (lightType != null)
                {
                    propertiesClassInstance = Activator.CreateInstance(lightType) as LevelLightProperties;
                    Debug.Assert(propertiesClassInstance != null);

                }

                // create the behaviour class instance
                if (!string.IsNullOrEmpty(light.Behaviour))
                    fullTypeName = "Game2.Level.LevelBehaviourClasses." + light.Behaviour;
                else
                    fullTypeName = "Game2.Level.LevelBehaviourClasses." + light.Name + "Behaviour";

                behaviourType = Type.GetType(fullTypeName);
                if (behaviourType != null)
                {
                    behaviourClassInstance = Activator.CreateInstance(behaviourType) as LevelLightBehaviour;
                    Debug.Assert(behaviourClassInstance != null);

                    behaviourClassInstance.Properties = propertiesClassInstance;
                }

                SceneNode currentLightNode = sceneService.AddSceneNode(light.Name);
                currentLightNode.Light = new PointLight(light.LightColor, light.Intensity, light.Radius);
                currentLightNode.Translation = light.Translation;

                if (behaviourClassInstance != null)
                    currentLightNode.Behaviour = behaviourClassInstance;
            }
        }

        private void GenerateObjects(ref List<LevelObject> objects)
        {
            // make our object nodes
            foreach (LevelObject obj in objects)
            {
                LevelObjectProperties propertiesClassInstance = null;
                LevelObjectBehaviour behaviourClassInstance = null;
                Type objectType = null;
                Type behaviourType = null;
                string fullTypeName = "";

                // create the properties class instance
                if (!string.IsNullOrEmpty(obj.Properties))
                    fullTypeName = "Game2.Level.LevelPropertyClasses." + obj.Properties;
                else
                    fullTypeName = "Game2.Level.LevelPropertyClasses." + obj.Name + "Properties";

                objectType = Type.GetType(fullTypeName);
                if (objectType != null)
                {
                    propertiesClassInstance = Activator.CreateInstance(objectType) as LevelObjectProperties;
                    Debug.Assert(propertiesClassInstance != null);

                }

                // create the behaviour class instance
                if (!string.IsNullOrEmpty(obj.Behaviour))
                    fullTypeName = "Game2.Level.LevelBehaviourClasses." + obj.Behaviour;
                else
                    fullTypeName = "Game2.Level.LevelBehaviourClasses." + obj.Name + "Behaviour";

                behaviourType = Type.GetType(fullTypeName);
                if (behaviourType != null)
                {
                    behaviourClassInstance = Activator.CreateInstance(behaviourType) as LevelObjectBehaviour;
                    Debug.Assert(behaviourClassInstance != null);

                    behaviourClassInstance.Properties = propertiesClassInstance;
                }

                SceneNode currentObjectNode = sceneService.AddSceneNode(obj.Name);
                currentObjectNode.Model = contentService["game"].Load<Model>(propertiesClassInstance.ModelFilename);
                currentObjectNode.Translation = obj.Translation;
                currentObjectNode.Orientation = obj.Orientation;

                if (propertiesClassInstance.EntityType != PhysicalEntityType.None)
                {
                    IPhysicalEntityProperties physicalProperties = null;

                    switch (propertiesClassInstance.EntityType)
                    {
                        case PhysicalEntityType.Box:
                            physicalProperties = new PhysicalBoxEntityProperties();
                            break;

                        case PhysicalEntityType.Capsule:
                            physicalProperties = new PhysicalCapsuleEntityProperties();
                            break;

                        case PhysicalEntityType.ConvexHull:
                            physicalProperties = new PhysicalConvexHullEntityProperties();
                            break;

                        case PhysicalEntityType.Cylinder:
                            physicalProperties = new PhysicalCylinderEntityProperties();
                            break;

                        case PhysicalEntityType.Sphere:
                            physicalProperties = new PhysicalSphereEntityProperties();
                            break;

                        case PhysicalEntityType.StaticMesh:
                            physicalProperties = new PhysicalStaticMeshEntityProperties(StaticMeshSidedness.DoubleSided);
                            break;
                    }
                    currentObjectNode.PhysicalEntity = new PhysicalEntity(currentObjectNode, physicalProperties, propertiesClassInstance.EntityMass, false);
                }

                if (behaviourClassInstance != null)
                    currentObjectNode.Behaviour = behaviourClassInstance;

                if (propertiesClassInstance != null)
                    propertiesClassInstance.Initialize(currentObjectNode);
            }
        }

        public List<Vector3> GetSkeletonSpawnPoints()
        {
            return skeletonSpawnPoints;
        }

        public Vector3 GetSpawnPoint(string name)
        {
            Debug.Assert(spawnPoints.ContainsKey(name));

            Vector3 position = Vector3.Zero;
            spawnPoints.TryGetValue(name, out position);
            
            return position;
        }

        public List<string> SpawnPointNames
        {
            get
            {
                return new List<string>(spawnPoints.Keys);
            }
        }

        private void GenerateSpawnPoints(ref List<LevelSpawnPoint> spawns)
        {
            foreach (LevelSpawnPoint spawn in spawns)
            {
                // There are multiple skeleton spawn points. The actual name of the
                // spawn point doesn't matter for the EnemyManager:
                if (spawn.Name.StartsWith("SkeletonSpawn"))
                    skeletonSpawnPoints.Add(spawn.Translation);

                // Other spawn points (e.g. DragonSpawn, other transition spawn pnts)
                else
                    spawnPoints[spawn.Name] = spawn.Translation;
            }
        }

        private void GenerateTriggers(ref List<LevelTrigger> triggers)
        {
            // make our object nodes
            foreach (LevelTrigger trig in triggers)
            {
                LevelTriggerProperties propertiesClassInstance = null;
                LevelTriggerBehaviour behaviourClassInstance = null;
                Type triggerType = null;
                Type behaviourType = null;
                string fullTypeName = "";

                // create the properties class instance
                if (!string.IsNullOrEmpty(trig.Properties))
                    fullTypeName = "Game2.Level.LevelPropertyClasses." + trig.Properties;
                else
                    fullTypeName = "Game2.Level.LevelPropertyClasses." + trig.Name + "Properties";

                triggerType = Type.GetType(fullTypeName);
                if (triggerType != null)
                {
                    propertiesClassInstance = Activator.CreateInstance(triggerType) as LevelTriggerProperties;
                    Debug.Assert(propertiesClassInstance != null);

                }

                // create the behaviour class instance
                if (!string.IsNullOrEmpty(trig.Behaviour))
                    fullTypeName = "Game2.Level.LevelBehaviourClasses." + trig.Behaviour;
                else
                    fullTypeName = "Game2.Level.LevelBehaviourClasses." + trig.Name + "Behaviour";

                behaviourType = Type.GetType(fullTypeName);
                if (behaviourType != null)
                {
                    behaviourClassInstance = Activator.CreateInstance(behaviourType) as LevelTriggerBehaviour;
                    Debug.Assert(behaviourClassInstance != null);

                    behaviourClassInstance.Properties = propertiesClassInstance;
                }

                SceneNode currentTriggerNode = sceneService.AddSceneNode(trig.Name);
                currentTriggerNode.Translation = trig.Translation;
                currentTriggerNode.Orientation = trig.Orientation;

                currentTriggerNode.PhysicalEntity = new PhysicalEntity(new PhysicalBoxEntityProperties(trig.Dimensions.X, trig.Dimensions.Y, trig.Dimensions.Z), true);

                this.triggers.Add(currentTriggerNode);

                if (behaviourClassInstance != null)
                    currentTriggerNode.Behaviour = behaviourClassInstance;

                
            }
        }

        public List<SceneNode> TriggerNodes
        {
            get
            {
                return triggers;
            }
        }

        private  void GenerateLevelNodes(ref List<LevelLight> lights, ref List<LevelObject> objects, ref List<LevelTrigger> triggers,
                ref List<LevelSpawnPoint> spawns)
        {
            GenerateLights(ref lights);
            GenerateObjects(ref objects);
            GenerateTriggers(ref triggers);
            GenerateSpawnPoints(ref spawns);
        }
    }
}
