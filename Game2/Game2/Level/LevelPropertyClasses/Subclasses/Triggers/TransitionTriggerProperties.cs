﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2.GameManager;

namespace Game2.Level.LevelPropertyClasses
{
    public abstract class TransitionTriggerProperties : LevelTriggerProperties
    {
        public abstract LevelType TransitionTarget { get; }
    }

    public class FireEnterTriggerProperties : TransitionTriggerProperties
    {
        public override LevelType TransitionTarget { get { return LevelType.Fire; } }
    }
    public class WaterEnterTriggerProperties : TransitionTriggerProperties
    {
        public override LevelType TransitionTarget { get { return LevelType.Water; } }
    }
    public class AirEnterTriggerProperties : TransitionTriggerProperties
    {
        public override LevelType TransitionTarget { get { return LevelType.Air; } }
    }
    public class EarthEnterTriggerProperties : TransitionTriggerProperties
    {
        public override LevelType TransitionTarget { get { return LevelType.Earth; } }
    }
    public class MainHallsEnterTriggerProperties : TransitionTriggerProperties
    {
        public override LevelType TransitionTarget { get { return LevelType.MainHalls; } }
    }
}
