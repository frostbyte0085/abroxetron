﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

namespace Game2.Level.LevelPropertyClasses
{
    public abstract class LevelLightProperties : LevelNodeProperties, IProperties
    {
        public virtual void Initialize(SceneNode node) {}
    }
}
