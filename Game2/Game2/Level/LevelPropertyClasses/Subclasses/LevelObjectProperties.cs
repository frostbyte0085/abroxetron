﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Physics;
using Engine.Scene;

namespace Game2.Level.LevelPropertyClasses
{
    public abstract class LevelObjectProperties : LevelNodeProperties, IProperties
    {
        public abstract string ModelFilename { get; }
        public abstract PhysicalEntityType EntityType { get; }
        public abstract float EntityMass { get; }

        public virtual void Initialize(SceneNode node) { }
    }
}
