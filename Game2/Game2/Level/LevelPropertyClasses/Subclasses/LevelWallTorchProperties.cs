﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

using Microsoft.Xna.Framework;

namespace Game2.Level.LevelPropertyClasses
{
    public abstract class LevelWallTorchProperties : LevelObjectProperties
    {
        public override string ModelFilename
        {
            get
            {
                return "Models/Props/WallTorch";
            }
        }

        public abstract string ParticleSystemFilename
        {
            get;
        }

        public abstract Vector4 LightColor
        {
            get;
        }

        public abstract float LightIntensity
        {
            get;
        }

        public abstract float LightRadius
        {
            get;
        }

        public abstract float LightTwitch
        {
            get;
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        {
            get
            {
                return Engine.Physics.PhysicalEntityType.None;
            }
        }

        public override float EntityMass
        {
            get
            {
                return 0.0f;
            }
        }
    }
}
