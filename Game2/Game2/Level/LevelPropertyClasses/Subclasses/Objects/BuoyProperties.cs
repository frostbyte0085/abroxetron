﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Microsoft.Xna.Framework;

namespace Game2.Level.LevelPropertyClasses
{
    public class BuoyProperties : LevelObjectProperties
    {
        private static ParticleAnimation __anim = null;
        private ParticleAnimation FireAnim
        {
            get
            {
                if (__anim == null)
                    __anim = ParticleAnimation.CreateParticleAnimation(this.ParticleSystemFilename);
                return __anim;
            }
        }

        public string ParticleSystemFilename
        {

            get
            {
                return "GameContent/Particles/Systems/TorchFireYellow.xml";
            }
        }

        /*public SceneNode LightNode
        {
            get;
            private set;
        }*/

        public Vector4 LightColor
        {
            get
            {
                return new Vector4(1, 0.85f, 0.65f, 1);
            }
        }

        public float LightIntensity
        {
            get
            {
                return 1.5f;
            }
        }

        public float LightRadius
        {
            get
            {
                return 3.5f;
            }
        }

        public float LightTwitch
        {
            get
            {
                return 0.05f;
            }
        }

        #region LevelObjectProperties override

        public override string ModelFilename { get { return "Models/Props/Buoy"; } }
        
        public override PhysicalEntityType EntityType { get { return PhysicalEntityType.None; } }
        public override float EntityMass { get { return 0.0f; } }

        public override void Initialize(SceneNode node)
        {
            /*SceneService ss = ServiceProvider.GetService<SceneService>();
            LightNode = ss.AddSceneNode(node.Name + "_light");
            node.Attach("Bone001", LightNode);*/
            node.Light = new PointLight(LightColor, LightIntensity, LightRadius);

            ParticleSystem ps = new ParticleSystem(FireAnim);
            node.Attach("Bone001", ps);
            ps.StartEmitting();
        }
        #endregion
    }
}
