﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;
using Microsoft.Xna.Framework;

namespace Game2.Level.LevelPropertyClasses
{
    public class WallTorchBlueHueProperties : LevelWallTorchProperties
    {
        public override string ParticleSystemFilename
        {
            get
            {
                return "GameContent/Particles/Systems/TorchFireBlue.xml";
            }
        }

        public override Vector4 LightColor
        {
            get
            {
                return new Vector4(0.52f,0.81f,1,1);
            }
        }

        public override float LightIntensity
        {
            get
            {
                return 1.3f;
            }
        }

        public override float LightRadius
        {
            get
            {
                return 4.3f;
            }
        }

        public override float LightTwitch
        {
            get
            {
                return 0.04f;
            }
        }
    }
}
