﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Level.LevelPropertyClasses
{
    public class TestStaticMazeProperties : LevelObjectProperties
    {

        public override string ModelFilename
        {
            get
            {
                return "Models/DummyBoxes/StaticMaze";
            }
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        {
            get
            {
                return Engine.Physics.PhysicalEntityType.StaticMesh;
            }
        }

        public override float EntityMass
        {
            get
            {
                return 0.0f;
            }
        }
    }
}
