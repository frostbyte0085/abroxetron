﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

namespace Game2.Level.LevelPropertyClasses
{
    public class CandleHolder3Properties : LevelObjectProperties
    {
        private ParticleSystem cand1;
        private ParticleSystem cand2;
        private ParticleSystem cand3;

        public ParticleSystem CandleParticle1 { get { return cand1; } }
        public ParticleSystem CandleParticle2 { get { return cand2; } }
        public ParticleSystem CandleParticle3 { get { return cand3; } }

        public string CandleParticleFilename { get { return "GameContent/Particles/Systems/CandleFire.xml"; } }

        #region LevelObjectProperties override

        public override string ModelFilename { get { return "Models/Props/CandleHolder3"; } }
        
        public override PhysicalEntityType EntityType { get { return PhysicalEntityType.None; } }
        public override float EntityMass { get { return 2.0f; } }

        public override void Initialize(SceneNode node)
        {
            ParticleAnimation anim
                = ParticleAnimation.CreateParticleAnimation(CandleParticleFilename);

            cand1 = new ParticleSystem(anim);
            cand2 = new ParticleSystem(anim);
            cand3 = new ParticleSystem(anim);

            node.Attach("Candle1", cand1);
            node.Attach("Candle2", cand2);
            node.Attach("Candle3", cand3);

            cand1.StartEmitting();
            cand2.StartEmitting();
            cand3.StartEmitting();
        }
        #endregion
    }
}
