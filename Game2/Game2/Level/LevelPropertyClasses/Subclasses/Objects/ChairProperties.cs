﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Level.LevelPropertyClasses
{
    public class ChairProperties : LevelObjectProperties
    {

        public override string ModelFilename
        {
            get
            {
                return "Models/Props/Chair";
            }
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        {
            get
            {
                return Engine.Physics.PhysicalEntityType.Box;
            }
        }

        public override float EntityMass
        {
            get
            {
                return 1.0f;
            }
        }
    }
}
