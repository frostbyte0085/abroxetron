﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Level.LevelPropertyClasses
{
    public class BoxProperties : LevelObjectProperties
    {

        public override string ModelFilename
        {
            get
            {
                return "Models/DummyBoxes/Box002";
            }
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        {
            get
            {
                return Engine.Physics.PhysicalEntityType.None;
            }
        }

        public override float EntityMass
        {
            get
            {
                return 0.0f;
            }
        }
    }
}
