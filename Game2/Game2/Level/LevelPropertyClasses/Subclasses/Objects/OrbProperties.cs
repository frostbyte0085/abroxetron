﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Level.LevelPropertyClasses
{
    public class OrbProperties : LevelObjectProperties
    {
        public override string ModelFilename
        {
            get { return "Models/Props/Orb"; }
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        { 
            get { return Engine.Physics.PhysicalEntityType.Sphere; } 
        }

        public override float EntityMass 
        { 
            get { return 0.0f; } 
        }
    }

    public class AirOrbProperties : OrbProperties
    { }
    public class EarthOrbProperties : OrbProperties
    { }
    public class FireOrbProperties : OrbProperties
    { }
    public class WaterOrbProperties : OrbProperties
    { }
}
