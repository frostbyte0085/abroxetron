﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Physics;

namespace Game2.Level.LevelPropertyClasses
{
    public class GroundProperties : LevelObjectProperties
    {

        public override string ModelFilename
        {
            get
            {
                return "Models/DummyBoxes/Plane001";
            }
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        {
            get
            {
                return PhysicalEntityType.Box;
            }  
        }

        public override float EntityMass
        {
            get
            {
                return 0.0f;
            }
        }
    }
}
