﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;
using Microsoft.Xna.Framework;

namespace Game2.Level.LevelPropertyClasses
{
    public class GroundTorchProperties : LevelGroundTorchProperties
    {
        public override string ParticleSystemFilename
        {

            get
            {
                return "GameContent/Particles/Systems/TorchFireYellow.xml";
            }
        }

        public override Vector4 LightColor
        {
            get
            {
                return new Vector4(1, 0.85f, 0.65f, 1);
            }
        }

        public override float LightIntensity
        {
            get
            {
                return 1.5f;
            }
        }

        public override float LightRadius
        {
            get
            {
                return 3.5f;
            }
        }

        public override float LightTwitch
        {
            get
            {
                return 0.05f;
            }
        }
    }
}
