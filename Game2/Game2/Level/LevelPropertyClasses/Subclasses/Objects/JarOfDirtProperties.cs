﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

namespace Game2.Level.LevelPropertyClasses
{
    public class JarOfDirtProperties : LevelObjectProperties
    {
        #region LevelObjectProperties override

        public override string ModelFilename { get { return "Models/Props/JarOfDirt"; } }
        
        public override PhysicalEntityType EntityType { get { return PhysicalEntityType.Sphere; } }
        public override float EntityMass { get { return 3.0f; } }

        #endregion
    }
}
