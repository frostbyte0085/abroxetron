﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Level.LevelPropertyClasses
{
    public class MainHallsProperties : LevelObjectProperties
    {

        public override string ModelFilename
        {
            get
            {
                return "Models/Dungeon/MainHalls/MainHalls";
            }
        }

        public override Engine.Physics.PhysicalEntityType EntityType
        {
            get
            {
                return Engine.Physics.PhysicalEntityType.StaticMesh;
            }
        }

        public override float EntityMass
        {
            get
            {
                return 0.0f;
            }
        }

        public override void Initialize(Engine.Scene.SceneNode node)
        {
            node.CategoryGroup = "Level";
            base.Initialize(node);
        }
    }
}
