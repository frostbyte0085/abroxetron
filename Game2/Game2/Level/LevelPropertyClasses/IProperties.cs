﻿using Engine.Scene;

namespace Game2.Level.LevelPropertyClasses
{
    public interface IProperties
    {
        void Initialize(SceneNode node);
    }
}
