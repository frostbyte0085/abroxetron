﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Core;

namespace Game2.Level.LevelBehaviourClasses
{
    public class FallingBoxBehaviour : LevelObjectBehaviour
    {
        bool triggered = false;

        public override bool Start()
        {
            return base.Start();
        }

        public override bool Update()
        {
            if (triggered)
                return false;

            //Console.WriteLine("updating");
            return base.Update();
        }

        public override void OnTriggerEnter(SceneNode otherNode)
        {
            Console.WriteLine("entered the trigger " + otherNode.Name);
            triggered = true;
            base.OnTriggerEnter(otherNode);
        }

        public override void Dispose()
        {
            Console.WriteLine("disposed!");
            base.Dispose();
        }
    }
}
