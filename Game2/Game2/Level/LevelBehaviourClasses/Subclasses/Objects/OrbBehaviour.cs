﻿using Engine.Scene;

using Game2.GameManager;

using Microsoft.Xna.Framework.Graphics;

namespace Game2.Level.LevelBehaviourClasses
{
    public class OrbBehaviour : LevelObjectBehaviour
    {
        public override void OnCollideEnter(SceneNode otherNode)
        {
            if (otherNode.CategoryGroup == "Wizard")
            {
                BaseGameManager gm = GameApplication.App.GameManager;
                gm.CollectMe(this.SceneNode);
            }
        }
    }

    public class AirOrbBehaviour : OrbBehaviour { }
    public class FireOrbBehaviour : OrbBehaviour { }
    public class WaterOrbBehaviour : OrbBehaviour { }
    public class EarthOrbBehaviour : OrbBehaviour { }
}
