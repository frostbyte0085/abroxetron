﻿using System;

using Engine.Scene;
using Engine.Core;
using Engine.Graphics;
using Engine.Tools;

using Game2.Level.LevelPropertyClasses;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.Level.LevelBehaviourClasses
{
    public sealed class BuoyBehaviour : LevelObjectBehaviour
    {
        BuoyProperties buoyProperties;
        float periodX, periodY, periodZ;
        Vector3 startPos;

        public override bool Start()
        {
            buoyProperties = this.Properties as BuoyProperties;

            periodX = Random2.RandomF(0.5f, 1f);
            periodY = Random2.RandomF(0.5f, 1f);
            periodZ = Random2.RandomF(0.5f, 1f);

            startPos = this.SceneNode.Translation;

            return true;
        }

        public override bool Update()
        {
            float t = GameTiming.TotalTimeElapsed;

            float twitching = (float)Math.Sin(t * 50) * buoyProperties.LightTwitch;
            this.SceneNode.Light.Intensity += twitching;
            this.SceneNode.Light.Radius += twitching;

            Vector3 s = new Vector3(
                (float)Math.Sin(t * periodX) * 0.2f,
                (float)Math.Sin(t * periodY) * 0.2f,
                (float)Math.Sin(t * periodZ) * 0.1f);

            this.SceneNode.Translation = startPos + s;

            return true;
        }
    }
}
