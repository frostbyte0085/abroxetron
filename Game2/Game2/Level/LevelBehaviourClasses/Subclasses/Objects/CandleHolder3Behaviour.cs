﻿using Engine.Scene;
using Engine.Core;
using Engine.Graphics;
using Engine;

using Microsoft.Xna.Framework.Graphics;

namespace Game2.Level.LevelBehaviourClasses
{
    public sealed class CandleHolder3Behaviour : LevelObjectBehaviour
    {
        public override bool Start()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();

            return true;
        }

        public override bool Update() { return true; }
        public override void Dispose() { base.Dispose(); }
        public override void OnCollideEnter(SceneNode otherNode) { }
        public override void OnCollideStay(SceneNode otherNode) { }
        public override void OnTriggerEnter(SceneNode otherNode) { }
    }
}
