﻿using Engine.Scene;
using Engine.Core;
using Engine.Graphics;

using Game2.GameManager;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Game2.Level;
using Game2.Level.LevelPropertyClasses;

using System;

namespace Game2.Level.LevelBehaviourClasses
{
    public class WallTorchBehaviour : LevelObjectBehaviour
    {
        SceneNode lightNode;
        float twitchA;

        public override bool Start()
        {
            LevelWallTorchProperties properties = Properties as LevelWallTorchProperties;

            SceneService ss = ServiceProvider.GetService<SceneService>();

            lightNode = ss.AddSceneNode(SceneNode.Name + "_lightnode");
            lightNode.Light = new PointLight(properties.LightColor, properties.LightIntensity, properties.LightRadius);
            lightNode.Translation = SceneNode.Translation;

            twitchA = properties.LightTwitch;

            SceneNode.ParticleAttachMode = ParticleAttachMode.Default;
            ParticleAnimation anim = ParticleAnimation.CreateParticleAnimation(properties.ParticleSystemFilename);
            ParticleSystem ps = new ParticleSystem(anim);
            ps.ParticleSystemSpaceMode = ParticleSystemSpaceMode.LocalSpace;
            SceneNode.Attach("Bone001", ps);
            ps.StartEmitting();

            return base.Start();
        }

        public override bool Update()
        {
            if (!GameTiming.Paused)
            {
                float twitching = (float)Math.Sin(GameTiming.TotalTimeElapsed * 50) * twitchA;

                lightNode.Light.Intensity += twitching;
                lightNode.Light.Radius += twitching;
            }
            return base.Update();
        }
    }
}
