﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2.Level.LevelPropertyClasses;
using Game2.GameManager;

namespace Game2.Level.LevelBehaviourClasses
{
    public class TransitionTrigger : LevelTriggerBehaviour
    {
        public override void OnCollideEnter(Engine.Scene.SceneNode otherNode)
        {
            BaseGameManager gm = GameApplication.App.GameManager;
            if (otherNode["unit"] == null || gm.InDragonMode)
                return;

            TransitionTriggerProperties p = (TransitionTriggerProperties)this.Properties;
            gm.Transition(p.TransitionTarget);
        }

        public override bool Update()
        {

            return true;
        }
    }
}
