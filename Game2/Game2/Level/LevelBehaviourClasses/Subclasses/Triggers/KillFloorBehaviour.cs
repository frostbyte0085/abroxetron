﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;

using Game2.Level.LevelPropertyClasses;
using Game2.Logic;

namespace Game2.Level.LevelBehaviourClasses
{
    public class KillFloorBehaviour : LevelTriggerBehaviour
    {
        private SceneService ss;

        public KillFloorBehaviour()
        {
            ss = ServiceProvider.GetService<SceneService>();
        }

        public override void OnCollideEnter(Engine.Scene.SceneNode otherNode)
        {
            if (otherNode["unit"] != null)
            {
                Unit u = (Unit) otherNode["unit"];
                u.Kill();
            }
        }

        public override bool Update()
        {

            return true;
        }
    }
}
