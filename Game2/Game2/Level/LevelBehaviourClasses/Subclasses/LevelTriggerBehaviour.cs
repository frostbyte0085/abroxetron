﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2.Level.LevelPropertyClasses;

namespace Game2.Level.LevelBehaviourClasses
{
    public class LevelTriggerBehaviour : LevelNodeBehaviour
    {
        #region Variables
        
        #endregion

        public LevelTriggerProperties Properties
        {
            set;
            get;
        }
    }
}
