﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2.Level.LevelPropertyClasses;

namespace Game2.Level.LevelBehaviourClasses
{
    public class LevelObjectBehaviour : LevelNodeBehaviour
    {
        #region Variables
        
        #endregion

        public LevelObjectProperties Properties
        {
            set;
            get;
        }
    }
}
