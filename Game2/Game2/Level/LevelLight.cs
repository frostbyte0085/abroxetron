﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Game2.Level
{
    public class LevelLight : LevelObject
    {
        public float Radius { set; get; }
        public bool Enabled { set; get; }
        public bool CastShadows { set; get; }
        public float Intensity { set; get; }
        public Vector4 ShadowColor { set; get; }
        public Vector4 LightColor { set; get; }
        //public string TranslationString { set; get; }
        //public Vector3 Translation { set; get; }
        //public string Name { set; get; }
        //public string Properties { set; get; }
        //public string Behaviour { set; get; }
    }
}
