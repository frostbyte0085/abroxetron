﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Game2.Level
{
    public class LevelObject
    {
        public string Name { set; get; }
        public Vector3 Translation { set; get; }
        public Quaternion Orientation { set; get; }
        public string Properties { set; get; }
        public string Behaviour { set; get; }
    }
}
