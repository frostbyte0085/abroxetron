using System;
using System.Collections.Generic;
using System.Linq;
#if WINDOWS
using System.Windows.Forms;
#endif

using Engine.Diagnostics;
using Engine.Tools;
using Engine.Core;
using Engine.Scene;

namespace Game2
{
#if WINDOWS || XBOX
    static class Program
    {
        /*
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Game1 game = new Game1())
            {
                game.Run();
            }
        }*
         * /
         * 
         */
 
#if WINDOWS
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new XNAWindowForm());
        }
#elif XBOX

        static void Main()
        {
            EngineConfiguration configuration = new EngineConfiguration();
            configuration.Handle = IntPtr.Zero;
            configuration.TargetWidth = 1280;
            configuration.TargetHeight = 720;
            configuration.Fullscreen = true;
            configuration.VSync = true;
            configuration.Name = "Game2";

            GameApplication app = GameApplication.App;
            app.Initialize(configuration);            

            try
            {

                while (app.ExitRequested == false)
                {
                    app.OneFrame();
                }


                app.Dispose();
            }
            catch (Exception exc)
            {
            }
        }
#endif

    }
#endif
}

