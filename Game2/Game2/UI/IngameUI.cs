﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.UI;
using Engine.Graphics;
using Engine.Scene;

using Game2.Logic;
using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.UI
{
    public class IngameUI : UIBasicLayer
    {
        private static int healthbarWidth = 100;
        private static int healthbarHeight = 7;
        private static int manabarWidth = 100;
        private static int manabarHeight = 7;

        private static int portraitWidth = 50;
        private static int portraitHeight = 50;

        private static int spellWidth = 24;
        private static int spellHeight = 24;

        private static int sepPortraitBars = 10;
        private static int sepManaHealthbar = 5;
        private static int sepManaSpell = 5;
        private static int sepScreenEdge = 10;

        private static int dragonHP_h = 12;
        private static int dragonHP_w = 600;
        private static int dragonHP_x = (1280 - dragonHP_w) / 2;
        private static int dragonHP_y = 30;

        private static int totalHeight
        {
            get
            {
                int h1 = portraitHeight;
                int h2 = healthbarHeight + manabarHeight + spellHeight + sepManaHealthbar + sepManaSpell;
                return h1 > h2 ? h1 : h2;
            }
        }
        private static int totalWidth
        {
            get
            {
                int w1 = portraitWidth + healthbarWidth + sepPortraitBars;
                int w2 = portraitWidth + manabarWidth + sepPortraitBars;
                return w1 > w2 ? w1 : w2;
            }
        }

        private Unit[] players;
        private GuardianBehaviour kinectPlayer;

        private Texture2D hp100; // hp texture used at 100% hp
        private Texture2D mana100; // mana texture used at 100% mana

        private UITexture[] HealthBar;
        private UITexture[] ManaBar;
        private UITiledTexture[] Portrait;
        private UITiledTexture[] Spell;
        private UITexture kinectCursor;

        private UITexture[] KinectSpellArrows;
        private UITexture[] KinectSpells;

        private UITexture DragonHealth;

        private UIRendererService urs;

        public IngameUI(UIAtlas atlas, IEnumerable<Unit> players, GuardianBehaviour kinectPlayer) : base(atlas)
        {
            urs = ServiceProvider.GetService<UIRendererService>();

            this.players = players.ToArray();
            this.kinectPlayer = kinectPlayer;
            this.hp100 = CreateRectangle(Color.Green);
            this.mana100 = CreateRectangle(Color.Blue);

            this.HealthBar = new UITexture[this.players.Length];
            this.ManaBar = new UITexture[this.players.Length];
            this.Portrait = new UITiledTexture[this.players.Length];
            this.Spell = new UITiledTexture[this.players.Length];

            this.kinectCursor = new UITexture();

            initWidgets();
        }

        #region Initialization
        private Texture2D CreateRectangle(Color color)
        {
            GraphicsDevice gd = ServiceProvider.GetService<GraphicsService>().GraphicsDevice;

            Texture2D rectangleTexture = new Texture2D(gd, 1, 1, false, SurfaceFormat.Color);
            Color[] colors = { color };
            rectangleTexture.SetData(colors);

            return rectangleTexture;
        }

        private void initWidgets()
        {
            for (int i = 0; i < this.players.Length; i++)
            {
                initBar(HealthBar, i, healthbarWidth, healthbarHeight, hp100);
                initBar(ManaBar, i, manabarWidth, manabarHeight, mana100);

                initPortrait(i);
                initSpell(i);

                this.Add(HealthBar[i]);
                this.Add(ManaBar[i]);
                this.Add(Spell[i]);
                this.Add(Portrait[i]);
            }

            initKinectUI();
            initDragonUI();
        }

        private void initWidgetRectangle(UIWidget[] widgets, int i, int x, int y, int width, int height)
        {
            Point vr = urs.VirtualResolution;

            widgets[i].Width = width;
            widgets[i].Height = height;

            // Find the top-left corner of the player's ui.

            int xoff = 0, yoff = 0;

            // Set x
            switch (i)
            {
                case 0:
                case 2:
                    xoff = sepScreenEdge;
                    break;

                case 1:
                case 3:
                    xoff = vr.X - sepScreenEdge - totalWidth;
                    break;

                default: break;
            }

            // Set y
            switch (i)
            {
                case 0:
                case 1:
                    yoff = sepScreenEdge;
                    break;

                case 2:
                case 3:
                    yoff = vr.Y - sepScreenEdge - totalHeight;
                    break;

                default: break;
            }

            widgets[i].X = x + xoff;
            widgets[i].Y = y + yoff;
        }

        private void initBar(UITexture[] bars, int i, int width, int height, Texture2D texture)
        {
            bars[i] = new UITexture();

            Point vr = urs.VirtualResolution;

            bars[i].Texture = texture;

            int woff = portraitWidth + sepPortraitBars;
            int hoff = 0;
            if (bars == ManaBar) hoff += healthbarHeight + sepManaHealthbar;

            initWidgetRectangle(bars, i, woff, hoff, width, height);
        }

        private void initPortrait(int i)
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            Portrait[i] = new UITiledTexture();
            Portrait[i].Texture = cs["game"].Load<Texture2D>("UI/Ingame/TestPortrait");
            Portrait[i].TileData = UITileData.LoadFromXml("GameContent/UI/Ingame/BasicTiling.xml");
            Portrait[i].Color = players[i].Color;

            int x = 0;
            if (i == 1 || i == 3) x += totalWidth - portraitWidth;

            initWidgetRectangle(Portrait, i, x, 0, portraitWidth, portraitHeight);
        }

        private void initSpell(int i)
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            Spell[i] = new UITiledTexture();
            //Spell[i].Texture = cs["game"].Load<Texture2D>("UI/Ingame/TestSpell");
            //Spell[i].TileData = UITileData.LoadFromXml("GameContent/UI/Ingame/Test.xml");

            int x = portraitWidth + sepPortraitBars;
            if (i == 1 || i == 3) x = totalWidth - x - spellWidth;

            initWidgetRectangle(Spell, i,
                x,
                healthbarHeight + manabarHeight + sepManaHealthbar + sepManaSpell,
                spellWidth,
                spellHeight);
        }

        private void initKinectCursor()
        {
            GraphicsDevice gd = ServiceProvider.GetService<GraphicsService>().GraphicsDevice;

            Texture2D rectangleTexture = new Texture2D(gd, 1, 1, false, SurfaceFormat.Color);
            rectangleTexture.SetData(new Color[] { Color.Red } );

            kinectCursor.Texture = rectangleTexture;
            kinectCursor.Rectangle = new Rectangle(0, 0, 10, 10);

            this.Add(kinectCursor);
        }

        private void initKinectUI ()
        {
            initKinectCursor();

            ContentService cs = ServiceProvider.GetService<ContentService>();
            Texture2D arrow = cs["game"].Load<Texture2D>("UI/Ingame/Arrow");
            float pi = (float)Math.PI;
            int yoff = 280;
            int xoff = 200;
            KinectSpellArrows = new UITexture[6];
            KinectSpells = new UITexture[3];


            KinectSpells[0] = new UITexture();
            KinectSpells[0].Texture = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Light");
            KinectSpellArrows[0] = new UITexture();
            KinectSpellArrows[0].Texture = arrow;
            KinectSpellArrows[0].Rotation = 0.0f;
            KinectSpellArrows[1] = new UITexture();
            KinectSpellArrows[1].Texture = arrow;
            KinectSpellArrows[1].Rotation = pi;
            initKinectSpell(KinectSpellArrows[0], KinectSpellArrows[1], KinectSpells[0], -xoff, yoff);


            KinectSpells[1] = new UITexture();
            KinectSpells[1].Texture = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Freeze");
            KinectSpellArrows[2] = new UITexture();
            KinectSpellArrows[2].Texture = arrow;
            KinectSpellArrows[2].Rotation = pi / 2.0f;
            KinectSpellArrows[3] = new UITexture();
            KinectSpellArrows[3].Texture = arrow;
            KinectSpellArrows[3].Rotation = - pi / 2.0f;
            initKinectSpell(KinectSpellArrows[2], KinectSpellArrows[3], KinectSpells[1], 0, yoff);


            KinectSpells[2] = new UITexture();
            KinectSpells[2].Texture = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Revive");
            KinectSpellArrows[4] = new UITexture();
            KinectSpellArrows[4].Texture = arrow;
            KinectSpellArrows[4].Rotation = pi;
            KinectSpellArrows[5] = new UITexture();
            KinectSpellArrows[5].Texture = arrow;
            KinectSpellArrows[5].Rotation = pi / 2.0f;
            initKinectSpell(KinectSpellArrows[4], KinectSpellArrows[5], KinectSpells[2], xoff, yoff);
        }

        private void initKinectSpell(UITexture dir1, UITexture dir2, UITexture spell, int xoff, int yoff) 
        {
            Point vres = urs.VirtualResolution;
            xoff += vres.X / 2;
            yoff += vres.Y / 2;

            dir1.Rectangle = new Rectangle(xoff - 57, yoff + 5, 35, 35);
            dir2.Rectangle = new Rectangle(xoff - 22, yoff + 5, 35, 35);
            spell.Rectangle = new Rectangle(xoff + 13, yoff, 45, 45);

            this.Add(dir1);
            this.Add(dir2);
            this.Add(spell);
        }

        private void initDragonUI()
        {
            DragonHealth = new UITexture(hp100, new Rectangle(dragonHP_x, dragonHP_y, dragonHP_w, dragonHP_h));
            this.Add(DragonHealth);
        }

        #endregion

        #region Updating
        private void UpdateHealthBar(int i)
        {
            Unit wizard = players[i];
            int width = (int)(healthbarWidth * wizard.UnitProperties.HPPercent + 0.5f);
            UpdateBar(HealthBar, i, width);
        }
        private void UpdateManaBar(int i)
        {
            Unit wizard = players[i];
            int width = (int)(healthbarWidth * wizard.UnitProperties.ManaPercent + 0.5f);
            UpdateBar(ManaBar, i, width);
        }

        private void UpdateBar(UITexture[]bars, int i, int width)
        {
            Point vr = urs.VirtualResolution;

            switch (i)
            {
                case 0:
                case 2:
                    bars[i].Width = width;
                    break;

                case 1:
                case 3:
                    bars[i].Width = width;
                    bars[i].X = vr.X - sepScreenEdge - portraitWidth - sepPortraitBars - width;
                    break;

                default: return;
            }
        }

        private void UpdateSpell(int i)
        {
            Unit wizard = players[i];
            if (wizard.SelectedSpell == null)
            {
                Spell[i].Texture = null;
                return;
            }

            Spell[i].Texture = wizard.SelectedSpell.GetTiledTexture();
            Spell[i].TileData = wizard.SelectedSpell.GetTileData();
        }

        private UITexture debugPoint = null;
        private void UpdateKinectDisplay()
        {
            // Uncomment for Debug markers:
            /*
            DebugRendererService debug = ServiceProvider.GetService<DebugRendererService>();
            Engine.Input.KinectDevice k = ServiceProvider.GetService<Engine.Input.InputService>().GetKinect();
            debug.SetSkeleton(k.PlayerSkeleton, 10, false, Color.Green, Color.White);
             */

            if (debugPoint == null)
            {
                GraphicsDevice gd = ServiceProvider.GetService<GraphicsService>().GraphicsDevice;
                debugPoint = new UITexture();
                debugPoint.Texture = new Texture2D(gd, 1,1);
                debugPoint.Texture.SetData(new Color[]{Color.Green});
                debugPoint.Rectangle = new Rectangle(0, 0, 10, 10);
                this.Add(debugPoint);
            }
            SceneService ss = ServiceProvider.GetService<SceneService>();
            Vector3 myScreenPos = ss.Project(kinectPlayer.Guardian.SceneNode.Translation);
            debugPoint.X = (int)(myScreenPos.X);
            debugPoint.Y = (int)(myScreenPos.Y);

            kinectCursor.X = kinectPlayer.CursorLocation.X;
            kinectCursor.Y = kinectPlayer.CursorLocation.Y;
        }

        bool wasInDragonMode = false;
        float dragonHpAnimation_fill = 0.0f;
        private void UpdateDragonUI()
        {
            if (!wasInDragonMode && GameApplication.App.GameManager.InDragonMode)
            {
                // Do fill up animation :)
                dragonHpAnimation_fill += GameTiming.TimeElapsed * 0.4f;
                if (dragonHpAnimation_fill > 1)
                {
                    dragonHpAnimation_fill = 1;
                    wasInDragonMode = true;
                }
                DragonHealth.Width = (int)(dragonHpAnimation_fill * dragonHP_w);
            }

            else if (GameApplication.App.GameManager.InDragonMode)
            {
                // Update health normally:

                DragonBehaviour dragon = GameApplication.App.GameManager.Dragon;
                if (dragon != null)
                {
                    UnitProperties stat = dragon.DragonUnit.UnitProperties;
                    float hp_percent = ((float)stat.CurrentHP) / ((float)stat.MaxHP);
                    DragonHealth.Width = (int)(hp_percent * dragonHP_w);
                }
            }

            else
            {
                // Remember state so we know when to do the animation:
                wasInDragonMode = GameApplication.App.GameManager.InDragonMode;
                DragonHealth.Width = 0;
            }
        }

        public override void Update()
        {
            for (int i = 0; i < players.Length; i++)
            {
                UpdateHealthBar(i);
                UpdateManaBar(i);
                UpdateSpell(i);
                UpdateDragonUI();
                // no need to update animated textures... handled by the engine.
            }
            UpdateKinectDisplay();

            base.Update();
        }
        #endregion
    }
}
