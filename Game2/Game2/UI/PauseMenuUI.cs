﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Graphics;
using Engine.UI;
using Engine.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.UI
{
    public class PauseMenuUI
    {
        const string quitText = "Quit";
        const string resumeText = "Resume";

        UIRendererService urs;
        InputService input;

        UIAtlas atlas;
        SpriteFont font;

        UIBasicLayer layer;

        public UIBasicLayer Layer
        {
            get
            {
                return layer;
            }
        }

        public PauseMenuUI(UIAtlas atlas)
        {
            urs = ServiceProvider.GetService<UIRendererService>();
            input = ServiceProvider.GetService<InputService>();
            ContentService cs = ServiceProvider.GetService<ContentService>();

            this.font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            this.atlas = atlas;

            layer = new UIBasicLayer(atlas);

            // Create dialog:
            int dialogWidth = 350;
            int dialogHeight = 200;
            UIDialog window = new UIDialog(new Rectangle(urs.VirtualResolution.X / 2 - dialogWidth / 2,
                                                        urs.VirtualResolution.Y / 2 - dialogHeight/2,
                                                        dialogWidth,
                                                        dialogHeight));

            DialogAutoAdjust a = window.DialogAutoAdjust;
            a.GapTop = -10;
            a.WidgetHeight = 40;
            a.GapWidget = 15;
            a.GapLeft = 35;
            a.GapRight = 35;
            window.DialogAutoAdjust = a;

            // Create the other widgets
            UIButton resumeButton = new UIButton(font, resumeText, Color.DarkRed);
            UIButton quitButton = new UIButton(font, quitText, Color.DarkRed);

            // Add the widgets to the layer:
            window.Add(resumeButton);
            window.Add(quitButton);
            layer.Add(window);

            UISelector selector = new UISelector();
            layer.Selector = selector;
            selector.AddLink(resumeButton, quitButton, InputAxis.LS_Y);
            selector.AddLink(resumeButton, quitButton, InputAxis.RS_Y);
            selector.AddLink(resumeButton, quitButton, InputAxis.D_Y);
        }

        public enum PauseItem
        {
            Resume,
            Quit,
            None
        }

        /// <summary>
        /// Toggle between on / off according to GameTiming.Paused
        /// </summary>
        public void Toggle()
        {
            if (GameTiming.Paused) 
                Off();

            else On();
        }

        private UILayer savedLayer = null;

        /// <summary>
        /// Show the pause menu and stop time.
        /// </summary>
        public void On()
        {
            GameTiming.Paused = true;
            IsOn = true;

            if(urs.CurrentLayer != this.Layer)
                savedLayer = urs.CurrentLayer;

            urs.CurrentLayer = this.Layer;
        }

        /// <summary>
        /// Hide the pause menu, restore old UILayer and resume time.
        /// </summary>
        public void Off()
        {
            GameTiming.Paused = false;
            IsOn = false;

            urs.CurrentLayer = savedLayer;
        }

        public bool IsOn { private set; get; }

        public PauseItem Update()
        {
            InputDevice d = input.GetDevice(0);
            string selectedText = ((UIButton)layer.Selector.Selected).Label.Text;

            if (d.IsClicked("A"))
            {
                if (selectedText == resumeText)
                {
                    return PauseItem.Resume;
                }
                else if (selectedText == quitText)
                {
                    return PauseItem.Quit;
                }
            }
            return PauseItem.None;
        }

    }
}
