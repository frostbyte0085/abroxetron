﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Engine.Core;
using Engine.UI;
using Engine.Graphics;
using Engine.Input;

using Game2.Logic;
using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.UI
{
    public class SpellAssignUI : UIBasicLayer
    {
        private UITiledTexture[] Portrait;
        private PlayerIndex[] players;
        private Color[] playerColors;

        private UIButton[] playerButton;
        private UILabel header;

        private UIRendererService urs;
        
        public string Header
        {
            set
            {
                header.Text = value;
            }
            get { return header.Text; }
        }

        public Nullable<PlayerIndex> TheChosenOne
        {
            get;
            private set;
        }

        public SpellAssignUI(UIAtlas atlas, PlayerIndex[] players, Color[] playerColors) : base(atlas)
        {
            Debug.Assert(players.Length != 0);
            Debug.Assert(playerColors.Length == players.Length);

            urs = ServiceProvider.GetService<UIRendererService>();

            this.Selector = new UISelector();
            this.Selector.InputDevice = ServiceProvider.GetService<InputService>().GetDevice(players[0]); ;

            this.players = players;
            this.playerColors = playerColors;
            Portrait = new UITiledTexture[players.Length];
            playerButton = new UIButton[players.Length];
            

            initPortraits();
            initLinks();
            initHeader();
        }

        public void Reset()
        {
            TheChosenOne = null;
        }

        private void initPortraits()
        {
            for (int i = 0; i < players.Length; i++)
                initPortrait(i);
        }

        private const int portraitWidth = 100;
        private const int portraitHeight = 100;
        private const int portraitXoff = 120;
        private const int portraitYoff = 60;
        private const int buttonWidth = 180;
        private const int buttonHeight = 60;
        private const int buttonYoff = 120;

        private void initPortrait(int i)
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");

            Point center = urs.VirtualResolution;
            center.X = center.X / 2;
            center.Y = center.Y / 2 - 50;
            int xoff = (i % 2 == 0 ? -(portraitXoff + portraitWidth) : portraitXoff);
            int yoff = (i / 2 == 0 ? -(portraitYoff + portraitHeight) : portraitYoff);

            Rectangle rect = new Rectangle(center.X, center.Y, portraitWidth, portraitHeight);
            rect.X += xoff;
            rect.Y += yoff;

            Portrait[i] = new UITiledTexture();
            Portrait[i].Texture = cs["game"].Load<Texture2D>("UI/Ingame/TestPortrait");
            Portrait[i].TileData = UITileData.LoadFromXml("GameContent/UI/Ingame/BasicTiling.xml");
            Portrait[i].Color = playerColors[i];
            Portrait[i].Rectangle = rect;
            this.Add(Portrait[i]);
            
            rect = new Rectangle(center.X, center.Y, buttonWidth, buttonHeight);
            playerButton[i] = new UIButton();
            playerButton[i].Rectangle = rect;
            playerButton[i].Y += buttonYoff;
            playerButton[i].X += xoff;
            playerButton[i].Y += yoff;
            playerButton[i].Label = new UILabel("Player " + players[i], font, Color.Black);
            this.Add(playerButton[i]);
        }

        private void initLinks()
        {
            if (players.Length == 1)
            {
                // Add a random so that Selector can actually select this single button.
                // With atleast 1 link the Selector will complain that the button is
                // unreachable.
                this.Selector.AddLink(playerButton[0], playerButton[0], InputAxis.D_Y);
            }

            if (players.Length > 1)
            {
                this.Selector.AddLink(playerButton[0], playerButton[1], InputAxis.D_X);
                this.Selector.AddLink(playerButton[0], playerButton[1], InputAxis.LS_X);
                this.Selector.AddLink(playerButton[0], playerButton[1], InputAxis.RS_X);
            }

            if (players.Length > 2)
            {
                this.Selector.AddLink(playerButton[0], playerButton[2], InputAxis.D_Y);
                this.Selector.AddLink(playerButton[0], playerButton[2], InputAxis.LS_Y);
                this.Selector.AddLink(playerButton[0], playerButton[2], InputAxis.RS_Y);
            }

            if (players.Length > 3)
            {
                this.Selector.AddLink(playerButton[1], playerButton[3], InputAxis.D_Y);
                this.Selector.AddLink(playerButton[1], playerButton[3], InputAxis.LS_Y);
                this.Selector.AddLink(playerButton[1], playerButton[3], InputAxis.RS_Y);

                this.Selector.AddLink(playerButton[2], playerButton[3], InputAxis.D_X);
                this.Selector.AddLink(playerButton[2], playerButton[3], InputAxis.LS_X);
                this.Selector.AddLink(playerButton[2], playerButton[3], InputAxis.RS_X);
            }
        }

        private void initHeader()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SpriteFont font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");

            Point center = urs.VirtualResolution;
            center.X = center.X / 2;
            center.Y = center.Y / 2 - 50;

            header = new UILabel("Who shall receive the spell?", font, Color.White);
            header.Rectangle = new Rectangle(
                center.X - (portraitHeight + portraitXoff),
                center.Y - (portraitHeight + portraitYoff + buttonHeight), 
                portraitWidth * 2 + portraitXoff * 2,
                buttonHeight);

            this.Add(header);
        }

        public override void Update()
        {
            if (this.Selector.InputDevice.IsClicked("A"))
            {
                int i=0;
                foreach (UIButton b in playerButton)
                {
                    if (this.Selector.Selected == b)
                    {
                        TheChosenOne = players[i];
                        break;
                    }
                    i++;
                }
            }

            base.Update();
        }
    }
}
