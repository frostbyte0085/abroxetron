﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Graphics;
using Engine.UI;
using Engine.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Game2.Logic.Behaviours;

namespace Game2.UI
{
    public class MainMenuUI
    {
        private const string joinString = "Press \"Start\" to join.";
        private const string beginString = "READY!\nPress \"Start\" to PLAY!";

        private class PlayerSlot
        {
            internal bool playerIsPlaying = false;
            internal UILabel label;
            internal Color color;
        }

        UIRendererService urs;
        InputService input;

        SpriteFont font;
        UIAtlas atlas;

        UITreeLayer home;
        public UITreeLayer HomeLayer { get { return home; } }

        UITreeLayer newGame;
        UITexture gameLogo;
        UITexture storyboard;
        UITexture storyboardLoading;

        UITreeLayer options;
        UIButton kinectMouseButton;
        UIButton option_backButton;
        UIButton fullwindowButton;

        private PlayerSlot[] slots = {null, null, null, null};
        public PlayerIndex[] PlayersPlaying
        {
            get
            {
                List<PlayerIndex> l = new List<PlayerIndex>();
                for (int i=0; i < 4; i++)
                {
                    if (slots[i].playerIsPlaying) l.Add((PlayerIndex)i);
                }
                return l.ToArray();
            }
        }

        public MainMenuUI(UIAtlas atlas)
        {
            urs = ServiceProvider.GetService<UIRendererService>();
            input = ServiceProvider.GetService<InputService>();
            ContentService cs = ServiceProvider.GetService<ContentService>();

            this.font = cs["game"].Load<SpriteFont>("UI/Fonts/MyFont");
            this.atlas = atlas;

            this.slots[0] = new PlayerSlot();
            this.slots[1] = new PlayerSlot();
            this.slots[2] = new PlayerSlot();
            this.slots[3] = new PlayerSlot();
            
            this.gameLogo = new UITexture();
            this.gameLogo.Texture = cs["game"].Load<Texture2D>("UI/MenuLogo");
            this.gameLogo.Rectangle = new Rectangle(urs.VirtualResolution.X/2 - gameLogo.Texture.Width/2, 50, gameLogo.Texture.Width, gameLogo.Texture.Height);

            this.storyboard = new UITexture();
            this.storyboard.Texture = cs["game"].Load<Texture2D>("UI/Storyboard");
            this.storyboard.Rectangle = new Rectangle(urs.VirtualResolution.X / 2 - storyboard.Texture.Width / 2,
                                urs.VirtualResolution.Y/2 - storyboard.Texture.Height/2,
                                storyboard.Texture.Width, 
                                storyboard.Texture.Height);

            this.storyboardLoading = new UITexture();
            this.storyboardLoading.Texture = cs["game"].Load<Texture2D>("UI/Storyboard_Loading");
            this.storyboardLoading.Rectangle = new Rectangle(urs.VirtualResolution.X / 2 - storyboardLoading.Texture.Width / 2,
                                urs.VirtualResolution.Y / 2 - storyboardLoading.Texture.Height / 2,
                                storyboardLoading.Texture.Width,
                                storyboardLoading.Texture.Height);


            this.home = initHome(initNewGame(), initOptions());
            this.HomeLayer.Add(this.gameLogo);
        }

        public UITreeLayer initHome(UITreeLayer newGame, UITreeLayer options)
        {
            // Create layer:
            UITreeLayer home = new UITreeLayer(this.atlas);

            // Create dialog:
            int dialogWidth = 350;
            int dialogHeight = 250;
            UIDialog window = new UIDialog(new Rectangle(urs.VirtualResolution.X/2 - dialogWidth/2, 
                                                        urs.VirtualResolution.Y/2, 
                                                        dialogWidth, 
                                                        dialogHeight));            

            DialogAutoAdjust a = window.DialogAutoAdjust;
            a.GapTop = -10;
            a.WidgetHeight = 40;
            a.GapWidget = 15;
            a.GapLeft = 35;
            a.GapRight = 35;
            window.DialogAutoAdjust = a;

            // Create the other widgets
            UIButton newGameButton = new UIButton(font, "New Game", Color.DarkRed);
            UIButton optionsButton = new UIButton(font, "Options", Color.DarkRed);
            UIButton exitButton = new UIButton(font, "Exit", Color.DarkRed);

            // Add the widgets to the layer:
            window.Add(newGameButton);
            window.Add(optionsButton);
            window.Add(exitButton);
            home.Add(window);

            // Create selector and links:
            UISelector selector = new UISelector();
            home.Selector = selector;
            selector.AddLink(newGameButton, optionsButton, InputAxis.LS_Y);
            selector.AddLink(newGameButton, optionsButton, InputAxis.RS_Y);
            selector.AddLink(newGameButton, optionsButton, InputAxis.D_Y);
            selector.AddLink(optionsButton, exitButton, InputAxis.LS_Y);
            selector.AddLink(optionsButton, exitButton, InputAxis.RS_Y);
            selector.AddLink(optionsButton, exitButton, InputAxis.D_Y);

            // Set up hierarchy:
            home.SetInvokerTarget(newGameButton, newGame, "A");
            home.SetInvokerTarget(optionsButton, options, "A");
            home.BackKey = "B";

            return home;
        }

        public UITreeLayer initNewGame()
        {            
            UITreeLayer layer = new UITreeLayer(atlas);
            layer.BackKey = "B";
            layer.Selector = new UISelector();

            slots[0].color = Color.Red;
            slots[1].color = Color.Blue;
            slots[2].color = Color.Green;
            slots[3].color = Color.Yellow;

            for (int i = 0; i < slots.Length; i++)
            {
                slots[i].label = new UILabel(joinString, font, slots[i].color);
                slots[i].label.Center(playerRectangle(i));
                layer.Add(slots[i].label);
            }

            this.newGame = layer;
            return layer;
        }

        private Rectangle playerRectangle(int i) { return playerRectangle((PlayerIndex)i); }
        private Rectangle playerRectangle(PlayerIndex player)
        {
            int i = (int)player;
            int w = urs.VirtualResolution.X / 2;
            int h = urs.VirtualResolution.Y / 2;

            int x = w * (i % 2);
            int y = h * (i / 2);

            return new Rectangle(x, y, w, h);
        }

        public UITreeLayer initOptions()
        {
            // Init layer:
            UITreeLayer layer = new UITreeLayer(atlas);
            layer.BackKey = "B";

            // Set up dialog:
            int dialogWidth = 350;
            int dialogHeight = 250;
            UIDialog window = new UIDialog(new Rectangle(urs.VirtualResolution.X / 2 - dialogWidth / 2,
                                                        urs.VirtualResolution.Y / 2,
                                                        dialogWidth,
                                                        dialogHeight));

            DialogAutoAdjust a = window.DialogAutoAdjust;
            a.GapTop = -35;
            a.WidgetHeight = 40;
            a.GapWidget = 15;
            a.GapLeft = 35;
            a.GapRight = 35;
            window.DialogAutoAdjust = a;

            // Add widgets:
            window.Add(new UILabel("Options", font, Color.Red));

            kinectMouseButton = new UIButton(font, "Kinect", Color.DarkRed);
            window.Add(kinectMouseButton);

            fullwindowButton = new UIButton(font, "Windowed", Color.DarkRed);
            window.Add(fullwindowButton);

            option_backButton = new UIButton(font, "Back", Color.DarkRed);
            window.Add(option_backButton);

            layer.Add(window);

            // Add links:
            layer.Selector = new UISelector();
            layer.Selector.AddLink(kinectMouseButton, fullwindowButton, InputAxis.LS_Y);
            layer.Selector.AddLink(kinectMouseButton, fullwindowButton, InputAxis.RS_Y);
            layer.Selector.AddLink(kinectMouseButton, fullwindowButton, InputAxis.D_Y);
            layer.Selector.AddLink(fullwindowButton, option_backButton, InputAxis.LS_Y);
            layer.Selector.AddLink(fullwindowButton, option_backButton, InputAxis.RS_Y);
            layer.Selector.AddLink(fullwindowButton, option_backButton, InputAxis.D_Y);

            this.options = layer;

            this.options.Add(gameLogo);
            return layer;
        }

        private void startGame()
        {
            object[] args = {PlayersPlaying};
            GameApplication.App.SetState("InGameState", args);
        }

        public bool IsShowingStory
        {
            private set;
            get;
        }

        private void listenTo(int i)
        {
            InputDevice d = input.GetDevice(i);

            if (d.IsClicked("Start"))
            {
                if (slots[i].playerIsPlaying)
                {
                    IsShowingStory = true;
                    newGame.Add(storyboard);
                }
                else
                    slots[i].playerIsPlaying = true;
            }
                
            else if (d.IsClicked("B"))
            {
                slots[i].playerIsPlaying = false;
            }
        }

        private void updateTextOf(int i)
        {
            slots[i].label.Text = slots[i].playerIsPlaying ? beginString : joinString;
        }

        public bool Update()
        {
            if (urs.CurrentLayer == this.home)
                return updateHome();

            else if (urs.CurrentLayer == this.newGame)
                updateNewGame();

            else if (urs.CurrentLayer == this.options)
                updateOptions();

            return true;
        }

        private bool updateHome()
        {
            InputDevice d = input.GetDevice(0);

            if (d.IsClicked("A"))
            {
                if (((UIButton)home.Selector.Selected).Label.Text == "Exit")
                {
                    return false;
                }
            }
            return true;
        }

        private void updateNewGame()
        {
            InputDevice d = input.GetDevice(0);
            if (!IsShowingStory)
            {
                for (int i = 0; i < 4; i++)
                {
                    listenTo(i);
                    updateTextOf(i);
                }
            }
            else
            {
                if (d.IsClicked("A"))
                {
                    IsShowingStory = false;
                    newGame.Remove(storyboard);
                    newGame.Add(storyboardLoading);
                    startGame();
                }
                else if (d.IsClicked("B"))
                {
                    // reset the new game screen
                    for (int i = 0; i < 4; i++)
                    {
                        slots[i].playerIsPlaying = false;
                    }
                    IsShowingStory = false;
                    newGame.Remove(storyboard);
                }
            }
        }

        private void updateOptions()
        {
            InputDevice d = input.GetDevice(0);
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();

            kinectMouseButton.Label.Text = GuardianBehaviour.UsingKinect ? "Kinect" : "Mouse";
            fullwindowButton.Label.Text = gs.GraphicsDevice.PresentationParameters.IsFullScreen ? "Fullscreen" : "Windowed";

            if (d.IsClicked("A"))
            {
                if (this.options.Selector.Selected == kinectMouseButton)
                {
                    GuardianBehaviour.UsingKinect = !GuardianBehaviour.UsingKinect;
                }

                if (this.options.Selector.Selected == option_backButton)
                {
                    urs.CurrentLayer = home;
                    d.Reset(); return;
                }
                if (this.options.Selector.Selected == fullwindowButton)
                {
                    bool full = gs.GraphicsDevice.PresentationParameters.IsFullScreen;
                    gs.ResetDevice(1280, 720, !full, true);
                }
            }
        }
    }
}
