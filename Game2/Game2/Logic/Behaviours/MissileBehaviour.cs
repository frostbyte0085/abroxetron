﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Game2.Logic.Missiles;

using Microsoft.Xna.Framework;

using BEPUphysics.Entities.Prefabs;

namespace Game2.Logic.Behaviours
{
    public class MissileBehaviour : SceneNodeBehaviour
    {
        #region Variables
        private Missile myMissile;

        public Unit HomingTarget { set; get; }
        public Vector3 Velocity { set; get; }

        private bool sentencedToDeath = false;
        #endregion

        #region Constructors
        public MissileBehaviour(Missile missile, Unit homeAt)
        {
            this.myMissile = missile;
            this.HomingTarget = homeAt;
            this.Velocity = Vector3.Zero;
        }
        public MissileBehaviour(Missile missile, Unit homeAt, Vector3 spawnAt)
            : this(missile, homeAt)
        {
            missile.SceneNode.Translation = spawnAt;
        }

        public MissileBehaviour(Missile missile, Vector3 velocity)
        {
            this.myMissile = missile;
            this.HomingTarget = null;
            this.Velocity = velocity;
        }
        public MissileBehaviour(Missile missile, Vector3 velocity, Vector3 spawnAt)
            : this(missile, velocity)
        {
            missile.SceneNode.Translation = spawnAt;
        }
        #endregion

        #region SceneNodeBehaviour overrides
        public override bool Start()
        {
            return true;
        }

        public override bool Update()
        {
            if (sentencedToDeath) return false;
            /*
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();
            if (SceneNode.PhysicalEntity.BEPUEntity is Sphere)
            {
                Sphere sph = SceneNode.PhysicalEntity.BEPUEntity as Sphere;
                drs.AddSphere(sph.Position, sph.Radius, Color.White);
            }*/
            
            if (HomingTarget == null)
            {
                // no homing
                float t = GameTiming.TimeElapsed;
                Vector3 s = SceneNode.Translation;
                s = s + Velocity * t;

                SceneNode.Translation = s;
            }
            else
            {
                // home at unit (todo)
            }

            if (!myMissile.Update(this)) return sentencedToDeath = true;

            return true;
        }

        public override void OnCollideEnter(SceneNode otherNode)
        {
            if (otherNode.CategoryGroup == "Guardian") return;
            if (sentencedToDeath) return; // This node doesn't exist from a game logic perspective anymore.

            // Do enter:
            object o = SceneNode["missile_source"];
            if (o != null && ((Unit)o).SceneNode == otherNode) return;

            // we have hit a unit
            o = otherNode["unit"];
            if (o != null)
            {
                myMissile.DetonateOn((Unit)o);
                myMissile.Explode();
            }

            // we have hit the level geometry
            if (otherNode.PhysicalEntity != null && otherNode.PhysicalEntity.EntityType == PhysicalEntityType.StaticMesh)
            {
                myMissile.Explode();
            }
        }

        public override void OnCollideStay(SceneNode otherNode)
        {
            if (otherNode.CategoryGroup == "Dragon")
                while (false) ;

            if (otherNode.CategoryGroup == "Guardian") return;
            if (sentencedToDeath)
            { // This node doesn't exist from a game logic perspective anymore. You're staying nowhere.
                OnCollideExit(otherNode);
                return;
            }

            // Do stay:
            object o = SceneNode["missile_source"];
            if (o != null && ((Unit)o).SceneNode == otherNode) return;

            o = otherNode["unit"];
            if (o != null)
            {
                myMissile.DetonateAgainOn((Unit)o);
            }
        }

        public override void OnCollideExit(SceneNode otherNode)
        {
            if (otherNode.CategoryGroup == "Guardian") return;

            // Do exit:
            object o = SceneNode["missile_source"];
            if (o != null && ((Unit)o).SceneNode == otherNode) return;

            o = otherNode["unit"];
            if (o != null)
            {
                myMissile.UndetonateOn((Unit)o);
            }
        }

        public override void Dispose()
        {
            return;
        }
        #endregion
    }
}
