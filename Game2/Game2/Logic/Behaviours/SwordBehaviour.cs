﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Game2.Logic.Missiles;

using Microsoft.Xna.Framework;

using Engine.Audio;
using BEPUphysics.Entities.Prefabs;

namespace Game2.Logic.Behaviours
{
    public class SwordBehaviour : SceneNodeBehaviour
    {
        ParticleAnimation __blood = null;
        ParticleAnimation blood
        {
            get
            {
                if (__blood == null)
                {
                    __blood = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/BloodSplatter.xml");
                }
                return __blood;
            }
        }
        private static long bloodId = 0;

        #region Variables
        SceneService ss;

        public Unit WhosSword { set; get; }
        public bool Active { set; get; }
        public Damage ActiveDamage { set; get; }
        public List<Unit> WhoGotHit { private set; get; }

        private string[] hitSounds = { "Sounds/sword1", "Sounds/sword2", "Sounds/sword3", "Sounds/sword4", "Sounds/sword5" };
        #endregion

        #region Constructors
        public SwordBehaviour(Unit whosSword, Damage dmg)
        {
            this.WhosSword = whosSword;
            this.ActiveDamage = dmg;
            this.WhoGotHit = new List<Unit>();
        }
        #endregion

        #region SceneNodeBehaviour overrides
        public override bool Start()
        {
            ss = ServiceProvider.GetService<SceneService>();

            this.Active = false;
            return true;
        }

        public override bool Update()
        {
            // Un-comment to view colliders:
            /*
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();
            if (SceneNode.PhysicalEntity.BEPUEntity is Box)
            {
                
                Box b = SceneNode.PhysicalEntity.BEPUEntity as Box;
                Vector3 x = new Vector3(b.HalfWidth, b.HalfHeight, b.HalfLength);
                Vector3 min = b.Position - x;
                Vector3 max = b.Position + x;
                drs.AddBox3D(min, max, Color.Red, SceneNode.Orientation, SceneNode.PhysicalEntity.Center);
            }
            */
            return true;
        }

        public override void OnCollideEnter(SceneNode otherNode)
        {
            if (otherNode.CategoryGroup == "Guardian")
                return; // can't hit a flesh-less being.

            if(!Active) return;
            object o = otherNode["unit"];
            if (o != null)
            {
                Unit u = (Unit)o;

                if (u != WhosSword && !WhoGotHit.Contains(u))
                {
                    //check to see if the skeleton sword is attacking the skeleton. we shouldn't allow that!
                    // we can't use collision groups for this, since the sword collider is a trigger. triggers override collision groups
                    // as the personal collision rule is set to NoSolver.
                    if (u.SceneNode.CategoryGroup == "Skeleton" && SceneNode.CategoryGroup == "SkeletonSkimitar")
                        return;

                    // the same applies for the wizard and his sword, don't attack other wizards!
                    if (u.SceneNode.CategoryGroup == "Wizard" && SceneNode.CategoryGroup == "WizardSword")
                        return;

                    SceneNode.AudioSource = AudioSource.RandomFromSoundNames(hitSounds, true);
                    
                    u.TakeDamage(ActiveDamage);

                    // Wizards bleeeeeed:
                    if (u.SceneNode.CategoryGroup == "Wizard")
                    {
                        SceneNode bloodNode = ss.AddSceneNode("bloodSplatter" + (bloodId++));
                        bloodNode.Translation = u.SceneNode.Translation;
                        bloodNode.Translate(0, 0.7f, 0);

                        bloodNode.ParticleAttachMode = ParticleAttachMode.DiscardOnCompletion;

                        ParticleSystem ps = new ParticleSystem(blood);
                        bloodNode.Attach("node", ps);
                        ps.StartEmitting();
                    }

                    WhoGotHit.Add(u);
                }
            }
        }

        public override void OnCollideStay(SceneNode otherNode) { }

        public override void OnCollideExit(SceneNode otherNode) { }

        public override void Dispose()
        {
            return;
        }
        #endregion
    }
}
