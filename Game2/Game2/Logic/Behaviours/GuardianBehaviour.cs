﻿#if WINDOWS
#define USE_KINECT
#endif

using Engine.Animation;
using Engine.Scene;
using Engine.Core;
using Engine.Graphics;
using Engine.Input;
using Engine.Audio;
using Engine.Physics;
using BEPUphysics.CollisionRuleManagement;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

using Game2.Logic.Buffs;

#if USE_KINECT
using Microsoft.Kinect;
#endif
using Microsoft.Xna.Framework.Input;

using Game2.Logic.Commands;
using Game2.GameManager;    

namespace Game2.Logic.Behaviours
{
    public class GuardianBehaviour : CharacterSceneNodeBehaviour
    {
#if USE_KINECT
        private static bool usingKinect = true;
#else
        private static bool usingKinect = false;
#endif  
        public static bool UsingKinect { set { usingKinect = value; } get { return usingKinect; } }

        private const float closeEnoughThreshold = 10f;

        // Engine vars:
        private ContentService cs;
        private SceneService ss;
        private UIRendererService urs;
        private GraphicsService gs;

        // Game Logic vars:
        public Unit Guardian {private set; get;}

        // Commands this behaviour can issue:
        private MoveWithVelocity2 move;
        private EmitLight lightSpell;
        private CastFreeze freezeSpell;
        private CastRevive revive;
        
        public Point CursorLocation { private set; get; }

        public GuardianBehaviour(Unit whichUnit)
        {
            this.urs = ServiceProvider.GetService<UIRendererService>();
            this.cs = ServiceProvider.GetService<ContentService>();
            this.ss = ServiceProvider.GetService<SceneService>();
            this.gs = ServiceProvider.GetService<GraphicsService>();

            InitInput();

            this.Guardian = whichUnit;
            this.Guardian.DeathCommand = new DieDeactivate();

            this.move = new MoveWithVelocity2(this);
            this.move.AnimationWalkName = null;
            this.MaxSpeed = 1f;

            this.lightSpell = new EmitLight();
            this.freezeSpell = new CastFreeze();
            this.revive = new CastRevive();
        }

        public override bool Start()
        {
            this.SceneNode.CategoryGroup = "Guardian";

            // Init model:
            ContentService cs = ServiceProvider.GetService<ContentService>();
            this.SceneNode.Model = cs["game"].Load<Model>("Models/Wizard/Wizard");
            this.SceneNode.TintColor = Color.White.ToVector3();
            this.SceneNode.TintAmount = 1f;
            this.SceneNode.EmissiveAdditive = 0.02f;
            this.SceneNode.Animation.StartClip("float", AnimationPlay.Loop);

            // Do base physics init.
            CreateCharacterController(1.3f, 20);
            MaxSlope = 20;

            // Init collision grouping for this node:
            CollisionGroup grp = CustomCollisionGroups.Get("Guardian");

            // Init physics for this scene node:
            this.SceneNode.PhysicalEntity.IsAffectedByGravity = true;
            this.SceneNode.PhysicalEntity.CollisionGroup = grp;

            // Make him invulnerable:
            this.Guardian.TakeBuff(new Invulnerable());

            return base.Start();
        }

        public override bool Update()
        {
            UpdateCursorLocation();
            
            // Process move:
            Vector3 myScreenPos = ss.Project(SceneNode.Translation);
            float dx = CursorLocation.X - myScreenPos.X;
            float dy = CursorLocation.Y - myScreenPos.Y;

            Vector2 direction = new Vector2(dx,dy);
            if (direction.Length() < closeEnoughThreshold) direction = Vector2.Zero;
            else  direction.Normalize();

            this.move.InputDirection = direction;
            this.move.Speed = 3.0f;
            Guardian.IssueCommand(move);

            // Process Spell 1 (emit light)
            if (Spell1Triggered())
            {
                Guardian.IssueCommand(lightSpell);
                lightSpell.FadeOut(); // ignored if fading in or out.
            }

            if (Spell2Triggered())
            {
                Guardian.IssueCommand(freezeSpell);
            }

            if (Spell3Triggered())
            {
                Guardian.IssueCommand(revive);
            }

            // Update this unit:
            Guardian.Update();
            return base.Update();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        #region Input handling
        private void InitInput()
        {
            if (UsingKinect) 
                InitInput_kinect();
            else
                InitInput_mouse();
        }

        private bool Spell1Triggered()
        {
            return UsingKinect ? Spell1Triggered_kinect() : Spell1Triggered_mouse();
        }

        private bool Spell2Triggered()
        {
            return UsingKinect ? Spell2Triggered_kinect() : Spell2Triggered_mouse();
        }

        private bool Spell3Triggered()
        {
            return UsingKinect ? Spell3Triggered_kinect() : Spell3Triggered_mouse();
        }

        private void UpdateCursorLocation()
        {
            if (UsingKinect)
                UpdateCursorLocation_kinect();
            else
                UpdateCursorLocation_mouse();
        }
        #endregion

        #region Kinect Input handling
#if USE_KINECT
        private KinectDevice kinect;

        /// <summary>
        /// Defines Kinect gestures.
        /// </summary>
        private void InitInput_kinect()
        {
            this.kinect = ServiceProvider.GetService<InputService>().GetKinect();

            // Spell 1 (Emit Light) Gesture Definition
            KinectGesture Spell1 = new KinectGesture(new JointMovement[]
            {
                new JointMovement() 
                {
                    Direction = JointMovement.North,
                    JointType = JointType.HandRight
                },
                new JointMovement() 
                {
                    Direction = JointMovement.South,
                    JointType = JointType.HandRight
                }
            });

            // Spell 2 (Freeze) Gesture Definition
            KinectGesture Spell2 = new KinectGesture(new JointMovement[]
            {
                new JointMovement()
                {
                    Direction = JointMovement.East,
                    JointType = JointType.HandRight
                },
                new JointMovement()
                {
                    Direction = JointMovement.West,
                    JointType = JointType.HandRight
                }
            });

            // Spell 3 (Revive) Gesture Definition:
            KinectGesture Spell3 = new KinectGesture(new JointMovement[]
            {
                new JointMovement() 
                {
                    Direction = JointMovement.South,
                    JointType = JointType.HandRight
                },
                new JointMovement() 
                {
                    Direction = JointMovement.East,
                    JointType = JointType.HandRight
                }
            });

            this.kinect.AddAction("spell1", Spell1);
            this.kinect.AddAction("spell2", Spell2);
            this.kinect.AddAction("spell3", Spell3);
        }

        private bool Spell1Triggered_kinect()
        {
            return this.kinect.IsTriggered("spell1");
        }

        private bool Spell2Triggered_kinect()
        {
            return this.kinect.IsTriggered("spell2");
        }

        private bool Spell3Triggered_kinect()
        {
            return this.kinect.IsTriggered("spell3");
        }

        private void UpdateCursorLocation_kinect()
        {
            if (kinect.PlayerSkeleton != null)
            {
                // The left hand has a large blind spot. It cannot access every point
                // on the screen:
                // The *1.45 is used to make to kinect cursor more sensitive.
                Joint j = kinect.PlayerSkeleton.Joints[JointType.HandLeft];
                CursorLocation = KinectDevice.GetScreenPoint(j,
                    (int)(gs.GraphicsDevice.Viewport.Width * 1.45f +0.5f),
                    (int)(gs.GraphicsDevice.Viewport.Height * 1.45f + 0.5f));
            }
        }
#endif
        #endregion

        #region Mouse Input handling
        private bool mouseLeftPressed;
        private bool mouseRightPressed;
        private bool mouseMiddlePressed;

        private void InitInput_mouse()
        {
            mouseLeftPressed = false;
            mouseRightPressed = false;
            mouseMiddlePressed = false;
        }

        private bool Spell1Triggered_mouse()
        {
            MouseState mouse = Mouse.GetState();

            bool pressed1 = mouseLeftPressed;
            bool pressed2 = mouse.LeftButton == ButtonState.Pressed;

            mouseLeftPressed = pressed2;

            return pressed1 && !pressed2;
        }

        private bool Spell2Triggered_mouse()
        {
            MouseState mouse = Mouse.GetState();

            bool pressed1 = mouseRightPressed;
            bool pressed2 = mouse.RightButton == ButtonState.Pressed;

            mouseRightPressed = pressed2;

            return pressed1 && !pressed2;
        }

        private bool Spell3Triggered_mouse()
        {
            MouseState mouse = Mouse.GetState();

            bool pressed1 = mouseMiddlePressed;
            bool pressed2 = mouse.MiddleButton == ButtonState.Pressed;

            mouseMiddlePressed = pressed2;

            return pressed1 && !pressed2;
        }

        private void UpdateCursorLocation_mouse()
        {
            MouseState mouse = Mouse.GetState();
            CursorLocation = new Point(mouse.X, mouse.Y);
        }
        #endregion
    }
}
