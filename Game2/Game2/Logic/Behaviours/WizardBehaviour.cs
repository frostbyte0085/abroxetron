﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Animation;
using Engine.Core;
using Engine.Tools;
using Engine.Graphics;
using Engine.Input;
using Engine.Physics;
using Engine.Audio;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


using BEPUphysics;
using BEPUphysics.Threading;
using BEPUphysics.Settings;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;

using System.Diagnostics;

using Game2.Logic.Buffs;
using Game2.Logic.Commands;
using Game2.GameManager;

namespace Game2.Logic.Behaviours
{
    public class WizardBehaviour : CharacterSceneNodeBehaviour
    {
        // Engine vars:
        private ContentService cs;
        private SceneService ss;
        private InputDevice input;

        // Game Logic vars:
        private Unit myWizard;

        // Commands this behaviour can issue:
        private MoveWithVelocity2 move;
        private Attack attack;
        private CastFire castFire;
        private CastWater castWater;
        private CastAir castAir;
        private CastEarth castEarth;
        private Dance dance;

        private PlayerIndex whichPlayer;

        public WizardBehaviour(Unit whichUnit, PlayerIndex whichPlayer)
        {
            this.whichPlayer = whichPlayer;

            this.input = ServiceProvider.GetService<InputService>().GetDevice(whichPlayer);
            this.cs = ServiceProvider.GetService<ContentService>();
            this.ss = ServiceProvider.GetService<SceneService>();

            this.input.AddAction("LS_X", InputButton.LS_X);
            this.input.AddAction("LS_Y", InputButton.LS_Y);

            // Init idle:
            this.myWizard = whichUnit;
            this.myWizard.IdleAnimations = new AnimationDiceRoller();
            this.myWizard.IdleAnimations.AnimationNames = new string[]{"idle1"};//, "idle2"};
            //this.myWizard.IdleAnimationsProbabilities = new float[]{0.8f, 0.2f};

            // Init Move:
            this.move = new MoveWithVelocity2(this);
            this.move.AnimationWalkName = "walk";
            this.move.AnimationWalkSpeed = 0.7f;

            // Init castings:
            this.castFire = new CastFire();
            this.castWater = new CastWater();
            this.castAir = new CastAir();
            this.castEarth = new CastEarth();

            // Init the easter egg :)
            this.dance = new Dance();
        }

        public override bool Start()
        {
            // Init Model:
            SceneNode.Model = cs["game"].Load<Model>("Models/Wizard/Wizard");
            SceneNode.CategoryGroup = "Wizard";
            SceneNode.MeshColor = myWizard.Color.ToVector3();

            // Init death:
            this.myWizard.DeathCommand = new DieDeactivate();
            this.myWizard.HitSoundNames
                = new string[] { "Sounds/playerGrunt1", "Sounds/playerGrunt2", "Sounds/playerGrunt3", "Sounds/playerGrunt4" };
            this.myWizard.DieSoundNames = new string[] { "Sounds/playerDie1" };
            this.SceneNode.Animation["death"].Speed = 1.2f;

            // Init physics:
            CreateCharacterController(1.3f, 20);
            MaxSlope = 20;
            this.SceneNode.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("Wizard");

            // Init sword:
            Damage dmg = new Damage(10, DamageType.Physical, true);
            SceneNode sword = this.ss.AddSceneNode("wizard_sword" + whichPlayer);
            SwordBehaviour swordBehave = new SwordBehaviour(myWizard, dmg);
            sword.Behaviour = swordBehave;
            sword.Model = cs["game"].Load<Model>("Models/Wizard/Sword");
            sword.PhysicalEntity = new PhysicalEntity(sword, new PhysicalBoxEntityProperties(), true);
            Box swordEntity = sword.PhysicalEntity.BEPUEntity as Box;
            swordEntity.Width *= 2;
            swordEntity.Height *= 2;
            swordEntity.Length *= 2;

            sword.CategoryGroup = "WizardSword";

            // Init attack:
            this.attack = new Attack(swordBehave);
            this.attack.AttackAnimations = new AnimationDiceRoller();
            this.attack.AttackAnimations.AnimationNames = new string[] { "melee1" };//, "melee2" };
            this.attack.SwordSwingSounds = new string[] { "Sounds/qubodup_swosh1", "Sounds/qubodup_swosh" };
            this.SceneNode.Animation["melee1"].Speed = 2.5f;
            this.SceneNode.Attach("Sword1", sword);

            // Init lantern:
            SceneNode lantern = this.ss.AddSceneNode("wizard_lantern" + whichPlayer);

            lantern.Model = cs["game"].Load<Model>("Models/Wizard/Lantern");
            ParticleAnimation pa = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/LanternLight.xml");
            ParticleSystem ps = new ParticleSystem(pa);
            ps.ParticleSystemSpaceMode = ParticleSystemSpaceMode.LocalSpace;
            lantern.Attach("Fire", ps);
            lantern.Light = new PointLight(Color.LightYellow.ToVector4(), 2f, 3); //new PointLight(new Vector4(1, 0.95f, 0.73f,1), 1.0f, 3);
            this.SceneNode.Attach("Lantern1", lantern);
            ps.StartEmitting();

            // Give wizards magic resist:
            this.myWizard.TakeBuff(new MagicResist());

            SceneNode.Animation.StartClip("idle1", Engine.Animation.AnimationPlay.Once);

            return base.Start();
        }

        private Cast GetCast(SpellType spell)
        {
            switch (spell)
            {
                case SpellType.Fire: return castFire;
                case SpellType.Water: return castWater;
                case SpellType.Air: return castAir;
                case SpellType.Earth: return castEarth;
                default: return null;
            }
        }


        private float lastFootstepTime = 0.0f;

        public override bool Update()
        {
            SceneNode footstepNode = ss["footsteps_" + whichPlayer];

            if (footstepNode == null)
            {
                footstepNode = ss.AddSceneNode("footsteps_" + whichPlayer);
                footstepNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/footstep"));
                footstepNode.AudioSource.Volume = 0.5f;
            }

            footstepNode.Translation = SceneNode.Translation;
            // Detect movement:
            move.InputDirection = new Vector2(input.GetValue("LS_X"), -input.GetValue("LS_Y"));
            move.Speed = 4;
            myWizard.IssueCommand(move);

            float moveMagn = move.InputDirection.Length();
            if (moveMagn > 0.1f)
            {
                lastFootstepTime += GameTiming.TimeElapsed;
                if (lastFootstepTime > 0.5f * 1 / moveMagn) // replace with walk animation length in seconds times speed here.
                {
                    footstepNode.AudioSource.Play();
                    lastFootstepTime = 0.0f;
                }
            }

            // Detect spell selection
            if (input.IsClicked("LB"))
            {
                BaseGameManager gm = GameApplication.App.GameManager;
                ISpell selected = myWizard.SelectedSpell;
                SpellType selectedType = selected == null? SpellType.NONE : selected.GetSpellType();

                myWizard.SelectedSpell = GetCast(gm.GetNextSpell(selectedType, whichPlayer));
            }
            if (input.IsClicked("RB"))
            {
                BaseGameManager gm = GameApplication.App.GameManager;
                ISpell selected = myWizard.SelectedSpell;
                SpellType selectedType = selected == null ? SpellType.NONE : selected.GetSpellType();

                myWizard.SelectedSpell = GetCast(gm.GetPreviousSpell(selectedType, whichPlayer));
            }

            // Detect spell cast:
            if (input.IsClicked("Y"))
            {
                ISpell spell = myWizard.SelectedSpell;
                if (spell != null)
                    myWizard.IssueCommand(GetCast(spell.GetSpellType()));
            }

            // Detect attack:
            if (input.IsClicked("A"))
            {
                myWizard.IssueCommand(this.attack);
            }

            if (input.IsClicked("X"))
            {
                myWizard.IssueCommand(this.dance);
            }

            // Update the unit (but update the engine first)
            bool b = base.Update();
            myWizard.Update();
            return b;
        }

        /*private float deadAt;
        public override void OnInactived()
        {
            deadAt = GameTiming.TotalTimeElapsed;
        }

        public override bool InactiveUpdate()
        {
            if (deadAt + 1 <= GameTiming.TotalTimeElapsed)
            {
                BaseGameManager gm = GameApplication.App.GameManager;
                this.myWizard.SceneNode.Translation = gm.KinectPlayer.Guardian.SceneNode.Translation;
                this.myWizard.SceneNode.Active = true;
                this.myWizard.UnitProperties.CurrentHP = this.myWizard.UnitProperties.MaxHP;
                this.myWizard.UnitProperties.CurrentMana = 0;
            }
            return true;
        }*/

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
