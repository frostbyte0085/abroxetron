﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Animation;
using Engine.Core;
using Engine.Tools;
using Engine.Graphics;
using Engine.Input;
using Engine.Physics;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using System.Diagnostics;

using Game2.Logic.Commands;
using Game2.GameManager;

namespace Game2.Logic.Behaviours
{
    public class SkeletonBehaviour : CharacterSceneNodeBehaviour
    {
        private const float attackDistance = 1.6f;

        // Engine vars:
        private ContentService cs;
        private SceneService ss;

        // Game Logic vars:
        private Unit skeleton;
        private List<Vector2> path;
        private BaseGameManager gm;

        // Commands this behaviour can issue:
        private MoveTowardsPoint move;
        private Attack attack;
        // NOTE: The spawn cmd is issued by the EnemyManager

        private long skeletonId;

        public SkeletonBehaviour(Unit whichUnit, long skeletongId)
        {
            this.skeletonId = skeletongId;

            this.cs = ServiceProvider.GetService<ContentService>();
            this.ss = ServiceProvider.GetService<SceneService>();

            this.skeleton = whichUnit;
            this.skeleton.DeathCommand = new DieRemove();

            this.skeleton.HitSoundNames = new string[] { "Sounds/skeletonGrunt1", "Sounds/skeletonGrunt2", "Sounds/skeletonGrunt3" };
            this.skeleton.DieSoundNames = new string[] { "Sounds/skeletonDie1", "Sounds/skeletonDie2" };

            this.skeleton.IdleAnimations = Unit.IDLE_PAUSE_ANIMATION;
        }

        public override bool Start()
        {
            gm = GameApplication.App.GameManager;

            SceneNode.Model = cs["game"].Load<Model>("Models/Skeleton/SkeletonWarrior");
            SceneNode.CategoryGroup = "Skeleton";
            SceneNode.MeshColor = Color.White.ToVector3();
            SceneNode["unit"] = this.skeleton;

            CreateCharacterController(1.3f, 20);
            this.SceneNode.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("Skeleton");

            // Init movement command:
            this.move = new MoveTowardsPoint(this);
            this.move.AnimationWalkName = "walk";
            this.move.AnimationWalkSpeed = 1;
            this.move.CloseEnough = 0.1f;
            this.move.Speed = 3f;
            MaxSlope = 20;

            // Add the sword:
            Damage dmg = new Damage(10, DamageType.Physical);
            SceneNode sword = this.ss.AddSceneNode("skeleton_sword" + skeletonId);
            SwordBehaviour swordBehave = new SwordBehaviour(skeleton, dmg);
            sword.Behaviour = swordBehave;
            sword.Model = cs["game"].Load<Model>("Models/Skeleton/Scimitar");
            sword.PhysicalEntity = new PhysicalEntity(sword, new PhysicalBoxEntityProperties(), true);
            sword.CategoryGroup = "SkeletonSkimitar";

            // Init attack command:
            this.attack = new Attack(swordBehave);
            this.attack.AttackAnimations = new AnimationDiceRoller();
            this.attack.AttackAnimations.AnimationNames = new string[] { "attack" };
            this.SceneNode.Animation["attack"].Speed = 2.2f;
            this.SceneNode.Attach("Sword1", sword);
            this.attack.SwordSwingSounds = new string[] { "Sounds/qubodup_swosh1", "Sounds/qubodup_swosh" };
            
            // Init damage command
            DamageKnockBack dkb = new DamageKnockBack();
            dkb.Speed = 2.0f;
            dkb.DamageAnimation = "damage";
            this.skeleton.DamageCommand = dkb;

            path = new List<Vector2>();

            return base.Start();
        }

        #region Pathfinding
        /// <summary>
        /// Make the path-find query.
        /// </summary>
        private void doPathFind()
        {
            // Find the closest (wizard) player:
            Vector3 gohere3 = Vector3.Zero; 
            float distance = float.MaxValue;
            IEnumerator<Unit> wizards = gm.Players;
            while (wizards.MoveNext())
            {
                if (wizards.Current.IsDeadOrDying) continue;

                Vector3 wizardPos = wizards.Current.SceneNode.Translation;
                float thisDistance  = (wizardPos - this.SceneNode.Translation).Length();

                if (thisDistance < distance)
                {
                    distance = thisDistance;
                    gohere3 = wizardPos;
                }
            }

            // Go to that wizard's position to slaughter him!
            Vector2 gohere2 = new Vector2(gohere3.X, gohere3.Z);
            Vector2 here = new Vector2(SceneNode.Translation.X, SceneNode.Translation.Z);
            this.PathFind(here, gohere2);
        }

        private float wait = 0.0f;
        /// <summary>
        /// Make the path-find query (if not already asked).
        /// </summary>
        private void doPathFindQuery()
        {
            if (WaitingForPathFind) return;

            wait += GameTiming.TimeElapsed;

            bool playerMovingAway = false;
            bool foundPlayer = false;
            // find the closest wizard
            float distance = float.MaxValue;
            Vector3 wizardPos = Vector3.Zero;
            IEnumerator<Unit> wizards = gm.Players;


            while (wizards.MoveNext())
            {
                wizardPos = wizards.Current.SceneNode.Translation;
                float skeleton_player_distance = (wizardPos - this.SceneNode.Translation).Length();
                if (skeleton_player_distance < distance)
                {
                    distance = skeleton_player_distance;
                    foundPlayer = true;
                }
            }

            if (foundPlayer)
            {
                float skeleton_wizard_dot = Vector3.Dot(Vector3.Normalize(SceneNode.Forward), Vector3.Normalize(wizardPos - SceneNode.Translation));
                if (skeleton_wizard_dot < 0.5f)
                {
                    playerMovingAway = true;
                }

                //Console.WriteLine("Dot is: " + skeleton_wizard_dot);
            }

            if ( (wait > 2) || playerMovingAway)
            {
                //Console.WriteLine("Pathfinding..." + count++);
                wait -= (int)wait;
                doPathFind();
            }
        }

        /// <summary>
        /// Respond to the path-find query.
        /// </summary>
        /// <param name="newPath"></param>
        public override void OnPathFindUpdated(List<Vector2> newPath) 
        {
            this.path = newPath;
        }
        #endregion


        #region Command issueing
        private void doMove()
        {
            // clear reached points:
            this.move.Puppet = skeleton;
            while (path.Count > 0)
            {
                this.move.Target = path[0];
                if (move.Reached()) path.RemoveAt(0);
                else break;
            }

            if (path.Count > 0)
            {
                this.move.Target = path[0];
                this.skeleton.IssueCommand(move);
            }
        }

        private void doAttack()
        {
            float distance = attackDistance;
            Unit attackHim = null;
            IEnumerator<Unit> i = gm.Players;

            // Get the closest player:
            while (i.MoveNext())
            {
                if (i.Current.IsDeadOrDying) continue;

                Vector3 diff = i.Current.SceneNode.Translation - this.SceneNode.Translation;
                float len = diff.Length();
                if (len < distance)
                {
                    distance = len;
                    attackHim = i.Current;
                }
            }

            // Is there a player close enough?
            if (attackHim != null)
            {
                // ATT-AAAAACCCCKKKK!!!
                if (this.skeleton.IssueCommand(attack))
                {
                    // Face him (only if the command was successfully issued
                    Vector3 facingDir = attackHim.SceneNode.Translation - this.SceneNode.Translation;
                    float yaw = (float)Math.Atan2(facingDir.X, facingDir.Z);
                    this.SceneNode.Orientation = Quaternion.CreateFromYawPitchRoll(yaw, 0, 0);
                }
            }
        }
        #endregion
        public override bool Update()
        {
            if (!skeleton.IsDeadOrDying)
            {
                doMove();
                doAttack();
                doPathFindQuery();
            }
            // Update the unit (but update the engine first)
            bool b = base.Update();
            skeleton.Update();
            return b;
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}
