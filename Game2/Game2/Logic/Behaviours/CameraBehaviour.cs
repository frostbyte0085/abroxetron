﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Game2;
using Game2.GameManager;

using Engine.Scene;
using Engine.Tools;
using Engine.Core;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Behaviours
{
    public class CameraBehaviour : SceneNodeBehaviour
    {
        /// <summary>
        /// Constant height of the camera from above the ground
        /// </summary>
        public static float HEIGHT = 13;
        /// <summary>
        /// Constant distance of the camera form the players'
        /// average position.
        /// </summary>
        public static Vector2 DISTANCE_FROM_PLAYERS = new Vector2(0,12);

        public static float LERP_FACTOR = 0.99f;

        private BaseGameManager gm;

        public override bool Start()
        {
            gm = GameApplication.App.GameManager;

            Vector3 goHere = gm.AveragePosition();
            LookingAt = new Vector2(goHere.X, goHere.Z);

            this.SceneNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-45), 0);

            return true;
        }

        public override bool Update()
        {
            if (GameTiming.Paused) return true;

            Vector3 avgPos = gm.AveragePosition();

            Vector2 newLookingAt = new Vector2(avgPos.X, avgPos.Z);
            LookingAt = Vector2.Lerp(newLookingAt, lookingAt, LERP_FACTOR);

            return true;
        }

        private Vector2 lookingAt = Vector2.Zero;
        public Vector2 LookingAt
        {
            set
            {
                Vector2 newLookingAt = value + DISTANCE_FROM_PLAYERS;
                SceneNode.Translation = new Vector3(newLookingAt.X, HEIGHT, newLookingAt.Y);
                lookingAt = value;
            }
            get { return lookingAt; }
        }

        public override void Dispose()
        {
        }
    }
}
