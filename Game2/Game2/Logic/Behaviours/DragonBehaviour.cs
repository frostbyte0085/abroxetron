﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Animation;
using Engine.Scene;
using Engine.Core;
using Engine.Graphics;
using Engine.Input;
using Engine.Audio;
using Engine.Physics;
using BEPUphysics.CollisionRuleManagement;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

using Game2.Logic.Buffs;
using Game2.Logic.Commands;
using Game2.GameManager;

using Engine.Tools;

namespace Game2.Logic.Behaviours
{
    public class DragonBehaviour : CharacterSceneNodeBehaviour
    {
        private const float meleeDistance = 2.5f;
        private const float breathDistance = 4.5f;

        private ContentService cs;
        private SceneService ss;
        private BaseGameManager gm;
        private Vector3 spawnPoint;
        private Unit dragon;

        private List<Vector2> path;
        // let's have some commands here
        private MoveTowardsPoint move;
        private DragonAttack melee;
        private DragonFireBreath breath;

        // blood stuff
        ParticleAnimation __blood = null;
        ParticleAnimation blood
        {
            get
            {
                if (__blood == null)
                {
                    __blood = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/BloodSplatter.xml");
                }
                return __blood;
            }
        }
        private static long bloodId = 0;

        public DragonBehaviour(Vector3 spawnPoint)
        {
            this.spawnPoint = spawnPoint;
        }

        public override bool Start()
        {
            dragonReady = false;

            path = new List<Vector2>();

            gm = GameApplication.App.GameManager;
            cs = ServiceProvider.GetService<ContentService>();
            ss = ServiceProvider.GetService<SceneService>();
            
            SceneNode.Model = cs["game"].Load<Model>("Models/Drangonus/Dragon");
            SceneNode.CategoryGroup = "Dragon";
            SceneNode.Translation = spawnPoint;
            SceneNode.Animation.StartClip("statue", Engine.Animation.AnimationPlay.Loop);

            dragon = new Unit(SceneNode, Color.White, UnitProperties.Dragon());
            
            SceneNode["unit"] = dragon;

            CreateCharacterController(SceneNode.Translation, 1.18f, 1, 1.3f, 20);
            this.SceneNode.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("Dragon");
            MaxSlope = 20;
            
            // COMMANDS
            // move
            move = new MoveTowardsPoint(this);
            move.AnimationWalkName = "walk";
            move.AnimationWalkSpeed = 1;
            move.CloseEnough = 0.5f;
            move.Speed = 3f;

            // melee attack
            melee = new DragonAttack();
            melee.AttackAnimations = new AnimationDiceRoller();
            melee.AttackAnimations.AnimationNames = new string[] { "attack" };
            melee.Damage = new Damage(5 + gm.PlayersAsNodes.Count * 2, DamageType.Physical, false);

            // fire breath
            breath = new DragonFireBreath();
            breath.BreathAnimations = new AnimationDiceRoller();
            breath.BreathAnimations.AnimationNames = new string[] { "fireBreath" };
            breath.Damage = new Damage(10 + gm.PlayersAsNodes.Count * 4, DamageType.Physical, true);

            // death
            SceneNode.Animation["death"].Speed = 0.3f;
            dragon.DeathCommand = new DieNoRemove();
            dragon.DieSoundNames = new string[] { "Sounds/dragonDie" };

            wakeupTimePassed = 0.0f;
            previousState = gm.IsDragonAwake;
            dragonReady = false;


            // adjust his HP based on the player count
            dragon.UnitProperties.BaseMaxHP *= gm.PlayersAsNodes.Count;
            dragon.UnitProperties.CurrentHP = dragon.UnitProperties.MaxHP;

            return base.Start();
        }

        public Unit DragonUnit
        {
            get
            {
                return dragon;
            }
        }

        #region Path finding

        /// <summary>
        /// Make the path-find query.
        /// </summary>
        private void doPathFind()
        {
            // Find the closest (wizard) player:
            Vector3 gohere3 = Vector3.Zero;
            float distance = float.MaxValue;
            IEnumerator<Unit> wizards = gm.Players;
            while (wizards.MoveNext())
            {
                if (wizards.Current.IsDeadOrDying) continue;

                Vector3 wizardPos = wizards.Current.SceneNode.Translation;
                float thisDistance = (wizardPos - this.SceneNode.Translation).Length();

                if (thisDistance < distance)
                {
                    distance = thisDistance;
                    gohere3 = wizardPos;
                }
            }

            // Go to that wizard's position to slaughter him!
            Vector2 gohere2 = new Vector2(gohere3.X, gohere3.Z);
            Vector2 here = new Vector2(SceneNode.Translation.X, SceneNode.Translation.Z);
            this.PathFind(here, gohere2);
        }

        private float wait = 0.0f;
        /// <summary>
        /// Make the path-find query (if not already asked).
        /// </summary>
        private void doPathFindQuery()
        {
            if (WaitingForPathFind) return;

            wait += GameTiming.TimeElapsed;

            bool playerMovingAway = false;
            bool foundPlayer = false;
            // find the closest wizard
            float distance = float.MaxValue;
            Vector3 wizardPos = Vector3.Zero;
            IEnumerator<Unit> wizards = gm.Players;
            
            while (wizards.MoveNext())
            {
                wizardPos = wizards.Current.SceneNode.Translation;
                float skeleton_player_distance = (wizardPos - this.SceneNode.Translation).Length();
                if (skeleton_player_distance < distance)
                {
                    distance = skeleton_player_distance;
                    foundPlayer = true;
                }
            }

            if (foundPlayer)
            {
                float skeleton_wizard_dot = Vector3.Dot(Vector3.Normalize(SceneNode.Forward), Vector3.Normalize(wizardPos - SceneNode.Translation));
                if (skeleton_wizard_dot < 0.5f)
                {
                    playerMovingAway = true;
                }
            }

            if ((wait > 2) || playerMovingAway)
            {
                wait -= (int)wait;
                doPathFind();
            }
        }

        public override void OnPathFindUpdated(List<Vector2> newPath)
        {
            this.path = newPath;
        }

        #endregion

        #region Commands issuing
        private void doMove()
        {
            // clear reached points:
            move.Puppet = dragon;
            while (path.Count > 0)
            {
                move.Target = path[0];
                if (move.Reached()) path.RemoveAt(0);
                else break;
            }

            if (path.Count > 0)
            {
                move.Target = path[0];
                dragon.IssueCommand(move);
            }
        }

        private void doFireBreath()
        {
            float distance = breathDistance;
            Unit attackHim = null;

            IEnumerator<Unit> i = gm.Players;

            // Get the closest player:
            while (i.MoveNext())
            {
                if (i.Current.IsDeadOrDying) continue;

                Vector3 diff = i.Current.SceneNode.Translation - this.SceneNode.Translation;
                float len = diff.Length();
                if (len < distance)
                {
                    distance = len;
                    attackHim = i.Current;
                }
            }

            if (attackHim != null)
            {
                breath.Target = attackHim;
                dragon.IssueCommand(breath);
            }
        }

        private void doMelee()
        {
            float distance = meleeDistance;
            Unit attackHim = null;
            IEnumerator<Unit> i = gm.Players;

            // Get the closest player:
            while (i.MoveNext())
            {
                if (i.Current.IsDeadOrDying) continue;

                Vector3 diff = i.Current.SceneNode.Translation - this.SceneNode.Translation;
                float len = diff.Length();
                if (len < distance)
                {
                    distance = len;
                    attackHim = i.Current;
                }
            }

            // Is there a player close enough?
            if (attackHim != null)
            {
                // charge, dragonnnnnn!
                melee.Target = attackHim;
                if (dragon.IssueCommand(melee))
                {
                    // bleed the wizard!
                    SceneNode bloodNode = ss.AddSceneNode("bloodSplatter_FromDragon" + (bloodId++));
                    bloodNode.Translation = attackHim.SceneNode.Translation;
                    bloodNode.Translate(0, 0.7f, 0);

                    bloodNode.ParticleAttachMode = ParticleAttachMode.DiscardOnCompletion;

                    ParticleSystem ps = new ParticleSystem(blood);
                    bloodNode.Attach("node", ps);
                    ps.StartEmitting();
                }
            }
        }

        private void doSelectAttack()
        {
            float random = Random2.RandomF(0.0f, 1.0f);
            if (random >= 0.0f && random < 0.95f)
            {
                doMelee();
            }
            else
            {
                doFireBreath();
            }
        }
        #endregion

        bool dragonReady;
        bool previousState;
        float wakeupTimePassed;

        public override bool Update()
        {
            bool stateChanged = (previousState != gm.IsDragonAwake);

            if (gm.IsDragonAwake)
            {
                // wake him up!
                if (stateChanged)
                {
                    SceneNode.Animation["wakeup"].Speed = 0.4f;
                    SceneNode.Animation.BlendInto("wakeup", AnimationPlay.Once, 0.1f);
                    SceneNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/dragonScream2"));
                    SceneNode.AudioSource.Play();                
                }

                // if the animation is finished playing, mark the dragon as "ready"
                if (wakeupTimePassed < SceneNode.Animation["wakeup"].Duration / 1000.0f)
                {
                    wakeupTimePassed += GameTiming.TimeElapsed;
                }
                else
                {
                    dragonReady = true;
                }

                // he is properly awake, let's hunt down wizards.
                if (dragonReady && !dragon.IsDeadOrDying)
                {
                    doMove();
                    doSelectAttack();
                    doPathFindQuery();
                }

                bool b = base.Update();
                dragon.Update();

                previousState = gm.IsDragonAwake;
                return b;
            }

            previousState = gm.IsDragonAwake;
            return base.Update();
        }
    }
}
