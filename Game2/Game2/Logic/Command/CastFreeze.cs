﻿using System;
using System.Collections;
using System.Collections.Generic;

using Engine.Core;
using Engine.Graphics;

using Microsoft.Xna.Framework;
using Engine.Tools;
using Engine.Scene;

using System.Diagnostics;

namespace Game2.Logic.Commands
{
    public class CastFreeze: LightEmittingCommand
    {
        #region Variables
        private SceneService ss;
        #endregion

        #region Constructor
        public CastFreeze()
            : base(1, new PointLight(Color.LightBlue.ToVector4(), 0, 0))
        {
            ss = ServiceProvider.GetService<SceneService>();

            Duration = 0.0f;
            FadeInTime = 0.2f;
            FadeOutTime = 0.2f;
            MaxIntensity = 1.5f;
            MaxRadius = 5.0f;
        }
        #endregion

        #region Command Overrides
        public override bool Begin()
        {
            if (!base.Begin()) return false;

            List<SceneNode> skeletons = ss.GetSceneNodesInCategoryGroup("Skeleton");
            foreach (SceneNode skeleton in skeletons)
            {
                if (Math2.IsPointInSphere(new BoundingSphere(Puppet.SceneNode.Translation, MaxRadius), skeleton.Translation))
                {
                    object o = skeleton["unit"];
                    if (o != null)
                    {
                        Unit u = (Unit)o;
                        u.IssueCommand(new Freeze());
                    }
                }
            }

            return true;
        }
        #endregion
    }
}
