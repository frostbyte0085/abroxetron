﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    public class PushMove : Command
    {
        private Vector3 force;
        private float oldDecel;

        #region Constructor
        public PushMove(Vector3 force) : base(4) { this.force = force; }
        #endregion

        private void setDeceleration(float d)
        {
            if (Puppet.SceneNode.Behaviour != null && Puppet.SceneNode.Behaviour is CharacterSceneNodeBehaviour)
            {
                CharacterSceneNodeBehaviour be = (CharacterSceneNodeBehaviour)Puppet.SceneNode.Behaviour;
                oldDecel = be.TractionDeceleration;
                be.TractionDeceleration = d;
            }
        }

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            // Validate:
            if (AssertPuppet()) 
                return false;
            if (AssertPhysicalEntity())
                return false;

            // Play animation?
            //Puppet.SceneNode.Animation.BlendInto(AnimationWalkName, Engine.Animation.AnimationPlay.Loop, 0.1f);
            Puppet.SceneNode.PhysicalEntity.LinearVelocity = force;
            setDeceleration(20);

            // Ok (Y)... good to go :)
            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            return Puppet.SceneNode.PhysicalEntity.LinearVelocity.Length() > 0.000001;
        }

        public override void Stop() { setDeceleration(oldDecel); }
        #endregion
    }
}
