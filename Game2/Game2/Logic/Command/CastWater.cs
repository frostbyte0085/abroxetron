﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Audio;

using Game2.Logic.Missiles;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Commands
{
    public class CastWater : Cast
    {
        #region Constructor
        public CastWater()
            : base(50) { }
        #endregion

        #region Cast overrides
        protected override void cast()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode fountainNode = ss.AddSceneNode("waterfountain"+(Missile.sceneNodeNameId++));
            fountainNode.Translation = Puppet.SceneNode.Translation;
            fountainNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/spellWater"));
            fountainNode.AudioSource.Play();

            Missile.Fire(new WaterFountain(fountainNode, 8.0f), Puppet, 0.0f, false);
        }
        #endregion

        #region ISpell
        private static Texture2D tiles = null;
        public override Texture2D GetTiledTexture()
        {
            if (tiles == null)
            {
                ContentService cs = ServiceProvider.GetService<ContentService>();
                tiles = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Water");
            }
            return tiles;
        }

        public override SpellType GetSpellType()
        {
            return SpellType.Water;
        }
        #endregion
    }
}