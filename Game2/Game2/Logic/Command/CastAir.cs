﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Audio;

using Game2.Logic.Missiles;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Commands
{
    public class CastAir: Cast
    {
        #region Constructor
        public CastAir()
            : base(10) { }
        #endregion

        #region Cast overrides
        protected override void cast()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode ballnode = ss.AddSceneNode("airpush"+(Missile.sceneNodeNameId++));
            ballnode.Translation = Puppet.SceneNode.Translation;
            ballnode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/spellAir"));
            ballnode.AudioSource.Play();

            float speed = 9.5f;
            float power = 18.5f;
            Vector3 force = Puppet.SceneNode.Forward * power;
            Airforce af = new Airforce(ballnode, 0.7f, force);
            Missile.Fire(af, Puppet, speed, true);
        }
        #endregion

        #region ISpell
        private static Texture2D tiles = null;
        public override Texture2D GetTiledTexture()
        {
            if (tiles == null)
            {
                ContentService cs = ServiceProvider.GetService<ContentService>();
                tiles = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Air");
            }
            return tiles;
        }

        public override SpellType GetSpellType()
        {
            return SpellType.Air;
        }
        #endregion
    }
}
