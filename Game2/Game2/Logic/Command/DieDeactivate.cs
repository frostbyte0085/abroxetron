﻿namespace Game2.Logic.Commands
{
    /// <summary>
    /// Death command for the wizards:
    /// </summary>
    public class DieDeactivate : Die
    {
        public DieDeactivate() : base() 
        {
            AnimationDiceRoller.AnimationNames = new string[]{"death"};
        }

        public override void Stop()
        {
            Puppet.SceneNode.Active = false;
            Puppet.SceneNode.Animation.Pause = true;
            GameApplication.App.GameManager.UnitDied(Puppet);
        }
    }
}
