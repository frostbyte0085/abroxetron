﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Audio;
using Engine.Scene;
using Engine.Animation;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// This command controls the skeletons when they are being attacked
    /// </summary>
    public class DamageKnockBack : Command
    {
        #region Properties
        /// <summary>
        /// Damage knock-back animations played.
        /// </summary>
        public string DamageAnimation {set;get;}

        /// <summary>
        /// Speed
        /// </summary>
        public float Speed { set; get; }
        #endregion

        #region Constructor
        public DamageKnockBack()
            : base(2)
        {
            DamageAnimation = null;
            Speed = 1;
        }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            if(AssertPuppet()) return false;
            if(AssertAnimation(DamageAnimation)) return false;

            Puppet.SceneNode.Animation.Pause = false;
            Puppet.SceneNode.Animation.BlendInto(DamageAnimation, AnimationPlay.Once, 0.1f);
            Puppet.SceneNode.Animation[DamageAnimation].Speed = Speed;

            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            return !Puppet.SceneNode.Animation.Looped;
        }

        public override void Stop()
        {
        }
        #endregion
    }
}
