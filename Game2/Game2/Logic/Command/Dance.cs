﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Audio;
using Engine.Scene;
using Engine.Animation;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// Dance easter egg!!! Everybody dance now! :D
    /// </summary>
    public class Dance : Command
    {
        public Dance() : base(-1) { }

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            if(AssertPuppet()) return false;
            if(AssertAnimation("dance")) return false;

            Puppet.SceneNode.Animation.Pause = false;
            Puppet.SceneNode.Animation.BlendInto("dance", AnimationPlay.Loop, 100f);

            return true;
        }
        public override bool Update()
        {
            // Don't stop the music!!!
            return true;
        }

        public override void Stop() { }
        #endregion
    }
}
