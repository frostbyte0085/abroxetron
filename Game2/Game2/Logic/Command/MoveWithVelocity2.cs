﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// This command handles everything other than the actual movement (because
    /// that's handled by the CharacterSceneNodeBehaviour for the physics).
    /// </summary>
    public class MoveWithVelocity2 : Move
    {
        #region Properties
        /// <summary>
        /// The input direction
        /// </summary>
        public Vector2 InputDirection { set; get; }
        #endregion

        #region Constructor
        public MoveWithVelocity2(CharacterSceneNodeBehaviour controller) : base(controller) { }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            // Nothing to move
            if (InputDirection == Vector2.Zero || !base.Begin()) 
                return false;

            // Ok (Y)... good to go :)
            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            float speed = Speed * InputDirection.Length();

            this.Controller.MovementDirection = Vector2.Normalize(InputDirection);
            this.Controller.MaxSpeed = speed;

            if (AnimationWalkName != null)
            {
                Puppet.SceneNode.Animation.Pause = false;
                Puppet.SceneNode.Animation[AnimationWalkName].Speed = speed * AnimationWalkSpeed;
            }

            bool stillInCommand = speed != 0;

            if (stillInCommand)
                updateAngle();

            return stillInCommand;
        }

        public override void Stop()
        {
            this.Controller.MovementDirection = Vector2.Zero;
        }
        #endregion
    }
}
