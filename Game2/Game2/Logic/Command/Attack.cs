﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Audio;
using Engine.Scene;
using Engine.Animation;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// This command handles everything other than the actual movement (because
    /// that's handled by the CharacterSceneNodeBehaviour for the physics).
    /// </summary>
    public class Attack : Command
    {
        #region Properties
        /// <summary>
        /// Attack animations played at random.
        /// </summary>
        public AnimationDiceRoller AttackAnimations {set;get;}

        /// <summary>
        /// The swoooooord
        /// </summary>
        public SwordBehaviour SwordBehaviour { private set; get; }

        /// <summary>
        /// Sounds to play when the sword is swinged.
        /// </summary>
        public string[] SwordSwingSounds { set; get; }

        /// <summary>
        /// Speed
        /// </summary>
        public float Speed { set; get; }
        #endregion

        #region Constructor
        public Attack(SwordBehaviour sword)
            : base(1)
        {
            SwordBehaviour = sword;
            AttackAnimations = null;
        }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            if(AssertPuppet()) return false;
            if(AttackAnimations == null || AttackAnimations.AnimationNames == null ||
                AttackAnimations.AnimationNames.Length == 0) return false;

            SwordBehaviour.Active = true;
            SwordBehaviour.WhoGotHit.Clear();
            if (SwordSwingSounds != null && SwordSwingSounds.Length != 0)
                SwordBehaviour.SceneNode.AudioSource = AudioSource.RandomFromSoundNames(SwordSwingSounds, true);

            Puppet.SceneNode.Animation.Pause = false;
            Puppet.SceneNode.Animation.BlendInto(AttackAnimations.Roll(), AnimationPlay.Once, 100f);

            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            return !Puppet.SceneNode.Animation.Looped;
        }

        public override void Stop()
        {
            SwordBehaviour.Active = false;
        }
        #endregion
    }
}
