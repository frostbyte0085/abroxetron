﻿using Engine.Core;
using Engine.Scene;

namespace Game2.Logic.Commands
{
    public class DieRemove : Die
    {
        public DieRemove() : base()
        {
            AnimationDiceRoller.AnimationNames = new string[] { "death" };
        }

        public override void Stop()
        {
            SceneService ss = ServiceProvider.GetService<SceneService>();
            ss.RemoveSceneNode(Puppet.SceneNode);
            GameApplication.App.GameManager.UnitDied(Puppet);
        }
    }
}
