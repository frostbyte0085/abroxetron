﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Animation;

using Game2.Logic.Buffs;

namespace Game2.Logic.Commands
{
    public abstract class Spawn : Command
    {
        public string AnimationName { set; get; }

        public float AnimationSpeed { set; get; }

        private readonly bool pauseAtStart;
        private readonly Invulnerable invul;

        public Spawn(bool pauseAtStart) : base(int.MaxValue) 
        {
            this.pauseAtStart = pauseAtStart;
            this.invul = new Invulnerable();
        }

        public override bool Begin()
        {
            if (AssertPuppet()) return false;
            if (AssertAnimation(AnimationName)) return false;

            // Note start clip before pausing. Otherwise the bone transforms will not
            // update if Pause is true.
            this.Puppet.SceneNode.Animation.StartClip(AnimationName, AnimationPlay.Once);
            this.Puppet.SceneNode.Animation.Pause = pauseAtStart;
            this.Puppet.SceneNode.Animation[AnimationName].Speed = AnimationSpeed;

            this.Puppet.TakeBuff(invul);

            return true;
        }

        public abstract override bool Update();

        public override void Stop()
        {
            this.Puppet.RemoveBuff(invul);
        }
    }
}
