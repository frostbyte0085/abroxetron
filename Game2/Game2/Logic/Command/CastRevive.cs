﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;

using Game2.GameManager;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    public class CastRevive: Command
    {
        private static ParticleAnimation __party = null;
        private static ParticleAnimation partyParts
        {
            get
            {
                if (__party == null)
                    __party = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/PartyParticles.xml");
                return __party;
            }
        }
        private static long partyPartsId = 0;

        private const float duration = 3.0f;

        private float birth;

        #region Constructor
        public CastRevive()
            : base(1) { }
        #endregion

        #region Command overrides
        private Unit GetPlayerToRez()
        {
            BaseGameManager gm = GameApplication.App.GameManager;
            IEnumerator<Unit> e = gm.Players;

            while (e.MoveNext())
                if (e.Current.IsDead)
                    return e.Current;

            return null;
        }

        private void DoRez(Unit u)
        {
            // Rez the unit
            BaseGameManager gm = GameApplication.App.GameManager;
            u.SceneNode.Translation = gm.KinectPlayer.Guardian.SceneNode.Translation;
            u.SceneNode.Active = true;
            u.UnitProperties.CurrentHP = u.UnitProperties.MaxHP / 2;
            u.UnitProperties.CurrentMana = 0;

            // Add some particle effect:
            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode node = ss.AddSceneNode("partyParticle" + (partyPartsId++));
            node.Translation = Puppet.SceneNode.Translation;
            ParticleSystem ps = new ParticleSystem(partyParts);
            node.ParticleAttachMode = ParticleAttachMode.DiscardOnCompletion;
            node.Attach("node", ps);
            ps.StartEmitting();
        }

        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            if (AssertPuppet()) return false;

            Unit rezMe = GetPlayerToRez();
            if (rezMe == null) return false;
            else DoRez(rezMe);

            birth = GameTiming.TotalTimeElapsed;
            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            return GameTiming.TotalTimeElapsed - birth <= duration;
        }

        /// <summary>
        /// Force this command to stop immediately. Note that when this method is invoked
        /// the unit has already begun another command.
        /// </summary>
        public override void Stop()
        {
        }
        #endregion
    }
}
