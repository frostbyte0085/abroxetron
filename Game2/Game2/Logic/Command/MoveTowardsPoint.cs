﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Engine.Animation;
using Engine.Scene;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// This command handles everything other than the actual movement (because
    /// that's handled by the CharacterSceneNodeBehaviour for the physics).
    /// </summary>
    public class MoveTowardsPoint : Move
    {
        #region Properties

        /// <summary>
        /// The input location
        /// </summary>
        public Vector2 Target { set; get; }

        /// <summary>
        /// Distance error tolerance between Target and Puppet.SceneNode.Translation.
        /// </summary>
        public float CloseEnough { set; get; }
        #endregion

        #region Constructor
        public MoveTowardsPoint(CharacterSceneNodeBehaviour controller) : base(controller) { }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            if (!base.Begin()) return false;

            // TODO remove me
            Puppet.SceneNode.Animation.StartClip(AnimationWalkName, AnimationPlay.Loop);

            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            Vector2 dif = diff();
            Controller.MovementDirection = dif;
            Controller.MovementDirection.Normalize();
            Controller.MaxSpeed = Speed;

            updateAngle();

            if (hasAnimation())
            {
                this.Puppet.SceneNode.Animation[AnimationWalkName].Speed = AnimationWalkSpeed;
            }

            return !Reached();
        }

        public override void Stop()
        {
            this.Controller.MovementDirection = Vector2.Zero;
        }
        #endregion

        private Vector2 diff()
        {
            Vector2 here = new Vector2(Puppet.SceneNode.Translation.X, Puppet.SceneNode.Translation.Z);
            return Target - here;
        }

        public bool Reached()
        {
            Debug.Assert(Puppet != null); 
            Debug.Assert(Puppet.SceneNode != null);

            return diff().Length() < CloseEnough;
        }
    }
}
