﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Audio;

using Game2.Logic.Missiles;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Commands
{
    public class CastFire : Cast
    {
        #region Constructor
        public CastFire()
            : base(20) { }
        #endregion

        #region Cast overrides
        protected override void cast()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode ballnode = ss.AddSceneNode("fireball"+(Missile.sceneNodeNameId++));
            ballnode.Translation = Puppet.SceneNode.Translation;
            ballnode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/spelFireball"));
            ballnode.AudioSource.Play();

            Missile.Fire(new Fireball(ballnode, 1.0f), Puppet, 9.5f);
        }
        #endregion

        #region ISpell
        private static Texture2D tiles = null;
        public override Texture2D GetTiledTexture()
        {
            if (tiles == null)
            {
                ContentService cs = ServiceProvider.GetService<ContentService>();
                tiles = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Fire");
            }
            return tiles;
        }

        public override SpellType GetSpellType()
        {
            return SpellType.Fire;
        }
        #endregion
    }
}