﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Audio;
using Engine.Scene;
using Engine.Animation;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// This command handles the melee attack of the dragon.
    /// </summary>
    public class DragonAttack : Command
    {
        #region Properties
        /// <summary>
        /// Attack animations played at random.
        /// </summary>
        public AnimationDiceRoller AttackAnimations { set; get; }

        /// <summary>
        /// Sounds to play when the dragon attacks.
        /// </summary>
        public string[] AttackSounds { set; get; }

        /// <summary>
        /// Unit which the dragon was decided to attaccccckkk!!
        /// </summary>
        public Unit Target { set; get; }

        /// <summary>
        /// Damage applied when the dragon attacks
        /// </summary>
        public Damage Damage { set; get; }
        #endregion

        #region Constructor
        public DragonAttack()
            : base(1)
        {
            AttackAnimations = null;
        }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            if (AssertPuppet()) return false;
            if (AttackAnimations == null || AttackAnimations.AnimationNames == null ||
                AttackAnimations.AnimationNames.Length == 0) return false;

            if (AttackSounds != null && AttackSounds.Length != 0)
                Puppet.SceneNode.AudioSource = AudioSource.RandomFromSoundNames(AttackSounds, true);

            Puppet.SceneNode.Animation.Pause = false;
            Puppet.SceneNode.Animation.BlendInto(AttackAnimations.Roll(), AnimationPlay.Once, 100f);

            // Face him (only if the command was successfully issued
            Vector3 facingDir = Target.SceneNode.Translation - this.Puppet.SceneNode.Translation;
            float yaw = (float)Math.Atan2(facingDir.X, facingDir.Z);
            this.Puppet.SceneNode.Orientation = Quaternion.CreateFromYawPitchRoll(yaw, 0, 0);

            // and of course remove some of his precious life
            Target.TakeDamage(this.Damage);

            ContentService cs = ServiceProvider.GetService<ContentService>();

            Puppet.SceneNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/dragonAttack"));
            Puppet.SceneNode.AudioSource.Play();

            IsAttacking = true;
            return true;
        }

        /// <summary>
        /// Returns true if the dragon is attacking
        /// </summary>
        public bool IsAttacking
        {
            private set;
            get;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            return !Puppet.SceneNode.Animation.Looped;
        }

        public override void Stop()
        {
            IsAttacking = false;
        }
        #endregion
    }
}
