﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Audio;
using Engine.Scene;
using Engine.Graphics;
using Engine.Tools;
using Engine.Animation;
using Engine.Core;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Game2.Logic.Commands
{
    public class DragonFireBreath : Command
    {
        private ParticleAnimation breathAnim;
        private ParticleSystem breathSystem;
        private SceneNode breathNode;

        /// <summary>
        /// Attack animations played at random.
        /// </summary>
        public AnimationDiceRoller BreathAnimations { set; get; }

        /// <summary>
        /// Sounds to play when the dragon attacks.
        /// </summary>
        public string[] BreathSounds { set; get; }

        /// <summary>
        /// Speed
        /// </summary>
        public float Speed { set; get; }

        /// <summary>
        /// Unit which the dragon was decided to attaccccckkk!!
        /// </summary>
        public Unit Target { set; get; }

        /// <summary>
        /// Damage applied when the dragon attacks
        /// </summary>
        public Damage Damage { set; get; }

        public DragonFireBreath() : base(1)
        {
            BreathAnimations = null;
        }

        SceneService ss;

        public override bool Begin()
        {
            if (BreathAnimations == null || BreathAnimations.AnimationNames == null ||
                BreathAnimations.AnimationNames.Length == 0) return false;

            // Face him the targetted unit:
            Vector3 facingDir = Target.SceneNode.Translation - this.Puppet.SceneNode.Translation;
            float yaw = (float)Math.Atan2(facingDir.X, facingDir.Z);
            this.Puppet.SceneNode.Orientation = Quaternion.CreateFromYawPitchRoll(yaw, 0, 0);

            // and of course remove some of his precious life
            Target.TakeDamage(this.Damage);

            breathAnim = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/DragonBreath.xml");
            ss = ServiceProvider.GetService<SceneService>();
            
            breathSystem = new ParticleSystem(breathAnim);
            breathNode = ss.AddSceneNode("dragonBreathParticles");
            Puppet.SceneNode.ParticleAttachMode = ParticleAttachMode.Default;
            Puppet.SceneNode.Attach("HeadJaw1", breathSystem);
            breathSystem.ParticleSystemSpaceMode = ParticleSystemSpaceMode.WorldSpace;
            breathAnim.MinBearing = yaw - 0.1f;
            breathAnim.MaxBearing = yaw + 0.1f;
            breathAnim.MaxElevation = breathAnim.MinElevation = 0.0f;

            if (BreathSounds != null && BreathSounds.Length != 0)
                Puppet.SceneNode.AudioSource = AudioSource.RandomFromSoundNames(BreathSounds, true);

            Puppet.SceneNode.Animation.Pause = false;
            Puppet.SceneNode.Animation.BlendInto(BreathAnimations.Roll(), AnimationPlay.Once, 100f);

            IsBreathing = true;

            ContentService cs = ServiceProvider.GetService<ContentService>();

            Puppet.SceneNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/dragonScream1"));
            Puppet.SceneNode.AudioSource.Play();
            return true;
        }

        public bool IsBreathing
        {
            private set;
            get;
        }

        public override void Stop()
        {
            IsBreathing = false;
            ss.RemoveSceneNode("dragonBreathParticles");
        }

        public override bool Update()
        {
            float animationDuration = Puppet.SceneNode.Animation["fireBreath"].Duration;
            float currentTime = Puppet.SceneNode.Animation.CurrentTime;

            if (currentTime / animationDuration > 0.4f && !breathSystem.IsEmitting)
            {
                breathSystem.StartEmitting();
            }

            if (currentTime / animationDuration > 0.7f)
            {
                breathSystem.StopEmitting();
            }

            return !Puppet.SceneNode.Animation.Looped;
        }
    }
}
