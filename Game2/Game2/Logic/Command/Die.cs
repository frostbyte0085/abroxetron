﻿using Engine.Animation;

namespace Game2.Logic.Commands
{
    public abstract class Die : Command
    {

        #region Properties
        public AnimationDiceRoller AnimationDiceRoller {private set;get;}
        #endregion

        #region Constructor
        public Die() : base(int.MaxValue) 
        {
            AnimationDiceRoller = new AnimationDiceRoller();
        }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            // Validate:
            if (AssertPuppet()) return false;

            string deathAnim = AnimationDiceRoller.Roll();

            if (deathAnim != null)
            {
                Puppet.SceneNode.Animation.Pause = false;
                Puppet.SceneNode.Animation.BlendInto(deathAnim, Engine.Animation.AnimationPlay.Once, 50f);
            }

            return true;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            if (Puppet.SceneNode.Animation == null ||
                AnimationDiceRoller.AnimationNames == null) 
                return false;

            return !Puppet.SceneNode.Animation.Looped;
        }
        
        public abstract override void Stop();
        #endregion
    }
}
