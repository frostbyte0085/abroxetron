﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.UI;

using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Commands
{
    public abstract class Cast : Command, ISpell
    {
        #region Variables
        protected float castTimeTotal = 3.0f;    // casting time in seconds
        protected float castPrepareTime = 1.1f; // time to prepare for cast
        protected bool casted;
        protected float castTimeCurrent = 0.0f;   // how long has the unit been casting for
        protected int manacost;
        #endregion

        #region Properties
        /// <summary>
        /// castTimeTotal taking AnimationSpeed into account.
        /// </summary>
        protected float CastTimeTotal { get { return castTimeTotal / AnimationSpeed; } }
        /// <summary>
        /// castPrepareTime taking AnimationSpeed into account.
        /// </summary>
        protected float CastPrepareTime { get { return castPrepareTime / AnimationSpeed; } }

        public float AnimationSpeed { set; get; }
        public string AnimationName { set; get; }
        #endregion

        #region Constructor
        public Cast(int manacost)
            : base(1)
        {
            this.manacost = manacost;
            this.AnimationName = "spell";
            this.AnimationSpeed = 2.5f;
        }
        #endregion

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            // Validate:
            if (AssertPuppet()) return false;

            bool oom = manacost > Puppet.UnitProperties.CurrentMana;
            if (!oom)
            {
                casted = false;
                Puppet.SceneNode.Animation[AnimationName].Speed = AnimationSpeed;
                Puppet.SceneNode.Animation.BlendInto(AnimationName, Engine.Animation.AnimationPlay.Once, 100f);
                castTimeCurrent = 0.0f;
                Puppet.UnitProperties.CurrentMana -= manacost;
                // play cast animation.
            }

            return !oom;
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public override bool Update()
        {
            castTimeCurrent += GameTiming.TimeElapsed;

            if (!casted && castTimeCurrent >= CastPrepareTime)
            {
                casted = true;
                cast();
            }

            return castTimeCurrent <= CastTimeTotal;
        }

        protected abstract void cast();

        public override void Stop() { }
        #endregion

        #region ISpell
        private static UITileData tileData = UITileData.LoadFromXml("GameContent/UI/Ingame/BasicTiling.xml");
        public UITileData GetTileData()
        {
            return tileData;
        }

        public abstract Texture2D GetTiledTexture();
        public abstract SpellType GetSpellType();
        #endregion
    }
}
