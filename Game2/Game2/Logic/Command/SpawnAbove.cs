﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    public class SpawnAbove : Spawn
    {
        private static float velo_threshold = 0.01f;

        public SpawnAbove()
            : base(true)
        {
            AnimationName = "spawnabove";
            AnimationSpeed = 3.2f;
        }

        public override bool Begin()
        {
            if (!base.Begin()) return false;
            
            Puppet.SceneNode.Translate(0, 5, 0);
            // Give the skeleton a head start. That way it looks like he's been falling for a while.
            Puppet.SceneNode.PhysicalEntity.LinearVelocity = new Vector3(0, -1.2f, 0);
            return true;
        }

        public override bool Update()
        {
            Puppet.SceneNode.Animation.Pause =
                Puppet.SceneNode.PhysicalEntity.LinearVelocity.Length() > velo_threshold;

            return !Puppet.SceneNode.Animation.Looped;
        }
    }
}
