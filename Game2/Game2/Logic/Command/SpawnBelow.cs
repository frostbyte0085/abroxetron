﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Physics;

namespace Game2.Logic.Commands
{
    public class SpawnBelow : Spawn
    {
        public SpawnBelow() : base(false) 
        {
            AnimationName = "spawnbelow";
            AnimationSpeed = 2.5f;
        }

        public override bool Begin()
        {
            if (!base.Begin()) return false;

            return true;
        }

        public override bool Update() { return !Puppet.SceneNode.Animation.Looped; }

        public override void Stop()
        {
            base.Stop();
        }
    }
}
