﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Animation;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// Some example commands:
    /// 
    /// Prio  Command
    /// 0     Move (velocity)
    ///       Move (to point)
    ///       Stand
    ///       
    /// 1     Attack
    ///       Cast
    ///       
    /// 2     Knockback
    /// 
    /// Insure that a command never crashes.
    /// </summary>

    public abstract class Command
    {
        /// <summary>
        /// The unit which this command is currently instructing
        /// </summary>
        public Unit Puppet { set; get; }

        private readonly int prio;
        /// <summary>
        /// Priority level of this cmd.
        /// </summary>
        public int Priority { get { return prio; } }

        /// <summary>
        /// Initial the command
        /// </summary>
        /// <param name="prio">Priority Level</param>
        public Command(int prio)
        {
            this.prio = prio;
            this.Puppet = null;
        }

        /// <summary>
        /// Check if this command has a puppet. Returns true on failure
        /// </summary>
        /// <returns>True if puppet is null.</returns>
        protected bool AssertPuppet()
        {
            if (Puppet == null)
            {
                Console.WriteLine("Warning: null puppet");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if this command's puppet has an animation called animName
        /// </summary>
        /// <param name="animName">name of the animation to check for</param>
        /// <returns>True on failure (animName doesn't exist)</returns>
        protected bool AssertAnimation(string animName)
        {
            try
            {
                AnimationClip clip = Puppet.SceneNode.Animation[animName];
            }
            catch (Exception e)
            {
                Console.WriteLine("Warning: could not get animation \""+animName+"\" - "+e.Message);
                return true;
            }
            return false;
        }

        protected bool AssertPhysicalEntity()
        {
            if (Puppet.SceneNode.PhysicalEntity == null)
            {
                Console.WriteLine("Warning: null PhysicalEntity");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public abstract bool Begin();

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public abstract bool Update();

        /// <summary>
        /// Force this command to stop immediately. Note that when this method is invoked
        /// the unit has already begun another command.
        /// </summary>
        public abstract void Stop();
    }
}