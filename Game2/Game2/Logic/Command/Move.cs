﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Commands
{
    /// <summary>
    /// This command handles everything other than the actual movement (because
    /// that's handled by the CharacterSceneNodeBehaviour for the physics).
    /// </summary>
    public abstract class Move : Command
    {
        #region Properties
        /// <summary>
        /// Value multiplied by Velocity.Length to get the animation speed.
        /// </summary>
        public float AnimationWalkSpeed { set; get; }

        /// <summary>
        /// Name of the animation used.
        /// </summary>
        public string AnimationWalkName { set; get; }

        /// <summary>
        /// The controller
        /// </summary>
        public CharacterSceneNodeBehaviour Controller { set; get; }

        /// <summary>
        /// Walking Speed
        /// </summary>
        public float Speed { set; get; }

        /// <summary>
        /// Turning speed for rotation
        /// </summary>
        public float TurnSpeed { set; get; }

        private float oldYaw;
        #endregion

        #region Constructor
        public Move(CharacterSceneNodeBehaviour controller)
            : base(0)
        {
            Controller = controller;
            TurnSpeed = 10;
        }
        #endregion

        protected bool hasAnimation() { return AnimationWalkName != null && Puppet.SceneNode.Animation != null; }

        #region Command overrides
        /// <summary>
        /// Try to start this command.
        /// </summary>
        /// <returns>True if the command can successfully start. False if impossible to start
        /// (e.g. out of mana)</returns>
        public override bool Begin()
        {
            // Validate:
            if (AssertPuppet()) return false;

            // Play animation (will never crash) :
            if (hasAnimation())
            {
                Puppet.SceneNode.Animation.Pause = false;
                Puppet.SceneNode.Animation.BlendInto(AnimationWalkName, Engine.Animation.AnimationPlay.Loop, 150f);
            }

            // Ok (Y)... good to go :)
            return true;
        }

        private const float pi = (float)Math.PI;
        private const float pi2 = pi * 2;
        protected void updateAngle()
        {
            // Calculate the intended yaw:
            float yaw = (float)Math.Atan2(Controller.MovementDirection.X, Controller.MovementDirection.Y);
            yaw += pi;

            // Get the difference:
            float diff = yaw - oldYaw;

            // if the difference is greater than a semi-circle then the character 
            // will spin the wrong way around
            if (diff < -pi)
            {
                yaw += pi2;
                diff = yaw - oldYaw;
            }
            else if (diff > pi)
            {
                yaw -= pi2;
                diff = yaw - oldYaw;
            }

            // Spin the unit based on time:
            yaw = oldYaw + diff * GameTiming.TimeElapsed * TurnSpeed;
            oldYaw = yaw;

            // 0 <= oldYaw < Pi*2 
            if (oldYaw > 0) oldYaw -= ((int)(oldYaw / pi2)) * pi2;
            if (oldYaw < 0) oldYaw += ((int)(-oldYaw / pi2) + 1) * pi2;

            Puppet.SceneNode.Orientation = Quaternion.CreateFromYawPitchRoll(yaw - pi, 0, 0);
        }

        /// <summary>
        /// Update this command.
        /// </summary>
        /// <returns>True if still commanding. False if finished.</returns>
        public abstract override bool Update();

        public abstract override void Stop();
        #endregion
    }
}
