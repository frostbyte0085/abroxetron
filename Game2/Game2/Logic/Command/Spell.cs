﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.UI;

using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Commands
{
    public enum SpellType
    {
        NONE,
        Fire,
        Water,
        Earth,
        Air
    }

    public interface ISpell
    {
        Texture2D GetTiledTexture();

        UITileData GetTileData();

        SpellType GetSpellType();
    }
}
