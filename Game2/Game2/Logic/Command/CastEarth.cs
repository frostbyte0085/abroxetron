﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Audio;

using Game2.Logic.Missiles;
using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Commands
{
    public class CastEarth: Cast
    {
        #region Constructor
        public CastEarth()
            : base(25) { }
        #endregion

        #region Cast overrides
        protected override void cast()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode ballnode = ss.AddSceneNode("rockmissile"+(Missile.sceneNodeNameId++));
            ballnode.Translation = Puppet.SceneNode.Translation;
            ballnode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/spellEarth"));
            ballnode.AudioSource.Play();
            
            float speed = 9.5f;
            Vector3 force = Puppet.SceneNode.Forward * speed;
            Vector3 spawn = Puppet.SceneNode.Forward * 1.0f + Puppet.SceneNode.Translation;
            
            RockMissile missile = new RockMissile(ballnode, force);
            missile.SceneNode.Behaviour = new MissileBehaviour(missile, Vector3.Zero, spawn);
            missile.SceneNode["missile"] = missile;
            missile.SceneNode["missile_source"] = Puppet;
        }
        #endregion

        #region ISpell
        private static Texture2D tiles = null;
        public override Texture2D GetTiledTexture()
        {
            if (tiles == null)
            {
                ContentService cs = ServiceProvider.GetService<ContentService>();
                tiles = cs["game"].Load<Texture2D>("UI/Ingame/SpellIcons/Earth");
            }
            return tiles;
        }

        public override SpellType GetSpellType()
        {
            return SpellType.Earth;
        }
        #endregion
    }
}
