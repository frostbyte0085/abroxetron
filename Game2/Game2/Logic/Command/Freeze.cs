﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Audio;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Game2.Logic.Commands
{
    public class Freeze : Command
    {
        /// <summary>
        /// 
        ///  <------------------ TotalDuration ------------------->
        ///  <- FreezingTime -> <- Duration -> <- UnfreezingTime ->
        /// 
        /// </summary>

        public float Duration { set; get; }
        public float FreezingTime { set; get; }
        public float UnfreezingTime { set; get; }

        public float TotalDuration
        {
            get { return Duration + FreezingTime + UnfreezingTime; }
        }

        private float life;

        public Freeze()
            : base(3)
        {
            Duration = 2.0f;
            FreezingTime = UnfreezingTime = 0.5f;
        }

        public override bool Begin()
        {
            if (AssertPuppet()) return false;

            life = 0.0f;
            Puppet.SceneNode.TintColor = Color.CornflowerBlue.ToVector3();

            if (Puppet.SceneNode.Animation != null)
                Puppet.SceneNode.Animation.Pause = true;

            ContentService cs = ServiceProvider.GetService<ContentService>();
            Puppet.SceneNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/spellFreeze"));
            Puppet.SceneNode.AudioSource.Play();

            return true;
        }

        public override bool Update()
        {
            life += GameTiming.TimeElapsed;
            float emissiveRation = 0.1f;

            if (life < FreezingTime)
            {
                float x = life / FreezingTime;
                Puppet.SceneNode.TintAmount = x;
                Puppet.SceneNode.EmissiveAdditive = x * emissiveRation;
            }

            else if (life > FreezingTime + Duration)
            {
                float x = 1 - ((life - FreezingTime - Duration) / UnfreezingTime);
                Puppet.SceneNode.TintAmount = x;
                Puppet.SceneNode.EmissiveAdditive = x * emissiveRation;
            }

            else
            {
                Puppet.SceneNode.TintAmount = 1;
                Puppet.SceneNode.EmissiveAdditive = emissiveRation;
            }

            return life < TotalDuration;
        }

        public override void Stop()
        {
            Puppet.SceneNode.TintAmount = 0.0f;
            Puppet.SceneNode.EmissiveAdditive = 0.0f;
            if (Puppet.SceneNode.Animation != null)
                Puppet.SceneNode.Animation.Pause = false;
        }
    }
}
