﻿using Engine.Core;
using Engine.Scene;

namespace Game2.Logic.Commands
{
    public class DieNoRemove : Die
    {
        public DieNoRemove()
            : base()
        {
            AnimationDiceRoller.AnimationNames = new string[] { "death" };
        }

        public override void Stop()
        {
            GameApplication.App.GameManager.UnitDied(Puppet);
        }
    }
}
