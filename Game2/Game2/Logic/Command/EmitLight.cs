﻿using Engine.Core;
using Engine.Graphics;
using Engine.Audio;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Game2.Logic.Commands
{
    public class EmitLight: LightEmittingCommand
    {
        #region Constructor
        public EmitLight()
            : base(1, new PointLight(Color.White.ToVector4(), 0, 0))
        {
            Duration = 3.5f;
            FadeInTime = 0.5f;
            FadeOutTime = 0.2f;
            MaxIntensity = 10.0f;
            MaxRadius = 10.0f;
        }
        #endregion

        public override bool Begin()
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            Puppet.SceneNode.AudioSource = new AudioSource(cs["game"].Load<SoundEffect>("Sounds/spellLight"));
            Puppet.SceneNode.AudioSource.Play();

            return base.Begin();
        }
    }
}
