﻿using Engine.Core;
using Engine.Graphics;
using Engine.Audio;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;


namespace Game2.Logic.Commands
{
    public class LightEmittingCommand: Command
    {
        #region Variables
        public float Duration { set; get; }
        public float FadeInTime { set; get; }
        public float FadeOutTime { set; get; }
        public float MaxIntensity { set; get; }
        public float MaxRadius { set; get; }

        protected float life;

        protected PointLight light;

        protected bool fadingin;
        protected bool fadingout;
        #endregion

        #region Constructor
        public LightEmittingCommand(int prio, PointLight light)
            : base(prio)
        {
            this.light = light;

            Duration = 3.5f;
            FadeInTime = 0.5f;
            FadeOutTime = 0.2f;
            MaxIntensity = 10.0f;
            MaxRadius = 10.0f;
        }
        #endregion

        #region Command Overrides
        public override bool Begin()
        {
            if (AssertPuppet()) return false;

            Puppet.SceneNode.Light = light;
            life = 0;
            fadingin = true;
            fadingout = false;

            return true;
        }

        public override bool Update()
        {
            life += GameTiming.TimeElapsed;

            if (fadingin)
            {
                UpdateFadeIn();
            }

            if (fadingout)
            {
                UpdateFadeOut();
                return life < FadeOutTime;
            }

            else
            {
                UpdateNormal();
            }

            return true;
        }

        protected virtual void UpdateFadeIn()
        {
            float k = life / FadeInTime;
            if (k < 0) k = 0;
            else if (k > 1) k = 1;

            light.Intensity = MaxIntensity * k;
            light.Radius = MaxRadius * k;

            if (k == 1)
            {
                fadingin = false;
                life -= FadeInTime;
            }
        }

        protected virtual void UpdateFadeOut()
        {
            float k = life / FadeOutTime;
            if (k < 0) k = 0;
            else if (k > 1) k = 1;
            k = 1 - k;

            light.Intensity = MaxIntensity * k;
            light.Radius = MaxRadius * k;
        }

        protected virtual void UpdateNormal()
        {
            if (life > Duration)
            {
                life -= Duration;
                fadingout = true;
            }
        }

        public void FadeOut()
        {
            if (!fadingin && !fadingout)
            {
                fadingout = true;
                life = 0;
            }
        }

        public override void Stop()
        {
            Puppet.SceneNode.Light = null;
        }
        #endregion
    }
}
