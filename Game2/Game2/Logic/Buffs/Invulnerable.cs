﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Buffs
{
    public class Invulnerable : Buff
    {
        public override Type GetBuffType()
        {
            return typeof(Invulnerable);
        }

        public override void Begin(Unit unit)
        {
            base.Begin(unit);
        }
        public override void End() { }
        public override bool Filter(Buff buff) { return false; }
        public override bool Filter(ref Damage dmg) { return false; }
        public override bool Update() { return true; }
    }
}
