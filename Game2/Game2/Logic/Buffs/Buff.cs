﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Buffs
{
    public abstract class Buff
    {
        protected Unit unit;

        /// <summary>
        /// Reference counting (how many times this buff is applied).
        /// Do not modify this privately. The Unit class updates this
        /// whenever the buff is added or removed several times.
        /// </summary>
        public int Stacked { set; get; }

        public Buff()
        {
            Stacked = 1;
        }

        /// <summary>
        /// Begin the buff and do stuff (optional) to a unit.
        /// </summary>
        /// <param name="unit">unit to do stuff to</param>
        public virtual void Begin(Unit unit)
        {
            this.unit = unit;
        }

        /// <summary>
        /// The unit invokes this whenever the buff is truely gone. The unit also
        /// invokes this when Update() return false.
        /// </summary>
        public abstract void End();

        /// <summary>
        /// Return was type of buff this is. Allows the unit to distinguish if 2 instances
        /// of the same/similar buff are considered the same.
        /// </summary>
        /// <returns>A type</returns>
        public abstract Type GetBuffType(); 

        /// <summary>
        /// Filters damage
        /// </summary>
        /// <param name="dmg">Any kind of damage</param>
        /// <returns>False if this damage should be ignored entirely</returns>
        public abstract bool Filter(ref Damage dmg);

        /// <summary>
        /// Filters a buff.
        /// </summary>
        /// <param name="buff">Buff to modify</param>
        /// <returns>False if this buff should be ignore entirely</returns>
        public abstract bool Filter(Buff buff);

        /// <summary>
        /// Update this buff.
        /// </summary>
        /// <returns>false if finished</returns>
        public abstract bool Update();
    }
}
