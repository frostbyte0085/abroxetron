﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Buffs
{
    public class FountainHealBuff : Buff
    {
        private static readonly float hpReg = 20.0f;

        public override void Begin(Unit unit)
        {
            base.Begin(unit);
            Console.WriteLine("Buff begin on " + unit.SceneNode.Name);
            unit.UnitProperties.BonusHPRegen += hpReg;
        }

        public override void End()
        {
            Console.WriteLine("Buff ended on " + unit.SceneNode.Name);
            unit.UnitProperties.BonusHPRegen -= hpReg;
        }

        public override Type GetBuffType()
        {
            return typeof(FountainHealBuff);
        }

        public override bool Filter(ref Damage dmg) { return true; }
        public override bool Filter(Buff buff) { return true; }
        public override bool Update() { return true; }
    }
}
