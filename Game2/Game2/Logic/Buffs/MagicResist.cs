﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic.Buffs
{
    /// <summary>
    /// 50% magic reduction
    /// </summary>
    public class MagicResist : Buff
    {
        public override Type GetBuffType()
        {
            return typeof(MagicResist);
        }

        public override void Begin(Unit unit)
        {
            base.Begin(unit);
        }
        public override void End() { }
        public override bool Filter(Buff buff) { return true; }
        public override bool Filter(ref Damage dmg)
        {
            if (dmg.DamageType == DamageType.Magical)
                dmg.Value = dmg.Value / 2;

            return true; 
        }
        public override bool Update() { return true; }
    }
}
