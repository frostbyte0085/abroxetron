﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic
{
    public enum DamageType
    {
        Pure,
        Physical,
        Magical
    }

    public struct Damage
    {
        public int Value;
        public DamageType DamageType;
        public bool KnockBack;

        //public Damage() : this(0) { }
        public Damage(int Value) : this(Value, DamageType.Pure) { }
        public Damage(int Value, DamageType Type) : this(Value, Type, false) { }
        public Damage(int Value, DamageType Type, bool KnockBack)
        {
            this.Value = Value;
            this.DamageType = Type;
            this.KnockBack = KnockBack;
        }
    }
}
