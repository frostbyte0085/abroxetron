﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Audio;
using Engine.Animation;
using Engine.Tools;

using Microsoft.Xna.Framework;

using Game2.Logic.Commands;
using Game2.Logic.Buffs;

namespace Game2.Logic
{
    public class Unit : GameObject
    {
        public static readonly AnimationDiceRoller IDLE_PAUSE_ANIMATION = new AnimationDiceRoller();

        #region Variables
        /// <summary>
        /// Stats such as current HP/mana of the Unit.
        /// </summary>
        private UnitProperties stat;

        /// <summary>
        /// Current command. null is the Idle command. The idle command
        /// is set automatically, you cannot issue null.
        /// </summary>
        private Command cmd;

        /// <summary>
        /// Buffs applied on this unit
        /// </summary>
        private List<Buff> buffs;
        #endregion

        #region Properties
        /// <summary>
        /// Get the stats of this Unit
        /// </summary>
        public UnitProperties UnitProperties { get { return stat; } }

        /// <summary>
        /// Idle animations used.
        /// </summary>
        public AnimationDiceRoller IdleAnimations { set; get; }

        /// <summary>
        /// Sounds to play when the unit gets hit (takes damage)
        /// </summary>
        public string[] HitSoundNames { set; get; }

        /// <summary>
        /// Sounds to play when the unit dies.
        /// </summary>
        public string[] DieSoundNames { set; get; }

        /// <summary>
        /// True is the unit is dying (death animation is playing).
        /// A unit that is dying is still considered alive. Therefore
        /// IsDying && IsDead == false always.
        /// </summary>
        public bool IsDying { get { return this.cmd == this.DeathCommand; } }

        /// <summary>
        /// True is the unit is dead. Commands, Buffs and Damage can still be issued
        /// in this state but the unit will no longer regenerate health / mana.
        /// Commands issued will probably be ignored because a death command should
        /// be issued.
        /// </summary>
        public bool IsDead { get { return !IsDying && stat.CurrentHP <= 0; } }

        /// <summary>
        /// Equivalent to IsDead || IsDying
        /// </summary>
        public bool IsDeadOrDying { get { return IsDead || IsDying; } }

        /// <summary>
        /// Color of the unit. Used by the UI to know how to draw the character portraits.
        /// </summary>
        public Color Color { set; get; }

        /// <summary>
        /// Which spell is selected by this unit. Only really applicable to the wizards.
        /// </summary>
        public ISpell SelectedSpell { set; get; }

        /// <summary>
        /// Command issued automatically when the unit's health goes below 1.
        /// </summary>
        public Command DeathCommand { set; get; }

        /// <summary>
        /// Command issued automatically when the unit takes damage.
        /// </summary>
        public Command DamageCommand { set; get; }
        #endregion

        #region Constructors
        public Unit(SceneNode node) : this(node, Color.White) { }
        public Unit(SceneNode node, Color col) : this(node, col, UnitProperties.Wizard()) { }
        public Unit(SceneNode node, Color col, UnitProperties stat) : base(node) 
        {
            buffs = new List<Buff>();
            this.Color = col;
            this.stat = stat;
            node["unit"] = this;
            DeathCommand = new DieRemove();
        }
        #endregion

        #region Damage intake control
        public void TakeDamage(Damage damage)
        {
            // Filter the damage:
            foreach (Buff b in buffs)
                if (!b.Filter(ref damage))
                    return;

            float hp = stat.CurrentHP;

            // Apply the damage:
            stat.CurrentHP -= damage.Value;
            if(damage.KnockBack) IssueCommand(DamageCommand);

            if (hp > 0 && IsDead)
            {
                // kill and play sound
                Kill();
            }
            else if(HitSoundNames != null && HitSoundNames.Length != 0)
            {
                // simple damage sound
                SceneNode.AudioSource = AudioSource.RandomFromSoundNames(HitSoundNames, true);
            }
        }

        public void Kill()
        {
            if (DieSoundNames != null && DieSoundNames.Length != 0)
                SceneNode.AudioSource = AudioSource.RandomFromSoundNames(DieSoundNames, true);
            this.stat.CurrentHP = 0;

            IssueCommand(DeathCommand);
        }
        #endregion

        #region Buff control
        public void TakeBuff(Buff buff)
        {
            foreach (Buff b in buffs)
            {
                if (b.GetBuffType() == buff.GetBuffType())
                {
                    b.Stacked += 1;
                    return;
                }
                else if (!b.Filter(buff)) return;
            }
            buffs.Add(buff);
            buff.Begin(this);
        }

        /// <summary>
        /// Increase the stack of the buff.
        /// </summary>
        /// <param name="buff">buff to remove</param>
        /// <param name="amount">amount of stacks to add/remove</param>
        public void IncreaseBuffStack(Buff buff, int amount)
        {
            foreach (Buff b in buffs)
                if (b.GetBuffType() == buff.GetBuffType())
                    b.Stacked += amount;
        }

        /// <summary>
        /// Force the removal of a buff
        /// <param name="buff">Buff to remove</param>
        /// <returns>true if buff was found. false if buff wasn't found.</returns>
        /// </summary>
        public bool RemoveBuff(Buff buff)
        {
            bool found = buffs.Remove(buff);
            if (found) buff.End();
            return found;
        }

        private List<Buff> finishedBuffs = new List<Buff>();
        private void UpdateBuffs()
        {
            finishedBuffs.Clear();

            foreach (Buff b in buffs)
                if (!b.Update() || b.Stacked <= 0)
                    finishedBuffs.Add(b);

            foreach (Buff removeMe in finishedBuffs)
            {
                removeMe.End();
                buffs.Remove(removeMe);
            }
        }
        #endregion

        #region Command handling
        
        /// <summary>
        /// Issue a command. Overwrite current command if this new one
        /// is of higher priority.
        /// </summary>
        /// <param name="newCmd">The newly issued command</param>
        /// <returns>False if newCmd was ignored. True if newCmd has overwritten the
        /// old command</returns>
        public bool IssueCommand(Command newCmd)
        {
            if (newCmd == null) return false; // ignore useless commands.

            newCmd.Puppet = this;

            // Check if the new cmd can overwrite the current one
            if (cmd != null && cmd.Priority >= newCmd.Priority)
                return false; // no it can't. ignore this cmd.

            // Check if the cmd can begin (e.g. wizard is not oom).
            else if (newCmd.Begin())
            {
                if (cmd != null)
                {
                    cmd.Stop();
                }

                // Overwrite
                cmd = newCmd;
                return true;
            }

            return false;
        }

        private void EnterIdle()
        {
            cmd = null;
            if (IdleAnimations == IDLE_PAUSE_ANIMATION)
            {
                SceneNode.Animation.Pause = true;
            }
            else if (IdleAnimations != null)
            {
                SceneNode.Animation.Pause = false;
                SceneNode.Animation.BlendInto(IdleAnimations.AnimationNames[0], AnimationPlay.Loop, 300f);
            }
        }

        private bool UpdateIdle()
        {
            if (IdleAnimations == null) return true;
            // What to do in idle state (just toggle between animations at random)

            if (this.SceneNode.Animation.Looped && IdleAnimations != IDLE_PAUSE_ANIMATION)
            {
                AnimationClip newClip = SceneNode.Animation[ IdleAnimations.Roll() ];
                SceneNode.Animation.BlendInto(newClip, AnimationPlay.Loop, 2000f);
            }

            return true;
        }
        #endregion

        public bool Update()
        {
            if (!IsDeadOrDying)
            {
                UpdateBuffs();
                stat.Regenerate(GameTiming.TimeElapsed);
            }

            if (cmd == null)
                // No command issued. Await for orders (idle state)
                return UpdateIdle();

            bool b;
            if ((b = cmd.Update()) == false)
            {
                cmd.Stop();
                // The issued command has ended. Return to idle state.
                EnterIdle();
            }

            return b;
        }
    }
}
