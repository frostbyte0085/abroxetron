﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic
{
    public class UnitProperties
    {
        #region Variables
        private float hp, mp; // current hit/mana points as floats (for regen)

        private float base_maxhp, base_maxmp; // max hit/mana points
        private float base_hpreg, base_mpreg; // regen per second

        private float bonus_maxhp, bonus_maxmp;
        private float bonus_hpreg, bonus_mpreg;
        #endregion

        #region Properties (stored)
        public int CurrentHP
        {
            set
            {
                if (value < 0)
                    value = 0;
                else if (value > base_maxhp)
                    value = MaxHP;

                hp -= (int)hp; // keep floating point value (ditch int)
                hp += value;
            }
            get { return (int)hp; }
        }
        public int CurrentMana
        {
            set
            {
                if (value < 0)
                    value = 0;
                else if (value > base_maxmp)
                    value = MaxMana;

                mp -= (int)mp; // keep floating point value (ditch int)
                mp += value;
            }
            get { return (int)mp; }
        }

        public int BaseMaxHP
        {
            set
            {
                base_maxhp = value;
                maxHPChanged();
            }
            get { return (int)base_maxhp; }
        }
        public int BaseMaxMana
        {
            set
            {
                base_maxmp = value;
                maxManaChanged();
            }
            get { return (int)base_maxmp; }
        }

        public int BonusMaxHP
        {
            set
            {
                bonus_maxhp = value;
                maxHPChanged();
            }
            get { return (int)bonus_maxhp; }
        }
        public int BonusMaxMana
        {
            set
            {
                bonus_maxmp = value;
                maxManaChanged();
            }
            get { return (int)bonus_maxmp; }
        }

        public float BaseHPRegen { set { base_hpreg = value; } get { return base_hpreg; } }
        public float BaseManaRegen { set { base_mpreg = value; } get { return base_mpreg; } }

        public float BonusHPRegen { set { bonus_hpreg = value; } get { return bonus_hpreg; } }
        public float BonusManaRegen { set { bonus_mpreg = value; } get { return bonus_mpreg; } }
        #endregion

        #region Properties (calculated)
        public int MaxHP { get { return (int)(base_maxhp + bonus_maxhp); } }
        public int MaxMana { get { return (int)(base_maxmp + bonus_maxmp); } }

        public float HPRegen { get { return base_hpreg + bonus_hpreg; } }
        public float ManaRegen { get { return base_mpreg + bonus_mpreg; } }

        public float HPPercent { get { return ((float)hp) / ((float)base_maxhp); } }
        public float ManaPercent { get { return ((float)mp) / ((float)base_maxmp); } }
        #endregion

        #region Functions
        private void maxManaChanged()
        {
            float percent = MaxMana == 0 ? 0.0f : ((float)mp) / ((float)MaxMana);
            mp *= percent; // calc new current mana and round
        }
        private void maxHPChanged()
        {
            float percent = MaxHP == 0 ? 0.0f : ((float)hp) / ((float)MaxHP);
            hp *= percent; // calc new current hp and round
        }

        /// <summary>
        /// Regenerate some mana and hitpoint over time.
        /// </summary>
        /// <param name="time">Time elapsed in seconds</param>
        public void Regenerate(float time)
        {
            hp += HPRegen * time;
            mp += ManaRegen * time;

            if (hp > MaxHP) hp = MaxHP;
            if (mp > MaxMana) mp = MaxMana;
        }
        #endregion

        #region Static values
        public static UnitProperties Wizard()
        {
            return new UnitProperties()
            {
                base_maxhp = 100.0f,
                base_maxmp = 100.0f,
                hp = 100.0f,
                mp = 100.0f,
                base_hpreg = 0.5f,
                base_mpreg = 3.0f
            };
        }

        public static UnitProperties Skeleton()
        {
            return new UnitProperties()
            {
                base_maxhp = 50.0f,
                base_maxmp = 0.0f,
                hp = 50.0f,
                mp = 0.0f,
                base_hpreg = 0.1f,
                base_mpreg = 0.0f
            };
        }

        public static UnitProperties Dragon()
        {
            // TODO:
            return new UnitProperties()
            {
                base_maxhp = 500.0f,
                base_maxmp = 500.0f,
                hp = 500.0f,
                mp = 500.0f,
                base_hpreg = 0.5f,
                base_mpreg = 3.0f
            };
        }
        #endregion
    }
}
