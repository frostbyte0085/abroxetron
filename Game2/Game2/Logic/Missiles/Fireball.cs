﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Physics;
using Engine.Graphics;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Missiles
{
    public class Fireball : ParticleMissile
    {
        private static ParticleAnimation __fire = null;
        private static ParticleAnimation __explode = null;

        private static ParticleAnimation fireAnim
        {
            get
            {
                if (__fire == null)
                    __fire = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/Fireball.xml");
                return __fire;
            }
        }
        protected override ParticleAnimation anim { get { return fireAnim; } }

        private ParticleAnimation explodeAnim
        {
            get
            {
                if (__explode == null)
                    __explode = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/FireballExplode.xml");
                return __explode;
            }
        }

        public Fireball(SceneNode node, float duration)
            : base(
            node,
            duration,
            //new PhysicalEntity(new Vector3(0,1,0), new PhysicalSphereEntityProperties(0.2f), 0.0f, true),
            new PhysicalEntity(new Vector3(0,1,0), new PhysicalCylinderEntityProperties(0.2f, 0.6f), 0.0f, true),
            new PointLight(Color.OrangeRed.ToVector4(), 2.8f, 3))
        {  }
        
        /// <summary>
        /// Invoke this when the missile hits a unit
        /// </summary>
        /// <param name="unit"></param>
        public override void DetonateOn(Unit unit)
        {
            life = duration + 1;
            unit.TakeDamage(new Damage(50, DamageType.Magical));
        }
        public override void DetonateAgainOn(Unit unit) { }
        public override void UndetonateOn(Unit unit) { }

        private bool exploding = false;
        public override void Explode()
        {
            // only explode once:
            if (exploding) return; exploding = true;

            SceneService ss = ServiceProvider.GetService<SceneService>();
            SceneNode node = ss.AddSceneNode(this.SceneNode.Name + "_explosion");
            node.Translation = this.SceneNode.Translation;
            ParticleSystem ps = new ParticleSystem(explodeAnim);
            node.Attach("node", ps);
            ps.StartEmitting();
            Crash();
        }
        /// <summary>
        /// Invoke this when the missile collides with something
        /// other than a unit.
        /// </summary>
        public override void Crash()
        {
            life = duration + 1;
        }

        /*
        public override bool Update(MissileBehaviour behave)
        {
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();
            ParticleSystem ps;
            this.SceneNode.GetAttachment("node", out ps);
            drs.AddSphere(ps.GetBoundingSphere(), Color.Red);
            return base.Update(behave);
        }*/
    }
}
