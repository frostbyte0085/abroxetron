﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Physics;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.Logic.Missiles
{
    public class RockMissile : Missile
    {
        private static Model __rock = null;
        private static Model rock
        {
            get
            {
                if (__rock == null)
                {
                    ContentService cs = ServiceProvider.GetService<ContentService>();
                    __rock = cs["game"].Load<Model>("Models/Props/RockMissile");
                }
                return __rock;
            }
        }

        private const float dmgPerMperS = 15;
        private const float maxDuration = 5;
        private float birth;

        public RockMissile(SceneNode node, Vector3 velocity)
            : base(node)
        {
            node.Model = rock;
            node.PhysicalEntity = new PhysicalEntity(node, new PhysicalSphereEntityProperties(), 20.0f, false);
            node.PhysicalEntity.LinearVelocity = velocity;
            node.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("Missile");

            birth = GameTiming.TotalTimeElapsed;
        }

        private float speed()
        {
            return SceneNode.PhysicalEntity.LinearVelocity.Length();
        }

        public override void DetonateOn(Unit unit)
        {
            Damage dmg = new Damage((int)(speed() * dmgPerMperS), DamageType.Physical);
            unit.TakeDamage(dmg);
        }

        public override void DetonateAgainOn(Unit unit) { }
        public override void UndetonateOn(Unit unit) { }
        public override void Crash() { }

        public override void Explode() { }

        public override bool Update(MissileBehaviour behave)
        {
            return 
                SceneNode.PhysicalEntity.LinearVelocity.Length() > 0.001 &&
                GameTiming.TotalTimeElapsed - birth <= maxDuration;
        }
    }
}
