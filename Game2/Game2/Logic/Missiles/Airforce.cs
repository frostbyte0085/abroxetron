﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Game2.Logic.Behaviours;
using Game2.Logic.Buffs;
using Game2.Logic.Commands;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Missiles
{
    /// <summary>
    /// Immobile missile.
    /// </summary>
    public class Airforce : ParticleMissile
    {
        private static ParticleAnimation __air = null;

        private static ParticleAnimation airAnim
        {
            get
            {
                if (__air == null)
                    __air = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/AirPush.xml");
                return __air;
            }
        }
        protected override ParticleAnimation anim { get { return airAnim; } }

        private Vector3 direction;
        private float power;

        public Airforce(SceneNode node, float duration, Vector3 force)
            : base(
            node,
            duration,
            new PhysicalEntity(Vector3.Zero, new PhysicalSphereEntityProperties(0.7f), 0, true),
            new PointLight(Color.Gray.ToVector4(), 0.2f, 4f))
        {
            power = force.Length();
            direction = force;
            direction.Normalize();
        }

        /// <summary>
        /// Invoke this when the missile hits a unit
        /// </summary>
        /// <param name="unit"></param>
        public override void DetonateOn(Unit unit) 
        {
            //if (unit.SceneNode.Behaviour != null && unit.SceneNode.Behaviour is CharacterSceneNodeBehaviour)
            //{
            //    CharacterSceneNodeBehaviour be = (CharacterSceneNodeBehaviour)unit.SceneNode.Behaviour;
            //}
            unit.IssueCommand(new PushMove(direction*power));
        }
        public override void DetonateAgainOn(Unit unit) { }
        public override void UndetonateOn(Unit unit) { }
        public override void Crash() { }

        public override void Explode() { }

        public override bool Update(MissileBehaviour behave)
        {
            // update behaviour... :?
            return base.Update(behave);
        }
    }
}
