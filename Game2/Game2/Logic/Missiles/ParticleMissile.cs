﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Physics;
using Engine.Scene;
using Engine.Graphics;

using Game2.Logic.Buffs;
using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Missiles
{
    public abstract class ParticleMissile : Missile
    {
        protected abstract ParticleAnimation anim {get;}

        protected ParticleSystem ps;

        protected float life, duration;
        protected float lightintensity;

        public ParticleMissile(SceneNode node, float duration, PhysicalEntity entity, ILight light)
            : base(node)
        {
            this.life = 0.0f;
            this.duration = duration;
            this.ps = new ParticleSystem(anim);
            this.lightintensity = light.Intensity;

            node.ParticleAttachMode = ParticleAttachMode.Default;
            node.Attach("node", ps);

            ps.ParticleSystemSpaceMode = ParticleSystemSpaceMode.WorldSpace;
            ps.StartEmitting();

            entity.CollisionGroup = CustomCollisionGroups.Get("Missile");
            node.PhysicalEntity = entity;
            node.Light = light;
        }

        public override abstract void DetonateOn(Unit unit);
        public override abstract void DetonateAgainOn(Unit unit);
        public override abstract void UndetonateOn(Unit unit);
        public override abstract void Crash();

        /// <summary>
        /// Invoke additional updates (e.g. checking time constraints).
        /// </summary>
        /// <param name="behave">Describes how the missile moves. Can be changed / used
        /// however wanted.</param>
        /// <returns>True is still travelling. False if the missile crashed / died / whatever.</returns>
        public override bool Update(MissileBehaviour behave)
        {
            this.life += GameTiming.TimeElapsed;
            if (life > duration)
            {
                if (ps.IsEmitting)
                {
                    ps.StopEmitting();
                    behave.Velocity = Vector3.Zero;
                }
                else
                {
                    // Fade light:
                    float overlife = life - duration;
                    float overduration = ps.Animation.MinEnergy;

                    float percent = 1.0f - (overlife / overduration);
                    if (percent < 0.0f) percent = 0.0f;

                    SceneNode.Light.Intensity = lightintensity * percent;
                }

                if (!ps.IsAlive)
                    return false;
            }

            return true;
        }
    }
}
