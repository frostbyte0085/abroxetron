﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;
using Engine.Tools;
using Engine.Physics;

using Game2.Logic.Behaviours;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Missiles
{
    public abstract class Missile : GameObject
    {
        public static int sceneNodeNameId = 0;

        public Missile(SceneNode node)
            : base(node) 
        {
            if(this.SceneNode.PhysicalEntity != null)
                this.SceneNode.PhysicalEntity.CollisionGroup = CustomCollisionGroups.Get("Missile");
        }

        /// <summary>
        /// (CollideEnter) Invoke this when the missile hits a unit
        /// </summary>
        /// <param name="unit">unit to detonate on</param>
        public abstract void DetonateOn(Unit unit);

        /// <summary>
        /// (CollideStay) Invoke this when the missile is still colliding with a unit
        /// </summary>
        /// <param name="unit">unit to detonate on</param>
        public abstract void DetonateAgainOn(Unit unit);

        /// <summary>
        /// (CollideExit) Invoke this when the missile has stopped colliding with a unit.
        /// </summary>
        /// <param name="unit">unit to un-detonate on</param>
        public abstract void UndetonateOn(Unit unit);

        /// <summary>
        /// Called when the missile collides with something and needs to explode.
        /// Most missiles do nothing here, as only the fireball explodes into flames!
        /// </summary>
        public abstract void Explode();

        /// <summary>
        /// Invoke this when the missile collides with something
        /// other than a unit.
        /// </summary>
        public abstract void Crash();

        /// <summary>
        /// Invoke additional updates (e.g. checking time constraints).
        /// </summary>
        /// <param name="behave">Describes how the missile moves. Can be changed / used
        /// however wanted.</param>
        /// <returns>True is still travelling. False if the missile crashed / died / whatever.</returns>
        public abstract bool Update(MissileBehaviour behave);

        public static void Fire(Missile missile, Unit shooter, float speed)
        {
            Fire(missile, shooter, speed, true);
        }
        public static void Fire(Missile missile, Unit shooter, float speed, bool ignoreSource)
        {
            Vector3 spawn = shooter.SceneNode.Translation;
            Vector3 direction = shooter.SceneNode.Forward * speed;

            //Vector3 angles = Math2.QuaternionToEuler(shooter.SceneNode.Orientation);
            //float yaw = angles.Y;
            //Vector3 direction = new Vector3((float)Math.Cos(yaw) * speed, 0, (float)Math.Sin(yaw) * speed);

            MissileBehaviour behave = new MissileBehaviour(missile, direction, spawn);
            missile.SceneNode.Behaviour = behave;

            if(ignoreSource) missile.SceneNode["missile_source"] = shooter;
            missile.SceneNode["missile"] = missile;
        }
    }
}
