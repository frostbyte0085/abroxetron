﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Graphics;
using Engine.Physics;

using Game2.Logic.Behaviours;
using Game2.Logic.Buffs;

using Microsoft.Xna.Framework;

namespace Game2.Logic.Missiles
{
    /// <summary>
    /// Immobile missile.
    /// </summary>
    public class WaterFountain : ParticleMissile
    {
        private static ParticleAnimation __water = null;

        private static ParticleAnimation waterAnim
        {
            get
            {
                if (__water == null)
                    __water = ParticleAnimation.CreateParticleAnimation("GameContent/Particles/Systems/WaterFountain.xml");
                return __water;
            }
        }
        protected override ParticleAnimation anim { get { return waterAnim; } }

        private List<FountainHealBuff> buffcreated;
        private static FountainHealBuff sampleBuff = new FountainHealBuff();

        public WaterFountain(SceneNode node, float duration)
            : base(
            node,
            duration, 
            new PhysicalEntity(Vector3.Zero, new PhysicalSphereEntityProperties(2.15f), 0.0f, true),
            new PointLight(Color.LightBlue.ToVector4(), 2f, 3f))
        {
            buffcreated = new List<FountainHealBuff>();
        }

        /// <summary>
        /// Invoke this when the missile hits a unit
        /// </summary>
        /// <param name="unit"></param>
        public override void DetonateOn(Unit unit)
        {
            FountainHealBuff b = new FountainHealBuff();
            unit.TakeBuff(b);
            buffcreated.Add(b);
        }

        public override void DetonateAgainOn(Unit unit) {}
        public override void UndetonateOn(Unit unit)
        {
            // Don't remove it yet! The wizard might exiting one fountain
            // and entering another. so we should keep the buff. If it's
            // 0  the unit will remove it on update().
            unit.IncreaseBuffStack(sampleBuff, -1);
        }

        public override void Crash() { }

        public override void Explode() { }
    }
}
