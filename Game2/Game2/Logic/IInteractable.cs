﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.Logic
{
    public interface IInteractable
    {
        bool InteractWith(Unit interacter);
    }
}
