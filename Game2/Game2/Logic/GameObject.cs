﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Scene;

namespace Game2.Logic
{
    public class GameObject
    {
        private readonly SceneNode node;

        public SceneNode SceneNode { get { return node; } }

        public GameObject(SceneNode node)
        {
            if (node == null) throw new ArgumentNullException("Game object must have a scene node");
            this.node = node;
        }
    }
}