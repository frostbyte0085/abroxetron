﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

using Game2.Level;
using Game2.Logic;
using Game2.Logic.Behaviours;
using Game2.Logic.Commands;
using Game2.UI;

using BEPUphysics;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionRuleManagement;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BEPUphysics.MathExtensions;

using Engine.Core;
using Engine.Graphics;
using Engine.Scene;
using Engine.Audio;
using Engine.Input;
using Engine.Physics;
using Engine.Tools;
using Engine.UI;

using System.Diagnostics;

namespace Game2.GameManager
{
    public enum LevelType
    {
        None, // this is used when the player is entering the game for the first time.
        MainHalls,
        Fire,
        Water,
        Air,
        Earth
    }

    public enum OrbType
    {
        Fire,
        Water,
        Air,
        Earth
    }

    public abstract class BaseGameManager
    {
        #region LevelSpawnLink
        // a class to keep track of the level spawn point links
        private class LevelSpawnLink
        {
            public LevelSpawnLink(LevelType currentLevel, LevelType nextLevel, string spawnName)
            {
                this.CurrentLevel = currentLevel;
                this.NextLevel = nextLevel;
                this.SpawnName = spawnName;
            }

            public LevelType CurrentLevel
            {
                set;
                get;
            }

            public LevelType NextLevel
            {
                set;
                get;
            }

            public string SpawnName
            {
                set;
                get;
            }
        }
        #endregion

        #region debug facilities
        public void DebugDragon()
        {
#if DEBUG_DRAGON
            // unlock all spells
            unlockedSpells.Add(SpellOfLevel(LevelType.Fire));
            assignedSpells.Add(PlayerIndex.One);

            unlockedSpells.Add(SpellOfLevel(LevelType.Air));
            assignedSpells.Add(PlayerIndex.One);
            
            unlockedSpells.Add(SpellOfLevel(LevelType.Water));
            assignedSpells.Add(PlayerIndex.One);

            unlockedSpells.Add(SpellOfLevel(LevelType.Earth));
            assignedSpells.Add(PlayerIndex.One);

            EnterDragonMode();
#endif
        }
        #endregion

        #region Dragon state
        public bool IsDragonAwake
        {
            set;
            get;
        }

        public bool InDragonMode
        {
            private set;
            get;
        }

        private void EnterDragonMode()
        {
            // no more enemies please
            enemyManager.Enabled = false;

            // convert the triggers into obstacles, the players shouldn't go back into the other levels to hide from the dragon
            SceneService ss = ServiceProvider.GetService<SceneService>();

            List<SceneNode> triggers = currentLevel.TriggerNodes;

            for (int i = 0; i < triggers.Count; i++)
            {
                SceneNode node = triggers[i];

                PhysicalEntity entity = node.PhysicalEntity;
                Box bepuEntity = entity.BEPUEntity as Box;

                node.PhysicalEntity = null;
                node.PhysicalEntity = new PhysicalEntity(new Vector3(0,0,0), new PhysicalBoxEntityProperties(bepuEntity.Width, bepuEntity.Height, bepuEntity.Length), 0, false);
            }

            InDragonMode = true;
        }

        #endregion

        #region Const Strings
        const string fireFilename = "Fire";
        const string waterFilename = "Water";
        const string earthFilename = "Earth";
        const string airFilename = "Air";
        const string mainHallsFilename = "MainHalls";


        const string fireOrbName = "FireOrb";
        const string waterOrbName = "WaterOrb";
        const string earthOrbName = "EarthOrb";
        const string airOrbName = "AirOrb";

        const string gameEnterSpawnName = "GameEnterSpawnPoint";
        const string airSpawnName = "AirSpawnPoint";
        const string waterSpawnName = "WaterSpawnPoint";
        const string earthSpawnName = "EarthSpawnPoint";
        const string fireSpawnName = "FireSpawnPoint";
        const string fromAirSpawnName = "FromAirSpawnPoint";
        const string fromEarthSpawnName = "FromEarthSpawnPoint";
        const string fromFireSpawnName = "FromFireSpawnPoint";
        const string fromWaterSpawnName = "FromWaterSpawnPoint";
        #endregion

        #region Private variables
        private LevelParser currentLevel;
        private GraphicsService graphics;
        private ContentService content;
        private SceneService scene;

        private List<KeyValuePair<LevelType, string>> levelFilenames;
        private List<KeyValuePair<LevelType, string>> levelMusicFilenames;
        private List<LevelSpawnLink> levelSpawnLinks;
        private List<KeyValuePair<LevelType, string>> levelDefaultSpawns;

        private LevelType currentLevelType;
        private Vector3 selectedPlayersSpawn;

        private PlayerIndex[] playerIndices;
        private List<Unit> currentPlayers;
        private GuardianBehaviour kinectPlayer;
        private DragonBehaviour dragonBehaviour;

        private List<SpellType> unlockedSpells;
        private List<PlayerIndex> assignedSpells;

        private EnemyManager enemyManager;
        /// <summary>
        /// The dragon! null if currentLevel isn't the MainHalls
        /// </summary>
        private Unit dragon;
        #endregion

        #region Properties
        public SpellType SpellOfLevel(LevelType levelType)
        {
            switch (levelType)
            {
                case LevelType.Air:
                    return SpellType.Air;

                case LevelType.Earth:
                    return SpellType.Earth;

                case LevelType.Fire:
                    return SpellType.Fire;

                case LevelType.Water:
                    return SpellType.Water;

                default:
                    return SpellType.NONE;
            }
        }

        public LevelType LevelOfSpell(SpellType spellType)
        {
            switch (spellType)
            {
                case SpellType.Air:
                    return LevelType.Air;

                case SpellType.Earth:
                    return LevelType.Earth;

                case SpellType.Fire:
                    return LevelType.Fire;

                case SpellType.Water:
                    return LevelType.Water;

                default:
                    return LevelType.None;
            }
        }

        public abstract string Name { get; }

        public LevelType CurrentLevelType
        {
            get
            {
                return currentLevelType;
            }
        }

        public IEnumerator<Unit> Players { get { return currentPlayers.GetEnumerator(); } }

        public List<SceneNode> PlayersAsNodes { 
            get
            {
                List<SceneNode> list = new List<SceneNode>();
                foreach (Unit u in currentPlayers)
                    list.Add(u.SceneNode);
                return list;
            }
        }

        public DragonBehaviour Dragon { get { return this.dragonBehaviour; } }

        public GuardianBehaviour KinectPlayer { get { return this.kinectPlayer; } }

        public Vector3 AveragePosition()
        {
            Vector3 avgPos = Vector3.Zero;
            int n = 1;

            IEnumerator<Unit> players = GameApplication.App.GameManager.Players;
            while (players.MoveNext())
            {
                if (players.Current.IsDead) continue;
                n++;
                avgPos += players.Current.SceneNode.Translation;
            }
            avgPos += kinectPlayer.SceneNode.Translation;

            return avgPos / n;
        }
        #endregion

        #region Constructor
        public BaseGameManager()
        {
            currentLevel = new LevelParser();

            graphics = ServiceProvider.GetService<GraphicsService>();
            Debug.Assert(graphics != null);

            scene = ServiceProvider.GetService<SceneService>();
            Debug.Assert(scene != null);

            content = ServiceProvider.GetService<ContentService>();
            Debug.Assert(content != null);

            PrepareLevelFilenames();
            PrepareLevelDefaultSpawns();
            PrepareLevelLinks();
            PrepareLevelMusicFilenames();

            currentLevelType = LevelType.None;

            currentPlayers = new List<Unit>();
            unlockedSpells = new List<SpellType>();
            assignedSpells = new List<PlayerIndex>();

            enemyManager = new EnemyManager();
        }
        #endregion

        #region Init Level Data
        // the default level spawn points are used for when the game is just starting or loading,
        // and there is no "current" level to transition from.
        private void PrepareLevelDefaultSpawns()
        {
            levelDefaultSpawns = new List<KeyValuePair<LevelType, string>>();

            levelDefaultSpawns.Add(new KeyValuePair<LevelType, string>(LevelType.Air, airSpawnName));
            levelDefaultSpawns.Add(new KeyValuePair<LevelType, string>(LevelType.Water, waterSpawnName));
            levelDefaultSpawns.Add(new KeyValuePair<LevelType, string>(LevelType.Earth, earthSpawnName));
            levelDefaultSpawns.Add(new KeyValuePair<LevelType, string>(LevelType.Fire, fireSpawnName));
            levelDefaultSpawns.Add(new KeyValuePair<LevelType, string>(LevelType.MainHalls, gameEnterSpawnName));
        }

        // which levels are we loading based on the level type?
        private void PrepareLevelFilenames()
        {
            levelFilenames = new List<KeyValuePair<LevelType, string>>();

            levelFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Air, airFilename));
            levelFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Water, waterFilename));
            levelFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Fire, fireFilename));
            levelFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Earth, earthFilename));
            levelFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.MainHalls, mainHallsFilename));
        }

        // which levels are we loading based on the level type?
        private void PrepareLevelMusicFilenames()
        {
            levelMusicFilenames = new List<KeyValuePair<LevelType, string>>();

            levelMusicFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Air, "Music/swords_crossed"));
            levelMusicFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Water, "Music/swords_crossed"));
            levelMusicFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Fire, "Music/swords_crossed"));
            levelMusicFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.Earth, "Music/swords_crossed"));
            levelMusicFilenames.Add(new KeyValuePair<LevelType, string>(LevelType.MainHalls, "Music/gloom"));
        }

        // this method will setup the links between current level, the level to be loaded, and their corresponding spawn points.
        // the spawn names correspond to the next level ones
        private void PrepareLevelLinks()
        {
            levelSpawnLinks = new List<LevelSpawnLink>();
            // when the player enters the game
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.None, LevelType.MainHalls, gameEnterSpawnName));

            // main halls links
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Air, airSpawnName));
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Earth, earthSpawnName));
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Water, waterSpawnName));
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Fire, fireSpawnName));
            // air links
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Air, LevelType.MainHalls, fromAirSpawnName));
            // water links
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Water, LevelType.MainHalls, fromWaterSpawnName));
            // fire links
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Fire, LevelType.MainHalls, fromFireSpawnName));
            // earth links
            levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Earth, LevelType.MainHalls, fromEarthSpawnName));
        }
        #endregion

        #region Query Level data
        // gets the proper spawn point from the current level to the new level.
        // if the currentLevel is null (game is just starting), then the spawn point
        // is set to that of the level
        private Vector3 GetSpawnPointForLevel(LevelType nextLevel)
        {
            // there is no current level type.
            // note: the currentLevel here is already loaded, as we get the spawn point *after* loading the level. However, the
            // currentLevelType is set to None in the class constructor, so the following code is valid.
            if (currentLevelType == LevelType.None)
            {
                foreach (KeyValuePair<LevelType, string> lt in levelDefaultSpawns)
                {
                    if (lt.Key == nextLevel)
                        return currentLevel.GetSpawnPoint(lt.Value);
                }
            }

            // currentLevelType is something other than None, so we must consult our nice map
            // to find the spawn point of the next level.
            foreach (LevelSpawnLink sl in levelSpawnLinks)
            {
                if (sl.CurrentLevel == CurrentLevelType && sl.NextLevel == nextLevel)
                    return currentLevel.GetSpawnPoint(sl.SpawnName);
            }
            // that sucked...
            return Vector3.Zero;
        }

        private string GetLevelFilename(LevelType level)
        {
            foreach (KeyValuePair<LevelType, string> l in levelFilenames)
            {
                if (l.Key == level)
                    return l.Value;
            }
            return "";
        }

        private string GetLevelMusicFilename(LevelType level)
        {
            foreach (KeyValuePair<LevelType, string> l in levelMusicFilenames)
            {
                if (l.Key == level)
                    return l.Value;
            }
            return "";
        }

        public string LevelToString(LevelType level)
        {
            switch (level)
            {
                case LevelType.Air:
                    return "Air";
                case LevelType.Earth:
                    return "Earth";
                case LevelType.Fire:
                    return "Fire";
                case LevelType.MainHalls:
                    return "Main Halls";
                case LevelType.Water:
                    return "Water";
                case LevelType.None:
                    return "Empty Level";
            }
            return "Invalid Type";
        }

        #endregion

        #region Query global game state
        public SpellType GetNextSpell(SpellType selected, PlayerIndex who)
        {
            if (selected == SpellType.NONE)
            {
                int i = 0;
                foreach (PlayerIndex pi in assignedSpells)
                    if (pi == who) return unlockedSpells[i];
                    else i++;
            }
            else
            {
                int i = unlockedSpells.IndexOf(selected);
                if (i != -1 && assignedSpells[i] == who)
                {
                    while (true)
                    {
                        i = (i + 1) % unlockedSpells.Count;
                        if (assignedSpells[i] == who) return unlockedSpells[i];
                    }
                }
            }
            return SpellType.NONE;
        }

        public SpellType GetPreviousSpell(SpellType selected, PlayerIndex who)
        {
            if (selected == SpellType.NONE)
            {
                int i = 0;
                foreach (PlayerIndex pi in assignedSpells)
                    if (pi == who) return unlockedSpells[i];
                    else i++;
            }
            else
            {
                int i = unlockedSpells.IndexOf(selected);
                if (i != -1 && assignedSpells[i] == who)
                {
                    while (true)
                    {
                        i--; i = (i == -1) ? unlockedSpells.Count - 1 : i;
                        if (assignedSpells[i] == who) return unlockedSpells[i];
                    }
                }
            }
            return SpellType.NONE;
        }
        #endregion

        #region Starting up
        private Unit makeNewPlayer(PlayerIndex i, Color col)
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            SceneNode node = scene.AddSceneNode("player" + i);
            Unit wizard = new Unit(node, col);
            node.Behaviour = new WizardBehaviour(wizard, i);

            return wizard;
        }

        private void makeKinectPlayer()
        {
            SceneNode guardianNode = scene.AddSceneNode("guardian");
            Unit guardian = new Unit(guardianNode);
            kinectPlayer = new GuardianBehaviour(guardian);
            guardianNode.Behaviour = kinectPlayer;
        }

        private void makeUI(PlayerIndex[] whosPlaying, Color[] allColors)
        {
            // get services
            ContentService cs = ServiceProvider.GetService<ContentService>();
            UIRendererService urs = ServiceProvider.GetService<UIRendererService>();

            // load content
            Microsoft.Xna.Framework.Graphics.Texture2D texture
                = cs["game"].Load<Microsoft.Xna.Framework.Graphics.Texture2D>("UI/UITextureAtlas");
            UIAtlas atlas = UIAtlas.LoadAtlas(texture, "GameContent/UI/UITextureAtlas.xml");

            urs.CurrentLayer = new IngameUI(atlas, this.currentPlayers, this.kinectPlayer);

            Color[] cols = new Color[whosPlaying.Length];
            for(int i =0 ; i<whosPlaying.Length;i++)
                cols[i] = allColors[(int)whosPlaying[i]];
            collectUI = new SpellAssignUI(atlas, whosPlaying, cols);
        }

        public bool Active { set; get; }

        public void Clear()
        {
            scene.Clear();
            currentPlayers.Clear();
            Active = false;
        }

        // begin the game! this is called only at the start of a new game
        public void Begin(PlayerIndex[] whosPlaying)
        {
            Clear();
            // Init players
            Color[] cols = { Color.Red, Color.Blue, Color.Green, Color.Yellow };
            foreach (PlayerIndex i in whosPlaying)
            {
                this.playerIndices = whosPlaying;
                Unit wizard = makeNewPlayer(i, cols[(int)i]);
                currentPlayers.Add(wizard);
            }
            // Init kinect player
            makeKinectPlayer();

            // Make the UI
            makeUI(whosPlaying, cols);

            // TODO load checkpoint:
            signaledTransition = true;
            nextLevel = LevelType.MainHalls;

            enemyManager.maxSkeletons = 2 + whosPlaying.Length * 3;
            Active = true;
        }
        #endregion

        #region Transition
        private bool signaledTransition;
        private LevelType nextLevel;

        public void Transition(LevelType level)
        {
            Console.WriteLine("Signaled level transition to " + LevelToString(level));

            ClearGameScene();
            signaledTransition = true;
            nextLevel = level;
        }

        private void removePickedupOrb()
        {
            SpellType spell = SpellOfLevel(nextLevel);

            // has this orb already been unlocked?
            if (unlockedSpells.IndexOf(spell) != -1)
            {
                string orbname;
                switch (spell)
                {
                    case SpellType.Air: orbname = airOrbName; break;
                    case SpellType.Earth: orbname = earthOrbName; break;
                    case SpellType.Fire: orbname = fireOrbName; break;
                    case SpellType.Water: orbname = waterOrbName; break;
                    default: orbname = null; break;
                }

                scene.RemoveSceneNode("OrbLight");
                scene.RemoveSceneNode(orbname);
            }
        }

        private void DoTransition()
        {
            if (ambientSoundNode != null && ambientSoundNode.AudioSource != null)
            {
                ambientSoundNode.AudioSource.Stop();
                ambientSoundNode.AudioSource = null;
            }

            currentLevel.MakeLevel(GetLevelFilename(nextLevel));
            removePickedupOrb();

            LoadDragon();
            LoadMusic();
            SpawnPlayers();
            LoadAI(nextLevel);
            SetupGameCamera();
            currentLevelType = nextLevel;

            SceneService ss = ServiceProvider.GetService<SceneService>();
            ambientSoundNode = ss.AddSceneNode("ambientSoundNode");

            Console.WriteLine ("Entered level " + LevelToString(nextLevel));

#if !DEBUG_DRAGON
            if (unlockedSpells.Count == 4)
            {
                // unlock the dragon here
                EnterDragonMode();
            }
#else
            DebugDragon();
#endif
        }

        private void LoadMusic()
        {
            MediaPlayer.Stop();

            ContentService cs = ServiceProvider.GetService<ContentService>();

            string songName = "Music/" + LevelToString(nextLevel);
            Song levelSong = cs["game"].Load<Song>(songName);
            MediaPlayerService mps = ServiceProvider.GetService<MediaPlayerService>();
            mps.Play(levelSong);
        }

        /// <summary>
        /// Translate the players to where they should be at the start of the 
        /// transition. Do not create new players, only moves the current players.
        /// </summary>
        private void SpawnPlayers()
        {
            // Get spawn points.
            selectedPlayersSpawn = GetSpawnPointForLevel(nextLevel);

            // Prepare offsets from this point (so that wizards don't cluster at 1 point).
            float x = 0.7f; // seperation from spawn pnt.
            Vector3[] offs = { new Vector3(x, 0, 0), new Vector3(-x, 0, 0), new Vector3(0, 0, x), new Vector3(0, 0, -x) };

            // Set spawn pnts
            int i = 0; // index of which offset.
            foreach (Unit wizard in currentPlayers)
            {
                wizard.SceneNode.PhysicalEntity.LinearVelocity = Vector3.Zero;
                wizard.SceneNode.Translation = selectedPlayersSpawn + offs[i++];
            }

            kinectPlayer.Guardian.SceneNode.PhysicalEntity.LinearVelocity = Vector3.Zero;
            kinectPlayer.Guardian.SceneNode.Translation = selectedPlayersSpawn;
        }

        private void LoadAI(LevelType loadMe)
        {
            List<SceneNode> levels = scene.GetSceneNodesInCategoryGroup("Level");
            enemyManager.Start(loadMe, levels[0].Translation, currentLevel.GetSkeletonSpawnPoints().ToArray());
        }

        // clears the game scene and prepares for a transition to a new level
        private void ClearGameScene()
        {
            List<SceneNode> excluded = new List<SceneNode>();
            foreach (Unit u in currentPlayers)
                excluded.Add(u.SceneNode);
            excluded.Add(kinectPlayer.SceneNode);
            scene.Clear(excluded);
        }

        private void LoadDragon()
        {
            if (nextLevel != LevelType.MainHalls) {
                this.dragon = null;
                return;
            }

            SceneNode dragonNode = scene.AddSceneNode("abroxetron");
            
            dragonBehaviour = new DragonBehaviour(currentLevel.GetSpawnPoint("DragonSpawn"));
            dragonNode.Behaviour = dragonBehaviour;
            this.dragon = dragonBehaviour.DragonUnit;
        }
        #endregion

        #region Orb Collection
        private SpellAssignUI collectUI;
        private bool signaledCollect = false;
        private UILayer oldLayer = null;

        /// <summary>
        /// Invoked by an orb when a collision happens
        /// </summary>
        public void CollectMe(SceneNode orb)
        {
            signaledCollect = true;

            UIRendererService urs = ServiceProvider.GetService<UIRendererService>();

            // Remove the un-needed orbs:
            scene.RemoveSceneNode(orb);
            scene.RemoveSceneNode("OrbLight");

            GameTiming.Paused = true;

            oldLayer = urs.CurrentLayer;
            urs.CurrentLayer = collectUI;
            collectUI.Reset();
        }
        #endregion

        #region Camera work
        Camera mainCamera;
        SceneNode mainCameraNode;

        public SceneNode CameraNode
        {
            get
            {
                return mainCameraNode;
            }
        }

        // adjust the camera based on the spawn point
        private void SetupGameCamera()
        {
            // make a new perspective projection camera
            mainCamera = new Camera();
            mainCamera.SetupPerspective(45.0f, graphics.GraphicsDevice.Viewport.AspectRatio, 0.1f, 2000.0f);

            // create our MainCamera node
            mainCameraNode = scene.AddSceneNode("MainCamera");
            mainCameraNode.Camera = mainCamera;
            mainCameraNode.Translation = new Vector3(0, 25, 0);
            mainCameraNode.Orientation = Quaternion.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-85), 0);
            mainCameraNode.Behaviour = new CameraBehaviour();

            // set it as the active one
            scene.ActiveCameraNode = mainCameraNode;
        }
        #endregion

        #region Global "sun" light
        DirectionalLight sunLight;
        SceneNode sunLightNode;

        public SceneNode SunLightNode
        {
            get
            {
                return sunLightNode;
            }
        }

        private void SetupSunLight()
        {
            // make a global light to spicen things up a bit :)
            sunLight = new DirectionalLight(Vector4.One, 0.08f);
            sunLightNode = scene.AddSceneNode("sunLight");
            //sunLightNode.Light = sunLight;
            //sunLightNode.Rotate(new Vector3(1, 0, 0), -75);

        }
        #endregion

        #region Update

        private SceneNode ambientSoundNode;

        private void PlayLevelAmbientSounds()
        {
            if (ambientSoundNode == null)
                return;

            switch (currentLevelType)
            {
                case LevelType.Air:
                    if (ambientSoundNode.AudioSource == null)
                    {
                        ambientSoundNode.AudioSource = new AudioSource(content["game"].Load<SoundEffect>("Sounds/wind"));
                        ambientSoundNode.AudioSource.Volume = 0.8f;
                        ambientSoundNode.AudioSource.Loop = true;
                        ambientSoundNode.AudioSource.Play();
                    }
                    break;

                case LevelType.Earth:
                    break;

                case LevelType.MainHalls:
                    
                    break;

                case LevelType.Water:
                    if (ambientSoundNode.AudioSource == null)
                    {
                        ambientSoundNode.AudioSource = new AudioSource(content["game"].Load<SoundEffect>("Sounds/waterFlow"));
                        ambientSoundNode.AudioSource.Volume = 0.4f;
                        ambientSoundNode.AudioSource.Loop = true;
                        ambientSoundNode.AudioSource.Play();
                    }
                    break;

                default: break;
            }
        }

        // handle crossplatform updating here
        public virtual void Update()
        {
#if DEBUG_DRAGON
            InputService input = ServiceProvider.GetService<InputService>();
            InputDevice device = input.GetDevice(0);

            if (device.IsClicked("B"))
            {
                IsDragonAwake = true;
            }
#else
            if (InDragonMode)
                IsDragonAwake = true;
#endif
            // lose conditions
            if (kinectPlayer.Guardian.IsDead)
                GameApplication.App.SetState("GameOverState", "The Guardian fell. You fool!");

            int deadCount = 0;
            foreach (Unit wizard in this.currentPlayers)
                if (wizard.IsDead) deadCount++;
                else break;

            if (deadCount == currentPlayers.Count)
            {
                GameApplication.App.SetState("GameOverState", "All wizards have died!");
                return;
            }

            PlayLevelAmbientSounds();
            // handle any transitions signaled
            if (signaledTransition)
            {
                signaledTransition = false;
                DoTransition();
                nextLevel = LevelType.None;
            }

            if (signaledCollect && collectUI.TheChosenOne != null)
            {
                UIRendererService urs = ServiceProvider.GetService<UIRendererService>();

                unlockedSpells.Add(SpellOfLevel(currentLevelType));
                assignedSpells.Add(collectUI.TheChosenOne.Value);

                urs.CurrentLayer = oldLayer;
                GameTiming.Paused = false;
                signaledCollect = false;
            }

            enemyManager.Update();
        }

        public void UnitDied(Unit unit)
        {
            if (unit.SceneNode.CategoryGroup == "Skeleton")
            {
                enemyManager.SkeletonDied();
            }

            // win condition here!
            if (unit.SceneNode.CategoryGroup == "Dragon")
            {
                int[] ints = new int[playerIndices.Length];
                for (int i = 0; i < ints.Length; i++)
                    ints[i] = (int)playerIndices[i];
                GameApplication.App.SetState("CongratulationsState", ints);
            }
        }
        #endregion
    }
}
