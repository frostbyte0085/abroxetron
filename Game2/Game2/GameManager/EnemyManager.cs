﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Engine.AI;
using Engine.Core;
using Engine.Scene;
using Engine.Tools;

using Game2.Logic;
using Game2.Logic.Behaviours;
using Game2.Logic.Commands;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game2.GameManager
{
    public class EnemyManager
    {
        #region consts
        // Filenames of the walkables used to create the grid.
        public const string mainHallsWalkable = "Models/Dungeon/MainHalls/MainHalls_walkable";
        public const string fireHallsWalkable = "Models/Dungeon/Fire/FireDungeon_walkable";
        public const string earthHallsWalkable = "Models/Dungeon/Earth/Earth_walkable";
        public const string waterHallsWalkable = "Models/Dungeon/Water/Water_walkable";
        public const string airHallsWalkable = "Models/Dungeon/Air/Air_walkable";

        // Cell sizes for the individual cells.
        public const int mainHallsCellSize = 1;
        public const int fireHallsCellSize = 1;
        public const int earthHallsCellSize = 1;
        public const int waterHallsCellSize = 1;
        public const int airHallsCellSize = 1;

        // Maximum skeletons allowed at once in the level. Decremented when SkeletonDied()
        // is invoked.
        public int maxSkeletons = 3;

        // Radius of the space the skeletons is required to have in order to spawn.
        public const float spawningSpace = 0.55f;

        // Radius around avgPos where skeletons are allowed to spawn.
        public const float spawningDistance = 5f;
        #endregion

        #region Variables
        private ContentService cs;
        private SceneService ss;

        /// <summary>
        /// Random instance used to generate pseudo-random numbers
        /// </summary>
        private Random rand;

        /// <summary>
        /// Auto-increment skeletonId. Used to avoid 2 skeletons from
        /// having the same SceneNode name.
        /// </summary>
        private static long skeletonId;

        /// <summary>
        /// Number of skeletons in the game.
        /// </summary>
        private int nSkeletons;

        /// <summary>
        /// An array of acceptable spawn positions.
        /// </summary>
        private Vector3[] spawnPoints;

        /// <summary>
        /// An array mapping to spawnPoints. This array takes some considerate time to
        /// compute, so it is only computed once at the start of Update(). 
        /// 
        /// If isSpawnPointValid[i] == true, then a skeleton is allowed to spawn 
        /// at spawnPoints[i].
        /// </summary>
        private bool[] isSpawnPointValid;

        /// <summary>
        /// Time when a new skeleton should be be spawned.
        /// </summary>
        private float nextSpawn;

        /// <summary>
        /// GameApplication.App.GameManager.AveragePosition(). Calculated once at
        /// the start of this.Update() to avoid re-calculating the same thing every
        /// time.
        /// </summary>
        private Vector3 avgPos;
        #endregion

        #region Constructor
        public EnemyManager()
        {
            cs = ServiceProvider.GetService<ContentService>();
            ss = ServiceProvider.GetService<SceneService>();

            nextSpawn = 0;
            skeletonId = 0;
            rand = new Random();
        }
        #endregion

        #region Starting
        /// <summary>
        /// Load the grid.
        /// </summary>
        /// <param name="level">Which level.</param>
        /// <param name="offset">Translation of the level's scene node</param>
        public void Start(LevelType level, Vector3 offset, Vector3[] spawnPoints)
        {
            Enabled = true;
            string modelName = null;
            int cellsize = 1;

            switch (level)
            {
                case LevelType.MainHalls:
                    modelName = mainHallsWalkable;
                    cellsize = mainHallsCellSize;
                    break;
                case LevelType.Fire:
                    modelName = fireHallsWalkable;
                    cellsize = fireHallsCellSize;
                    break;
                case LevelType.Earth:
                    modelName = earthHallsWalkable;
                    cellsize = earthHallsCellSize;
                    break;
                case LevelType.Water:
                    modelName = waterHallsWalkable;
                    cellsize = waterHallsCellSize;
                    break;
                case LevelType.Air:
                    modelName = airHallsWalkable;
                    cellsize = airHallsCellSize;
                    break;
            }

            Debug.Assert(modelName != null);

            Model model = cs["game"].Load<Model>(modelName);
            ss.PathGrid = PathGridMaker.MakeGrid(model, offset, cellsize);

            this.spawnPoints = spawnPoints;
            this.isSpawnPointValid = new bool[spawnPoints.Length];
            this.nSkeletons = 0;
        }
        #endregion

        #region Private tests
        /// <summary>
        /// Returns true is v is a valid spawn point
        /// </summary>
        /// <param name="v">Any spawn point in this.spawnPoints</param>
        /// <returns>True if you can use this point to spawn a skeleton</returns>
        [Obsolete("Use IsValid(int) instead. Utilizes pre-computation", true)]
        private bool IsValid(Vector3 v)
        {
            return (v - avgPos).LengthSquared() <= spawningDistance * spawningDistance;
        }

        /// <summary>
        /// Returns true is v is a valid spawn point.
        /// </summary>
        /// <param name="i">Index of a spawn point in this.spawnPoints</param>
        /// <returns>True if you can use this point to spawn a skeleton</returns>
        private bool IsValid(int i)
        {
            return isSpawnPointValid[i];
        }

        /// <summary>
        /// Enables/Disables the spawning of skeletons
        /// </summary>
        public bool Enabled
        {
            set;
            get;
        }

        /// <summary>
        /// Return whether a skeletons can be spawned in this frame
        /// </summary>
        /// <returns>True if it's possible to spawn. False otherwise (e.g. max skeletons
        /// reached or no valid spawn pnts)</returns>
        private bool CanSpawn()
        {
            // Are there no many skeletons?
            if ((nSkeletons >= maxSkeletons) || !Enabled) return false;

            // No, not too many skeletons. Can we spawn one near a player?

            bool hasValidSpawn = false;
            for (int i = 0; i < spawnPoints.Length; i++ )
            {
                if (IsValid(i))
                {
                    // Yes we can spawn a skeleton near a player:
                    hasValidSpawn = true;
                    break;
                }
            }

            return hasValidSpawn;
        }
        #endregion

        #region Private random generators
        /// <summary>
        /// Gets a random valid point. Assumes a valid spawn point exists.
        /// </summary>
        /// <returns>A random spawn point</returns>
        private Vector3 GetRandomSpawnPoint()
        {
            List<Vector3> inReach = new List<Vector3>();

            for (int i =0 ; i<spawnPoints.Length; i++)
            {
                if (IsValid(i))
                    inReach.Add(spawnPoints[i]);
            }

            int j = rand.Next(inReach.Count);
            return inReach[j];
        }

        /// <summary>
        /// Get a spawn command that controls how the scene node behaves
        /// when spawning
        /// </summary>
        /// <returns>SpawnBelow or SpawnAbove command</returns>
        private Spawn GetRandomSpawnCmd()
        {
            if (rand.Next(2) == 0)
                return new SpawnBelow();

            else
                return new SpawnAbove();
        }

        /// <summary>
        /// Get a new time when to spawn another skeleton
        /// </summary>
        /// <returns>Time in seconds</returns>
        private float GetRandomNextSpawnTime()
        {
            return GameTiming.TotalTimeElapsed + 3;
        }
        #endregion

        #region Spawning / updating
        /// <summary>
        /// Spawns a skeleton on a random valid spawn point
        /// </summary>
        private void SpawnSkeleton()
        {
            // Create scene node and unit
            SceneNode skeletonNode = ss.AddSceneNode("skeleton"+skeletonId);
            Unit skeleton = new Unit(skeletonNode, Color.White, UnitProperties.Skeleton());

            // Init scene node:
            skeleton.SceneNode.Behaviour = new SkeletonBehaviour(skeleton, skeletonId++);
            skeleton.SceneNode.Translation = GetRandomSpawnPoint();
            skeleton.SceneNode.Orientation = Quaternion.CreateFromYawPitchRoll(Random2.RandomAngle(), 0, 0);

            // Init unit:
            skeleton.IssueCommand(GetRandomSpawnCmd());

            nSkeletons++;
        }

        /// <summary>
        /// Invoke on update.
        /// </summary>
        public void Update()
        {
            preCompute();

            // Spawning:
            if (CanSpawn())
            {
                // Has the time come?
                if (GameTiming.TotalTimeElapsed >= nextSpawn)
                {
                    // Spawn a skeleton and get a time for the newer spawn.
                    nextSpawn = GetRandomNextSpawnTime();
                    SpawnSkeleton();
                }
            }
            else
            {
                // Delay the spawn (cannot spawn skeletons yet)
                nextSpawn += GameTiming.TimeElapsed;
            }
        }

        /// <summary>
        /// Invoke when a skeleton dies
        /// </summary>
        public void SkeletonDied()
        {
            nSkeletons--;
        }
        #endregion

        #region Pre-computing
        private void preCompute()
        {
            avgPos = GameApplication.App.GameManager.AveragePosition();
            ComputeValidPoints();
        }

        /// <summary>
        /// Compute isSpawnPointValid. Requires avgPos to be computed.
        /// </summary>
        private void ComputeValidPoints()
        {
            Debug.Assert(isSpawnPointValid.Length == spawnPoints.Length);

            // Get all the units in the game:
            SceneService ss = ServiceProvider.GetService<SceneService>();
            List<SceneNode> wizards = ss.GetSceneNodesInCategoryGroup("Wizard");
            List<SceneNode> skeletons = ss.GetSceneNodesInCategoryGroup("Skeleton");
            IEnumerable<SceneNode> allUnits = wizards.Concat(skeletons);

            // Loop through all the spawn points
            for (int i = 0; i < isSpawnPointValid.Length; i++)
            {
                // is the spawn pnt close enough to the players?
                isSpawnPointValid[i] = (spawnPoints[i] - avgPos).Length() <= spawningDistance;

                IEnumerator<SceneNode> ienum = allUnits.GetEnumerator();
                while (isSpawnPointValid[i] && ienum.MoveNext())
                {
                    // Is a unit blocking this spawn pnt?
                    SceneNode unitNode = ienum.Current;
                    isSpawnPointValid[i] = (unitNode.Translation - spawnPoints[i]).Length() >= spawningSpace;
                }
            }
        }
        #endregion
    }
}
