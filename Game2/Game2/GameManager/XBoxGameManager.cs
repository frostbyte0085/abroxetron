﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.GameManager
{
    public sealed class XBoxGameManager : BaseGameManager
    {
        public XBoxGameManager() : base()
        {

        }

        public override string Name
        {
            get
            {
                return "XBox Game Manager";
            }
        }
    }
}
