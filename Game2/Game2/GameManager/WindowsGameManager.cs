﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game2.GameManager
{
    public sealed class WindowsGameManager : BaseGameManager
    {
        public WindowsGameManager()
            : base()
        {
        }

        public override string Name
        {
            get
            {
                return "Windows Game Manager";
            }
        }
    }
}
