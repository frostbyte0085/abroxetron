//Maya ASCII 2012 scene
//Name: 1.ma
//Last modified: Tue, Jan 31, 2012 11:11:35 PM
//Codeset: 1252
requires maya "2012";
currentUnit -l meter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "001200000000-796618";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.0604322634592951 50.010854152191335 -4.9194301490361525 ;
	setAttr ".r" -type "double3" 640.46164723896004 -961.80000000027792 0 ;
	setAttr ".rp" -type "double3" 0 0 -9.0949470177292826e-015 ;
	setAttr ".rpt" -type "double3" -3.7514004159992209e-018 -1.0768337277022717e-016 
		1.819070955446523e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 45.950747542098505;
	setAttr ".ow" 0.1;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -491.71794162994001 695.75671506956917 -320.08410018743666 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1.001 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 1.001;
	setAttr ".ow" 0.3;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.21810093847547138 1.6084944212565913 1.001 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 1.001;
	setAttr ".ow" 16.757678558127786;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.0010000000000003 4.19416451599289 -1.2750260128618407 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 1.001;
	setAttr ".ow" 40.834385727706781;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	setAttr ".t" -type "double3" 0.022520325076435484 3.5081025816038949 2.8288738491206744 ;
	setAttr ".r" -type "double3" 0 179.46326455843405 0 ;
	setAttr ".s" -type "double3" 0.81768167699899641 1 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	setAttr -k off ".v";
	setAttr -av ".iog[0].og[0].gid";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[364]" -type "float3" 0 -1.3113022e-008 0 ;
	setAttr ".pt[422]" -type "float3" 7.4505804e-011 0 4.7683715e-009 ;
	setAttr ".dr" 3;
	setAttr ".dsm" 2;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	setAttr ".w" 1.997163776120858;
	setAttr ".h" 1.729785543397488;
	setAttr ".d" 1.9937870183226851;
	setAttr ".cuv" 4;
createNode polyBridgeEdge -n "polyBridgeEdge1";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 5;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 0.86489278 -0.99701995 ;
	setAttr ".rs" 58157;
	setAttr ".lt" -type "double3" 0 1.1355266824280663e-015 5.7910659748245736 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.97606159936204107 -3.5308091952401808e-008 -0.9970199777334855 ;
	setAttr ".cbx" -type "double3" 1.021102249514912 1.72978557870558 -0.9970199777334855 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 0.86489278 0.9967671 ;
	setAttr ".rs" 46896;
	setAttr ".lt" -type "double3" -8.8817841970012525e-018 0 0.92191159653796573 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.97606159936204107 -3.5308091952401808e-008 0.99676710112393641 ;
	setAttr ".cbx" -type "double3" 1.021102249514912 1.72978557870558 0.99676710112393641 ;
createNode polyTweak -n "polyTweak1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  78.76412201 -27.73101807 -40.6628952
		 -78.76412201 -27.73101807 -40.6628952 -78.76412201 108.70757294 -40.6628952 78.76412201
		 108.70757294 -40.6628952;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 0.86489278 1.9186788 ;
	setAttr ".rs" 36861;
	setAttr ".lt" -type "double3" 0 -2.8421709430404008e-016 2.7079383629720111 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.97606159936204107 -3.5308091952401808e-008 1.918678798511632 ;
	setAttr ".cbx" -type "double3" 1.021102249514912 1.72978557870558 1.918678798511632 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 0.86489278 4.626617 ;
	setAttr ".rs" 38174;
	setAttr ".lt" -type "double3" 0 -2.8421709430404008e-016 0.49538793560072919 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.97606159936204107 -3.5308091952401808e-008 4.6266167257577253 ;
	setAttr ".cbx" -type "double3" 1.021102249514912 1.72978557870558 4.6266167257577253 ;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 0.86489278 5.122005 ;
	setAttr ".rs" 48332;
	setAttr ".lt" -type "double3" 4.4408920985006263e-018 0 0.91159424088796648 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.97606159936204107 -3.5308091952401808e-008 5.1220049093514755 ;
	setAttr ".cbx" -type "double3" 1.021102249514912 1.72978557870558 5.1220049093514755 ;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 0.86489278 6.0335994 ;
	setAttr ".rs" 52149;
	setAttr ".lt" -type "double3" 0 1.4716918944868649 1.7062533247544514 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.97606159936204107 -3.5308091952401808e-008 6.0335991476327253 ;
	setAttr ".cbx" -type "double3" 1.021102249514912 1.72978557870558 6.0335991476327253 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.022520326 2.5506423 8.3818655 ;
	setAttr ".rs" 57263;
	setAttr ".lt" -type "double3" 0 -0.32284343972879581 1.0288316174026897 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.63791073571458012 1.9786288862006973 8.3818657491952244 ;
	setAttr ".cbx" -type "double3" 0.68295138586745108 3.1226555280464003 8.3818657491952244 ;
createNode polyTweak -n "polyTweak2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  33.81508636 50.69369888 64.20126343
		 -33.81508636 50.69369888 64.20126343 -33.81508636 -7.88219595 64.20126343 33.81508636
		 -7.88219595 64.20126343;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[11]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.4367725 0.84146339 -0.56193376 ;
	setAttr ".rs" 63626;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.8881886488930348 -0.98428325369188108 -2.1112365389079812 ;
	setAttr ".cbx" -type "double3" -0.9853563474372049 2.6672099714057755 0.98736898030373044 ;
createNode polyTweak -n "polyTweak3";
	setAttr ".uopa" yes;
	setAttr -s 28 ".tk";
	setAttr ".tk[0:3]" -type "float3" -66.342552 -98.428329 31.519234  66.342552 
		-98.428329 31.519234  -66.342552 54.505058 31.519234  66.342552 54.505058 31.519234 ;
	setAttr ".tk[12:35]" -type "float3" -87.605659 -98.428329 17.431343  93.181931 
		-98.428329 17.431343  93.181931 93.742439 17.431343  -87.605659 93.742439 17.431343  
		-87.605659 -98.428329 -23.949121  93.181931 -98.428329 -23.949121  93.181931 82.694923 
		-23.949121  -87.605659 82.694923 -23.949121  -66.342552 -98.428329 -31.519234  66.342552 
		-98.428329 -31.519234  66.342552 43.457542 -31.519234  -66.342552 43.457542 -31.519234  
		22.478268 31.552441 26.429962  -22.478268 31.552441 26.429962  -22.478268 -7.3853607 
		26.429962  22.478268 -7.3853607 26.429962  14.866427 -12.98687 -26.429962  -14.866427 
		-12.98687 -26.429962  -14.866427 -38.739124 -26.429962  14.866427 -38.739124 -26.429962  
		35.928268 16.849968 0.36186436  -35.879505 16.905849 0.32960686  -35.928268 -45.292931 
		-0.36186436  35.879505 -45.348812 -0.32960686 ;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[11]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.9840947 0.52832508 -0.70744669 ;
	setAttr ".rs" 53096;
	setAttr ".lt" -type "double3" 0 -0.89545592682917974 2.001978956303724 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.2375555276729417 -0.49679576589891228 -1.5773500920002688 ;
	setAttr ".cbx" -type "double3" -1.7306337306639998 1.5534459333198378 0.16245668505388394 ;
createNode polyTweak -n "polyTweak4";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[36:41]" -type "float3"  75.29721832 5.58605146 81.78945923
		 46.20470428 48.74874878 -19.46387863 75.29721832 -70.26834106 81.78945923 46.20470428
		 -94.16992188 -19.46387863 34.43502808 -111.37640381 -53.71357727 34.43502808 48.74874878
		 -53.71357727;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[11]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 86.489277169874399 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.2950492 -1.542202 -0.14963506 ;
	setAttr ".rs" 48741;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.721751052447305 -1.7038618059379746 -0.57179428456066372 ;
	setAttr ".cbx" -type "double3" -2.8618057396597822 -1.3805421037895371 0.26969736426256996 ;
createNode polyTweak -n "polyTweak5";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[42:47]" -type "float3"  -48.57360458 -71.98149109
		 43.21291351 -113.53944397 -29.51195717 -29.40181732 -7.77333641 -153.78936768 49.75074005
		 -36.66683197 -183.6474762 -17.083839417 -41.86000061 -203.85322571 -36.88039398 -127.98757935
		 -31.16100883 -50.68144989;
createNode polySplitRing -n "polySplitRing1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 15 "e[0:3]" "e[13]" "e[17]" "e[21]" "e[25]" "e[29]" "e[33]" "e[37]" "e[41]" "e[45]" "e[49]" "e[53]" "e[57]" "e[61]" "e[65]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.4622751772403717;
	setAttr ".re" 65;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[3]" -type "float3" -0.35557133 31.892509 -37.95565 ;
	setAttr ".tk[38:40]" -type "float3" 31.052942 17.231764 -0.29090634  43.991913 
		49.124279 -38.371101  31.052942 17.231764 -0.29090634 ;
	setAttr ".tk[45:53]" -type "float3" 23.591593 0 -0.22100781  -21.119589 
		0 21.832766  0.25900465 0 27.647589  -4.9529181 -103.61722 1.861594  -0.74165475 
		-90.974892 5.4976091  -1.7096505 -120.03738 -3.7055292  15.666101 -121.91237 -5.0879726  
		-13.310257 -124.94041 15.636039  1.2219522 -90.278252 33.202805 ;
createNode polySplitRing -n "polySplitRing2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[91:92]" "e[94]" "e[96]" "e[99]" "e[101]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.59796375036239624;
	setAttr ".dr" no;
	setAttr ".re" 99;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[43:44]" "e[46]" "e[48]" "e[110]" "e[134]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.35606870055198669;
	setAttr ".re" 46;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 29 "e[4]" "e[7:8]" "e[15]" "e[18]" "e[23]" "e[26]" "e[31]" "e[34]" "e[39]" "e[42]" "e[47]" "e[50]" "e[55]" "e[58]" "e[63]" "e[66]" "e[71]" "e[77]" "e[83]" "e[89]" "e[95]" "e[98]" "e[102]" "e[122]" "e[139]" "e[146]" "e[151]" "e[154]" "e[160]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.39906010031700134;
	setAttr ".re" 63;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[11:12]" "e[14]" "e[16]" "e[120]" "e[124]" "e[186]" "e[192]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.39200744032859802;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[124]" "e[192]" "e[224:225]" "e[227]" "e[229]" "e[231]" "e[235]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.56460243463516235;
	setAttr ".dr" no;
	setAttr ".re" 224;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	setAttr ".ics" -type "componentList" 2 "f[75]" "f[102]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.2271163 0.26441652 -0.74235719 ;
	setAttr ".rs" 56271;
	setAttr ".lt" -type "double3" 6.1062266354383615e-017 4.8849813083506888e-017 0.84956082287435652 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.5871897440483371 0.013263714416394805 -0.7578495833795641 ;
	setAttr ".cbx" -type "double3" -2.8672523470605955 0.51556931744373857 -0.72690096251841152 ;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	setAttr ".ics" -type "componentList" 3 "f[19]" "f[23]" "f[107:108]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.56993 3.4294357 -4.8778172 ;
	setAttr ".rs" 46942;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.8666979826101235 1.6589265562132698 -5.3503655302646234 ;
	setAttr ".cbx" -type "double3" -1.273161914870254 5.1999449277953014 -4.4052688736383434 ;
createNode polyTweak -n "polyTweak7";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[130:135]" -type "float3"  -23.10820961 -1.43853199 0.02859609
		 -5.45101547 -1.61749685 -0.0024008534 -22.86080551 1.42469883 -0.14759107 -4.50160837
		 1.68443358 -0.029850855 21.088275909 -1.98128021 -0.048671052 23.094818115 1.98128021
		 0.14786141;
createNode polyExtrudeFace -n "polyExtrudeFace13";
	setAttr ".ics" -type "componentList" 3 "f[19]" "f[23]" "f[107:108]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.8423902 3.190733 -5.0336475 ;
	setAttr ".rs" 43957;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.2236610384891318 1.9604298521117074 -5.4788355692478081 ;
	setAttr ".cbx" -type "double3" -1.4608794672721188 4.4210360075072153 -4.5875563353404392 ;
createNode polyTweak -n "polyTweak8";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk[135:143]" -type "float3"  2.99297333 30.1503334 18.20148659
		 16.14492798 -12.96453857 21.68968201 19.94339752 -6.58295107 15.55835629 8.24881458
		 31.75439453 12.4566927 28.13451195 -11.094854355 8.73717403 18.83131027 19.40296555
		 6.26974535 35.95026779 -77.89089203 26.94250107 37.55413818 -64.31494904 20.22910881
		 42.14406586 -57.021450043 12.45281219;
createNode polyExtrudeFace -n "polyExtrudeFace14";
	setAttr ".ics" -type "componentList" 3 "f[19]" "f[23]" "f[107:108]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7514973 1.1388398 -5.0337334 ;
	setAttr ".rs" 55476;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.1506306718299202 1.0718071104124887 -5.2526372238559009 ;
	setAttr ".cbx" -type "double3" -2.3543802845190207 1.2058726621703011 -4.8134459329309562 ;
createNode polyTweak -n "polyTweak9";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk[143:151]" -type "float3"  48.1557312 -88.8622818 22.13882828
		 65.99789429 -183.93856812 20.34369469 83.76866913 -171.84967041 1.90037799 67.90349579
		 -87.30854034 3.49661088 109.52915192 -183.76878357 -21.91273308 96.90817261 -116.5151825
		 -20.64289856 92.86625671 -327.11276245 17.64035225 107.65979004 -299.15878296 -0.50337011
		 128.53485107 -285.044769287 -23.82490921;
createNode polySplitRing -n "polySplitRing7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[296:297]" "e[300]" "e[303]" "e[305]" "e[308]" "e[310]" "e[313]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.61904501914978027;
	setAttr ".dr" no;
	setAttr ".re" 313;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak10";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk[151:159]" -type "float3"  -0.24707085 -89.84757996 -0.0015548418
		 -0.097313464 -92.32539368 -0.0015548418 0.14336342 -91.91469574 -0.0015548418 0.010199964
		 -89.71144867 -0.0015548418 0.46700996 -92.11305237 -0.0015548418 0.36107582 -90.36032867
		 -0.0015548418 0.12820381 -96.056724548 -0.0015548418 0.34390205 -95.23256683 -0.0015548418
		 0.62654346 -94.75243378 -0.0015548418;
createNode polyExtrudeFace -n "polyExtrudeFace15";
	setAttr ".ics" -type "componentList" 3 "f[19]" "f[23]" "f[107:108]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7475672 0.21098299 -5.0344319 ;
	setAttr ".rs" 62057;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.108941984606056 0.20638230572498856 -5.3966525151618736 ;
	setAttr ".cbx" -type "double3" -2.3866226949707303 0.21558366070545731 -4.6709351965629242 ;
createNode polyTweak -n "polyTweak11";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk";
	setAttr ".tk[22]" -type "float3" 24.343987 25.503799 -0.22805628 ;
	setAttr ".tk[109]" -type "float3" -0.30415207 0 -32.466873 ;
	setAttr ".tk[136:138]" -type "float3" -0.47830111 0 -51.056503  -7.662189 
		-30.622055 0.071779974  0.15789139 0 16.854197 ;
	setAttr ".tk[141]" -type "float3" 24.343987 25.503799 -0.22805628 ;
	setAttr ".tk[144:147]" -type "float3" -0.32273233 0 -34.450237  -7.662189 
		-30.622055 0.071779974  0.31332204 0 33.445724  -5.8545947 0 0.054846279 ;
	setAttr ".tk[149]" -type "float3" 14.766803 0 -0.13833651 ;
	setAttr ".tk[151:152]" -type "float3" 4.8072858 4.134469 0  0.76177055 3.5863092 
		-16.239729 ;
	setAttr ".tk[154:159]" -type "float3" 3.214359 0.51359612 0.019253038  1.2556731 
		-4.3331389 16.500221  1.1765974 -3.9258821 0.0046697333  -5.1208882 2.3724883 0  
		6.0888906 -0.90882295 -0.04831212  -3.0721126 -5.1965919 0 ;
	setAttr ".tk[161]" -type "float3" 13.727875 1.388274 -0.18664868 ;
	setAttr ".tk[163]" -type "float3" -0.24434547 1.6102318 -20.065222 ;
	setAttr ".tk[165:167]" -type "float3" -2.0551753 0 0.019253038  -3.6730702 
		0.25596073 0.059516013  0.27858347 1.2227564 26.383327 ;
createNode polySplitRing -n "polySplitRing8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 25 "e[103:104]" "e[106]" "e[108]" "e[110]" "e[112]" "e[114]" "e[116]" "e[118]" "e[120]" "e[122]" "e[124]" "e[126]" "e[128]" "e[130]" "e[132]" "e[134]" "e[136]" "e[155]" "e[162]" "e[165]" "e[189]" "e[221]" "e[231]" "e[237]" "e[247]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.91937965154647827;
	setAttr ".dr" no;
	setAttr ".re" 103;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak12";
	setAttr ".uopa" yes;
	setAttr -s 57 ".tk";
	setAttr ".tk[3]" -type "float3" -2.6563997 -10.243559 0 ;
	setAttr ".tk[13:14]" -type "float3" 0.1069639 28.508442 11.417918  0.38461855 
		-20.412323 41.056309 ;
	setAttr ".tk[17:18]" -type "float3" -32.671646 28.163097 -45.900715  -1.565591 
		-11.737681 -53.006718 ;
	setAttr ".tk[21:22]" -type "float3" -43.383156 2.8421709e-014 0.40641657  
		0 6.4490366 0 ;
	setAttr ".tk[45]" -type "float3" -8.6644926 0 0.081169613 ;
	setAttr ".tk[54:71]" -type "float3" 1.7763568e-015 6.1737099 1.2814231  
		1.7763568e-015 15.768254 -1.2814236  -8.8817842e-016 26.505381 -1.1368684e-013  1.4210855e-014 
		61.286007 2.5047536  1.1492095 38.74749 6.0551224  0 36.280392 5.6843419e-014  2.6564031 
		50.412663 0  1.7763568e-015 27.094723 0  -0.24913585 6.0024223 -26.594135  -0.61474836 
		-7.2260723 -65.621628  0 -20.871712 0  0 -20.254942 0  0 -29.984396 0  0 -27.66559 
		0  0 -29.535431 0  0 -45.715157 -2.2737368e-013  -8.8817842e-016 -28.74379 -1.1368684e-013  
		0 -11.671827 0 ;
	setAttr ".tk[73]" -type "float3" -8.6644926 0 0.081169613 ;
	setAttr ".tk[78:80]" -type "float3" 0.25673735 -9.1657495 27.405554  0.30453041 
		12.084836 32.507256  0 -50.36554 -1.1368684e-013 ;
	setAttr ".tk[83]" -type "float3" -1.7763568e-015 47.094925 -1.1368684e-013 ;
	setAttr ".tk[96]" -type "float3" -0.86388439 0 -92.215736 ;
	setAttr ".tk[108:110]" -type "float3" 0.49158242 28.508442 52.474228  -0.54008907 
		28.163097 -57.652092  -2.6108174 19.623238 58.450935 ;
	setAttr ".tk[114]" -type "float3" 0 25.378456 0 ;
	setAttr ".tk[118]" -type "float3" 0 -20.81127 0 ;
	setAttr ".tk[122]" -type "float3" 0 16.528835 0 ;
	setAttr ".tk[126]" -type "float3" 0 -17.288673 0 ;
	setAttr ".tk[137:138]" -type "float3" -2.8421709e-014 4.547039 0  3.0726967 
		24.025297 -9.1463795 ;
	setAttr ".tk[140]" -type "float3" -0.1439182 0 -15.362626 ;
	setAttr ".tk[142]" -type "float3" 7.1525574e-007 -1.9073486e-006 3.8146973e-006 ;
	setAttr ".tk[146]" -type "float3" -0.31306654 0 -33.418446 ;
	setAttr ".tk[149]" -type "float3" -5.8724508 0 0.055013556 ;
	setAttr ".tk[154]" -type "float3" -0.11575647 0.69962639 -9.5053205 ;
	setAttr ".tk[160]" -type "float3" -5.8724508 0 0.055013556 ;
	setAttr ".tk[166:175]" -type "float3" -0.18064874 0 -19.28344  -5.6843419e-014 
		-23.43635 0  -5.6843419e-014 -23.43635 0  -5.6843419e-014 -23.43635 0  -5.6843419e-014 
		-23.43635 0  -0.033563774 -24.135975 -6.4339514  -5.6843419e-014 -23.43635 0  -5.6843419e-014 
		-23.43635 0  -5.6843419e-014 -23.43635 0  -5.6843419e-014 -23.43635 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace16";
	setAttr ".ics" -type "componentList" 2 "f[168]" "f[173]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7014704 0.091028452 -5.2763381 ;
	setAttr ".rs" 57075;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.9564866187804864 -0.033149554626573943 -5.3323119807687647 ;
	setAttr ".cbx" -type "double3" -2.4472280432301354 0.2152064634398323 -5.2176580128148213 ;
createNode polyExtrudeFace -n "polyExtrudeFace17";
	setAttr ".ics" -type "componentList" 1 "f[177:178]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3966179 4.8759251 -4.7481813 ;
	setAttr ".rs" 55856;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.8560083346058707 4.5348774839476453 -5.6244315114842323 ;
	setAttr ".cbx" -type "double3" -0.94259273696023671 5.2169725156859261 -3.8750786396805483 ;
createNode polyTweak -n "polyTweak13";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk";
	setAttr ".tk[179:181]" -type "float3" -21.906219 40.467316 -55.066193  -69.785408 
		6.6224937 0.65375483  -40.48312 0 27.629337 ;
	setAttr ".tk[202:207]" -type "float3" -1.3143231 0 55.387032  -12.761387 0 
		55.387032  -12.761387 0 55.387032  -1.2715487 0 55.387032  13.760165 0 55.387032  
		13.760165 0 55.387032 ;
createNode polyExtrudeFace -n "polyExtrudeFace18";
	setAttr ".ics" -type "componentList" 1 "f[177:178]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3978703 4.9814653 -4.7489161 ;
	setAttr ".rs" 51252;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.6428440564151299 4.7995985532835821 -5.2161845919367726 ;
	setAttr ".cbx" -type "double3" -1.1557578026260065 5.1633325315550671 -4.2833264469275196 ;
createNode polyTweak -n "polyTweak14";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[208:213]" -type "float3"  19.7144165 -5.36400509 -3.60779762
		 -20.49917984 -1.16927719 -3.19983983 21.18492317 5.46734428 -14.14513206 5.50470448
		 26.47211075 -40.87810135 5.55912018 -0.99050713 25.90447998 -20.93307877 0.90916061
		 41.022663116;
createNode polyExtrudeFace -n "polyExtrudeFace19";
	setAttr ".ics" -type "componentList" 1 "f[177:178]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3978705 4.9814653 -4.7489161 ;
	setAttr ".rs" 54575;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.6428442118551096 4.7995985532835821 -5.2161845933661652 ;
	setAttr ".cbx" -type "double3" -1.1557578054847906 5.1633323789671763 -4.2833261431945209 ;
createNode polyExtrudeFace -n "polyExtrudeFace20";
	setAttr ".ics" -type "componentList" 1 "f[177:178]";
	setAttr ".ix" -type "matrix" -0.99995612247620391 0 -0.0093676636551053277 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3892555 5.1935887 -4.7875142 ;
	setAttr ".rs" 48536;
	setAttr ".lt" -type "double3" -7.5460471204991109e-017 8.7041485130612271e-016 2.3959680987671534 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.5772014112701069 5.1832017637328009 -4.9198594756201057 ;
	setAttr ".cbx" -type "double3" -1.2014621674244679 5.2039755369261602 -4.6558197305186333 ;
createNode polyTweak -n "polyTweak15";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[180]" -type "float3" -4.5025454 0 0.042180173 ;
	setAttr ".tk[208]" -type "float3" -16.767916 2.2793982 0.15708308 ;
	setAttr ".tk[210]" -type "float3" -8.1430511 0 4.2936325 ;
	setAttr ".tk[212]" -type "float3" -0.18361123 0 -19.599674 ;
	setAttr ".tk[214:225]" -type "float3" -8.1499338 2.6796079 0.076349214  0 
		9.5367432e-007 0  4.7683716e-007 9.1509323 0  0 9.5367432e-007 9.5367432e-007  -1.1920929e-007 
		1.1920929e-006 0  -4.7683716e-007 4.7683716e-007 9.5367432e-007  2.7689023 2.0593789 
		-0.96168965  -5.6036382 8.8569527 -0.87471741  5.7910967 14.869036 -3.8667121  10.105905 
		38.360321 -29.728477  -8.2674742 7.4268484 26.479542  -22.723354 9.9519701 37.463902 ;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n"
		+ "            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n"
		+ "                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n"
		+ "                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n"
		+ "                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n"
		+ "                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n"
		+ "                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n"
		+ "            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n"
		+ "                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n"
		+ "                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n"
		+ "                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode polyTweak -n "polyTweak16";
	setAttr ".uopa" yes;
	setAttr -s 47 ".tk";
	setAttr ".tk[13]" -type "float3" 0 26.8568 0 ;
	setAttr ".tk[17]" -type "float3" 0 26.8568 0 ;
	setAttr ".tk[37]" -type "float3" -13.694607 0 0.1282921 ;
	setAttr ".tk[43]" -type "float3" -13.694607 0 0.1282921 ;
	setAttr ".tk[45]" -type "float3" -17.377628 -0.065984547 0.1103553 ;
	setAttr ".tk[59]" -type "float3" 0.49351171 0 52.680153 ;
	setAttr ".tk[66:67]" -type "float3" 0 -19.885696 0  0 -19.885696 0 ;
	setAttr ".tk[73]" -type "float3" -6.1822429 0.065985262 0.1103553 ;
	setAttr ".tk[98:100]" -type "float3" -0.29875949 0 -31.891233  -0.40476933 
		0 -43.207314  -0.10600991 0 -11.316084 ;
	setAttr ".tk[106:110]" -type "float3" 0.23357733 0 24.933336  -8.8098297 0 
		25.018055  37.177448 0 -0.34828115  46.22086 0 -0.43300045  0.16585639 0 17.704428 ;
	setAttr ".tk[138]" -type "float3" 0.35948393 0 38.373295 ;
	setAttr ".tk[146]" -type "float3" 0.36858395 0 39.344685 ;
	setAttr ".tk[148:149]" -type "float3" -6.4955101 0 0.060850415  -7.2376809 
		0 0.067803122 ;
	setAttr ".tk[156:157]" -type "float3" -15.661515 0 0.14671823  -9.7611294 
		0 0.091442995 ;
	setAttr ".tk[160:161]" -type "float3" -8.8252964 0 0.082676023  -14.252361 
		0 0.13351716 ;
	setAttr ".tk[166]" -type "float3" 0.1749564 0 18.67581 ;
	setAttr ".tk[168]" -type "float3" 1.6566645 0 -2.4147072 ;
	setAttr ".tk[173:175]" -type "float3" -17.964569 0 2.5234983  -15.02859 0 
		-1.9537365  6.3752012 0 2.0787868 ;
	setAttr ".tk[181:184]" -type "float3" -51.748474 35.694458 0.48478368  -100.70415 
		40.060688 0.94340408  -72.866478 14.973751 0.68261844  -23.910793 10.607515 0.2239981 ;
	setAttr ".tk[194:196]" -type "float3" 0 9.6851454 -5.6843419e-014  0 15.050061 
		-1.1368684e-013  1.4210855e-014 5.3649158 -5.6843419e-014 ;
	setAttr ".tk[209]" -type "float3" 5.2168617 0 -0.048871968 ;
	setAttr ".tk[211]" -type "float3" 0.081345275 0 8.6832447 ;
	setAttr ".tk[213]" -type "float3" 5.0286942 0 -6.2245202 ;
	setAttr ".tk[217]" -type "float3" -7.1159749 0 -11.504655 ;
	setAttr ".tk[226:231]" -type "float3" 59.927624 -168.39899 -2.4445255  29.345249 
		-191.04893 -6.2526131  58.840065 -171.73085 -12.411372  40.244431 -184.02702 -17.119457  
		56.261951 -169.52997 5.2784734  42.565659 -178.28616 6.9643521 ;
createNode deleteComponent -n "deleteComponent1";
	setAttr ".dc" -type "componentList" 11 "f[1]" "f[3]" "f[5]" "f[10]" "f[12:14]" "f[16:18]" "f[20:22]" "f[24:26]" "f[28:29]" "f[78:80]" "f[85:91]";
createNode deleteComponent -n "deleteComponent2";
	setAttr ".dc" -type "componentList" 6 "f[1]" "f[3]" "f[5:6]" "f[64:65]" "f[85:88]" "f[93:96]";
createNode polyExtrudeFace -n "polyExtrudeFace21";
	setAttr ".ics" -type "componentList" 1 "f[135:136]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.4120549 5.7888427 -4.7783647 ;
	setAttr ".rs" 48348;
	setAttr ".lt" -type "double3" 0.85721353235915387 1.1723955140041654e-015 1.0980861824602108 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.4435066248380211 5.6856176878050668 -4.879456014225771 ;
	setAttr ".cbx" -type "double3" -1.3813248910239466 5.8920677305296758 -4.67709176319224 ;
createNode polyExtrudeFace -n "polyExtrudeFace22";
	setAttr ".ics" -type "componentList" 1 "f[135:136]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7280304 6.3073468 -5.4968419 ;
	setAttr ".rs" 57278;
	setAttr ".lt" -type "double3" 5.4178883601707638e-016 -1.1013412404281553e-015 2.377715169505656 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.779825274456897 6.2208622861937384 -5.6018952342961903 ;
	setAttr ".cbx" -type "double3" -2.6754206986052593 6.3938310361937383 -5.391919676173619 ;
createNode polyTweak -n "polyTweak17";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[202:207]" -type "float3"  60.084552765 -57.5262413 103.19548798
		 65.68979645 -54.17809677 108.80805969 55.3841362 -56.04076004 98.4559021 58.34420395
		 -54.16672134 100.67064667 65.24043274 -58.27789688 108.45967102 68.57450104 -57.12586594
		 111.82084656;
createNode polyExtrudeFace -n "polyExtrudeFace23";
	setAttr ".ics" -type "componentList" 1 "f[135:136]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.5540948 6.9489136 -4.5090575 ;
	setAttr ".rs" 53601;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -5.5769985804249682 6.9279396177367074 -4.5412387780994576 ;
	setAttr ".cbx" -type "double3" -5.5311361102652317 6.9698878599242073 -4.4768195782245801 ;
createNode polyTweak -n "polyTweak18";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[208:213]" -type "float3"  99.76525116 -62.33041763 -120.3640976
		 89.95204163 -49.12253952 -122.72075653 100.9778595 -59.75592422 -131.89178467 96.0495224
		 -51.41043854 -134.27249146 96.98265076 -62.67232132 -109.95320129 92.46697998 -57.13659668
		 -109.49465942;
createNode polySplitRing -n "polySplitRing9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 38 "e[1]" "e[5]" "e[35]" "e[47]" "e[59]" "e[86]" "e[110]" "e[118]" "e[123:124]" "e[126]" "e[128]" "e[143]" "e[145]" "e[147:148]" "e[150]" "e[152]" "e[154]" "e[157]" "e[159]" "e[170]" "e[179]" "e[189]" "e[191]" "e[203]" "e[208]" "e[219]" "e[224]" "e[235]" "e[240]" "e[247]" "e[256]" "e[269]" "e[272]" "e[275]" "e[299]" "e[326]" "e[336]" "e[338]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.50074678659439087;
	setAttr ".dr" no;
	setAttr ".re" 123;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak19";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[214:219]" -type "float3"  142.41014099 1.39028907 -225.99705505
		 142.12055969 -1.46417153 -225.99705505 142.16983032 0.83391768 -225.99705505 141.99641418
		 -0.96966666 -225.99705505 142.58685303 1.46417153 -225.99705505 142.48965454 0.26783195
		 -225.99705505;
createNode polySplitRing -n "polySplitRing10";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 39 "e[8]" "e[11]" "e[16]" "e[20]" "e[22]" "e[27]" "e[30]" "e[41]" "e[53]" "e[62]" "e[66]" "e[103]" "e[115]" "e[130:131]" "e[133]" "e[135]" "e[137]" "e[139]" "e[141]" "e[156]" "e[169]" "e[178]" "e[182]" "e[186]" "e[195]" "e[200]" "e[211]" "e[216]" "e[227]" "e[232]" "e[249]" "e[255]" "e[259]" "e[262]" "e[266]" "e[301]" "e[325]" "e[329]" "e[333]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.39090171456336975;
	setAttr ".re" 27;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak20";
	setAttr ".uopa" yes;
	setAttr -s 34 ".tk";
	setAttr ".tk[14:21]" -type "float3" -0.60151631 63.839443 -52.502663  -0.60151631 
		63.839443 -52.502663  -0.60151631 63.839443 -52.502663  -0.60151631 63.839443 -52.502663  
		0 83.68084 0  0 83.68084 0  0 83.68084 0  0 83.68084 0 ;
	setAttr ".tk[40:41]" -type "float3" 0 83.68084 0  -0.60151631 63.839443 
		-52.502663 ;
	setAttr ".tk[56:57]" -type "float3" -0.60151631 63.839443 -52.502663  0 
		83.68084 0 ;
	setAttr ".tk[68:71]" -type "float3" -1.1756145 88.398445 -102.61216  -1.1756145 
		88.398445 -102.61216  -1.1756145 88.398445 -102.61216  -0.60151631 63.839443 -52.502663 ;
	setAttr ".tk[88]" -type "float3" -0.60151631 63.839443 -52.502663 ;
	setAttr ".tk[146:147]" -type "float3" 0 83.68084 0  -0.60151631 63.839443 
		-52.502663 ;
	setAttr ".tk[169:171]" -type "float3" -0.60151631 63.839443 -52.502663  0 
		83.68084 0  -1.1756145 88.398445 -102.61216 ;
	setAttr ".tk[254:259]" -type "float3" -0.60151631 63.839443 -52.502663  0 
		83.68084 0  0 83.68084 0  0 83.68084 0  0 83.68084 0  -0.60151631 63.839443 -52.502663 ;
	setAttr ".tk[294:299]" -type "float3" -0.60151631 63.839443 -52.502663  0 
		83.68084 0  0 83.68084 0  0 83.68084 0  -1.1756145 88.398445 -102.61216  -0.60151631 
		63.839443 -52.502663 ;
createNode deleteComponent -n "deleteComponent3";
	setAttr ".dc" -type "componentList" 6 "f[0]" "f[8]" "f[10:11]" "f[58:59]" "f[243:244]" "f[283]";
createNode deleteComponent -n "deleteComponent4";
	setAttr ".dc" -type "componentList" 1 "f[274]";
createNode polySplitRing -n "polySplitRing11";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[376:378]" "e[380]" "e[383:384]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.94287306070327759;
	setAttr ".dr" no;
	setAttr ".re" 383;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak21";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk";
	setAttr ".tk[64:65]" -type "float3" -0.2533659 0 -22.114782  -0.43739352 
		0 -38.177402 ;
	setAttr ".tk[165]" -type "float3" -0.2533659 0 -22.114782 ;
createNode polySplitRing -n "polySplitRing12";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[388:390]" "e[392]" "e[395:396]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.035059086978435516;
	setAttr ".re" 395;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing13";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[575:576]" "e[578]" "e[580]" "e[582]" "e[584]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.98776757717132568;
	setAttr ".dr" no;
	setAttr ".re" 575;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing14";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[400:401]" "e[403]" "e[405]" "e[408:409]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.013061630539596081;
	setAttr ".re" 408;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace24";
	setAttr ".ics" -type "componentList" 3 "f[191]" "f[193]" "f[274:275]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7492504 6.2922902 -5.4263759 ;
	setAttr ".rs" 48746;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.877561542682169 6.1902855039671758 -5.501159646315493 ;
	setAttr ".cbx" -type "double3" -2.6211414392063448 6.3942939878538949 -5.3510825949900891 ;
createNode polyTweak -n "polyTweak22";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[311:325]" -type "float3" -4.7683716e-007 3.8146973e-006 7.6293945e-006  
		0 0 -3.8146973e-006  0 -1.1444092e-005 -7.6293945e-006  0 -1.9073486e-006 0  0 -9.5367432e-007 
		-1.1444092e-005  -8.9406967e-008 1.1444092e-005 0  0 -1.9073486e-006 -3.8146973e-006  
		0 5.7220459e-006 -3.8146973e-006  0 9.5367432e-006 0  9.5367432e-007 -9.5367432e-006 
		0  9.5367432e-007 -9.5367432e-007 3.8146973e-006  0 -3.8146973e-006 9.5367432e-007  
		2.3841858e-007 -5.7220459e-006 3.8146973e-006  9.5367432e-007 -1.9073486e-006 0  
		-1.9073486e-006 1.9073486e-006 -7.6293945e-006 ;
createNode polyExtrudeFace -n "polyExtrudeFace25";
	setAttr ".ics" -type "componentList" 2 "f[189]" "f[277]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7157209 6.325026 -5.5711732 ;
	setAttr ".rs" 56752;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.8289738562352698 6.2448216367796761 -5.6018933939715438 ;
	setAttr ".cbx" -type "double3" -2.6028253909093069 6.405230572326551 -5.5395203439555996 ;
createNode polyTweak -n "polyTweak23";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[313:321]" -type "float3" 110.49726 -40.23 -257.84528  106.24888 
		-34.5783 -256.87738  101.05061 -41.860401 -255.26015  96.925591 -36.359989 -254.30489  
		102.4434 -28.339659 -265.36511  93.156624 -30.272245 -262.56744  117.69367 -38.055851 
		-254.86751  113.59379 -32.352852 -253.74995  109.75953 -25.955908 -262.10565 ;
createNode polyExtrudeFace -n "polyExtrudeFace26";
	setAttr ".ics" -type "componentList" 3 "f[197]" "f[199]" "f[286:287]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.5403876 6.9448357 -4.48141 ;
	setAttr ".rs" 62875;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -5.5830369196441838 6.9195933652953014 -4.515453512074953 ;
	setAttr ".cbx" -type "double3" -5.4976739342846104 6.9700785947874886 -4.4474434603363866 ;
createNode polyTweak -n "polyTweak24";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk";
	setAttr ".tk[322:327]" -type "float3" -2.4662752 -4.0661931 21.513212  -6.9336438 
		3.338362 20.03113  -15.17712 1.7724164 22.617929  -10.826777 -5.4339027 24.043484  
		-0.77335817 5.4339027 22.780107  3.5795279 -2.0431876 24.176552 ;
createNode polySplitRing -n "polySplitRing15";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[364:366]" "e[368]" "e[371:372]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.83355826139450073;
	setAttr ".dr" no;
	setAttr ".re" 371;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak25";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk[327:335]" -type "float3"  -47.33426285 -34.77109146
		 -113.017776489 -47.9552536 -33.63140106 -112.49720001 -48.57283783 -34.78380203 -111.054733276
		 -49.19288635 -33.63375473 -110.53408813 -50.31301498 -31.98139954 -114.26560974 -51.5474472
		 -31.9686985 -112.30244446 -44.62115479 -34.30284882 -113.76030731 -45.27762604 -33.11972046
		 -113.23610687 -47.6459198 -31.42636871 -115.069046021;
createNode polySplitRing -n "polySplitRing16";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[376:378]" "e[380]" "e[382:383]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 -0.012643830477458096 1;
	setAttr ".wt" 0.074040085077285767;
	setAttr ".re" 382;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing17";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[567:568]" "e[570]" "e[572]" "e[574]" "e[576]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.017243275418877602;
	setAttr ".re" 567;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak26";
	setAttr ".uopa" yes;
	setAttr -s 76 ".tk";
	setAttr ".tk[0:9]" -type "float3" 0 57.535229 0  0 -47.723156 0  0 -7.7527752 
		0  0 6.5315928 0  316.30371 4.7683716e-007 13.127059  316.30368 -4.7683716e-007 13.127064  
		0 55.018684 0  0 -71.065338 0  0 29.501581 0  0 -29.010925 0 ;
	setAttr ".tk[18]" -type "float3" 0 6.1121101 0 ;
	setAttr ".tk[20]" -type "float3" 0 -13.359636 0 ;
	setAttr ".tk[22:23]" -type "float3" 0 -21.769634 0  0 34.755203 0 ;
	setAttr ".tk[40:49]" -type "float3" 0 -39.291321 0  0 -83.794159 0  0 -44.385746 
		0  0 -4.8884029 0  326.34363 4.7683716e-007 8.9355631  339.74167 0 10.417336  0 8.0989475 
		0  0 63.820744 0  0 126.40472 0  0 88.800346 0 ;
	setAttr ".tk[66:69]" -type "float3" 348.87112 -9.5367432e-007 11.427104  
		316.30368 0 13.127066  0 1.3478903 0  0 2.6851709 0 ;
	setAttr ".tk[77:79]" -type "float3" 0 12.1984 0  0 6.741797 0  0 6.195672 
		0 ;
	setAttr ".tk[83:92]" -type "float3" 59.49485 -5.6418438 -21.829477  72.668739 
		-7.5476313 -56.338161  72.668732 4.8943086 -56.338158  59.49485 3.3314905 -21.829479  
		59.49485 -0.24940778 -21.829477  259.20657 -2.3841858e-007 11.325911  261.01636 0 
		0.98349106  261.01639 0 0.98348379  259.20657 0 11.325911  259.20657 0 11.325911 ;
	setAttr ".tk[145:159]" -type "float3" 0 -37.108269 0  0 -81.086082 0  0 -46.30545 
		0  0 -6.4004555 0  60.556919 -5.7954898 -24.611565  259.35248 0 10.492079  317.11298 
		0 12.789116  318.92926 0 12.990017  318.19312 4.7683716e-007 12.908578  259.35245 
		0 10.49209  60.556915 3.4574881 -24.611561  0 6.6579537 0  0 58.041965 0  0 63.908615 
		0  0 32.822346 0 ;
	setAttr ".tk[212:218]" -type "float3" 337.5907 0 10.179446  318.01987 4.7683716e-007 
		12.889399  316.30365 0 13.127079  259.2066 0 11.325902  59.49485 -2.9496524 -21.829477  
		0 -2.5609763 0  0 0.10102424 0 ;
	setAttr ".tk[228:230]" -type "float3" 0 -4.810987 0  0 -27.938097 0  0 -11.433919 
		0 ;
	setAttr ".tk[250:256]" -type "float3" 343.31036 0 10.812035  318.4808 0 12.94043  
		316.30371 -4.7683716e-007 13.127059  259.20667 0 11.325894  59.49485 1.9317122 -21.829477  
		0 4.5052762 0  0 4.7725143 0 ;
	setAttr ".tk[266:268]" -type "float3" 0 25.93771 0  0 36.147163 0  0 20.391264 
		0 ;
createNode polySplitRing -n "polySplitRing18";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[667:668]" "e[670]" "e[672]" "e[674]" "e[676]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.95785176753997803;
	setAttr ".dr" no;
	setAttr ".re" 667;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing19";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[602]" "e[604]" "e[606]" "e[609]" "e[611]" "e[614:615]" "e[619]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.037673782557249069;
	setAttr ".re" 606;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing20";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[679:680]" "e[682]" "e[684]" "e[686]" "e[688]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.99044710397720337;
	setAttr ".dr" no;
	setAttr ".re" 679;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing21";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[590:591]" "e[593]" "e[595]" "e[597]" "e[599]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.025069450959563255;
	setAttr ".re" 590;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing22";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[635]" "e[637]" "e[639]" "e[642]" "e[644]" "e[647:648]" "e[652]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.047249644994735718;
	setAttr ".re" 637;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode deleteComponent -n "deleteComponent5";
	setAttr ".dc" -type "componentList" 5 "e[669]" "e[671]" "e[673]" "e[675]" "e[677:678]";
createNode deleteComponent -n "deleteComponent6";
	setAttr ".dc" -type "componentList" 5 "e[657]" "e[659]" "e[661]" "e[663]" "e[665:666]";
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[615]" "e[698]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.1405578 6.1105757 -1.2577373 ;
	setAttr ".rs" 55718;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.6168402147124961 5.9488115049437384 -2.5220804719151442 ;
	setAttr ".cbx" -type "double3" -2.6642752546488544 6.2723399473265511 0.006605951724941406 ;
createNode polyTweak -n "polyTweak27";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[50]" -type "float3" -20.642498 63.51709 0.15812366 ;
	setAttr ".tk[62]" -type "float3" -20.642498 48.174812 0.15812366 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[749]" "e[751]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.6399896 5.7439923 -1.190531 ;
	setAttr ".rs" 38609;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -1.8638636605513161 5.6679545713499886 -1.7848311857824501 ;
	setAttr ".cbx" -type "double3" -1.4161155510009094 5.8200302244261604 -0.59623077188588869 ;
createNode polyTweak -n "polyTweak28";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[388:390]" -type "float3"  -153.48405457 -45.23097229
		 -72.55245972 -155.75218201 -44.58505249 -67.48638916 -213.68363953 -28.085697174
		 61.92314148;
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[606]" "e[691]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.24719 6.1419401 -1.263081 ;
	setAttr ".rs" 53277;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.6553550384542453 5.960140240295301 -2.5308388224133025 ;
	setAttr ".cbx" -type "double3" -2.8390247529237564 6.3237400938109261 0.0046770800122169481 ;
createNode polyTweak -n "polyTweak29";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[391:393]" -type "float3" -31.104713 -57.426723 -4.714715  -31.104713 
		-57.426723 -4.714715  -31.104713 -57.426723 -4.714715 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[639]" "e[744]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.3584027 6.7846165 -1.0639126 ;
	setAttr ".rs" 50500;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -5.5543370409829507 6.6164476133421761 -1.6184428677485465 ;
	setAttr ".cbx" -type "double3" -5.1624685576548579 6.952785503967176 -0.50938243142635031 ;
createNode polyTweak -n "polyTweak30";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[394:396]" -type "float3"  320.62402344 60.8960495 -90.27787018
		 315.36126709 60.91326523 -85.01537323 180.93403625 61.35267258 49.40632629;
createNode polyMergeVert -n "polyMergeVert1";
	setAttr ".ics" -type "componentList" 1 "vtx[397:399]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".d" 0.01;
createNode polyTweak -n "polyTweak31";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[397:399]" -type "float3"  97.061538696 -40.41830063
		 -242.68887329 99.38568115 -38.82912064 -237.46618652 146.25431824 -6.78452587 -132.15478516;
createNode polyMergeVert -n "polyMergeVert2";
	setAttr ".ics" -type "componentList" 2 "vtx[211]" "vtx[397]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[211]";
createNode polyTweak -n "polyTweak32";
	setAttr ".uopa" yes;
	setAttr ".tk[397]" -type "float3"  43.49648285 40.23244858 20.37059784;
createNode polyMergeVert -n "polyMergeVert3";
	setAttr ".ics" -type "componentList" 2 "vtx[334]" "vtx[396]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[334]";
createNode polyMergeVert -n "polyMergeVert4";
	setAttr ".ics" -type "componentList" 2 "vtx[383]" "vtx[395]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[383]";
createNode polyMergeVert -n "polyMergeVert5";
	setAttr ".ics" -type "componentList" 2 "vtx[301]" "vtx[394]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[301]";
createNode polyMergeVert -n "polyMergeVert6";
	setAttr ".ics" -type "componentList" 2 "vtx[195]" "vtx[388]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[195]";
createNode polyMergeVert -n "polyMergeVert7";
	setAttr ".ics" -type "componentList" 2 "vtx[189]" "vtx[390]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[189]";
createNode polyMergeVert -n "polyMergeVert8";
	setAttr ".ics" -type "componentList" 2 "vtx[183]" "vtx[390]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[183]";
createNode polyMergeVert -n "polyMergeVert9";
	setAttr ".ics" -type "componentList" 2 "vtx[9]" "vtx[390]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[9]";
createNode polySplitRing -n "polySplitRing23";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[747:748]" "e[750]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.035077739506959915;
	setAttr ".re" 750;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing24";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[756:758]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.016974784433841705;
	setAttr ".re" 757;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing25";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[767:768]" "e[770]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.99053722620010376;
	setAttr ".dr" no;
	setAttr ".re" 767;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing26";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[719:720]" "e[722]" "e[724]" "e[726]" "e[728]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.81931167840957642;
	setAttr ".dr" no;
	setAttr ".re" 719;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyMergeVert -n "polyMergeVert10";
	setAttr ".ics" -type "componentList" 2 "vtx[355]" "vtx[392]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[392]";
createNode deleteComponent -n "deleteComponent7";
	setAttr ".dc" -type "componentList" 1 "f[367]";
createNode deleteComponent -n "deleteComponent8";
	setAttr ".dc" -type "componentList" 1 "f[366]";
createNode polyExtrudeEdge -n "polyExtrudeEdge5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[639]" "e[744]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.3584027 6.7846165 -1.0639126 ;
	setAttr ".rs" 36532;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -5.5543370409829507 6.6164476133421761 -1.6184428677485465 ;
	setAttr ".cbx" -type "double3" -5.1624685576548579 6.952785503967176 -0.50938243142635031 ;
createNode polySplitRing -n "polySplitRing27";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[785:786]" "e[788]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.83012926578521729;
	setAttr ".dr" no;
	setAttr ".re" 785;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak33";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[404:406]" -type "float3"  123.1185379 0 -245.64100647
		 123.1185379 0 -245.64100647 123.1185379 0 -245.64100647;
createNode polyMergeVert -n "polyMergeVert11";
	setAttr ".ics" -type "componentList" 1 "vtx[404:406]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".d" 0.01;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert12";
	setAttr ".ics" -type "componentList" 1 "vtx[404:406]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".d" 0.01;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert13";
	setAttr ".ics" -type "componentList" 1 "vtx[404:406]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".d" 0.01;
createNode polyTweak -n "polyTweak34";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk[404:406]" -type "float3"  -17.17230225 -11.74099731
		 -38.58558655 -14.84814453 -10.15182495 -33.36289978 32.020507813 21.89279175 71.94850159;
createNode polySplitRing -n "polySplitRing28";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[785:787]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.02919367328286171;
	setAttr ".re" 786;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyMergeVert -n "polyMergeVert14";
	setAttr ".ics" -type "componentList" 2 "vtx[375]" "vtx[410]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[410]";
createNode polyMergeVert -n "polyMergeVert15";
	setAttr ".ics" -type "componentList" 2 "vtx[399]" "vtx[405]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[399]";
createNode polyMergeVert -n "polyMergeVert16";
	setAttr ".ics" -type "componentList" 2 "vtx[211]" "vtx[404]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[211]";
createNode polyTweak -n "polyTweak35";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[406]" -type "float3" 0 -3.0517578e-005 0 ;
createNode polyMergeVert -n "polyMergeVert17";
	setAttr ".ics" -type "componentList" 1 "vtx[404:405]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".d" 0.01;
createNode polyTweak -n "polyTweak36";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[404:405]" -type "float3"  -23.43432617 -16.02230835
		 -52.65570068 23.43432617 16.022338867 52.65570068;
createNode polyMergeVert -n "polyMergeVert18";
	setAttr ".ics" -type "componentList" 2 "vtx[369]" "vtx[397]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[369]";
createNode polyTweak -n "polyTweak37";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[369]" -type "float3" 0.0079266233 0 0.69186634 ;
	setAttr ".tk[404:405]" -type "float3" 0.79943949 0 69.778168  12.204321 0 
		3.5176685 ;
createNode polyMergeVert -n "polyMergeVert19";
	setAttr ".ics" -type "componentList" 2 "vtx[349]" "vtx[394]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[349]";
createNode polyTweak -n "polyTweak38";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[349]" -type "float3" 0.013661425 0 1.1924226 ;
	setAttr ".tk[355]" -type "float3" -1.9790605e-009 0 7.8231096e-007 ;
createNode polySplitRing -n "polySplitRing29";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[691:692]" "e[694]" "e[696]" "e[698]" "e[700]" "e[702]" "e[704]" "e[750]" "e[754]" "e[759]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.30005466938018799;
	setAttr ".re" 754;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyMergeVert -n "polyMergeVert20";
	setAttr ".ics" -type "componentList" 2 "vtx[177]" "vtx[405]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[177]";
createNode deleteComponent -n "deleteComponent9";
	setAttr ".dc" -type "componentList" 1 "vtx[336:341]";
createNode deleteComponent -n "deleteComponent10";
	setAttr ".dc" -type "componentList" 1 "vtx[336:341]";
createNode deleteComponent -n "deleteComponent11";
	setAttr ".dc" -type "componentList" 0;
createNode polySplitRing -n "polySplitRing30";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[649:654]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.32554543018341064;
	setAttr ".re" 649;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing31";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[643:648]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.55112332105636597;
	setAttr ".dr" no;
	setAttr ".re" 643;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing32";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[739:741]" "e[778]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.42518565058708191;
	setAttr ".re" 741;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing33";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[744:745]" "e[747]" "e[780]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".wt" 0.67991113662719727;
	setAttr ".dr" no;
	setAttr ".re" 744;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyMergeVert -n "polyMergeVert21";
	setAttr ".ics" -type "componentList" 2 "vtx[408]" "vtx[426]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[426]";
createNode polyMergeVert -n "polyMergeVert22";
	setAttr ".ics" -type "componentList" 2 "vtx[414]" "vtx[422]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".mtc" -type "componentList" 1 "vtx[422]";
createNode polyAutoProj -n "polyAutoProj1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:414]";
	setAttr ".ix" -type "matrix" -0.81764579915175628 0 -0.0076597669270690727 0 0 1 0 0
		 0.0093676636551053277 0 -0.99995612247620391 0 2.2520325076435483 350.81025816038948 282.88738491206743 1;
	setAttr ".s" -type "double3" 17.395213464145908 17.395213464145908 17.395213464145908 ;
	setAttr ".ps" 0.20000000298023224;
	setAttr ".dl" yes;
select -ne :time1;
	setAttr ".o" 6;
	setAttr ".unw" 6;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "polyAutoProj1.out" "pCubeShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace3.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace4.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace5.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyTweak2.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyExtrudeFace10.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace10.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace10.out" "polyTweak6.ip";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polyExtrudeFace11.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace11.mp";
connectAttr "polyTweak7.out" "polyExtrudeFace12.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace12.mp";
connectAttr "polyExtrudeFace11.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polyExtrudeFace13.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace13.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polyExtrudeFace14.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace14.mp";
connectAttr "polyExtrudeFace13.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polyExtrudeFace14.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyExtrudeFace15.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace15.mp";
connectAttr "polySplitRing7.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polyExtrudeFace15.out" "polyTweak12.ip";
connectAttr "polySplitRing8.out" "polyExtrudeFace16.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace16.mp";
connectAttr "polyTweak13.out" "polyExtrudeFace17.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace17.mp";
connectAttr "polyExtrudeFace16.out" "polyTweak13.ip";
connectAttr "polyTweak14.out" "polyExtrudeFace18.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace18.mp";
connectAttr "polyExtrudeFace17.out" "polyTweak14.ip";
connectAttr "polyExtrudeFace18.out" "polyExtrudeFace19.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace19.mp";
connectAttr "polyTweak15.out" "polyExtrudeFace20.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace20.mp";
connectAttr "polyExtrudeFace19.out" "polyTweak15.ip";
connectAttr "polyExtrudeFace20.out" "polyTweak16.ip";
connectAttr "polyTweak16.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyExtrudeFace21.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace21.mp";
connectAttr "polyTweak17.out" "polyExtrudeFace22.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace22.mp";
connectAttr "polyExtrudeFace21.out" "polyTweak17.ip";
connectAttr "polyTweak18.out" "polyExtrudeFace23.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace23.mp";
connectAttr "polyExtrudeFace22.out" "polyTweak18.ip";
connectAttr "polyTweak19.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polyExtrudeFace23.out" "polyTweak19.ip";
connectAttr "polySplitRing9.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing10.out" "polyTweak20.ip";
connectAttr "polyTweak20.out" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "deleteComponent4.ig";
connectAttr "polyTweak21.out" "polySplitRing11.ip";
connectAttr "pCubeShape1.wm" "polySplitRing11.mp";
connectAttr "deleteComponent4.og" "polyTweak21.ip";
connectAttr "polySplitRing11.out" "polySplitRing12.ip";
connectAttr "pCubeShape1.wm" "polySplitRing12.mp";
connectAttr "polySplitRing12.out" "polySplitRing13.ip";
connectAttr "pCubeShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing13.out" "polySplitRing14.ip";
connectAttr "pCubeShape1.wm" "polySplitRing14.mp";
connectAttr "polyTweak22.out" "polyExtrudeFace24.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace24.mp";
connectAttr "polySplitRing14.out" "polyTweak22.ip";
connectAttr "polyTweak23.out" "polyExtrudeFace25.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace25.mp";
connectAttr "polyExtrudeFace24.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polyExtrudeFace26.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace26.mp";
connectAttr "polyExtrudeFace25.out" "polyTweak24.ip";
connectAttr "polyTweak25.out" "polySplitRing15.ip";
connectAttr "pCubeShape1.wm" "polySplitRing15.mp";
connectAttr "polyExtrudeFace26.out" "polyTweak25.ip";
connectAttr "polySplitRing15.out" "polySplitRing16.ip";
connectAttr "pCubeShape1.wm" "polySplitRing16.mp";
connectAttr "polyTweak26.out" "polySplitRing17.ip";
connectAttr "pCubeShape1.wm" "polySplitRing17.mp";
connectAttr "polySplitRing16.out" "polyTweak26.ip";
connectAttr "polySplitRing17.out" "polySplitRing18.ip";
connectAttr "pCubeShape1.wm" "polySplitRing18.mp";
connectAttr "polySplitRing18.out" "polySplitRing19.ip";
connectAttr "pCubeShape1.wm" "polySplitRing19.mp";
connectAttr "polySplitRing19.out" "polySplitRing20.ip";
connectAttr "pCubeShape1.wm" "polySplitRing20.mp";
connectAttr "polySplitRing20.out" "polySplitRing21.ip";
connectAttr "pCubeShape1.wm" "polySplitRing21.mp";
connectAttr "polySplitRing21.out" "polySplitRing22.ip";
connectAttr "pCubeShape1.wm" "polySplitRing22.mp";
connectAttr "polySplitRing22.out" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "deleteComponent6.ig";
connectAttr "polyTweak27.out" "polyExtrudeEdge1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge1.mp";
connectAttr "deleteComponent6.og" "polyTweak27.ip";
connectAttr "polyTweak28.out" "polyExtrudeEdge2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge1.out" "polyTweak28.ip";
connectAttr "polyTweak29.out" "polyExtrudeEdge3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge3.mp";
connectAttr "polyExtrudeEdge2.out" "polyTweak29.ip";
connectAttr "polyTweak30.out" "polyExtrudeEdge4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge4.mp";
connectAttr "polyExtrudeEdge3.out" "polyTweak30.ip";
connectAttr "polyTweak31.out" "polyMergeVert1.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert1.mp";
connectAttr "polyExtrudeEdge4.out" "polyTweak31.ip";
connectAttr "polyTweak32.out" "polyMergeVert2.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak32.ip";
connectAttr "polyMergeVert2.out" "polyMergeVert3.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert3.mp";
connectAttr "polyMergeVert3.out" "polyMergeVert4.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert4.mp";
connectAttr "polyMergeVert4.out" "polyMergeVert5.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert5.mp";
connectAttr "polyMergeVert5.out" "polyMergeVert6.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert6.mp";
connectAttr "polyMergeVert6.out" "polyMergeVert7.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert7.mp";
connectAttr "polyMergeVert7.out" "polyMergeVert8.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert8.mp";
connectAttr "polyMergeVert8.out" "polyMergeVert9.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert9.mp";
connectAttr "polyMergeVert9.out" "polySplitRing23.ip";
connectAttr "pCubeShape1.wm" "polySplitRing23.mp";
connectAttr "polySplitRing23.out" "polySplitRing24.ip";
connectAttr "pCubeShape1.wm" "polySplitRing24.mp";
connectAttr "polySplitRing24.out" "polySplitRing25.ip";
connectAttr "pCubeShape1.wm" "polySplitRing25.mp";
connectAttr "polySplitRing25.out" "polySplitRing26.ip";
connectAttr "pCubeShape1.wm" "polySplitRing26.mp";
connectAttr "polySplitRing26.out" "polyMergeVert10.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert10.mp";
connectAttr "polyMergeVert10.out" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "polyExtrudeEdge5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge5.mp";
connectAttr "polyTweak33.out" "polySplitRing27.ip";
connectAttr "pCubeShape1.wm" "polySplitRing27.mp";
connectAttr "polyExtrudeEdge5.out" "polyTweak33.ip";
connectAttr "polySplitRing27.out" "polyMergeVert11.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert11.mp";
connectAttr "polyMergeVert11.out" "polyMergeVert12.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert12.mp";
connectAttr "polyTweak34.out" "polyMergeVert13.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert13.mp";
connectAttr "polyMergeVert12.out" "polyTweak34.ip";
connectAttr "polyMergeVert13.out" "polySplitRing28.ip";
connectAttr "pCubeShape1.wm" "polySplitRing28.mp";
connectAttr "polySplitRing28.out" "polyMergeVert14.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert14.mp";
connectAttr "polyMergeVert14.out" "polyMergeVert15.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert15.mp";
connectAttr "polyTweak35.out" "polyMergeVert16.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert16.mp";
connectAttr "polyMergeVert15.out" "polyTweak35.ip";
connectAttr "polyTweak36.out" "polyMergeVert17.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert17.mp";
connectAttr "polyMergeVert16.out" "polyTweak36.ip";
connectAttr "polyTweak37.out" "polyMergeVert18.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert18.mp";
connectAttr "polyMergeVert17.out" "polyTweak37.ip";
connectAttr "polyTweak38.out" "polyMergeVert19.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert19.mp";
connectAttr "polyMergeVert18.out" "polyTweak38.ip";
connectAttr "polyMergeVert19.out" "polySplitRing29.ip";
connectAttr "pCubeShape1.wm" "polySplitRing29.mp";
connectAttr "polySplitRing29.out" "polyMergeVert20.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert20.mp";
connectAttr "polyMergeVert20.out" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "polySplitRing30.ip";
connectAttr "pCubeShape1.wm" "polySplitRing30.mp";
connectAttr "polySplitRing30.out" "polySplitRing31.ip";
connectAttr "pCubeShape1.wm" "polySplitRing31.mp";
connectAttr "polySplitRing31.out" "polySplitRing32.ip";
connectAttr "pCubeShape1.wm" "polySplitRing32.mp";
connectAttr "polySplitRing32.out" "polySplitRing33.ip";
connectAttr "pCubeShape1.wm" "polySplitRing33.mp";
connectAttr "polySplitRing33.out" "polyMergeVert21.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert21.mp";
connectAttr "polyMergeVert21.out" "polyMergeVert22.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert22.mp";
connectAttr "polyMergeVert22.out" "polyAutoProj1.ip";
connectAttr "pCubeShape1.wm" "polyAutoProj1.mp";
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of 1.ma
