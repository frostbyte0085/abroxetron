
WkXmlLibrary - A script for 3dsMax
----------------------------------
A script by WerwacK
3dsMax Supported Versions:	from 3dsMax 9 to the most recent release (currently 3dsMax 2011)
Contact: werwack@werwackfx.com
Website: www.werwackfx.com
Copyright: WerwacK
Free for commercial and non-commercial use


Purpose:
--------
The purpose of this script is to give an easy and full access to XML files: creation, reading and modifications
It is intented to be used with the same flexibility as a library: instanciate the structure where you need it
in your code and use the member functions to manipulate the XML files.

This script strongly relies on the DotNet interface.


Content:
--------
This script is made of only 1 file named WkXmlLibrary.ms
This file has 2 parts: the code and a demo to learn how to use it.


Installation:
-------------
Copy the folder "Scripts" extracted from the archive (alternatively you can also just copy
the file WkXmlLibrary.ms it contains) to the script folder of 3dsMax (<3dsMax folder>\scripts\)


Use:
----
Open the script file in a text editor, go to the end and follow the instructions described in the demo part


Bugs and feedback:
------------------
Feel free to contact me at werwack@werwackfx.com


