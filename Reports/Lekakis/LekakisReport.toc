\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Engine Development}{2}
\contentsline {subsection}{\numberline {2.1}Scene Representation}{2}
\contentsline {subsection}{\numberline {2.2}SceneNodeBehaviour}{2}
\contentsline {subsection}{\numberline {2.3}Mounting Nodes}{3}
\contentsline {subsection}{\numberline {2.4}Deferred Rendering}{3}
\contentsline {subsection}{\numberline {2.5}Path-finding}{5}
\contentsline {subsubsection}{\numberline {2.5.1}AsyncPathFindingService}{5}
\contentsline {subsubsection}{\numberline {2.5.2}Producing}{6}
\contentsline {subsubsection}{\numberline {2.5.3}Consuming}{6}
\contentsline {subsection}{\numberline {2.6}Physics Integration}{6}
\contentsline {subsubsection}{\numberline {2.6.1}PhysicalEntity class}{7}
\contentsline {subsubsection}{\numberline {2.6.2}Handling collision response / gravity}{7}
\contentsline {subsubsection}{\numberline {2.6.3}Synchronising SceneNode and PhysicalEntity transformations}{8}
\contentsline {subsubsection}{\numberline {2.6.4}Collision groups}{8}
\contentsline {subsection}{\numberline {2.7}Post-Process Effects}{9}
\contentsline {subsection}{\numberline {2.8}3D Sound and Music}{10}
\contentsline {section}{\numberline {3}Game Development}{11}
\contentsline {subsection}{\numberline {3.1}Abstract Game Manager}{11}
\contentsline {subsubsection}{\numberline {3.1.1}Generating Level Links}{11}
\contentsline {subsubsection}{\numberline {3.1.2}Level XML Parsing}{12}
\contentsline {subsubsection}{\numberline {3.1.3}Handling Object Properties and Behaviours}{13}
\contentsline {subsection}{\numberline {3.2}Dragon Behaviour}{15}
\contentsline {section}{\numberline {4}Audio and 3D Model Assets}{15}
\contentsline {subsection}{\numberline {4.1}Audio Assets}{15}
\contentsline {subsection}{\numberline {4.2}3D Models \& Textures contribution}{16}
