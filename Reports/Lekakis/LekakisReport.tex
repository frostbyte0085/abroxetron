\documentclass{ueacmpstyle}
\usepackage[utf8]{inputenc}
\usepackage{subfig}
%\usepackage[textwidth=18cm,textheight=23cm]{geometry}
\usepackage[english]{babel} % English
\usepackage{amssymb} % Extra symbols                                    % http://milde.users.sourceforge.net/LUCR/Math/mathpackages/amssymb-symbols.pdf
\usepackage{amsmath} % AMS mathematical facilities for LaTeX.           % http://tug.ctan.org/pkg/amsmath
\usepackage{amsthm} % Typesetting theorems (AMS style).                 % http://www.ctan.org/pkg/amsthm
\usepackage{underscore}
\usepackage{graphics}
\usepackage[usenames,dvipsnames]{color}
\usepackage{listings}
\usepackage{url}
\usepackage{caption}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{black}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\usepackage{float}

\definecolor{lightlightgray}{gray}{0.9}
\lstset{
	language=C++,
	backgroundcolor=\color{lightlightgray},
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue},
	commentstyle=\color{OliveGreen},
	stringstyle=\color{red},
	tabsize=4,
	captionpos=t,
	title=\lstname,
	frame=single,
	morekeywords={set,get,readonly,ref,out,internal,foreach,in,is,partial,abstract,sealed,string,select,from,null,as} % C# keywords
}

\begin{document}
\title{Abroxetron Individual Report\\CMPSME22 Assignment}

\author{Author: Pantelis Lekakis}

\maketitle

\tableofcontents

\pagebreak

\section{Introduction}
Throughout the development of the game, I re-used and improved some concepts that I introduced in the previous game, however as \textit{Abroxetron} was a much more complex game than \textit{Dr Goggler}, drastic changes and additions were required. More importantly, the engine was redesigned with a deferred renderer, a flat scene-graph with mountable nodes, path-finding using a separate thread, physics engine, 3D sound and post-processing effects. From the game logic point of view, we make extensive use of behaviours and properties for all the game objects in the scene and I spent some more time on refining the sound effects.\\
In this report I will try to outline all the features that I have worked on.

\section{Engine Development}

\subsection{Scene Representation}
For the needs of this game a modification of \textit{Dr Goggler's} scene-graph was used. The main difference was that the new scene-graph is a flat named dictionary of {\tt SceneNode} class instances with the ability to mount a node upon another to inherit its translation and orientation. Below a simple flowchart of the updating of the scene-graph is shown:
\begin{figure}[H]
  \caption{Scene-graph update sequence}
  \centering
    \includegraphics[width=0.95\textwidth]{data/scenegraph.png}
\end{figure}

\subsection{SceneNodeBehaviour}
As in the previous game, the {\tt SceneNodeBehaviour} defines how the node will be updated and its actions. It is especially useful to handle collision response from the physics engine, issuing new path-find requests and responding to path-find updates, and most {\tt SceneNode} instances in the game subclass it. An outline of the most important methods in the class is illustrated below:
\begin{lstlisting} [caption=SceneNodeBehaviour class outline]
public abstract class SceneNodeBehaviour
{
	public SceneNode SceneNode
	{
	    set;get
	}
	
	public abstract bool Start();
	// if the Update method returns false, the node is removed from the scene
	public abstract bool Update();
	public abstract void Dispose();
	
	public virtual void OnTriggerEnter(SceneNode otherNode) {}
	public virtual void OnTriggerExit(SceneNode otherNode) {}
	public virtual void OnTriggerStay(SceneNode otherNode) {}
	
	public virtual void OnCollideEnter(SceneNode otherNode) {}
	public virtual void OnCollideExit(SceneNode otherNode) {}
	public virtual void OnCollideStay(SceneNode otherNode) {}
	
	public virtual void OnPathFindUpdated(List<Vector2> newPath) { }
	public virtual void PathFind(Vector2 worldStart, Vector2 worldEnd)
	{
	    WaitingForPathFind = true;
	    pathFindQuery = new PathFindQuery(SceneNode.Name, worldStart, worldEnd);
	}
}
\end{lstlisting}
The collision and trigger response methods are called automatically from the physics engine delegates, which are handled in the {\tt SceneService} class and the {\tt SceneNodeBehaviour} is also the producer for path-find requests (more in the \textit{Path-finding} section).

\subsection{Mounting Nodes}
Instead of using a hierarchy, we went with mountable elements on a {\tt SceneNode}; elements supported are other {\tt SceneNode} and {\tt ParticleSystem} instances, mountable on either the {\tt SceneNode} position or on a bone (if there is a {\tt Model} set for that node). In the case of another {\tt SceneNode}:
\begin{lstlisting} [caption=Attaching another SceneNode]
public void Attach(string attachPoint, SceneNode node)
{
    node.mountedOn = this;
    node.mountPoint = attachPoint;

    mountedNodes[attachPoint] = node;
}
\end{lstlisting}

Now when that {\tt SceneNode} updates, it will follow the {\tt attachPoint} translation and orientation.\\
The same concept would apply for mounting a {\tt ParticleSystem}, but I will not go into that as it is Olivier's work.

\subsection{Deferred Rendering}
At the heart of the engine's rendering system is a deferred renderer (following Catalin's article on XNA deferred rendering\cite{urlDeferred}, extended to support skinning, emission mapping and a tighter G-Buffer by packing components) featuring the \textit{Phong} illumination model with \textit{Diffuse}, \textit{Normal}, \textit{Specular} and \textit{Emission} maps, and is used to render most of the game's 3D content. The reason for this decision was mainly the dark theme of the game and the need to dynamically illuminate multiple areas of the dungeon at the same time, something that a forward renderer has issues handling in real-time frame-rates.\\
Before going into further detail, let us have a look at the \textit{G-Buffer} structure for this renderer:

\begin{figure}[H]
  \caption{G-Buffer structure}
  \centering
    \includegraphics[width=0.55\textwidth]{data/deferred.png}
\end{figure}

As you can see the goal is to tightly pack whatever data we might need in the illumination process, as the \textit{G-Buffer} is the only link between the lighting shaders and our scene geometry, since lighting is applied as a post-process. Especially for the \textit{Normals Sampler}, I selected a higher precision texture format to minimise lighting artefacts due to floating point precision errors. The packing/unpacking algorithm for that is based on \textit{Skytiger's} Blog \cite{urlPacking} and a snippet of how it is used follows:

\begin{lstlisting} [caption=Packing/Unpacking Normals and Specular components]
// In G-Buffer construction, packing:
output.Normals = PackNIEtoRGB10A2(normal, specular_i, specular_e);

// In Point and Directional light shaders, unpacking:
float3 Normal = UnpackNIEfromRGB10A2(encodedNormal, SpecularIntensity, SpecularPower);
SpecularPower *= 255.0;
\end{lstlisting}
I believe it is better to show you a simple flow-chart of how the {\tt DeferredModelRenderer} works instead of posting a lot of code, so below you can see a diagram:
\begin{figure}[H]
  \caption{G-Buffer structure}
  \centering
    \includegraphics[width=0.98\textwidth]{data/deferred2.png}
\end{figure}
What cannot be inferred directly from the above is how the lights are actually rendered. In a traditional forward renderer, the lighting calculation is applied in the model's shader, however this is not the case in deferred. The way to do it is, after all the models have dumped their diffuse, normals, specular and emission values to the G-Buffer multiple-render target array, render light geometry (spheres for point, cones for spot and full-screen quads for directional lights), whose shader calculates the \textit{Phong} model based on the data of the G-Buffer. The result is written to a lightmap texture, which will finally contain only the coloured lights of our scene. This lightmap texture is the one to be used in combination with the base albedo texture sampler to construct the final image.\\
Moreover, in the composition shader I pass the depth sampler. The reason for that is to reconstruct the \textit{Z} values, since we need them in the framebuffer for forward-based effects such as particles systems, to be rendered correctly. Reconstructing the depth buffer is simple (just write the depth values to a DEPTH semantic) but very important:
\begin{lstlisting} [caption=Reconstructing depth in Composition shader]
// write back the depth so we can reuse it in forward-pass rendering,
// such as the particle system
output.Depth = tex2D(DepthMap, input.UV).r;
\end{lstlisting}
The composition shader itself is simple as well:
\begin{itemize}
\item Sample albedo and lightmap textures.
\item Combine them, also add some ambient dark colour.
\item Finally add emission (the \textit{w} component of albedo as we shown in the G-Buffer structure).
\end{itemize}
\begin{lstlisting} [caption=Lighting composition]
// to create the accumulated image, we combine the base albedo colour and the calculated lightmap
float4 Color = tex2D(Albedo, input.UV);
float4 Lighting = tex2D(LightMap, input.UV);
// add the emissive map color
float realEmissive = Color.w*10;
float blend = min(1,realEmissive);
float4 finalColor = saturate(Color * float4(1,1,1,1)*0.035 + 
		float4(Color.xyz * Lighting.xyz + Lighting.w, 1.0) * (1-blend) + Color * realEmissive);
output.Color = finalColor;
\end{lstlisting}

\subsection{Path-finding}
For navigating the NPCs around the level we are using an A* implementation\cite{urlAStar} in a \textit{Producer} / \textit{Consumer} design pattern. To improve the response of the enemies, I implemented a threaded environment for the path-finding solver where new queries are issued from the {\tt SceneNodeBehaviour} using the {\tt PathFind} method, but actually pushed to the path-finding thread on the behaviour-update in the scene-graph.

\subsubsection{AsyncPathFindingService}
The path-finding service {\tt DoWork} method is very simple: it retrieves the first element in the blocking-queue {\tt Queries}, path-finds it using the A* solver and pushes the result (if found) in the {\tt Results} blocking-queue as such:
\begin{lstlisting} [caption=AsyncPathFindingService work cycle]
while (!shouldStop)
{
	...
	...
    PathFindQuery query = queries.Get();
	...
	...
    PathNode foundPath = finder.FindPath(grid, ptStart, ptEnd);
    if (foundPath != null)
    {
        results.Enqueue(new PathFindResult(query.Name, foundPath.ToWorldList()));
    }
}
\end{lstlisting}

\subsubsection{Producing}
The {\tt SceneService} is responsible for checking if a node's {\tt SceneNodeBehaviour} has a path-find query issued and if it does, it pushes it to the {\tt Queries} blocking-queue of the {\tt AsyncPathFindingService} for processing. A simple illustration is shown below:
\begin{lstlisting} [caption=Pushing a query to the A* thread]
void BehaviourUpdate()
{
    foreach (SceneNode node in nodes.Values)
    {
        ...
        AsyncPathFindingService pfs = ServiceProvider.GetService<AsyncPathFindingService>();
        Debug.Assert(pfs != null);

        if (node.Behaviour.pathFindQuery != null)
        {
            pfs.Queries.Enqueue(node.Behaviour.pathFindQuery);
            node.Behaviour.pathFindQuery = null;
        }
        ...
    }
}
\end{lstlisting}

\subsubsection{Consuming}
When the {\tt SceneService} {\tt PathFindUpdate} detects that the {\tt AsyncPathFindingService} {\tt Results} blocking-queue has elements, it pops them one at a time and calls the respective node's {\tt OnPathFindUpdated}. A small code snippet follows:
\begin{lstlisting} [caption=Processing path-find results]
void PathFindUpdate()
{
    while(!pfs.Results.IsEmpty)
    {
        PathFindResult result = pfs.Results.Get();
        SceneNode callerNode = this[result.Name];
        if (callerNode != null && callerNode.Behaviour != null)
        {
            // moved this in the async service.
            callerNode.Behaviour.WaitingForPathFind = false;
            callerNode.Behaviour.OnPathFindUpdated(result.FoundPath);
        }
    }
}
\end{lstlisting}
Then it is the job of the {\tt SceneNodeBehaviour} to handle the movement of the entity (in our case a skeleton or the dragon) to follow the {\tt result.FoundPath} passed to the {\tt OnPathFindUpdated} method.

\subsection{Physics Integration}
Since in the game we wanted to use all kinds of physical objects from character controllers to rope physics, I decided to use a complete physics engine to do the job for us. Initially, we were deploying on \textit{XBox}, thus a fully managed physics engine was selected, \textit{BEPUPhysics}.\\
For creating and using physical entities in the game, I wanted to make it as transparent as possible; at times it was as if we were not using physics since everything is handled automatically by the scene-graph.
\subsubsection{PhysicalEntity class}
The main physics encapsulation happens in the {\tt PhysicalEntity} class, where we can create a physical object by either:
\begin{itemize}
\item Specifying its properties manually.
\item Automatically create the entity by using the node's {\tt Model} bounding box.
\end{itemize}
An example of how we do it the capsule entity is illustrated below:
\begin{lstlisting} [caption=Creating a capsule entity]
case PhysicalEntityType.Capsule:
    PhysicalCapsuleEntityProperties capsuleProperties = 
    				properties as PhysicalCapsuleEntityProperties;
    if (node != null)
    {
        Debug.Assert(node.Model != null);
        Debug.Assert(node.HasBoundingBox);
        BoundingBox bb = node.BoundingBox;
        Vector3[] bbCorners = bb.GetCorners();

        float capsuleHeight = Math.Abs(bbCorners[0].Y - bbCorners[2].Y);
        float capsuleWidth = Math.Abs(bbCorners[0].X);
        capsuleProperties.Length = capsuleHeight - capsuleWidth;
        capsuleProperties.Radius = capsuleWidth / 2;
        
        center = (bb.Max + bb.Min) / 2;
    }
\end{lstlisting}
Now that the capsule dimensions and centre are calculated, we can just create a \textit{BEPU} {\tt Capsule} class instance and add it as our physical entity.
\begin{lstlisting} [caption=Creating a capsule entity \#2]
entity = new Capsule(Vector3.Zero, capsuleProperties.Length, capsuleProperties.Radius, mass);
\end{lstlisting}
Valid entity types are:
\begin{itemize}
\item Box
\item Sphere
\item Cylinder
\item Capsule
\item ConvexHull
\item StaticMesh
\end{itemize}
where {\tt StaticMesh} is a special entity used mainly for level geometry, objects that will never be moved once loaded; this is how the collision against the dungeon is primarily checked.

\subsubsection{Handling collision response / gravity}
In order to correctly map a \textit{BEPU} Collidable to a {\tt SceneNode} I used a dictionary which keeps track of those relations. Whenever a {\tt PhysicalEntity} is created, the corresponding node - entity pair is added to that structure, likewise, when a {\tt PhysicalEntity} is destroyed, the pair is removed. This helps greatly in deciding which {\tt SceneNodeBehaviour's} collision response methods to call, as shown in the following code snippet for handling initial collision events:
\begin{lstlisting} [caption=Propagating collision events to SceneNodeBehaviours]
internal static void Physics_CollisionStarted(EntityCollidable sender,
					 Collidable other, CollidablePairHandler pair)
{
    SceneService sceneService = ServiceProvider.GetService<SceneService>();

    Collidable senderCollidable = sender as Collidable;
    SceneNode senderNode = null;
    sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);

    if (other != null && senderNode != null)
    {
        SceneNode otherNode = null;
        sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);

        if (otherNode != null && senderNode.Behaviour != null &&
            otherNode.Active && senderNode.Active)
        {
            Debug.Assert(otherNode.PhysicalEntity != null);

            if (otherNode.PhysicalEntity.IsTrigger)
                senderNode.Behaviour.OnTriggerEnter(otherNode);
            else
                senderNode.Behaviour.OnCollideEnter(otherNode);
        }
    }
}
\end{lstlisting}
Also, a check if the entity is a trigger is performed, and the correct type of collision response method is called on the {\tt senderNode} behaviour (where {\tt senderNode} is the node that initiated the collision).

\subsubsection{Synchronising SceneNode and PhysicalEntity transformations}
It is important to be able to automatically synchronise the transformations of the {\tt SceneNode} and its respective {\tt PhysicalEntity}. This happens in the {\tt SceneNode Translation} and {\tt Orientation} C\# properties; if there is a {\tt PhysicalEntity} present for this node, make sure to update its position and orientation to match that of the {\tt SceneNode}.\\
Code snippet follows:
\begin{lstlisting} [caption=Synchronising translation]
public Vector3 Translation
{
    set
    {
        // check to see if this is a static mesh. We cannot move those!
        if (PhysicalEntity != null && !translationIssuedByBEPU)
        {
            Debug.Assert(PhysicalEntity.EntityType != PhysicalEntityType.StaticMesh);
            // ok, if we reached here, it is not a StaticMesh, we can move it.
            // check that it's not null, just to make sure:
            Debug.Assert(PhysicalEntity.BEPUEntity != null);

            PhysicalEntity.Translation = value + PhysicalEntity.Center;
        }
        translation = value;
    }
    ...
}
\end{lstlisting}

\subsubsection{Collision groups}
As in the previous game, in this engine we support user defined collision groups to allow/prohibit collision response between certain objects or category groups. Keeping track of the {\tt CollisionGroup} instances and their relation with each other is handled through the {\tt CustomCollisionGroup} and {\tt CustomCollisionGroupPair} classes, which are simply string dictionaries. An example on how to set collision groups follows:
\begin{lstlisting} [caption=Creating collision groups]
CustomCollisionGroupPairs.Clear();
CustomCollisionGroups.Clear();

// Global Collision Groups
CustomCollisionGroups.Add("Terrain");
CustomCollisionGroups.Add("Skeleton");
CustomCollisionGroups.Add("Wizard");
CustomCollisionGroups.Add("Guardian");
CustomCollisionGroups.Add("Missile");
CustomCollisionGroups.Add("Dragon");

// Can't hit a flesh-less being.
CustomCollisionGroupPairs.Add("Guardian-Skeleton", CustomCollisionGroups.Get("Guardian"),
		 CustomCollisionGroups.Get("Skeleton"));
CustomCollisionGroupPairs.Add("Guardian-Wizard", CustomCollisionGroups.Get("Guardian"),
		CustomCollisionGroups.Get("Wizard"));
CustomCollisionGroupPairs.Add("Guardian-Missile", CustomCollisionGroups.Get("Guardian"),
		CustomCollisionGroups.Get("Missile"));
CustomCollisionGroupPairs.Add("Guardian-Dragon", CustomCollisionGroups.Get("Guardian"),
		CustomCollisionGroups.Get("Dragon"));
\end{lstlisting}
When a collision group pair is defined, by default there will be no collision response, however the collision events will be propagated normally. This is useful because at times we wanted to detect a collision but the physics engine should not respond.

\subsection{Post-Process Effects}
In our game we make extensive use of post-process effects, mainly glow, bloom and post-process anti-aliasing. To implement those effects, and any effects we might need, I designed a flexible, extendible and effect order-independent post-process manager, with access to the deferred pipeline. The manager itself holds a list of {\tt PostProcessEffect} instances and iterates through them, rendering the effect while doing ping-pong between two full-screen {\tt RenderTarget} textures. Finally, if the loop has reached the end of the list, it resolves the {\tt RenderTarget} and renders the composited effect on the screen with depth-write disabled.
\begin{figure}[H]
  \caption{Post-process manager render sequence}
  \centering
    \includegraphics[width=0.55\textwidth]{data/post-process.png}
\end{figure}

Since the post-process effects are applied \textit{after} the deferred composition, we need to pass the G-Buffer data to the respective shaders, so we do that before drawing such an effect. An example of that is the \textit{glow} effect which depends on the emission attribute.\\
The {\tt GlowDownsample} shader gets the albedo \textit{G-Buffer} sampler and scales the blurred pixels by the {\tt w} component, which is the emission intensity.
\begin{lstlisting} [caption=Downsampling glow regions]
float4 PS(float2 TexCoord : TEXCOORD0) : COLOR0
{
	float2 texelSize = float2(1.0 / TextureSize.xy);
	float2 texcoord = TexCoord;
	float4 albedo=0;
	for (int i=0; i<4; i++)
	{
		texcoord.x = TexCoord.x;
		for (int j=0; j<4; j++)
		{
			albedo += tex2D(GBufferAlbedoSampler, texcoord);
			texcoord.x += texelSize.x;
		}
		texcoord.y += texelSize.y;
	}
	albedo /= 16;

	return saturate(float4(albedo.rgb * albedo.w, 1));
}
\end{lstlisting}

That is an example how we can expose \textit{G-Buffer} data in the post-process effects.

\subsection{3D Sound and Music}
The {\tt AudioSource} class is the primary way to playback sound effects with 3D support and it can be instantiated for use with a {\tt SceneNode}. Nothing extra-ordinary is happening here, the class itself is an encapsulation of a {\tt SoundEffect} and {\tt SoundEffectInstance} of XNA. The 3D position and direction of movement is updated every frame in the respective {\tt Update} method, called when a {\tt SceneNode} is updating.\\
Music is played through the {\tt MediaPlayer} XNA static class.

\section{Game Development}

\subsection{Abstract Game Manager}
The concept of a \textit{game manager} was introduced in this game to allow for a central point of game state management to exist. Important game logic decisions such as level transitions, keeping track of each level's spawn points, orb collection, available spells and dragon state are all handled through the {\tt BaseGameManager} class.\\
As I come from a cross-platform development background and the game was initially being developed for both XBox and Windows systems, I improved on a concept I had used before for handling features implemented differently in each platform, by just extending the abstract {\tt BaseGameManager} to a {\tt WindowsGameManager} and an {\tt XBoxGameManager}. A typical scenario would be an online scoring system, which in the XBox implementation would use the \textit{XBox Live!}, whereas in Windows our own system. Moreover, we planned on using different input devices (such as a keyboard for the Windows version), or supporting different display modes for each platform.\\
Sadly, we decided to drop the XBox implementation as the \textit{Kinect SDK} is currently available only for Windows, so we did not use any subclasses of the {\tt BaseGameManager}.\\
\linebreak
Finally, as the game manager is holding the current state, on each \textit{New Game} request we re-create it, ensuring a fresh game start without residues from the previous session.

\subsubsection{Generating Level Links}
When transitioning between levels, it is important to know which point in the level to use to spawn the players, an example being transitioning from the \textit{Main Halls} to the \textit{Fire Dungeon}. How will the game know where to position the players? The solution devised was a {\tt LevelSpawnLink} mechanism. When the {\tt BaseGameManager} is initialised, we setup relations between the levels and the player spawn location as illustrated in the below code listing:
\begin{lstlisting} [caption=Setting up level relations]
// This method will setup the links between current level, the level to be loaded, 
// and their corresponding spawn points. The spawn names correspond to the next level ones
private void PrepareLevelLinks()
{
    levelSpawnLinks = new List<LevelSpawnLink>();
    // when the player enters the game
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.None, LevelType.MainHalls,
    						gameEnterSpawnName));

    // main halls links
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Air, 
    						airSpawnName));
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Earth, 
    						earthSpawnName));
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Water, 
    						waterSpawnName));
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.MainHalls, LevelType.Fire, 
    						fireSpawnName));
    // air links
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Air, LevelType.MainHalls, 
    						fromAirSpawnName));
    // water links
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Water, LevelType.MainHalls, 
    						fromWaterSpawnName));
    // fire links
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Fire, LevelType.MainHalls,
    						fromFireSpawnName));
    // earth links
    levelSpawnLinks.Add(new LevelSpawnLink(LevelType.Earth, LevelType.MainHalls, 
    						fromEarthSpawnName));
}
\end{lstlisting}

where {\tt LevelSpawnLink} is a simple struct holding the passed parameters, the last one of them being the target player spawn name, as defined in the \textit{3D Studio MAX MaxScript} editor.
Having created the proper links between the levels, we can now easily select a spawn point like this:
\begin{lstlisting}[caption=Selecting a spawn point]
// gets the proper spawn point from the current level to the new level.
// if the currentLevel is null (game is just starting), then the spawn point
// is set to that of the level
private Vector3 GetSpawnPointForLevel(LevelType nextLevel)
{
    // there is no current level type.
    // note: the currentLevel here is already loaded, as we get the 
    // spawn point *after* loading the level. However, the currentLevelType
    // is set to None in the class constructor, so the following code is valid.
    if (currentLevelType == LevelType.None)
    {
        foreach (KeyValuePair<LevelType, string> lt in levelDefaultSpawns)
        {
            if (lt.Key == nextLevel)
                return currentLevel.GetSpawnPoint(lt.Value);
        }
    }
    // currentLevelType is something other than None, so we must consult our nice map
    // to find the spawn point of the next level.
    foreach (LevelSpawnLink sl in levelSpawnLinks)
    {
        if (sl.CurrentLevel == CurrentLevelType && sl.NextLevel == nextLevel)
            return currentLevel.GetSpawnPoint(sl.SpawnName);
    }
    // that sucked...
    return Vector3.Zero;
}
\end{lstlisting}

\subsubsection{Level XML Parsing}
As defined in the \textit{MaxScript} editor, the \textit{Abroxetron} level XML describes how objects are positioned in the scene and with what attributes. Supported scene elements are:
\begin{itemize}
\item Point Lights: used to illuminate dark regions of the dungeon.
\item Objects: can be the level itself, or scene props.
\item Triggers: transition triggers, or death regions (for example the water plane in the water dungeon).
\item Spawn Points: can be skeleton, player or dragon ones, depending on the name prefix.
\end{itemize}

As an example, below a part of the XML file for the fire dungeon is illustrated. You can see some torch objects, a skeleton spawn point, the main halls trigger and the fire spawn point, where players are placed when they enter the dungeon:
\begin{figure}[H]
  \caption{Fire Dungeon XML}
  \centering
    \includegraphics[width=0.7\textwidth]{data/fireXml.png}
\end{figure}

The extra fields \textit{properties} and \textit{behaviour} will be discussed in the next section.\\
\linebreak
To parse the XML, the \textit{.NET} \textit{Xml.Linq} assembly is used and more importantly the {\tt XDocument} class. By using a data query language such as \textit{LINQ}, an XML file can be parsed very easily in few lines of code. As an example, let us have a look at how the scene lights are parsed and stored in a {\tt List<LevelLight>} data structure:

\begin{lstlisting}[caption=Parsing the lights defined in a level XML file]
lights = (from light in sceneXml.Descendants("light")
select new LevelLight()
  {
  Name = light.Attribute("name").Value,
  Enabled = Convert.ToBoolean(light.Element("enabled").Value),
  CastShadows = Convert.ToBoolean(light.Element("castshadows").Value),
  Radius = Convert.ToSingle(light.Element("radius").Value),
  Intensity = Convert.ToSingle(light.Element("intensity").Value),
  ShadowColor = new Vector4(Parser.ParseVector3(light.Element("shadow").Value), 1.0f),
  LightColor = new Vector4(Parser.ParseVector3(light.Element("color").Value), 1.0f),
  Translation = Parser.ParseVector3(light.Element("translation").Value),
  Properties = light.Attribute("properties") != null ? light.Attribute("properties").Value : "",
  Behaviour = light.Attribute("behaviour") != null ? light.Attribute("behaviour").Value : ""
}).ToList();
\end{lstlisting}

In a similar manner, we parse the objects, triggers and spawn points that make up our dungeon level.\\
Once parsed, the {\tt LevelParser} class instance which represent our currently loaded level is stored in the {\tt BaseGameManager} for easier retrieval during the gameplay.

\subsubsection{Handling Object Properties and Behaviours}
Each element in the scene can have certain properties and a {\tt SceneNodeBehaviour} which can optionally use them. However, we did not want to add those properties in the XML file directly, we wished to be able to tweak them however we wanted, even add new fields without much hassle. Thankfully, the \textit{.NET Framework} already gave us the solution to that; \textit{Reflection}.\\
In few words, we are able to give the properties and behaviour class name in the scene XML file, and the game will automatically instantiate the correct class to generate the scene element as we wish.\\
Let us see in detail how this happens. First of all, for each scene element (object, light, trigger and spawn point) we define a set of base classes. We will examine again the case of the light. For the light, we have the base classes {\tt LevelLightProperties} and {\tt LevelLightBehaviour}, as illustrated in the following code listings:
\begin{lstlisting}[caption=Properties class]
namespace Game2.Level.LevelPropertyClasses
{
    public abstract class LevelLightProperties : LevelNodeProperties, IProperties
    {
        public virtual void Initialize(SceneNode node) {}
    }
}
\end{lstlisting}
\begin{lstlisting}[caption=Behaviour class]
namespace Game2.Level.LevelBehaviourClasses
{
    public class LevelLightBehaviour : LevelNodeBehaviour
    {
        public LevelLightProperties Properties
        {
            set;
            get;
        }
    }
}
\end{lstlisting}

where {\tt LevelNodeBehaviour} and {\tt LevelNodeProperties} are just two base abstract classes. When it is time for the {\tt LevelParser} class to generate the actual {\tt SceneNode} instances for the scene (in our case, the lights), it iterates through the light list and checks to see if the \textit{property} or \textit{behaviour} fields are set. If they are, it tries to create an instance of the designated class using the \textit{.NET Framework }{\tt Activator} static class' {\tt CreateInstance} method:
\begin{lstlisting}[caption=Instantiating properties and behaviours from string]
LevelLightProperties propertiesClassInstance = null;
LevelLightBehaviour behaviourClassInstance = null;
Type lightType = null;
Type behaviourType = null;
string fullTypeName = "";


// create the properties class instance
if (!string.IsNullOrEmpty(light.Properties))
    fullTypeName = "Game2.Level.LevelPropertyClasses." + light.Properties;
else
    fullTypeName = "Game2.Level.LevelPropertyClasses." + light.Name + "Properties";

lightType = Type.GetType(fullTypeName);
if (lightType != null)
{
    propertiesClassInstance = Activator.CreateInstance(lightType) as LevelLightProperties;
    Debug.Assert(propertiesClassInstance != null);

}

// create the behaviour class instance
if (!string.IsNullOrEmpty(light.Behaviour))
    fullTypeName = "Game2.Level.LevelBehaviourClasses." + light.Behaviour;
else
    fullTypeName = "Game2.Level.LevelBehaviourClasses." + light.Name + "Behaviour";

behaviourType = Type.GetType(fullTypeName);
if (behaviourType != null)
{
    behaviourClassInstance = Activator.CreateInstance(behaviourType) as LevelLightBehaviour;
    Debug.Assert(behaviourClassInstance != null);

    behaviourClassInstance.Properties = propertiesClassInstance;
}
\end{lstlisting}
So, by subclassing the aforementioned classes, we can add extra details to a light. For example, a twitch parameter could be defined in the subclasses properties class, and the behaviour would take it into consideration in its {\tt Update} method to adjust the light's intensity and radius according to that twitch parameter.\\
In the case of a 3D Object defined in the XML, its properties can (and usually do as seen in the game's sourcecode) include the {\tt PhysicalEntity} type, as well as mass and collider size values. Especially for the 3D Objects, the {\tt LevelParser} takes those values into consideration when creating the actual {\tt SceneNode} to create a physical object or a trigger.\\
After the properties and behaviour of the light have been instantiated, we can just create the {\tt SceneNode} as usually:
\begin{lstlisting}[caption=Creating a light node for the level]
SceneNode currentLightNode = sceneService.AddSceneNode(light.Name);
currentLightNode.Light = new PointLight(light.LightColor, light.Intensity, light.Radius);
currentLightNode.Translation = light.Translation;

if (behaviourClassInstance != null)
    currentLightNode.Behaviour = behaviourClassInstance;
\end{lstlisting}
A very simple concept at its core, but powerful and time-saving solution which is used extensively throughout the game's levels.

\subsection{Dragon Behaviour}
As with the skeletons, the dragon behaviour is subclassing the {\tt CharacterSceneNodeBehaviour} to able to smoothly walk on surfaces. Normally, the character capsule is generated automatically from the model's bounding volume, however in the case of the dragon we needed to set it smaller manually, as the bounding box was too big because of the dragon wings. This is the reason why the dragon's wings can pass through scene objects, however it does not affect the gameplay and it happens in most games anyway.\\
The sequence which the dragon follows is the following:
\begin{itemize}
\item If all orbs are collected, enter wake-up sequence.
\item When dragon is awake, start hunting the closest wizard.
\item If he is within attack range, select an attack type based on random probability. Probability of [0.0, 0.9] is a bite attack, while [0.9, 1.0] is a fire breath. The dragon will also favour the breath attack if the wizard is further away.
\end{itemize}

\section{Audio and 3D Model Assets}
\subsection{Audio Assets}
Apart from the various aspects in the development of the engine and the game, I was also responsible for selecting the music and sound effects to accompany the final game. All of the sound effects were downloaded from \textit{freesound.org}, apart the dragon screams that are borrowed from \textit{Skyrim} game.\\
Regarding the music, the tracks were selected from the royalty free music database found on \textit{nosoapradio.us}

\subsection{3D Models \& Textures contribution}
Despite the fact that I am not very good in modelling, I managed to make a simple ground torch in 3D Studio MAX and texture it with a simple wooden surface. The only place where this model is seen, is the air dungeon.\\
I also found a wall torch to be used in various dungeon locations, downloaded from \textit{opengameart.org}.\\
Lastly, in the last days of development, I created a logo for the game featuring a gradient dragon behind the text "Abroxetron", inspired by the \textit{Final Fantasy} games.

\bibliographystyle{apalike}
\bibliography{master}

\end{document}
