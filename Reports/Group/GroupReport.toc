\select@language {english}
\contentsline {section}{\numberline {1}Game Concept}{3}
\contentsline {subsection}{\numberline {1.1}Rules}{3}
\contentsline {subsubsection}{\numberline {1.1.1}Win Conditions}{3}
\contentsline {subsubsection}{\numberline {1.1.2}Lose Conditions}{3}
\contentsline {subsection}{\numberline {1.2}Perspective}{3}
\contentsline {subsection}{\numberline {1.3}Dungeons}{3}
\contentsline {subsection}{\numberline {1.4}Characters}{4}
\contentsline {subsubsection}{\numberline {1.4.1}Wizard}{4}
\contentsline {subsubsection}{\numberline {1.4.2}Guardian}{5}
\contentsline {subsubsection}{\numberline {1.4.3}Skeleton}{5}
\contentsline {subsubsection}{\numberline {1.4.4}Abroxetron Dragon}{5}
\contentsline {subsection}{\numberline {1.5}Spells}{5}
\contentsline {subsection}{\numberline {1.6}AI}{6}
\contentsline {subsubsection}{\numberline {1.6.1}Skeleton Attack Behaviour}{7}
\contentsline {subsubsection}{\numberline {1.6.2}Dragon Attack Behaviour}{7}
\contentsline {section}{\numberline {2}Technologies Outline}{7}
\contentsline {subsection}{\numberline {2.1}C\# XNA 4.0}{7}
\contentsline {subsection}{\numberline {2.2}BEPU Physics}{7}
\contentsline {subsection}{\numberline {2.3}Kinect}{7}
\contentsline {section}{\numberline {3}Tools}{8}
\contentsline {subsection}{\numberline {3.1}Used}{8}
\contentsline {subsubsection}{\numberline {3.1.1}Audacity}{8}
\contentsline {subsubsection}{\numberline {3.1.2}Evart}{8}
\contentsline {subsubsection}{\numberline {3.1.3}Motion Builder}{8}
\contentsline {subsubsection}{\numberline {3.1.4}Maya}{8}
\contentsline {subsubsection}{\numberline {3.1.5}ZBrush}{8}
\contentsline {subsubsection}{\numberline {3.1.6}3D Studio MAX}{9}
\contentsline {subsubsection}{\numberline {3.1.7}CrazyBump}{9}
\contentsline {subsubsection}{\numberline {3.1.8}Photoshop}{9}
\contentsline {subsection}{\numberline {3.2}Developed}{9}
\contentsline {subsubsection}{\numberline {3.2.1}Ghast Particle Editor}{9}
\contentsline {subsubsection}{\numberline {3.2.2}Gem Model Viewer}{9}
\contentsline {subsubsection}{\numberline {3.2.3}XmlExporter42}{10}
\contentsline {section}{\numberline {4}User's Manual}{10}
\contentsline {subsection}{\numberline {4.1}Kinect/Mouse Controls}{10}
\contentsline {subsection}{\numberline {4.2}XBox 360 Gamepad Controls}{11}
