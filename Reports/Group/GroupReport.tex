\documentclass{ueacmpstyle}
\usepackage[utf8]{inputenc}
\usepackage{subfig}
\usepackage{float}
\usepackage[english]{babel} % English
\usepackage{amssymb} % Extra symbols                                    % http://milde.users.sourceforge.net/LUCR/Math/mathpackages/amssymb-symbols.pdf
\usepackage{amsmath} % AMS mathematical facilities for LaTeX.           % http://tug.ctan.org/pkg/amsmath
\usepackage{amsthm} % Typesetting theorems (AMS style).                 % http://www.ctan.org/pkg/amsthm
\usepackage{underscore}
\usepackage{graphics}
\usepackage{url}

\begin{document}
\title{Abroxetron Group Report\\CMPSME22 Assignment}

\author{Developers:\\ Olivier Legat, Pantelis Lekakis, Sundar Muthu Rajagopalan}

\maketitle

\pagebreak

\tableofcontents

\pagebreak

\section{Game Concept}
\subsection{Rules}
As stated in the game's loading screen, a group of wizards travel to the dungeon where \textit{Abroxetron} dragon is eternally sleeping. Their goal is to re-assemble the four elements of power (\textit{Fire, Air, Water and Earth} orbs) so they can awaken the dragon and kill him. Assisting the wizards, is a (Kinect controlled) guardian spirit. All-together, they need to collect the elements of power, gain the respective spells and use their power to defeat the mighty dragon.

\subsubsection{Win Conditions}
The wizards need to collect all the orbs and finally kill the dragon. This can be done easily assuming that the players co-operate strategically.

\subsubsection{Lose Conditions}
It is also easy to lose if the players do not pay attention to their surroundings. Typical lose conditions are:
\begin{itemize}
\item Wizards or the guardian fall from cliffs in certain dungeons.
\item Getting all the wizards killed by skeletons or the dragon.
\end{itemize}

\subsection{Perspective}
Initially the game was viewed from a bird-eye view. However, we noticed that this perspective wasn't good because it obscured a lot of detail, thus we decided to switch perspective. We moved the camera further from the players, this allows the players to view more elements of the scene at once. We also changed the camera's pitch angle from \(-\pi/2\) to \(-\pi/4\), this allows more detail to be viewed. The position of the camera is calculated by average position of all the players and a lerp factor was also added to the camera to make the motion more smooth.

\subsection{Dungeons}
The players' game world is a complex of dungeons, starting off with the \textit{Main Halls}, where the dragon is located. The wizards then need to visit the four dungeons where the elements of power are located: the \textit{Air}, \textit{Water}, \textit{Earth} and \textit{Fire} dungeons, all connected to the Main Halls.\\
We wanted to give the feel of exploring, so in each dungeon exists several decoy routes which lead to a dead-end. While being chased by skeletons, the wizards need to search for the orb, get it and leave the dungeon in pursuit of the rest orbs. Be careful though, there are many skeletons protecting the orb, so wizards need to pay extra attention when collecting it.

\begin{figure}[H]
\centering
\subfloat[Main Halls]{\label{fig:mainHalls}\includegraphics[width=0.35\textwidth]{data/mainHalls.png}}\\

\subfloat[Air Dungeon]{\label{fig:airDungeon}\includegraphics[width=0.35\textwidth]{data/air.png}}
\subfloat[Water Dungeon]{\label{fig:waterDungeon}\includegraphics[width=0.35\textwidth]{data/water.png}}\\
\subfloat[Fire Dungeon]{\label{fig:fireDungeon}\includegraphics[width=0.35\textwidth]{data/fire.png}}
\subfloat[Earth Dungeon]{\label{fig:earthDungeon}\includegraphics[width=0.35\textwidth]{data/earth.png}}

\label{Dungeons}
\end{figure}

\subsection{Characters}
In total, there are three main animated characters in the game and one extra guardian, each one having unique motion captured and keyframed animations. The guardian is just the wizard base model with a white robe and an emission map to glow softly.

\begin{figure}[H]
\centering

\subfloat[Wizard]{\label{fig:wizardChar}\includegraphics[width=0.35\textwidth]{data/wizard.png}}
\subfloat[Skeleton]{\label{fig:skeletonChar}\includegraphics[width=0.2\textwidth]{data/skeleton.png}}
\subfloat[Dragon]{\label{fig:dragonChar}\includegraphics[width=0.4\textwidth]{data/dragon.png}}

\label{In-game characters}
\end{figure}

\subsubsection{Wizard}
Animation list:
\begin{itemize}
\item \textbf{Idle}: Motion captured, used when the player is doing nothing but standing in the dungeon.
\item \textbf{Walk}: Motion captured, used one cycle for Motion Builder and translated the skeleton to the origin.
\item \textbf{Melee}: Motion captured, this is the sword attack animation.
\item \textbf{Spell}: Motion captured, played when the wizard is casting a spell.
\item \textbf{Death}: Keyframed, played when the wizard dies.
\item \textbf{Dance}: Keyframed, the game is won if you see this.
\end{itemize}

Apart from the in-game animations, there is an extra baked pose for the Game-Over screen; the wizard just lays on the floor, dead.

\subsubsection{Guardian}
Due to the spiritual nature of the guardian, he only has one keyframed animation, \textbf{Floating}. 

\subsubsection{Skeleton}
Animation list:
\begin{itemize}
\item \textbf{Walk}: Motion captured, a typical walk animation.
\item \textbf{Attack}: Motion captured, used when the skeleton attacks the player with the scimitar.
\item \textbf{Damage}: Motion captured, played when the skeleton is damaged.
\item \textbf{Spawn Below / Above}: Motion captured, used once when the skeleton is spawned.
\item \textbf{Death}: Keyframed, played when the skeleton dies.
\end{itemize}

\subsubsection{Abroxetron Dragon}
Animation list:
\begin{itemize}
\item \textbf{Wakeup}: Keyframed, played once when the dragon wakes up from his statue form.
\item \textbf{Attack}: Keyframed, the dragon bites a wizard.
\item \textbf{Fire Breath}: Keyframed, the dragon breathes fire towards a wizard.
\item \textbf{Death}: Keyframed, the dragon is dead.
\end{itemize}

\subsection{Spells}
As the players proceed through the dungeon they will unlock various powerful spells which can be assigned to the wizards. The guardian is not effected by any of these spells since it is merely a flesh-less being. The spell include: 
\begin{itemize}
\item {\bf Air:} Although this spell does not deal direct damage, it can be used to push foes (and friends) a considerate distance away. This can be used to push enemies into hazardous regions.
\item {\bf Earth:} The caster will thrust a rock forward, dealing immense damage proportional to the speed of the rock. The Guardian and caster are immune to the rock damage but ally wizards must be careful not to touch it.
\item {\bf Fire:} This spell will grant the owner the power to throw fire-balls. The fire-balls deal large area-of-effect magical damage. Wizards have magical resistance and take less damage than the skeletons do, but they still take damage so the caster must take care.
\item {\bf Water:} When casting the spell an enclosed water-fountain will emit. Any friend or foe standing in the fountain will quickly recover their hit points. This spell is recommended to get acquired as soon as possible. 
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{Data/spells.png}
\caption{All four spells: Air (top-left), Earth (top-right), Fire (bottom-left), and Water (bottom-right)}
\end{figure}

The Guardian also has 3 spells which are unlocked at the start of the game. These spells are triggered by moving the right hand in two directions indicated in-game.
\begin{itemize}
\item {\bf Emit Light:} The Guardian stands still and emits light. This spell will automatically stop after 3.5 seconds but can also be stopped manually if re-casted.
\item {\bf Freeze:} The Guardian emits a blast of cold freezing nearby skeletons for a few seconds. Frozen skeletons can still take damage and die. The Guardian cannot freeze the Dragon or a spawning skeleton.
\item {\bf Revive:} The Guardian revives a fallen wizard at his location. The reborn wizard will have only a portion of health and no mana. As a side-effect, the Guardian will remain immobile for 3 seconds after casting.
\end{itemize}

\subsection{AI}
The development of the enemies' AI started just before the Easter break and it proved to be a challenging task. At the heart of the enemies' movement is a threaded A* path-finding implementation, driving each enemy towards the closest wizard, functionality which is common for both the skeletons and the dragon.\\
When the enemy spawns, he starts looking for the closest alive wizard and uses path-finding to get to him. The path-finding query is executed once every 2 seconds or when the wizard is moving away from the him. Even though the implementation runs on its own thread, we still needed to reduce the number of queries per frame, effectively being able to have dozens of skeletons path-finding at the same time.

\subsubsection{Skeleton Attack Behaviour}
The skeletons are equipped with scimitars and thus they use them to attack the wizards when they are close enough. The logic is simple, if the skeleton is close to the player, attack, otherwise path-find and move closer to him.\\
Even though one skeleton will not necessarily threaten the life of a wizard, they tend to move in a group so they are more dangerous; in that case the player should run away or use powerful spells to kill them. 

\subsubsection{Dragon Attack Behaviour}
As a boss, the dragon has more than one way to damage the wizards, one is a normal dragon bite and the other is a fire breath attack. The way the dragon selects which attack to use is based on two factors:
\begin{itemize}
\item \textbf{Random probability}: Every frame a probability is generated and if it lies within the [0.0, 0.9] range, the dragon uses the bite attack, otherwise he attacks with the fire breath.
\item \textbf{Distance from wizard}: If the dragon cannot reach to bite the wizard, he will use the fire breath instead.
\end{itemize}

Players should be careful as the fire breath is a very strong attack which can easily kill a wizard. Close range combat using sword attacks while other wizards attack using spells is the best way to kill the dragon.

\section{Technologies Outline}
\subsection{C\# XNA 4.0}
The Engine and Game logic were both developed in C\# with the use of Microsoft's XNA Framework. C\# is not as fast as a lower-level language such as C or C++, but fortunately C\# runs on the .NET framework with garbage collection which makes programming easier, and hence easier to develop. Additionally with support from XNA a lot things are made simplier. Features of XNA include, but not limited to: collision detection, mesh rendering, input handling, mathematics functions and outputting audio.

\subsection{BEPU Physics}
Considering the complexity of the \textit{Abroxetron} game, we decided to use a physics engine to drive our virtual world. As we initially planned on running the game on the XBox 360, we selected a managed library written in C\#, called \textit{BEPU Physics}. Using BEPU enabled us to have a fully physically-enabled world running on a separate thread, with features such as static meshes for the level object, convex hulls and robust capsule-based character controllers. 

\subsection{Kinect}
The essence of the game is based around team-work. Each player has a specific role to carry out, and the Guardian player has a very distinct role. This distinction is due to the fact that the Guardian player uses a Kinect instead of an XBox 360 controller to control their character. The Kinect player uses their left hand to control a red pointer on screen that the Guardian will follow. The right hand is used to cast spells through the use of gestures. The Microsoft Kinect SDK was used to implement this. Unfortunately, this SDK is not support on XBox 360 and so the game could not be ported to the console.

\section{Tools}
\subsection{Used}

\subsubsection{Audacity}
Our main usage of \textit{Audacity} was to trim and adjust the length of the various sound effects that we are using in-game and also to make the music as loopable as possible.

\subsubsection{Evart}
We used \textit{Evart} extensively while motion capturing the wizard and skeleton animations. We spent most of the time post-processing and correcting the captured data, as many markers were switching positions or sometimes twitching.

\subsubsection{Motion Builder}
We used \textit{Motion Builder} to import the motion capture data generated using Evart and apply them to our character skeletons, generating the animations. Before exporting the animations for usage in Maya, data correction had to take place, such us adjusting limb transformations on data that were visually wrong.

\subsubsection{Maya}
Our main 3D modelling software was \textit{Maya}, however apart from modelling, we also used Maya for rigging, skinning and UVW unwrapping of the created models. Finally, the characters were animated using the Motion Builder generated animation data. Even though our primary source of animations were the motion capture data, certain animations could not be achieved using that method, such as the wizard/warrior death and of course the dragon (dragons do not exist in real-life unfortunately); those had to be keyframed manually.

\subsubsection{ZBrush}
Since in our game we make extensive use of normal maps, it was essential to generate high polygon variants of the different models in-game, especially for the characters.

\subsubsection{3D Studio MAX}
3D Studio MAX was primarily used for level editing using our custom editor. Basically we were able to lay level elements such as scene props, lights, spawn points and triggers and use the XmlExporter42 (See below) to create an XML file, usable by the game.

\subsubsection{CrazyBump}
For models that we did not have high polygon versions, we decided to generate the normals from the diffuse map using the tool \textit{CrazyBump}. The result was very convincing and it added a lot of detail in the final scene.

\subsubsection{Photoshop}
Last but not least, almost daily \textit{Photoshop} usage was needed to create the various textures and 2D elements required for the models and the user interface.

\subsection{Developed}

\subsubsection{Ghast Particle Editor}
\textit{Ghast} was a tool developed in order to help us create various particle effects without much hassle. Developed in C\#, Ghast exposed all the functionality of the engine's particle system in a nice windowed application with a live preview. By allowing to easily tweak the particle system's values in realtime, we boosted productivity and saved a lot of time of manually testing particle settings. Lastly, of course the tool allows of loading/saving of particle systems.
\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{Data/ghast.png}
\caption{Ghast particle editor}
\end{figure}

\subsubsection{Gem Model Viewer}
When we were creating and testing models and animations, it quickly became tedious setting up a custom game state just for that, so the need for a custom model viewer emerged. \textit{Gem} was developed quickly in one night to allow for loading models from our content pipeline and viewing the available animations. Various settings were added for animation playback, including animation blending to test for all possible scenarios and as simple as it looks, it saved us a lot of time.
\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{Data/gem.png}
\caption{Gem model viewer}
\end{figure}

\subsubsection{XmlExporter42}
In the order to position objects in the level and define their game logic properties and behaviours we needed some sort of level editor. Initially we intended to write our own 3D level editor. That project was quickly abandoned because it seemed far too time consuming. As an alternative solution we decided to use 3DS Max as a level-editor. We wrote our own MaxScript called XmlExporter42 to export the content of the 3DS Max scene into an XML file that could be parsed by the game. Naturally, this MaxScript is specifically design to be use for our game and has no real use outside the scope of this project.

\section{User's Manual}
The game is meant to be played co-operatively with up to 5 players (4 XBox 360 gamepads and 1 Kinect/Mouse). It can be played with just a Kinect/Mouse and one gamepad, however the experience will not be as enjoyable.

\subsection{Kinect/Mouse Controls}
The player can select whether to use a Kinect or a Mouse to control the guardian spirit from the main \textit{Options} menu.\\
\linebreak

In the case of a Kinect, it is tracking the left player's hand to move the guardian spirit, and the right player's hand to recognise certain gestures for his spells.

\textbf{Spell Gestures:}
\begin{itemize}
\item Up-Down: light.
\item Right-Left: freeze opponents.
\item Down-Right: revive one dead player.
\end{itemize}

In the case of a Mouse, the pointer movement is used to move the guardian while the 3 mouse buttons are responsible for the spells.\\
\textbf{Spell Buttons:}
\begin{itemize}
\item Left Button: light.
\item Right Button: freeze opponents.
\item Middle Button: revive one dead player.
\end{itemize}

\subsection{XBox 360 Gamepad Controls}
The Gamepad is used to control the wizard and his spells. The button bindings are illustrated below:
\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{Data/gamepad.png}
\caption{Gamepad In-Game Controls}
\end{figure}

\end{document}
