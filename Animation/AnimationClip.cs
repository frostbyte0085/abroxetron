#region File Description
//-----------------------------------------------------------------------------
// AnimationClip.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#endregion

namespace Engine.Animation
{
    /// <summary>
    /// An animation clip is the runtime equivalent of the
    /// Microsoft.Xna.Framework.Content.Pipeline.Graphics.AnimationContent type.
    /// It holds all the keyframes needed to describe a single animation.
    /// </summary>
    public class AnimationClip
    {
        /// <summary>
        /// Constructs a new animation clip object.
        /// </summary>
        public AnimationClip(string name, TimeSpan duration, List<KeyframePart> keyframes)
        {
            Name = name;
            this.duration = (float)duration.TotalMilliseconds;
            Keyframes = Keyframe.ToKeyframeList(keyframes);
            Speed = 1;
        }

        /// <summary>
        /// Private constructor for use by the XNB deserializer.
        /// </summary>
        private AnimationClip()
        {
        }

        [ContentSerializer]
        public string Name { get; private set; }

        /// <summary>
        /// Current time in milli-seconds.
        /// </summary>
        [ContentSerializer]
        private float duration;

        /// <summary>
        /// Gets the total length of the animation in milli-seconds.
        /// </summary>
        public float Duration { get { return (duration / Speed); } }

        /// <summary>
        /// The speed of this animation
        /// </summary>
        public float Speed { set; get; }

        /// <summary>
        /// Gets a combined list containing all the keyframes for all bones,
        /// sorted by time.
        /// </summary>
        [ContentSerializer]
        public List<Keyframe> Keyframes { get; private set; }
    }
}
