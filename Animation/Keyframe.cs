﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Engine.Animation
{
    public class Keyframe
    {
        /// <summary>
        /// Constructs a new keyframe object.
        /// </summary>
        public Keyframe(float time, KeyframePart[] parts)
        {
            Time = time;
            Parts = parts;
        }

        /// <summary>
        /// Private constructor for use by the XNB deserializer.
        /// </summary>
        private Keyframe()
        {
        }

        /// <summary>
        /// Bones + transforms of the keyframe
        /// </summary>
        [ContentSerializer]
        public KeyframePart[] Parts { get; private set; }


        /// <summary>
        /// Gets the time offset from the start of the animation to this keyframe in milli-seconds.
        /// </summary>
        [ContentSerializer]
        public float Time { get; private set; }

        internal static List<Keyframe> ToKeyframeList(List<KeyframePart> allParts)
        {
            List<Keyframe> keys = new List<Keyframe>();
            List<KeyframePart> keyframeParts = new List<KeyframePart>();
            float time = allParts[0].Time;
            KeyPartSorter sort = new KeyPartSorter();

            foreach (KeyframePart part in allParts)
            {
                // Are we done constructing this key?
                if (part.Time != time)
                {
                    keyframeParts.Sort(sort);

                    // The data for this key frame is ready. Make the keyframe.
                    Keyframe keyframe = new Keyframe();
                    keyframe.Time = time;
                    keyframe.Parts = keyframeParts.ToArray();

                    // Add this key to the return value:
                    keys.Add(keyframe);

                    // Prepare variables for the next key:
                    keyframeParts.Clear();
                    time = part.Time;
                }

                // Add this bone data to this current keyframe.
                keyframeParts.Add(part);
            }

            return keys;
        }

        private class KeyPartSorter : IComparer<KeyframePart>
        {
            public int Compare(KeyframePart x, KeyframePart y)
            {
                return x.Bone - y.Bone;
            }
        }
    }
}
