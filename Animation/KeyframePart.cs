#region File Description
//-----------------------------------------------------------------------------
// Keyframe.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#endregion

namespace Engine.Animation
{
    /// <summary>
    /// Describes the position of a single bone at a single point in time.
    /// </summary>
    public class KeyframePart
    {
        /// <summary>
        /// Constructs a new keyframe object.
        /// </summary>
        public KeyframePart(int bone, TimeSpan time, Matrix transform)
        {
            Bone = bone;
            Time = (float)time.TotalMilliseconds;
            Vector3 scale;
            Quaternion rot;
            Vector3 pos;
            transform.Decompose(out scale, out rot, out pos);

            Orientation = rot;
            Translation = pos;
            Scale = scale;
        }


        /// <summary>
        /// Private constructor for use by the XNB deserializer.
        /// </summary>
        private KeyframePart()
        {
        }


        /// <summary>
        /// Gets the index of the target bone that is animated by this keyframe.
        /// </summary>
        [ContentSerializer]
        public int Bone { get; private set; }


        /// <summary>
        /// Gets the time offset from the start of the animation to this keyframe in milli-seconds.
        /// </summary>
        [ContentSerializer]
        public float Time { get; private set; }

        /// <summary>
        /// Gets the bone transform for this keyframe.
        /// </summary>
        public Matrix Transform
        {
            get
            {
                return 
                    Matrix.CreateScale(Scale) *
                    Matrix.CreateFromQuaternion(Orientation) *
                    Matrix.CreateTranslation(Translation);
            }
        }

        /// <summary>
        /// Get the rotation transformation of this bone.
        /// </summary>
        [ContentSerializer]
        public Quaternion Orientation { get; private set; }

        /// <summary>
        /// Get the translate transformation of this bone.
        /// </summary>
        [ContentSerializer]
        public Vector3 Translation { get; private set; }

        [ContentSerializer]
        public Vector3 Scale { get; private set; }
    }
}
