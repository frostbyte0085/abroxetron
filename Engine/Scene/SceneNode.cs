﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Engine.Core;
using Engine.Audio;
using Engine.Graphics;
using Engine.Tools;
using Engine.Physics;

using System.Diagnostics;

using Engine.Animation;

using BEPUphysics;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;

namespace Engine.Scene
{
    // Describe what to do with the scene node when a particle system is attached.
    public enum ParticleAttachMode
    {
        Default,   // update the scene node and particle system normally
        DiscardOnCompletion // destroy the scene node when the particle system is done animating
    }

    [DebuggerDisplay("{name}")]
    public class SceneNode : IDisposable
    {
        string name;

        SceneService sceneService;
        PhysicsService physicsService;
        BoundingSphere boundingSphere;
        BoundingSphere transformedBoundingSphere;

        BoundingBox boundingBox;
        BoundingBox transformedBoundingBox;

        bool hasBoundingSphere;
        bool hasBoundingBox;

        Matrix[] meshBoneMatrices;

        internal SceneNode(string name)
        {
            this.name = name;

            orientation = new Quaternion(Vector3.Zero, 1.0f);
            translation = Vector3.Zero;

            physicsService = ServiceProvider.GetService<PhysicsService>();
            Debug.Assert(physicsService != null);

            sceneService = ServiceProvider.GetService<SceneService>();
            Debug.Assert(sceneService != null);

            particleSystems = new List<ParticleSystem>();
            ParticleAttachMode = ParticleAttachMode.DiscardOnCompletion;


            mountedNodes = new Dictionary<string, SceneNode>();

            MeshColor = Color.White.ToVector3();
            TintColor = Color.White.ToVector3();
            TintAmount = 0;

            EmissiveAdditive = 0;
        }

        protected SceneNode()
        {
        }

        #region Properties

        public bool HasParticleSystem { get { return particleSystems.Count != 0; } }

        public ParticleAttachMode ParticleAttachMode { set; get; }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string CategoryGroup
        {
            set;
            get;
        }

        private bool active = true;
        /// <summary>
        /// Set a node Active or Inactive.
        /// Inactive work as normal nodes but don't collide and are not rendered
        /// </summary>
        public bool Active
        {
            set
            {
                // Update the children
                foreach (SceneNode mounted in mountedNodes.Values)
                {
                    mounted.Active = value;
                }
                // Update this value
                this.active = value;

                // Enter inactive mode
                if (this.Behaviour != null) this.Behaviour.OnInactived();

                // Update physics
                if (this.PhysicalEntity != null)
                {
                    if (value) 
                        physicsService.Add(this.PhysicalEntity);
                    else 
                        physicsService.Remove(this.PhysicalEntity);
                }
            }
            get { return this.active; }
        }

        bool culled;
        public bool FrustumCulled
        {
            get
            {
                return culled;
            }
            internal set
            {
                culled = value;
            }
        }
        #endregion

        #region Attachements
        private List<ParticleSystem> particleSystems = null;
        internal List<ParticleSystem> ParticleSystems { get { return particleSystems; } }

        private Dictionary<string, SceneNode> mountedNodes = null;
        internal Dictionary<string, SceneNode> MountedNodes { get { return mountedNodes; } }

        private SceneNode mountedOn = null;
        public SceneNode MountedOn
        {
            internal set
            {
                mountedOn = value;
            }
            get
            {
                return mountedOn;
            }
        }

        private string mountPoint;
        public string MountPoint
        {
            internal set
            {
                mountPoint = value;
            }
            get
            {
                return mountPoint;
            }
        }
        // attaching of particle systems
        public void Attach(string attachPoint, ParticleSystem ps)
        {
            ps.AttachmentPoint = attachPoint;
            UpdateTranslation(ps);

            particleSystems.Add(ps);

            UpdateParticleSystemTranslation();
        }
        public void GetAttachment(string attachPoint, out ParticleSystem pout)
        {
            pout = null;
            foreach (ParticleSystem ps in particleSystems)
                if (ps.AttachmentPoint == attachPoint)
                    pout = ps;
        }
        public void Detach(ParticleSystem ps)
        {
            particleSystems.Remove(ps);
        }
        public void Detach(string attachPoint)
        {
            // scan both the particles and the mounted nodes, we might have both!
            // particles first
            List<ParticleSystem> toDetach = new List<ParticleSystem>();
            foreach (ParticleSystem ps in particleSystems)
            {
                if (ps.AttachmentPoint == attachPoint)
                    toDetach.Add(ps);
            }

            foreach (ParticleSystem ps in toDetach)
                particleSystems.Remove(ps);

            // mounted nodes
            SceneNode node = null;
            mountedNodes.TryGetValue(attachPoint, out node);
            if (node != null)
            {
                node.mountedOn = null;
                node.mountPoint = "";
                mountedNodes.Remove(attachPoint);
            }
        }

        // attaching of other nodes as mountable objects
        public void Attach(string attachPoint, SceneNode node)
        {
            node.mountedOn = this;
            node.mountPoint = attachPoint;

            mountedNodes[attachPoint] = node;
        }

        public void GetAttachment(string attachPoint, out SceneNode outnode)
        {
            mountedNodes.TryGetValue(attachPoint, out outnode);
        }
        public void Detach(SceneNode node)
        {
            List<string> keysToRemove = new List<string>();

            foreach (KeyValuePair<string, SceneNode> kvp in mountedNodes)
            {
                if (kvp.Value == node)
                {
                    keysToRemove.Add(kvp.Key);
                    node.mountPoint = "";
                    node.mountedOn = null;
                }
            }

            foreach (string s in keysToRemove)
            {
                mountedNodes.Remove(s);
            }
        }

        #endregion

        #region Scene Node Components

        Dictionary<string, object> tags = new Dictionary<string,object>();
        public object this[string s]
        {
            set { tags[s] = value; }
            get
            {
                return HasTag(s) ? tags[s] : null;
            }
        }
        public bool HasTag(string s) { return tags.ContainsKey(s); }

        SceneNodeBehaviour behaviour;
        public SceneNodeBehaviour Behaviour
        {
            set
            {
                Debug.Assert(value != null);
                behaviour = value;
                behaviour.SceneNode = this;
                behaviour.Start();
            }
            get
            {
                return behaviour;
            }
        }

        Model model;
        public Model Model
        {
            set
            {
                Debug.Assert(value != null);
                model = value;

                meshBoneMatrices = new Matrix[model.Bones.Count];

                // check if the model has animation data in it and if so, create the animation component
                if (model.Tag != null)
                {
                    object tagData = null;
                    Dictionary<string, object> tags = model.Tag as Dictionary<string, object>;

                    tags.TryGetValue("SkinningData", out tagData);
                    if (tagData != null)
                    {
                        animation = new AnimationPlayer(tagData as SkinningData);
                    }

                    tags.TryGetValue("BoundingSphere", out tagData);
                    if (tagData != null)
                    {
                        boundingSphere = (BoundingSphere)tagData;
                        transformedBoundingSphere = boundingSphere;
                        hasBoundingSphere = true;
                    }

                    tags.TryGetValue("BoundingBox", out tagData);
                    if (tagData != null)
                    {
                        boundingBox = (BoundingBox)tagData;
                        transformedBoundingBox = boundingBox;
                        hasBoundingBox = true;
                    }
                }
            }
            get
            {
                return model;
            }
        }

        public Vector3 MeshColor { set; get; }

        public Vector3 TintColor { set; get; }

        public float TintAmount { set; get; }

        public float EmissiveAdditive { set; get; }

        AudioSource audioSource;
        public AudioSource AudioSource
        {
            get
            {
                return audioSource;
            }
            set
            {
                audioSource = value;
            }
        }

        PhysicalEntity entity;
        public PhysicalEntity PhysicalEntity
        {
            set
            {
                Collidable c = null;
                // remove the physical entity
                if (value == null && entity != null)
                {
                    
                    if (entity.EntityType == PhysicalEntityType.StaticMesh)
                        c = entity.BEPUStaticMesh;
                    else
                        c = entity.BEPUEntity.CollisionInformation;

                    if (c != null)
                    {
                        physicsService.Remove(entity);
                        sceneService.UnmapSceneNodeFromCollidable(c, this);
                    }                    
                }

                // if we removed the entity, just return.
                entity = value;
                if (entity == null)
                    return;

                // we have a new entity, so let's create it!
                if (entity.EntityType != PhysicalEntityType.StaticMesh)
                {
                    Debug.Assert(entity.BEPUEntity != null);

                    entity.BEPUEntity.PositionUpdated += new Action<Entity>(Physics_EntityPositionUpdated);

                    entity.BEPUEntity.CollisionInformation.Events.InitialCollisionDetected +=
                        new BEPUphysics.Collidables.Events.InitialCollisionDetectedEventHandler<EntityCollidable>(SceneService.Physics_CollisionStarted);

                    entity.BEPUEntity.CollisionInformation.Events.PairTouched +=
                        new BEPUphysics.Collidables.Events.PairTouchedEventHandler<EntityCollidable>(SceneService.Physics_PairTouched);

                    entity.BEPUEntity.CollisionInformation.Events.CollisionEnded +=
                        new BEPUphysics.Collidables.Events.CollisionEndedEventHandler<EntityCollidable>(SceneService.Physics_CollisionEnded);

                    c = entity.BEPUEntity.CollisionInformation;

                    
                    entity.Translation = Translation +entity.Center;
                    entity.Orientation = Math2.RotateAroundPivot(entity.Center, Orientation);
                    entity.UnpivotedOrientation = Orientation;
                }
                else
                {
                    Debug.Assert(entity.BEPUStaticMesh != null);

                    entity.BEPUStaticMesh.Events.DetectingInitialCollision += 
                        new BEPUphysics.Collidables.Events.DetectingInitialCollisionEventHandler<StaticMesh>(SceneService.Physics_CollisionStarting_Static);

                    entity.BEPUStaticMesh.Events.PairTouching += 
                        new BEPUphysics.Collidables.Events.PairTouchingEventHandler<StaticMesh>(SceneService.Physics_PairTouching_Static);

                    entity.BEPUStaticMesh.Events.CollisionEnding += 
                        new BEPUphysics.Collidables.Events.CollisionEndingEventHandler<StaticMesh>(SceneService.Physics_CollisionEnding_Static);

                    c = entity.BEPUStaticMesh;
                }
                physicsService.Add(entity);

                sceneService.MapSceneNodeToCollidable(c, this);
            }
            get
            {
                return entity;
            }
        }

        #region physics delegates

        void Physics_EntityPositionUpdated(Entity sender)
        {
            SceneService ss = ServiceProvider.GetService<SceneService>();
            Collidable col = sender.CollisionInformation;
            SceneNode colNode = ss.collidableNodeMappings[col];
            
            // the nod might be removed, just return
            if (colNode == null || colNode.PhysicalEntity == null)
            {
                return;
            }
            
            // when those values are true, the Translation and Orientation properties
            // will not update the BEPU physics values again, as this would lead to
            // an endless loop.
            translationIssuedByBEPU = orientationIssuedByBEPU = true;

            Translation = sender.Position - colNode.PhysicalEntity.Center;
            //Orientation = colNode.PhysicalEntity.UnpivotedOrientation;// sender.Orientation; // there could be a bug here. we need to set the unpivoted orientation
            Orientation = sender.Orientation;

            // but restore them so we can move the object and the entity later on.
            translationIssuedByBEPU = orientationIssuedByBEPU = false;
        }

        #endregion


        AnimationPlayer animation;
        public AnimationPlayer Animation
        {
            get
            {
                return animation;
            }
        }

        Camera camera;
        public Camera Camera
        {
            set
            {
                Debug.Assert(value != null);
                camera = value;
            }
            get
            {
                return camera;
            }
        }

        ILight light;
        public ILight Light
        {
            set
            {
                light = value;
            }
            get
            {
                return light;
            }
        }
        #endregion

        #region Translating
        Vector3 translation;
        bool translationIssuedByBEPU;

        private void UpdateTranslation(ParticleSystem ps)
        {
            if (ps.AttachmentPoint == "node")
            {
                ps.AttachementTranslation = translation;
            }
            else if (Animation == null)
            {
                ps.AttachementTranslation = translation;
                int idx = model.Bones[ps.AttachmentPoint].Index;
                ps.AttachementTranslation += Vector3.Transform( meshBoneMatrices[idx].Translation, Orientation);
            }
            else
            {
                int idx = Animation.GetBoneIndex(ps.AttachmentPoint);
                ps.AttachementTranslation = translation + Vector3.Transform(Animation.WorldTransforms[idx].Translation, orientation);
            }
        }

        private void UpdateTranslation(SceneNode node)
        {
            if (Animation != null)
            {
                int idx = Animation.GetBoneIndex(node.mountPoint);
                node.Translation = translation + Vector3.Transform(Animation.WorldTransforms[idx].Translation, orientation);
            }
        }

        private void UpdateOrientation(SceneNode node)
        {
            if (Animation != null)
            {
                Vector3 s, t;
                Quaternion o;
                int idx = Animation.GetBoneIndex(node.mountPoint);

                Animation.WorldTransforms[idx].Decompose(out s, out o, out t);
                if (!float.IsNaN(o.X) && !float.IsNaN(o.Y) && !float.IsNaN(o.Z) && !float.IsNaN(o.W))
                {
                    node.Orientation = orientation * o;
                }
            }
        }

        private void UpdateParticleSystemTranslation()
        {
            foreach (ParticleSystem ps in particleSystems)
            {
                UpdateTranslation(ps);
            }
        }

        private void UpdateMountedNodeTranslation()
        {
            foreach (SceneNode node in mountedNodes.Values)
            {
                UpdateTranslation(node);
            }
        }

        private void UpdateMountedNodeOrientation()
        {
            foreach (SceneNode node in mountedNodes.Values)
            {
                UpdateOrientation(node);
            }
        }

        public Vector3 Translation
        {
            set
            {
                // check to see if this is a static mesh. We cannot move those!
                if (PhysicalEntity != null && !translationIssuedByBEPU)
                {
                    Debug.Assert(PhysicalEntity.EntityType != PhysicalEntityType.StaticMesh);
                    // ok, if we reached here, it is not a StaticMesh, we can move it.
                    // check that it's not null, just to make sure:
                    Debug.Assert(PhysicalEntity.BEPUEntity != null);

                    PhysicalEntity.Translation = value + PhysicalEntity.Center;
                }
                translation = value;
            }
            get
            {
                return translation;
            }
        }

        public void Translate(float x, float y, float z)
        {
            Translate(new Vector3(x,y,z));
        }
        public void Translate(Vector3 v)
        {
            if (PhysicalEntity != null && !translationIssuedByBEPU)
            {
                Debug.Assert(PhysicalEntity.EntityType != PhysicalEntityType.StaticMesh);
                // ok, if we reached here, it is not a StaticMesh, we can move it.
                // check that it's not null, just to make sure:
                Debug.Assert(PhysicalEntity.BEPUEntity != null);

                PhysicalEntity.Translation += v + PhysicalEntity.Center;
            }
            translation += v;
        }
        
        #endregion

        #region Orientation
        Quaternion orientation;
        Vector3 up = new Vector3(0, 1, 0);
        Vector3 right = new Vector3(1, 0, 0);
        Vector3 forward = new Vector3(0, 0, 1);

        public Vector3 Up
        {
            get
            {
                return up;
            }
        }

        public Vector3 Right
        {
            get
            {
                return right;
            }
        }

        public Vector3 Forward
        {
            get
            {
                return forward;
            }
        }

        bool orientationIssuedByBEPU;

        public Quaternion Orientation
        {
            set
            {
                // check to see if this is a static mesh. We cannot rotate those!
                if (PhysicalEntity != null && !orientationIssuedByBEPU)
                {
                    Debug.Assert(PhysicalEntity.EntityType != PhysicalEntityType.StaticMesh);
                    // ok, if we reached here, it is not a StaticMesh, we can rotate it.
                    // check that it's not null, just to make sure:
                    Debug.Assert(PhysicalEntity.BEPUEntity != null);

                    PhysicalEntity.UnpivotedOrientation = value;
                    PhysicalEntity.Orientation = Math2.RotateAroundPivot(PhysicalEntity.Center, value);
                }

                orientation = value;

                up = Vector3.Up;
                up = Vector3.Transform(up, orientation);
                up.Normalize();

                forward = -Vector3.Forward;
                forward = Vector3.Transform(forward, orientation);
                forward.Normalize();

                right = -Vector3.Right;
                right = Vector3.Transform(right, orientation);
                right.Normalize();
            }
            get
            {
                return orientation;
            }
        }

        public void Rotate(Quaternion q)
        {
            Orientation *= q;
        }

        public void Rotate(Vector3 axis, float degrees)
        {
            Rotate (Quaternion.CreateFromAxisAngle(axis, MathHelper.ToRadians(degrees)) );
        }

        public void Rotate(float yawDegrees, float pitchDegrees, float rollDegrees)
        {
            Rotate(Quaternion.CreateFromYawPitchRoll(MathHelper.ToRadians(yawDegrees), MathHelper.ToRadians(pitchDegrees), MathHelper.ToRadians(rollDegrees)) );
        }

        Vector3 lookTarget;
        public Vector3 LookAt
        {
            set
            {
                lookTarget = value;

                Matrix rotation = new Matrix();

                rotation.Forward = Vector3.Normalize(lookTarget - Translation);
                rotation.Right = Vector3.Normalize(Vector3.Cross(rotation.Forward, Up));
                rotation.Up = Vector3.Normalize(Vector3.Cross(rotation.Right, rotation.Forward));

                Orientation = Quaternion.CreateFromRotationMatrix(rotation);
            }
            get
            {
                return lookTarget;
            }
        }

        #endregion

        #region Updating Scene Node
        Matrix worldMatrix;

        public Matrix WorldMatrix
        {
            get
            {
                return worldMatrix;
            }
        }

        private Vector3 RotateAroundPoint(Vector3 point, Vector3 originPoint, Quaternion q)
        {
            Vector3 diffVect = point - originPoint;
            Vector3 rotatedVect = Vector3.Transform(diffVect, q);
            rotatedVect += originPoint;
            return rotatedVect;
        }

        internal void UpdateTransform()
        {
            // update the bone matrices. we need those for certain translations in the sceneNode, just get them here.
            if (Model != null)
            {
                Model.CopyAbsoluteBoneTransformsTo(meshBoneMatrices);
            }

            worldMatrix = Matrix.CreateFromQuaternion(Orientation) * Matrix.CreateTranslation(Translation);

            if (HasBoundingSphere)
            {
                transformedBoundingSphere = boundingSphere.Transform(Matrix.CreateTranslation(Translation));
            }
            
            if (HasBoundingBox)
            {
                transformedBoundingBox = boundingBox;
                
                Vector3 center = (transformedBoundingBox.Max + transformedBoundingBox.Min)/2;

                transformedBoundingBox.Max = Vector3.Transform(transformedBoundingBox.Max, Matrix.CreateTranslation(translation));
                transformedBoundingBox.Min = Vector3.Transform(transformedBoundingBox.Min, Matrix.CreateTranslation(translation));
                
            }

            // mounted nodes transformation
            UpdateMountedNodeTranslation();
            UpdateMountedNodeOrientation();
            UpdateParticleSystemTranslation();
        }

        internal bool HasBoundingBox
        {
            get
            {
                return hasBoundingBox;
            }
        }

        internal BoundingBox TransformedBoundingBox
        {
            get
            {
                return transformedBoundingBox;
            }
        }

        internal BoundingBox BoundingBox
        {
            get
            {
                return boundingBox;
            }
        }

        internal bool HasBoundingSphere
        {
            get
            {
                return hasBoundingSphere;
            }
        }

        internal BoundingSphere TransformedBoundingSphere
        {
            get
            {
                return transformedBoundingSphere;
            }
        }

        internal BoundingSphere BoundingSphere
        {
            get
            {
                return boundingSphere;
            }
        }

        #endregion

        public void Dispose()
        {
            if (PhysicalEntity != null)
            {
                Collidable c = PhysicalEntity.EntityType == PhysicalEntityType.StaticMesh ? (Collidable)PhysicalEntity.BEPUStaticMesh : PhysicalEntity.BEPUEntity.CollisionInformation;
                sceneService.UnmapSceneNodeFromCollidable(c, this);
                physicsService.Remove(PhysicalEntity);

                entity = null;
            }

            if (Behaviour != null)
            {
                Behaviour.Dispose();
                Behaviour.node = null;
                behaviour = null;
            }

            // this is wrong... mounted node should be removed from scene
            mountedNodes.Clear();
            particleSystems.Clear();
        }
    }
}
