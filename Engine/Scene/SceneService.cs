﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using System.Linq;

using System.Threading;

using BEPUphysics;
using BEPUphysics.Threading;
using BEPUphysics.Settings;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;

using System.Diagnostics;

using Engine.Core;
using Engine.Scene;
using Engine.Physics;
using Engine.Graphics;
using Engine.AI;

namespace Engine.Scene
{
    public enum LightQuery
    {
        All,
        Directional,
        Point
    }

    public sealed class SceneService : IDisposable
    {
        #region Variables
        AudioListener audioListener;
        PhysicsService physics;
        GraphicsService graphics;

        #endregion

        internal SceneService()
        {
            nodes = new Dictionary<string, SceneNode>(1000);
            nodesForRemoval = new List<SceneNode>(100);

            audioListener = new AudioListener();

            physics = ServiceProvider.GetService<PhysicsService>();
            graphics = ServiceProvider.GetService<GraphicsService>();
            
            collidableNodeMappings = new Dictionary<Collidable, SceneNode>(1000);
        }

        private PathGrid grid;
        public PathGrid PathGrid
        {
            set
            {
                lock (this)
                {
                    Debug.Assert(value != null);
                    grid = value;
                }
            }
            get
            {
                lock (this)
                {
                    return grid;
                }
            }
        }

        public bool ShowPathGrid { set; get; }

        #region Scene Node Management

        private Dictionary<string, SceneNode> nodes;
        private List<SceneNode> nodesForRemoval;
        private List<SceneNode> nodesToAdd = new List<SceneNode>();

        internal Dictionary<Collidable, SceneNode> collidableNodeMappings;

        SceneNode activeCameraNode;

        DebugRendererService debugRenderer;

        internal void MapSceneNodeToCollidable(Collidable c, SceneNode node)
        {
            // do not allow adding the same key twice, assert, assert!!
            Debug.Assert(!collidableNodeMappings.ContainsKey(c));

            collidableNodeMappings[c] = node;
        }

        internal void UnmapSceneNodeFromCollidable(Collidable c, SceneNode node)
        {
            collidableNodeMappings.Remove(c);
        }

        // if forceClear is true, 
        // forces the objects to be removed now, instead of the end of frame
        public void Clear(List<SceneNode> excludeNodes)
        {
            List<SceneNode> modifiedExcludedNodes = null;

            if (excludeNodes != null)
            {
                modifiedExcludedNodes = new List<SceneNode>(excludeNodes);
                // add the mounted nodes as well
                foreach (SceneNode node in excludeNodes)
                {
                    foreach (SceneNode mountedNode in node.MountedNodes.Values)
                    {
                        // ignore nodes to be removed
                        if (!nodesForRemoval.Contains(mountedNode))
                            modifiedExcludedNodes.Add(mountedNode);
                    }
                }
            }

            foreach (SceneNode node in nodes.Values)
            {
                bool ignore = false;

                if (nodesForRemoval.Contains(node))
                    continue;

                if (modifiedExcludedNodes != null)
                {
                    foreach (SceneNode n in modifiedExcludedNodes)
                    {
                        if (!nodesForRemoval.Contains(n) && node == n)
                        {
                            ignore = true;
                            break;
                        }
                    }
                }

                // add it to the removal list for the next frame
                if (!ignore)
                    RemoveSceneNode(node);
            }
        }

        
        public void Clear()
        {
            Clear(null);
        }

        // if forceClear is true, 
        // forces the objects to be removed now, instead of the end of frame
        public void Clear(bool force)
        {
            Clear();
            if(force) RemovePendingNodes();
        }
        
        public int NodeCount
        {
            get
            {
                return nodes.Count;
            }
        }

        public SceneNode AddSceneNode(string name, bool forceAdd)
        {
            // don't add the scene node immediately. otherwise the loop in BehaviourUpdate()
            // could crash if the dictionary changes. This crash would not allow children to
            // create new nodes... not good... no no no.

            SceneNode node = new SceneNode(name);
            if (forceAdd)
            {
                Debug.Assert(!nodes.ContainsKey(node.Name));
                nodes[node.Name] = node;
            }
            else
                nodesToAdd.Add(node);

            return node;
        }

        public SceneNode AddSceneNode(string name)
        {
            return AddSceneNode(name, false);
        }

        private void PendSceneNodeRemoval(SceneNode node)
        {
            nodesForRemoval.Add(node);
            foreach (SceneNode mounted in node.MountedNodes.Values)
                if (!nodesForRemoval.Contains(mounted))
                    nodesForRemoval.Add(mounted);
        }

        public void RemoveSceneNode(string name)
        {
            SceneNode node = this[name];

            // This node is in the scene. Remove it.
            if (node != null)
                PendSceneNodeRemoval(node);

            // This node is going to be added. Remove it from the adding.
            else
            {
                for (int i = 0; i < nodesToAdd.Count; i++)
                {
                    if (nodesToAdd[i].Name == name)
                    {
                        SceneNode removeMe = nodesToAdd[i];
                        nodesToAdd.RemoveAt(i);

                        if (removeMe.PhysicalEntity != null)
                            removeMe.PhysicalEntity = null;

                        return;
                    }
                }
            }
        }

        public void RemoveSceneNode(SceneNode node)
        {
            // Node is already in the scene? Remove it in the next frame.
            if (nodes.ContainsValue(node))
            {
                PendSceneNodeRemoval(node);
            }
            // Node was going to be added next frame. Remove it from the adding.
            else if (nodesToAdd.Contains(node))
            {
                nodesToAdd.Remove(node);
                if (node.PhysicalEntity != null) node.PhysicalEntity = null;
            }
            // Failed:
            else Debug.Assert(false);
        }

        public SceneNode ActiveCameraNode
        {
            set
            {
                Debug.Assert (value != null);
                activeCameraNode = value;
            }
            get
            {
                return activeCameraNode;
            }
        }

        public bool ShowLightBoundingSpheres
        {
            set;
            get;
        }

        public bool ShowModelBoundingSpheres
        {
            set;
            get;
        }

        public bool ShowPhysicalEntities
        {
            set;
            get;
        }

        #endregion

        #region Scene Node Querrying
        public SceneNode this[string name]
        {
            get
            {
                return GetSceneNode(name);
            }
        }

        public SceneNode GetSceneNode(string name)
        {
            SceneNode value;
            if (nodes.TryGetValue(name, out value))
                return value;

            return null;
        }

        public List<SceneNode> GetSceneNodesInCategoryGroup(string groupName)
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.CategoryGroup == groupName)
                    nodes.Add(node);
            }

            foreach (SceneNode node in this.nodesToAdd)
            {
                if (node.CategoryGroup == groupName)
                    nodes.Add(node);
            }

            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithModel()
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.Model != null && node.Active)
                    nodes.Add(node);
            }
            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithCamera()
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.Camera != null && node.Active)
                    nodes.Add(node);
            }
            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithLight(LightQuery lightTypes)
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.Light != null && node.Active)
                {
                    if (lightTypes == LightQuery.All)
                        nodes.Add(node);
                    else
                    {
                        if (lightTypes == LightQuery.Directional && node.Light.LightType == LightType.Directional)
                            nodes.Add(node);
                        if (lightTypes == LightQuery.Point && node.Light.LightType == LightType.Point)
                            nodes.Add(node);
                    }
                }
            }
            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithAnimation()
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.Animation != null && node.Active)
                    nodes.Add(node);
            }
            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithAudioSource()
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.AudioSource != null && node.Active)
                    nodes.Add(node);
            }
            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithPhysicalEntity()
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.PhysicalEntity != null && node.Active)
                    nodes.Add(node);
            }
            return nodes;
        }

        public List<SceneNode> GetSceneNodesWithParticleSystems()
        {
            List<SceneNode> nodes = new List<SceneNode>();

            foreach (SceneNode node in this.nodes.Values)
            {
                if (node.HasParticleSystem && node.Active)
                {
                    nodes.Add(node);
                }
            }
            return nodes;
        }

        #endregion

        #region Unproject / Project
        public Vector3 Project(Vector3 source)
        {
            Debug.Assert (ActiveCameraNode != null);
            Debug.Assert(ActiveCameraNode.Camera != null);

            Matrix projection = ActiveCameraNode.Camera.ProjectionMatrix;
            Matrix view = Matrix.Invert(ActiveCameraNode.WorldMatrix);

            return graphics.GraphicsDevice.Viewport.Project(source, projection, view, Matrix.Identity);
        }

        public Vector3 Unproject(Vector3 source)
        {
            Debug.Assert(ActiveCameraNode != null);
            Debug.Assert(ActiveCameraNode.Camera != null);

            Matrix projection = ActiveCameraNode.Camera.ProjectionMatrix;
            Matrix view = Matrix.Invert(ActiveCameraNode.WorldMatrix);

            return graphics.GraphicsDevice.Viewport.Unproject(source, projection, view, Matrix.Identity);
        }
        #endregion

        #region Scene Updating
        BoundingFrustum frustum;

        public BoundingFrustum Frustum
        {
            get
            {
                return frustum;
            }
        }

        internal void Update()
        {
            frustum = null;
            if (activeCameraNode != null && activeCameraNode.Camera != null)
            {
                activeCameraNode.Camera.AspectRatio = graphics.GraphicsDevice.Viewport.AspectRatio;
                activeCameraNode.Camera.MakeProjectionMatrix();
                activeCameraNode.UpdateTransform();

                audioListener.Position = activeCameraNode.Translation;
                audioListener.Up = new Vector3(0, 1, 0);
                audioListener.Forward = new Vector3(0, 0, -1);

                Matrix viewMatrix = Matrix.Invert(activeCameraNode.WorldMatrix);
                Matrix projectionMatrix = activeCameraNode.Camera.ProjectionMatrix;

                frustum = new BoundingFrustum(viewMatrix * projectionMatrix);
            }
            PathFindUpdate();

            BehaviourUpdate();

            physics.Update();

            GenericUpdate();

            RemovePendingNodes();

            AddPendingNodes();
        }

        void RemovePendingNodes()
        {
            for (int i = 0; i < nodesForRemoval.Count; i++ )
            {
                SceneNode node = nodesForRemoval[i];
                Debug.Assert(nodes.ContainsKey(node.Name));

                node.Dispose();
                nodes.Remove(node.Name);
            }
            nodesForRemoval.Clear();
        }

        void AddPendingNodes()
        {
            foreach (SceneNode node in nodesToAdd)
            {
                // do not allow to overwrite an existing node, asseeeeertt!!!
                Debug.Assert(!nodes.ContainsKey(node.Name));

                nodes[node.Name] = node;
            }
            nodesToAdd.Clear();
        }

        void PathFindUpdate()
        {
            AsyncPathFindingService pfs = ServiceProvider.GetService<AsyncPathFindingService>();
            Debug.Assert(pfs != null);

            while(!pfs.Results.IsEmpty)
            {
                PathFindResult result = pfs.Results.Get();
                SceneNode callerNode = this[result.Name];
                if (callerNode != null && callerNode.Behaviour != null) // the unit might have been killed, don't assert here
                {
                    // moved this in the async service.
                    callerNode.Behaviour.WaitingForPathFind = false;
                    callerNode.Behaviour.OnPathFindUpdated(result.FoundPath);
                }
            }
        }

        void BehaviourUpdate()
        {
            foreach (SceneNode node in nodes.Values)
            {
                if (node.Behaviour != null)
                {
                    bool keep = node.Active ?
                        node.Behaviour.Update() :
                        node.Behaviour.InactiveUpdate();

                    if (!keep)
                    {
                        RemoveSceneNode(node);
                    }
                    else
                    {
                        AsyncPathFindingService pfs = ServiceProvider.GetService<AsyncPathFindingService>();
                        Debug.Assert(pfs != null);

                        if (node.Behaviour.pathFindQuery != null)
                        {
                            pfs.Queries.Enqueue(node.Behaviour.pathFindQuery);
                            node.Behaviour.pathFindQuery = null;
                        }
                    }
                }
            }
        }

        void GenericUpdate()
        {
            debugRenderer = ServiceProvider.GetService<DebugRendererService>();

            if (ShowPathGrid && this.PathGrid != null)
            {
                debugRenderer.AddGrid3D(this.PathGrid, Color.Red, Color.Green, 0.5f);
            }

            foreach (SceneNode node in nodes.Values)
            {
                if (!node.Active) continue;

                node.UpdateTransform();

                if (node.Animation != null)
                {
                    // x1000 because animation code is in milliseconds. gametiming is in seconds
                    node.Animation.Update(GameTiming.TimeElapsed*1000, true, Matrix.Identity);
                }

                if (node.AudioSource != null)
                {
                    node.AudioSource.Update(audioListener, node.Translation, new Vector3(0,0,1), new Vector3(0,1,0));
                }

                if (node.PhysicalEntity != null)
                {
                    if (ShowPhysicalEntities)
                        node.PhysicalEntity.RenderDebugEntities();
                }

                if (node.Light != null)
                {
                    if (node.Light.LightType == LightType.Point)
                    {
                        if (ShowLightBoundingSpheres)
                        {
                            debugRenderer.AddSphere(node.Translation, node.Light.Radius, Color.Blue);
                        }
                    }
                }

                if (node.Model != null)
                {
                    if (node.HasBoundingSphere)
                    {
                        ContainmentType currentContainmentType = ContainmentType.Disjoint;
                        currentContainmentType = Frustum.Contains(node.TransformedBoundingSphere);
                        node.FrustumCulled = (currentContainmentType == ContainmentType.Disjoint);

                        if (ShowModelBoundingSpheres)
                            debugRenderer.AddSphere(node.TransformedBoundingSphere.Center, node.TransformedBoundingSphere.Radius, Color.Red);
                    }
                }

                if (node.HasParticleSystem)
                {
                    bool b = true;
                    foreach (ParticleSystem ps in node.ParticleSystems)
                    {
                        if (ps.IsAlive)
                        {
                            bool stillAlive = ps.Update();
                            ps.FrustumCulled = Frustum.Contains(ps.GetBoundingSphere()) == ContainmentType.Disjoint;

                            if (!stillAlive)
                            { // The particle animation is finished.

                                // Remove the scene node?
                                if (node.ParticleAttachMode == ParticleAttachMode.DiscardOnCompletion)
                                {
                                    if (b)
                                    {
                                        this.RemoveSceneNode(node);
                                        b = false;
                                    }
                                }
                                /* else keep this scene node alive */
                            }
                            /* else keep this particle system attached */
                        }
                        else
                        {
                            /* particle system attached but hasn't started yet. */
                            ps.InitOldAttachementTranslation(node.Translation);
                        }
                    }
                }
            }
        }
        #endregion

        #region Physics delegates

        internal static void Physics_PairTouched(EntityCollidable sender, Collidable other, CollidablePairHandler pair)
        {
            SceneService sceneService = ServiceProvider.GetService<SceneService>();

            Collidable senderCollidable = sender as Collidable;
            SceneNode senderNode = null;
            sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);

            if (other != null && senderNode != null)
            {
                SceneNode otherNode = null;
                sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);
                if (otherNode != null && senderNode.Behaviour != null &&
                    otherNode.Active && senderNode.Active)
                {
                    Debug.Assert(otherNode.PhysicalEntity != null);

                    if (otherNode.PhysicalEntity.IsTrigger)
                        senderNode.Behaviour.OnTriggerStay(otherNode);
                    else
                        senderNode.Behaviour.OnCollideStay(otherNode);
                }
            }
        }

        internal static void Physics_CollisionStarted(EntityCollidable sender, Collidable other, CollidablePairHandler pair)
        {
            SceneService sceneService = ServiceProvider.GetService<SceneService>();

            Collidable senderCollidable = sender as Collidable;
            SceneNode senderNode = null;
            sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);

            if (other != null && senderNode != null)
            {
                SceneNode otherNode = null;
                sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);

                if (otherNode != null && senderNode.Behaviour != null &&
                    otherNode.Active && senderNode.Active)
                {
                    Debug.Assert(otherNode.PhysicalEntity != null);

                    if (otherNode.PhysicalEntity.IsTrigger)
                        senderNode.Behaviour.OnTriggerEnter(otherNode);
                    else
                        senderNode.Behaviour.OnCollideEnter(otherNode);
                }
            }
        }

        internal static void Physics_CollisionEnded(EntityCollidable sender, Collidable other, CollidablePairHandler pair)
        {
            SceneService sceneService = ServiceProvider.GetService<SceneService>();

            Collidable senderCollidable = sender as Collidable;
            SceneNode senderNode = null;
            sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);

            if (other != null && senderNode != null)
            {
                SceneNode otherNode = null;
                sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);
                if (otherNode != null && senderNode.Behaviour != null &&
                    otherNode.Active && senderNode.Active)
                {
                    Debug.Assert(otherNode.PhysicalEntity != null);

                    if (otherNode.PhysicalEntity.IsTrigger)
                        senderNode.Behaviour.OnTriggerExit(otherNode);
                    else
                        senderNode.Behaviour.OnCollideExit(otherNode);
                }
            }
        }

        internal static void Physics_PairTouching_Static(StaticMesh sender, Collidable other, CollidablePairHandler pair)
        {
            SceneService sceneService = ServiceProvider.GetService<SceneService>();

            Collidable senderCollidable = sender as Collidable;
            SceneNode senderNode = null;
            sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);
            if (other != null && senderNode != null)
            {
                SceneNode otherNode = null;
                sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);
                if (otherNode != null && senderNode.Behaviour != null &&
                    otherNode.Active && senderNode.Active)
                {
                    Debug.Assert(otherNode.PhysicalEntity != null);

                    if (otherNode.PhysicalEntity.IsTrigger)
                        senderNode.Behaviour.OnTriggerStay(otherNode);
                    else
                        senderNode.Behaviour.OnCollideStay(otherNode);
                }
            }
        }

        internal static void Physics_CollisionStarting_Static(StaticMesh sender, Collidable other, CollidablePairHandler pair)
        {
            SceneService sceneService = ServiceProvider.GetService<SceneService>();

            Collidable senderCollidable = sender as Collidable;
            SceneNode senderNode = null;
            sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);
            if (other != null && senderNode != null)
            {
                SceneNode otherNode = null;
                sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);
                if (otherNode != null && senderNode.Behaviour != null &&
                    otherNode.Active && senderNode.Active)
                {
                    Debug.Assert(otherNode.PhysicalEntity != null);

                    if (otherNode.PhysicalEntity.IsTrigger)
                        senderNode.Behaviour.OnTriggerEnter(otherNode);
                    else
                        senderNode.Behaviour.OnCollideEnter(otherNode);
                }
            }
        }

        internal static void Physics_CollisionEnding_Static(StaticMesh sender, Collidable other, CollidablePairHandler pair)
        {
            SceneService sceneService = ServiceProvider.GetService<SceneService>();

            Collidable senderCollidable = sender as Collidable;
            SceneNode senderNode = null;
            sceneService.collidableNodeMappings.TryGetValue(senderCollidable, out senderNode);
            if (other != null && senderNode != null)
            {
                SceneNode otherNode = null;
                sceneService.collidableNodeMappings.TryGetValue(other, out otherNode);
                if (otherNode != null && senderNode.Behaviour != null &&
                    otherNode.Active && senderNode.Active)
                {
                    Debug.Assert(otherNode.PhysicalEntity != null);

                    if (otherNode.PhysicalEntity.IsTrigger)
                        senderNode.Behaviour.OnTriggerExit(otherNode);
                    else
                        senderNode.Behaviour.OnCollideExit(otherNode);
                }
            }
        }

        #endregion

        public void Dispose()
        {
            foreach (SceneNode node in nodes.Values)
            {
                if (node.Behaviour != null)
                    node.Behaviour.Dispose();
            }
            nodes.Clear();
            nodesForRemoval.Clear();
        }
    }
}
