﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

using Microsoft.Xna.Framework;

using Engine.AI;
using Engine.Scene;
using Engine.Core;

namespace Engine.Scene
{
    public abstract class SceneNodeBehaviour
    {
        internal SceneNode node;
        public SceneNode SceneNode
        {
            internal set
            {
                Debug.Assert(value != null);
                node = value;
            }
            get
            {
                return node;
            }
        }

        public SceneNodeBehaviour()
        {
            WaitingForPathFind = false;
        }

        public abstract bool Start();
        // if the Update method returns false, the node is removed from the scene
        public abstract bool Update();
        public abstract void Dispose();

        public virtual void OnTriggerEnter(SceneNode otherNode) {}
        public virtual void OnTriggerExit(SceneNode otherNode) {}
        public virtual void OnTriggerStay(SceneNode otherNode) {}

        public virtual void OnCollideEnter(SceneNode otherNode) {}
        public virtual void OnCollideExit(SceneNode otherNode) {}
        public virtual void OnCollideStay(SceneNode otherNode) {}

        // Inactive nodes:
        public virtual bool InactiveUpdate() { return true; }
        public virtual void OnInactived() { }

        // pathfind response
        public virtual void OnPathFindUpdated(List<Vector2> newPath) { }

        // pathfind query
        internal PathFindQuery pathFindQuery;

        /// <summary>
        /// True when PathFind is invoked. Reset to false once
        /// OnPathFindUpdate is invoked.
        /// </summary>
        public bool WaitingForPathFind
        {
            get;
            internal set;
        }

        public virtual void PathFind(Vector2 worldStart, Vector2 worldEnd)
        {
            WaitingForPathFind = true;
            pathFindQuery = new PathFindQuery(SceneNode.Name, worldStart, worldEnd);
        }
    }
}
