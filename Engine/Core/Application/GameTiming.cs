﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Core
{
    public static class GameTiming
    {
        /// <summary>
        /// Elapsed ticks since previous frame
        /// </summary>
        public static long ElapsedTicks
        {
            set;
            get;
        }

        /// <summary>
        /// Time scale
        /// </summary>
        public static float TimeScale
        {
            set;
            get;
        }

        /// <summary>
        /// Time elapsed since previous frame in seconds
        /// </summary>
        public static float TimeElapsed
        {
            set;
            get;
        }

        /// <summary>
        /// Total time elapsed (Current time) in seconds. Pause dependant.
        /// </summary>
        public static float TotalTimeElapsed
        {
            set;
            get;
        }
        
        /// <summary>
        /// Is the game time paused?
        /// </summary>
        public static bool Paused
        {
            set;
            get;
        }
        
    }
}
