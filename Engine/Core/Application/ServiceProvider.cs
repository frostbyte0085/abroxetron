﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Core
{
    public static class ServiceProvider
    {
        private static Dictionary<Type, Object> services = new Dictionary<Type, object>();

        public static void AddService(Type type, Object instance)
        {
            services.Add(type, instance);
        }

        public static T GetService<T>() where T: class
        {
            object obj = null;
            services.TryGetValue (typeof(T), out obj);
            return obj as T;
        }
    }
}
