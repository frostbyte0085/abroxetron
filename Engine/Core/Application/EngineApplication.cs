﻿#define SMOOTH_TIME_ELAPSED

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

using Engine.Audio;
using Engine.Diagnostics;
using Engine.Tools;
using Engine.Graphics;
using Engine.Scene;
using Engine.Input;
using Engine.Physics;

using System.ComponentModel;
using System.Threading;

namespace Engine.Core
{
    public class State
    {
        protected State()
        {
            parameters = new List<object>();       
        }

        public virtual bool OnEnter()
        {
            return true;
        }

        public virtual bool OnUpdate()
        {
            return true;
        }
        
        public virtual bool OnExit()
        {
            parameters.Clear();
            return true;
        }

        public String Name
        {
            get
            {
                return name;
            }
        }

        public List<object> Parameters
        {
            get
            {
                return parameters;
            }
        }
        
        internal String name;
        internal List<object> parameters;
    }

    public class EngineApplication
    {
        #region Variables
        BackgroundWorker dispatcherWorker;
        EngineConfiguration configuration;

        ConsoleLogService logService;
        GraphicsService graphicsService;
        StorageService storageService;
        SceneService sceneService;
        ContentService contentService;
        PhysicsService physicsService;
        InputService inputService;
        MediaPlayerService mediaPlayerService;

        // renderers
        ModelRendererService modelRendererService;
        DebugRendererService debugRendererService;
        ParticleRendererService particleRenderService;
        UIRendererService uiRendererService;

        // post-processing
        PostProcessService postProcessService;

        // pathfinding worker service
        AsyncPathFindingService pathFindingService;

        // timing related
        Stopwatch clock;
        float avgRenderTime;
        float avgUpdateTime;
        float avgPostProcessTime;
        float avgParticleTime;
        float avgModelTime;
        float avgUITime;

        int numFrames;
        float elapsedFPS;
        float timeElapsed;
        float smoothElapsed;
        float currentTime;
        float persCurrentTime;

        // game state management
        Dictionary<string, State> states;
        State currentState;
        State requestedState = null;
        bool exitRequested;

        #endregion

        #region Methods
        public EngineApplication()
        {
            states = new Dictionary<string, State>();
            dispatcherWorker = new BackgroundWorker();
            dispatcherWorker.WorkerSupportsCancellation = true;
            dispatcherWorker.DoWork += new DoWorkEventHandler(dispatcherWorker_Work);
        }

        private void dispatcherWorker_Work(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (worker.CancellationPending)
                e.Cancel = true;
            else
            {
                do
                {
                    FrameworkDispatcher.Update();
                    System.Threading.Thread.Sleep(20);
                } while (!ExitRequested);
            }
        }

        public virtual void SetupStates()
        {

        }

        public void AddState(String stateName, State state)
        {
            if (!states.ContainsKey(stateName) && state != null)
            {
                state.name = stateName;
                states.Add(stateName, state);
            }
        }

        public void SetState(String stateName, params object[] args)
        {
            State state = states[stateName];

            if (state != null)
            {
                if (args != null && args.Length > 0)
                {
                    foreach (object o in args)
                    {
                        state.parameters.Add(o);
                    }
                }

                requestedState = state;
            }
        }

        public void SetState(String stateName)
        {
            SetState(stateName, null);
        }

        public State GetCurrentState()
        {
            return currentState;
        }

        private void EnterState()
        {
            // Exit old state:
            if (currentState != null)
                currentState.OnExit();

            // Reset everything for the new state:
            CustomCollisionGroupPairs.Clear();
            CustomCollisionGroups.Clear();
            sceneService.Clear(true);
            inputService.Clear();

            // Enter the new state
            currentState = requestedState;
            requestedState.OnEnter();
            requestedState = null;
        }

        public virtual void Initialize(EngineConfiguration configuration)
        {
            clock = new Stopwatch();
            clock.Start();

            this.configuration = configuration;

            // major components
            //
            logService = new ConsoleLogService();
            ServiceProvider.AddService(typeof(ConsoleLogService), logService);

            storageService = new StorageService(configuration.Name);
            ServiceProvider.AddService(typeof(StorageService), storageService);

            physicsService = new PhysicsService();
            ServiceProvider.AddService(typeof(PhysicsService), physicsService);

            graphicsService = GraphicsService.AddRef(configuration.Handle, configuration.TargetWidth, configuration.TargetHeight, configuration.Fullscreen, configuration.VSync);
            ServiceProvider.AddService(typeof(GraphicsService), graphicsService);

            inputService = new InputService();
            ServiceProvider.AddService(typeof(InputService), inputService);
            KinectHandler.InitializeHandler(graphicsService.GraphicsDevice);

            contentService = new ContentService();
            ServiceProvider.AddService(typeof(ContentService), contentService);

            // 3D SoundEffect initialisation
            SoundEffect.DistanceScale = 20;
            SoundEffect.DopplerScale = 0.1f;

            mediaPlayerService = new MediaPlayerService();
            ServiceProvider.AddService(typeof(MediaPlayerService), mediaPlayerService);

            // scene
            sceneService = new SceneService();
            ServiceProvider.AddService(typeof(SceneService), sceneService);
            //

            // renderers
            //
            modelRendererService = new ModelRendererService();
            debugRendererService = new DebugRendererService();
            particleRenderService = new ParticleRendererService();
            uiRendererService = new UIRendererService();
            ServiceProvider.AddService(typeof(ModelRendererService), modelRendererService);
            ServiceProvider.AddService(typeof(DebugRendererService), debugRendererService);
            ServiceProvider.AddService(typeof(ParticleRendererService), particleRenderService);
            ServiceProvider.AddService(typeof(UIRendererService), uiRendererService);
            //

            // pathfinding worker service
            pathFindingService = new AsyncPathFindingService();
            ServiceProvider.AddService(typeof(AsyncPathFindingService), pathFindingService);

            // post-process
            postProcessService = new PostProcessService();
            ServiceProvider.AddService(typeof(PostProcessService), postProcessService);

            // setup the timing classes
            GameTiming.Paused = false;
            GameTiming.TimeScale = 1.0f;
            PersistentTiming.TimeScale = 1.0f;

            numFrames = 0;
            elapsedFPS = 0.0f;
            timeElapsed = 0.0f;
            currentTime = 0.0f;
            persCurrentTime = 0.0f;

            //

            exitRequested = false;
            dispatcherWorker.RunWorkerAsync();

            SetupStates();

            IsInitialized = true;

            // Enter initial state:
            EnterState();
        }

        public bool IsInitialized
        {
            internal set;
            get;
        }

        public virtual void Dispose()
        {
            states.Clear();

            modelRendererService.Dispose();

            contentService["game"].Unload();
            contentService["game"].Dispose();

            contentService["shared"].Unload();
            contentService["shared"].Dispose();

            contentService["engine"].Unload();
            contentService["engine"].Dispose();

            sceneService.Dispose();

            KinectHandler.Dispose();

            physicsService.Dispose();
            postProcessService.Dispose();

            contentService.Release();
            graphicsService.Release(true);
            storageService.Release(true);

            IsInitialized = false;
        }

        public bool ExitRequested
        {
            set
            {
                exitRequested = value;
                if (exitRequested)
                {
                    dispatcherWorker.CancelAsync();
                    pathFindingService.RequestStop();
                }

            }
            get
            {
                return exitRequested;
            }
        }

        public virtual void OnPreStateUpdate()
        {

        }

        /*public bool IsLoading
        {
            set;
            get;
        }*/

        public void OneFrame()
        {
            float timePreFrame = (float)clock.Elapsed.TotalSeconds;
            
            float persistentScale = PersistentTiming.TimeScale;
            float gameScale = GameTiming.TimeScale;

#if SMOOTH_TIME_ELAPSED
            persCurrentTime += smoothElapsed * persistentScale;
#else
            persCurrentTime += timeElapsed * persistentScale;
#endif

            // update the 2 game timers
#if SMOOTH_TIME_ELAPSED
            PersistentTiming.TimeElapsed = smoothElapsed * persistentScale;
#else
            PersistentTiming.TimeElapsed = timeElapsed * persistentScale;
#endif

            PersistentTiming.TotalTimeElapsed += persCurrentTime;
            PersistentTiming.ElapsedTicks = (long)(PersistentTiming.TimeElapsed * TimeSpan.TicksPerSecond);

            // if the application is paused, only update the persistent timing, pause the game timing.
            if (GameTiming.Paused)
            {
                GameTiming.TimeElapsed = 0.0f;
            }
            else
            {
#if SMOOTH_TIME_ELAPSED
                currentTime += smoothElapsed * gameScale;
#else
                currentTime += timeElapsed * gameScale;
#endif
                GameTiming.TimeElapsed = timeElapsed * gameScale;
                GameTiming.TotalTimeElapsed = currentTime;
            }
            GameTiming.ElapsedTicks = (long)(GameTiming.TimeElapsed * TimeSpan.TicksPerSecond);

            //
            
            //storageService.Update();

            float updateStartTime = (float)clock.Elapsed.TotalMilliseconds;

            inputService.Update();

            // handle any pre-state updating from game logic, such as the game manager.
            OnPreStateUpdate();

            // scene & logic
            if (currentState != null)
            {
                if (!currentState.OnUpdate())
                    exitRequested = true;
            }

            mediaPlayerService.Update();
            sceneService.Update();
            uiRendererService.Update();
            //
            float updateEndTime = (float)clock.Elapsed.TotalMilliseconds;

            float particleStartTime = 0;
            float particleEndTime = 0;

            float modelStartTime = 0;
            float modelEndTime = 0;

            float uiStartTime = 0;
            float uiEndTime = 0;

            float postProcessStartTime = 0;
            float postProcessEndTime = 0;

            float renderStartTime = (float)clock.Elapsed.TotalMilliseconds;
            // rendering
            if (graphicsService.GraphicsDevice.GraphicsDeviceStatus == GraphicsDeviceStatus.Normal && !graphicsService.IsResetting)
            {
                // this prepares the projection matrix for the ps3 version of sprite batch used in the deferred renderer and post process service.
                SpriteHelper.PrepareProjection();
                
                graphicsService.SetViewport(0, 0, graphicsService.PresentationParameters.BackBufferWidth, graphicsService.PresentationParameters.BackBufferHeight);
                graphicsService.GraphicsDevice.Clear(Color.Transparent);

                if (sceneService.ActiveCameraNode != null && sceneService.ActiveCameraNode.Camera != null)
                {
                    modelStartTime = (float)clock.Elapsed.TotalMilliseconds;

                    modelRendererService.Render();

                    if (postProcessService.Count > 0)
                        postProcessService.Begin();

                    modelRendererService.RenderFinal();

                    modelEndTime = (float)clock.Elapsed.TotalMilliseconds;

                    particleStartTime = (float)clock.Elapsed.TotalMilliseconds;

                    particleRenderService.Render();

                    particleEndTime = (float)clock.Elapsed.TotalMilliseconds;

                    postProcessStartTime = (float)clock.Elapsed.TotalMilliseconds;

                    if (postProcessService.Count > 0)
                    {
                        postProcessService.PostProcess(modelRendererService.DeferredProcessor.DiffuseBufferBinding,
                                                       modelRendererService.DeferredProcessor.DepthBufferBinding,
                                                       modelRendererService.DeferredProcessor.NormalBufferBinding);
                    }

                    postProcessEndTime = (float)clock.Elapsed.TotalMilliseconds;
                    
                    debugRendererService.Render(sceneService.ActiveCameraNode);
                }

                uiStartTime = (float)clock.Elapsed.TotalMilliseconds;

                uiRendererService.Render();

                uiEndTime = (float)clock.Elapsed.TotalMilliseconds;

                debugRendererService.RenderDebugOverlay();


                graphicsService.GraphicsDevice.Present();
            }
            float renderEndTime = (float)clock.Elapsed.TotalMilliseconds;
            //

            // end time
            float timePostFrame = (float)clock.Elapsed.TotalSeconds;
            timeElapsed = timePostFrame - timePreFrame;

            avgRenderTime += renderEndTime - renderStartTime;
            avgUpdateTime += updateEndTime - updateStartTime;
            avgModelTime += modelEndTime - modelStartTime;
            avgUITime += uiEndTime - uiStartTime;
            avgPostProcessTime += postProcessEndTime - postProcessStartTime;
            avgParticleTime += particleEndTime - particleStartTime;

            // fps calculation            
            numFrames++;
            elapsedFPS += timeElapsed;
            if (elapsedFPS >= 1.0f) {
                PersistentTiming.FPS = numFrames;
                PersistentTiming.FrameTime = 1000.0f / PersistentTiming.FPS;

                PersistentTiming.UpdateTime = avgUpdateTime / numFrames;
                PersistentTiming.TotalRenderTime = avgRenderTime / numFrames;
                PersistentTiming.ParticleRenderTime = avgParticleTime / numFrames;
                PersistentTiming.PostProcessRenderTime = avgPostProcessTime / numFrames;
                PersistentTiming.ModelRenderTime = avgModelTime / numFrames;
                PersistentTiming.UIRenderTime = avgUITime / numFrames;

                elapsedFPS = 0.0f;
                numFrames = 0;
                avgRenderTime = 0;
                avgUpdateTime = 0;
                avgPostProcessTime = 0;
                avgUITime = 0;
                avgParticleTime = 0;
                avgModelTime = 0;
            }
            
#if SMOOTH_TIME_ELAPSED
            // smooth the timeElapsed
            smoothElapsed = 0.1f * timeElapsed + 0.9f * smoothElapsed;
#endif

            // Check for state change:
            if (requestedState != null)
                EnterState();
        }

        #endregion
    }
}
