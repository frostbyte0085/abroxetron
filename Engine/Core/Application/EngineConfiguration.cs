﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Core
{
    public class EngineConfiguration
    {
        public EngineConfiguration()
        {
            Fullscreen = false;
            VSync = true;
            TargetWidth = 640;
            TargetHeight = 480;
            Handle = IntPtr.Zero;
        }

        public IntPtr Handle
        {
            set;
            get;
        }

        public int TargetWidth
        {
            set;
            get;
        }

        public int TargetHeight
        {
            set;
            get;
        }

        public bool Fullscreen
        {
            set;
            get;
        }

        public bool VSync
        {
            set;
            get;
        }

        public string Name
        {
            set;
            get;
        }
    }
}
