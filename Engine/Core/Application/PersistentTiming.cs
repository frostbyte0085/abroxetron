﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Core
{
    public static class PersistentTiming
    {
        public static long ElapsedTicks
        {
            set;
            get;
        }

        public static float TimeScale
        {
            set;
            get;
        }

        public static float TimeElapsed
        {
            set;
            get;
        }

        public static float TotalTimeElapsed
        {
            set;
            get;
        }

        // debug stats
        public static int FPS
        {
            set;
            get;
        }

        public static float FrameTime
        {
            set;
            get;
        }

        public static float UpdateTime
        {
            set;
            get;
        }

        public static float TotalRenderTime
        {
            set;
            get;
        }

        public static float ParticleRenderTime
        {
            set;
            get;
        }

        public static float UIRenderTime
        {
            set;
            get;
        }

        public static float ModelRenderTime
        {
            set;
            get;
        }

        public static float PostProcessRenderTime
        {
            set;
            get;
        }
    }
}
