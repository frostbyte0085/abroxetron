﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Engine.Core
{
    public sealed class ContentService
    {
        Dictionary<string, ThreadSafeContentManager> managers;
        ContentServiceProvider csp;

        internal ContentService()
        {
            csp = new ContentServiceProvider();
            managers = new Dictionary<string, ThreadSafeContentManager>();

            AddContentManager("engine", "EngineContent");
            AddContentManager("shared", "SharedContent");
            AddContentManager("game", "GameContent");
        }

        public void AddContentManager(string name, string contentDirectory)
        {
            if (managers.ContainsKey(name))
                return;

            managers[name] = new ThreadSafeContentManager(csp, contentDirectory);
        }

        public ThreadSafeContentManager this[string name]
        {
            get
            {
                if (managers.ContainsKey(name))
                    return managers[name];
                return null;
            }
        }

        public string[] AvailableContentManagers
        {
            get
            {
                return new List<string>(managers.Keys).ToArray();
            }
        }

        public void Release()
        {
            managers.Clear();
        }
    }
}
