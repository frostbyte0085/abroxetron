﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
namespace Engine.Core
{
    internal class ContentServiceProvider : IServiceProvider
    {
        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(IGraphicsDeviceService))
                return ServiceProvider.GetService<GraphicsService>();
            
            return null;
        }
    }
}
