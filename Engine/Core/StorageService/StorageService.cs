﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using EasyStorage;

namespace Engine.Core
{
    public class StorageService
    {
        #region Variables
        string gameName;
        SharedSaveDevice device;

        #endregion

        internal StorageService(string gameName)
        {
            this.gameName = gameName;

            EasyStorageSettings.SetSupportedLanguages(Language.English);

            device = new SharedSaveDevice();
            device.Initialize();

            device.DeviceSelectorCanceled +=
                (s, e) => e.Response = SaveDeviceEventResponse.Force;
            device.DeviceDisconnected +=
                (s, e) => e.Response = SaveDeviceEventResponse.Force;

            
            device.PromptForDevice();
#if XBOX
        // setup the gamer services here
#endif


        }

        internal void Release(bool disposing)
        {
            
        }

        internal void Update()
        {
            device.Update(null);
        }

        public SharedSaveDevice SaveDevice
        {
            get
            {
                return device;
            }
        }
    }
}
