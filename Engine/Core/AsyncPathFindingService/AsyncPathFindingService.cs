using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Tools;
using Engine.AI;
using Engine.Scene;

using System.Threading;

using System.Diagnostics;

using Microsoft.Xna.Framework;

namespace Engine.Core
{
    public class AsyncPathFindingService
    {
        SceneService ss;

        volatile BlockingQueue<PathFindQuery> queries;
        volatile BlockingQueue<PathFindResult> results;
        
        Thread thread;
        volatile bool shouldStop;

        PathFinder finder;

        internal AsyncPathFindingService()
        {
            ss = ServiceProvider.GetService<SceneService>();
            Debug.Assert(ss != null);

            finder = new PathFinder();
            
            queries = new BlockingQueue<PathFindQuery>();
            results = new BlockingQueue<PathFindResult>();

            thread = new Thread(new ThreadStart(DoWork));
            thread.Name = "PathFinding";
            thread.Start();
            
            //while (!thread.IsAlive);
        }

        #region Threading

        internal BlockingQueue<PathFindQuery> Queries
        {
            get
            {
                return queries;
            }
        }

        internal BlockingQueue<PathFindResult> Results
        {
            get
            {
                return results;
            }
        }

        public void RequestStop()
        {
            shouldStop = true;
            thread.Join();
        }

        public void ForceClear()
        {
            while (!queries.IsEmpty)
                queries.Get();

            while (!results.IsEmpty)
                results.Get();
        }

        private void DoWork()
        {
            while (!shouldStop)
            {                
                PathGrid grid = ss.PathGrid;
                
                if (grid == null || queries.IsEmpty)
                {
                    Thread.Sleep(1);
                    continue;
                }

                PathFindQuery query = queries.Get();

                // pathfind it here
                // convert from world space to grid space
                Point ptStart = grid.ClosestUnblockedGridPoint(query.WorldStart);
                Point ptEnd = grid.ClosestUnblockedGridPoint(query.WorldEnd);

                PathNode foundPath = finder.FindPath(grid, ptStart, ptEnd);

                if (foundPath != null)
                {
                    results.Enqueue(new PathFindResult(query.Name, foundPath.ToWorldList()));
                }
                else
                {
                    // get the query and set the WaitingForPath to false
                    SceneNode noNode = ss[query.Name];
                    if (noNode != null && noNode.Behaviour != null)
                    {
                        Console.WriteLine ("Path not found, resetted WaitingForPathFind");
                        noNode.Behaviour.WaitingForPathFind = false;
                    }
                }
                
            }
        }

        #endregion

    }
}
