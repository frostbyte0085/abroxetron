﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;

using Microsoft.Xna.Framework.Media;

namespace Engine.Audio
{
    public class MediaPlayerService
    {
        private float fadeOutStart, fadeOutDuration;
        private bool fadingOut;
        private Song targetSong;

        internal MediaPlayerService()
        {
            fadingOut = false;
            IsRepeating = true;
        }

        public void TransitionInto(Song target, float fadeOutTime) 
        {
            targetSong = target;
            if (fadingOut) 
                return;

            fadeOutStart = PersistentTiming.TotalTimeElapsed;
            fadeOutDuration = fadeOutTime;
            fadingOut = true;
        }

        private void breakFadeOut()
        {
            fadingOut = false;
            updateActualVolume();
        }

        public void Update() 
        {
            if (fadingOut)
            {
                float now = PersistentTiming.TotalTimeElapsed;
                if (now >= (fadeOutDuration + fadeOutStart))
                    Play(targetSong);
                else
                    updateActualVolume();
            }
        }

        #region MediaPlayer wrapper
        // Summary:
        //     Determines whether the game has control of the background music.
        public bool GameHasControl { get { return MediaPlayer.GameHasControl; } }
        //
        // Summary:
        //     Gets or set the muted setting for the media player.
        public bool IsMuted { get { return MediaPlayer.IsMuted; } set { MediaPlayer.IsMuted = value; } }
        //
        // Summary:
        //     Gets or sets the repeat setting for the media player.
        public bool IsRepeating { get { return MediaPlayer.IsRepeating; } set { MediaPlayer.IsRepeating = value; } }
        //
        // Summary:
        //     Gets or sets the shuffle setting for the media player.
        public bool IsShuffled { get { return MediaPlayer.IsShuffled; } set { MediaPlayer.IsShuffled = value; } }
        //
        // Summary:
        //     Gets or sets the visualization enabled setting for the media player.
        public bool IsVisualizationEnabled
        {
            get { return MediaPlayer.IsVisualizationEnabled; } 
            set { MediaPlayer.IsVisualizationEnabled = value; } 
        }
        //
        // Summary:
        //     Gets the play position within the currently playing song.
        public TimeSpan PlayPosition { get { return MediaPlayer.PlayPosition; } }
        //
        // Summary:
        //     Gets the media playback queue, MediaQueue.
        public MediaQueue Queue { get { return MediaPlayer.Queue; } }
        //
        // Summary:
        //     Gets the media playback state, MediaState.
        public MediaState State { get { return MediaPlayer.State; } }
        //
        // Summary:
        //     Gets or sets the media player volume.
        private float volume = MediaPlayer.Volume;
        public float Volume 
        { 
            get 
            { 
                return volume; 
            }
            set 
            {
                volume = value;
                updateActualVolume();
            }
        }
        private void updateActualVolume()
        {
            if (fadingOut)
            {
                float now = PersistentTiming.TotalTimeElapsed - fadeOutStart;
                float time01 = now / fadeOutDuration;
                MediaPlayer.Volume = (1.0f - time01) * volume;
            }
            else
                MediaPlayer.Volume = volume;
        }

        // Summary:
        //     Retrieves visualization (frequency and sample) data for the currently-playing
        //     song. Reference page contains code sample.
        //
        // Parameters:
        //   visualizationData:
        //     Visualization (frequency and sample) data for the currently playing song.
        public void GetVisualizationData(VisualizationData visualizationData)
        {
            MediaPlayer.GetVisualizationData(visualizationData);
        }
        //
        // Summary:
        //     Moves to the next song in the queue of playing songs.
        public void MoveNext()
        {
            breakFadeOut();
            MediaPlayer.MoveNext();
        }
        //
        // Summary:
        //     Moves to the previous song in the queue of playing songs.
        public void MovePrevious()
        {
            breakFadeOut();
            MediaPlayer.MovePrevious();
        }
        //
        // Summary:
        //     Pauses the currently playing song.
        public void Pause()
        {
            breakFadeOut();
            MediaPlayer.Pause();
        }
        //
        // Summary:
        //     Plays a Song. Reference page contains links to related code samples.
        //
        // Parameters:
        //   song:
        //     Song to play.
        public void Play(Song song)
        {
            breakFadeOut();
            MediaPlayer.Play(song);
        }
        //
        // Summary:
        //     Plays a SongCollection. Reference page contains links to related code samples.
        //
        // Parameters:
        //   songs:
        //     SongCollection to play.
        public void Play(SongCollection songs)
        {
            breakFadeOut();
            MediaPlayer.Play(songs);
        }
        //
        // Summary:
        //     Plays a SongCollection, starting with the Song at the specified index. Reference
        //     page contains links to related code samples.
        //
        // Parameters:
        //   songs:
        //     SongCollection to play.
        //
        //   index:
        //     Index of the song in the collection at which playback should begin.
        public void Play(SongCollection songs, int index)
        {
            breakFadeOut();
            MediaPlayer.Play(songs, index);
        }
        //
        // Summary:
        //     Resumes a paused song.
        public void Resume()
        {
            breakFadeOut();
            MediaPlayer.Resume();
        }
        //
        // Summary:
        //     Stops playing a song.
        public void Stop()
        {
            breakFadeOut();
            MediaPlayer.Stop();
        }
        #endregion
    }
}
