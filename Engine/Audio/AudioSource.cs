﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Core;
using Engine.Tools;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Engine.Audio
{
    public sealed class AudioSource
    {
        #region Variables
        SoundEffect sound;
        SoundEffectInstance soundInstance;
        AudioEmitter emitter;

        #endregion

        #region Methods
        public AudioSource(SoundEffect sound)
        {
            this.sound = sound;
            Volume = 1.0f;
            Pitch = 0.0f;
            Pan = 0.0f;
            Loop = false;
        }

        public static AudioSource RandomFromSoundNames(string[] soundNames)
        {
            return RandomFromSoundNames(soundNames, false);
        }

        public static AudioSource RandomFromSoundNames (string[] soundNames, bool autoPlay)
        {
            if (soundNames.Length == 0)
                return null;

            ContentService cs = ServiceProvider.GetService<ContentService>();

            Random rnd = new Random();
            int index = rnd.Next(soundNames.Length);

            SoundEffect sfx = cs["game"].Load<SoundEffect>(soundNames[index]);
            if (sfx == null)
                return null;

            AudioSource source = new AudioSource(sfx);
            if (autoPlay)
                source.Play();
            return source;
        }

        public bool Loop
        {
            get;
            set;
        }

        public float Volume
        {
            get;
            set;
        }

        public float Pan
        {
            get;
            set;
        }

        public float Pitch
        {
            get;
            set;
        }

        bool positionalSource;
        public bool PositionalSource
        {
            get
            {
                return positionalSource;
            }

            set
            {
                positionalSource = value;
                if (positionalSource)
                {
                    emitter = new AudioEmitter();
                }
                else
                {
                    emitter = null;
                }
            }
        }

        bool isPaused;
        public bool Paused
        {
            set
            {
                if (soundInstance != null)
                {
                    if (isPaused != value)
                    {
                        isPaused = value;

                        if (isPaused)
                            soundInstance.Pause();
                        else
                            soundInstance.Resume();
                    }
                }
            }

            get
            {
                return isPaused;
            }
        }

        bool firstPlay;
        public void Play()
        {
            if (soundInstance == null)
            {
                soundInstance = sound.CreateInstance();
                firstPlay = true;
            }
        }

        public void Stop()
        {
            if (soundInstance != null)
            {
                isPaused = false;
                soundInstance.Stop();
            }
        }

        private void Apply3DSound(AudioListener listener, Vector3 emitterPosition, Vector3 emitterForward, Vector3 emitterUp)
        {
            if (PositionalSource)
            {
                emitter.Position = emitterPosition;
                emitter.Forward = emitterForward;
                emitter.Up = emitterUp;

                soundInstance.Apply3D(listener, emitter);
            }
        }

        internal void Update(AudioListener listener, Vector3 emitterPosition, Vector3 emitterForward, Vector3 emitterUp)
        {
            if (soundInstance != null)
            {
                if (firstPlay)
                {
                    Apply3DSound(listener, emitterPosition, emitterForward, emitterUp);
                    soundInstance.Play();

                    firstPlay = false;
                }

                if (soundInstance.State != SoundState.Stopped)
                {
                    if (!PositionalSource)
                        soundInstance.Pan = Pan;

                    soundInstance.Volume = Volume;
                    soundInstance.Pitch = Pitch;

                    Apply3DSound(listener, emitterPosition, emitterForward, emitterUp);
                }
                else
                {
                    // the sound instance has stopped playing, dispose it
                    isPaused = false;

                    soundInstance.Dispose();
                    soundInstance = null;
                }
            }
        }
        #endregion

    }
}
