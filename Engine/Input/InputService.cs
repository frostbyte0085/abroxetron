﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Engine.Input
{
    public sealed class InputService
    {
        private InputDevice[] devices;
        private KinectDevice kinect;

        public InputService()
        {
#if TEST_KEY_INPUT
            devices = new InputDevice[5];
            devices[0] = new InputDevice(PlayerIndex.One);
            devices[1] = new InputDevice(PlayerIndex.Two);
            devices[2] = new InputDevice(PlayerIndex.Three);
            devices[3] = new InputDevice(PlayerIndex.Four);
            devices[4] = new TestInputDevice();
#else
            devices = new InputDevice[4];
            devices[0] = new InputDevice(PlayerIndex.One);
            devices[1] = new InputDevice(PlayerIndex.Two);
            devices[2] = new InputDevice(PlayerIndex.Three);
            devices[3] = new InputDevice(PlayerIndex.Four);
#endif
            kinect = new KinectDevice();
        }

        public InputDevice GetDevice(int id)
        {
#if !TEST_KEY_INPUT
            if (id == 4) return devices[0];
#endif
            return devices[id];
        }

        public void Clear()
        {
            foreach (InputDevice d in this.devices)
                d.ClearAll();

            kinect.ClearAll();
        }

        public InputDevice GetDevice(PlayerIndex id)
        {
            return devices[(int)id];
        }

        public KinectDevice GetKinect()
        {
            return kinect;
        }

        public void Update()
        {
            foreach (InputDevice dev in devices)
                dev.Update();
            kinect.Update();
        }


        public static string ButtonToString(InputButton button)
        {
            switch (button)
            {
                case InputButton.A:
                    return "Buttons.A";

                case InputButton.B:
                    return "Buttons.B";

                case InputButton.X:
                    return "Buttons.X";

                case InputButton.Y:
                    return "Buttons.Y";

                case InputButton.D_Up:
                    return "DPad.Up";

                case InputButton.D_Down:
                    return "DPad.Down";

                case InputButton.D_Left:
                    return "DPad.Left";

                case InputButton.D_Right:
                    return "DPad.Right";

                case InputButton.LB:
                    return "Buttons.LeftShoulder";

                case InputButton.RB:
                    return "Buttons.RightShoulder";

                case InputButton.LT:
                    return "Triggers.Left";

                case InputButton.RT:
                    return "Triggers.Right";

                case InputButton.LS:
                    return "Buttons.LeftStick";

                case InputButton.RS:
                    return "Buttons.RightStick";

                case InputButton.LS_X:
                    return "ThumbSticks.Left.X";

                case InputButton.LS_Y:
                    return "ThumbSticks.Left.Y";

                case InputButton.RS_X:
                    return "ThumbSticks.Right.X";

                case InputButton.RS_Y:
                    return "ThumbSticks.Right.Y";

                case InputButton.Start:
                    return "Buttons.Start";

                case InputButton.Back:
                    return "Buttons.Back";

                case InputButton.BigButton:
                    return "Buttons.BigButton";

                default:
                    return "not a button";
            }
        }
    }
}