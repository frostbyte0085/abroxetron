﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Input
{
    public enum InputButton
    {
        // Buttons
        A,
        B,
        X,
        Y,

        // D-pad
        D_Up,
        D_Down,
        D_Left,
        D_Right,

        // Bumpers
        LB,
        RB,

        // Triggers
        LT,
        RT,

        // Thumbsticks
        LS, //(click)
        RS, //(click)
        LS_X,
        LS_Y,
        LS_Up,
        LS_Down,
        LS_Left,
        LS_Right,
        RS_X,
        RS_Y,
        RS_Up,
        RS_Down,
        RS_Left,
        RS_Right,

        // Buttons in the middle:
        Start,
        Back,
        BigButton
    };

    // Used by UISelector
    public enum InputAxis
    {
        LS_X,
        LS_Y,
        RS_X,
        RS_Y,
        D_X,
        D_Y,
        Bumpers,
        Triggers
    };
}
