﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Engine.Input
{
#if TEST_KEY_INPUT
    public class TestInputDevice : InputDevice
    {
        internal TestInputDevice()
            : base(PlayerIndex.One)
        {
        }

        override public void AddAction(string key, InputButton whichButton)
        {
            TestInputAction action = new TestInputAction(whichButton);
            this.hash.Add(key, action);
            return;
        }
    }
#endif
}

