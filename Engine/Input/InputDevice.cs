﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Engine.Input
{
#if TEST_KEY_INPUT
    public class InputDevice
#else
    public sealed class InputDevice
#endif
    {
        #region Instance variables
        private PlayerIndex playerNum;
        /// <summary>
        /// What is the player's number? This has nothing to
        /// do with the gamepad number.
        /// </summary>
        private PlayerIndex PlayerNum
        {
            set { playerNum = value; }
            get { return playerNum; }
        }

#if TEST_KEY_INPUT
        internal Dictionary<string, InputAction> hash; 
#else
        private Dictionary<string, InputAction> hash; 
#endif
        #endregion

        internal InputDevice(PlayerIndex playerNum)
        {
            this.playerNum = playerNum;
            hash = new Dictionary<string, InputAction>();
            basicMappings();
        }

        #region Hashtable modification methods
#if TEST_KEY_INPUT
        public virtual void AddAction(string key, InputButton whichButton)
#else
        public void AddAction(string key, InputButton whichButton)
#endif
        {
            InputAction action = new InputAction(whichButton);
            hash.Add(key, action);
        }
        public void RemoveAction(string key)
        {
            hash.Remove(key);
        }
        public void basicMappings()
        {
            AddAction("A", InputButton.A);
            AddAction("B", InputButton.B);
            AddAction("X", InputButton.X);
            AddAction("Y", InputButton.Y);

            // D-pad
            AddAction("D_Up", InputButton.D_Up);
            AddAction("D_Down", InputButton.D_Down);
            AddAction("D_Left", InputButton.D_Left);
            AddAction("D_Right", InputButton.D_Right);

            AddAction("LB", InputButton.LB);
            AddAction("RB", InputButton.RB);

            AddAction("LT", InputButton.LT);
            AddAction("RT", InputButton.RT);

            AddAction("LS", InputButton.LS);
            AddAction("RS", InputButton.RS);
            AddAction("LS_Up", InputButton.LS_Up);
            AddAction("LS_Down", InputButton.LS_Down);
            AddAction("LS_Left", InputButton.LS_Left);
            AddAction("LS_Right", InputButton.LS_Right);
            AddAction("RS_Up", InputButton.RS_Up);
            AddAction("RS_Down", InputButton.RS_Down);
            AddAction("RS_Left", InputButton.RS_Left);
            AddAction("RS_Right", InputButton.RS_Right);

            AddAction("Start", InputButton.Start);
            AddAction("Back", InputButton.Back);
            AddAction("BigButton", InputButton.BigButton);
        }
        public void ClearAll()
        {
            hash.Clear();
            basicMappings();
        }
        /// <summary>
        /// Reset all the clicked and pressed values to false:
        /// </summary>
        public void Reset()
        {
            foreach (InputAction action in hash.Values)
                action.Reset();
        }
        internal void Update()
        {
            GamePadState state = GamePad.GetState(playerNum);
            foreach (InputAction action in hash.Values)
            {
                action.Update(state);
            }
        }
        #endregion

        #region Hashtable query methods
        public float GetValue(string key)
        {
            return hash[key].Value;
        }
        public bool IsPressed(string key)
        {
            return hash[key].IsPressed;
        }
        public bool IsClicked(string key)
        {
            bool b = hash[key].IsClicked;
            return b;
        }
        #endregion

    }// end InputDevice class
}// end namespace
