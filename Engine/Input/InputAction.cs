﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Engine.Input
{
#if TEST_KEY_INPUT
    internal class InputAction
    {
        protected InputButton button;
        protected bool isPressed;
        protected bool isClicked;
        protected float value;
#else
    internal sealed class InputAction
    {
        private InputButton button;
        private bool isPressed;
        private bool isClicked;
        private float value;
#endif

        internal InputButton Button { get { return button; } }
        internal bool IsPressed { get { return isPressed; } }
        internal bool IsClicked { get { return isClicked; } }
        internal float Value { get { return value; } }

        internal InputAction(InputButton button)
        {
            this.button = button;

            isPressed = false;
            isClicked = false;
            value = 0.0f;
        }

#if TEST_KEY_INPUT
        internal virtual void Update(GamePadState state)
#else
        internal void Update(GamePadState state)
#endif
        {
            bool wasPressed = isPressed;
            isPressed = IsButtonPressed(state, button);
            isClicked = (wasPressed && !isPressed) ? true : false;
            value = isPressed ? GetValue(state, button) : 0.0f;
        }

        internal void Reset()
        {
            this.isClicked = false;
            this.isPressed = false;
        }

        #region Switch mapping methods
        internal static float GetValue(GamePadState state, InputButton button)
        {
            float a;

            switch (button)
            {
                case InputButton.LT:
                    return state.Triggers.Left;

                case InputButton.RT:
                    return state.Triggers.Right;

                case InputButton.LS_X:
                    return state.ThumbSticks.Left.X;

                case InputButton.LS_Y:
                    return state.ThumbSticks.Left.Y;

                case InputButton.LS_Up:
                    a = state.ThumbSticks.Left.Y;
                    return a < 0.0f ? 0.0f : a;
                case InputButton.LS_Down:
                    a = state.ThumbSticks.Left.Y;
                    return a > 0.0f ? 0.0f : a;
                case InputButton.LS_Left:
                    a = state.ThumbSticks.Left.X;
                    return a > 0.0f ? 0.0f : a;
                case InputButton.LS_Right:
                    a = state.ThumbSticks.Left.X;
                    return a < 0.0f ? 0.0f : a;

                case InputButton.RS_X:
                    return state.ThumbSticks.Right.X;

                case InputButton.RS_Y:
                    return state.ThumbSticks.Right.Y;

                case InputButton.RS_Up:
                    a = state.ThumbSticks.Right.Y;
                    return a < 0.0f ? 0.0f : a;
                case InputButton.RS_Down:
                    a = state.ThumbSticks.Right.Y;
                    return a > 0.0f ? 0.0f : a;
                case InputButton.RS_Left:
                    a = state.ThumbSticks.Right.X;
                    return a > 0.0f ? 0.0f : a;
                case InputButton.RS_Right:
                    a = state.ThumbSticks.Right.X;
                    return a < 0.0f ? 0.0f : a;

                    // TODO LS/RS up,down,left,right

                default:
                    return 1.0f;
            }
        }

        internal static bool IsButtonPressed(GamePadState state, InputButton button)
        {
            switch (button)
            {
                case InputButton.A:
                    return state.Buttons.A == ButtonState.Pressed;

                case InputButton.B:
                    return state.Buttons.B == ButtonState.Pressed;

                case InputButton.X:
                    return state.Buttons.X == ButtonState.Pressed;

                case InputButton.Y:
                    return state.Buttons.Y == ButtonState.Pressed;

                case InputButton.D_Up:
                    return state.DPad.Up == ButtonState.Pressed;

                case InputButton.D_Down:
                    return state.DPad.Down == ButtonState.Pressed;

                case InputButton.D_Left:
                    return state.DPad.Left == ButtonState.Pressed;

                case InputButton.D_Right:
                    return state.DPad.Right == ButtonState.Pressed;

                case InputButton.LB:
                    return state.Buttons.LeftShoulder == ButtonState.Pressed;

                case InputButton.RB:
                    return state.Buttons.RightShoulder == ButtonState.Pressed;

                case InputButton.LT:
                    return state.Triggers.Left != 0.0f;

                case InputButton.RT:
                    return state.Triggers.Right != 0.0f;

                case InputButton.LS:
                    return state.Buttons.LeftStick == ButtonState.Pressed;

                case InputButton.RS:
                    return state.Buttons.RightStick == ButtonState.Pressed;

                case InputButton.LS_X:
                    return state.ThumbSticks.Left.X != 0.0f;

                case InputButton.LS_Y:
                    return state.ThumbSticks.Left.Y != 0.0f;

                case InputButton.LS_Up:
                    return state.ThumbSticks.Left.Y > 0.0f;

                case InputButton.LS_Down:
                    return state.ThumbSticks.Left.Y < 0.0f;

                case InputButton.LS_Left:
                    return state.ThumbSticks.Left.X < 0.0f;

                case InputButton.LS_Right:
                    return state.ThumbSticks.Left.X > 0.0f;

                case InputButton.RS_X:
                    return state.ThumbSticks.Right.X != 0.0f;

                case InputButton.RS_Y:
                    return state.ThumbSticks.Right.Y != 0.0f;

                case InputButton.RS_Up:
                    return state.ThumbSticks.Right.Y > 0.0f;

                case InputButton.RS_Down:
                    return state.ThumbSticks.Right.Y < 0.0f;

                case InputButton.RS_Left:
                    return state.ThumbSticks.Right.X < 0.0f;

                case InputButton.RS_Right:
                    return state.ThumbSticks.Right.X > 0.0f;

                case InputButton.Start:
                    return state.Buttons.Start == ButtonState.Pressed;

                case InputButton.Back:
                    return state.Buttons.Back == ButtonState.Pressed;

                case InputButton.BigButton:
                    return state.Buttons.BigButton == ButtonState.Pressed;

                default:
                    throw new Exception("Enum error");
            }
        }
        #endregion
    }

#if TEST_KEY_INPUT
    internal class TestInputAction : InputAction
    {
        internal TestInputAction(InputButton button) 
            : base(button)
        {
            this.button = button;

            isPressed = false;
            isClicked = false;
            value = 0.0f;
        }

        override internal void Update(GamePadState gps)
        {
            KeyboardState state = Keyboard.GetState();
            bool wasPressed = isPressed;
            isPressed = IsButtonPressed(state, button);
            isClicked = (wasPressed && !isPressed) ? true : false;
            value = isPressed ? GetValue(state, button) : 0.0f;
        }

        #region Switch mapping methods
        internal static float GetValue(KeyboardState state, InputButton button)
        {
            float sum = 0.0f;
            switch (button)
            {

                case InputButton.LS_X:
                    sum += (state.IsKeyDown(Keys.D)) ? 1.0f : 0.0f;
                    sum += (state.IsKeyDown(Keys.A)) ? -1.0f : 0.0f;
                    return sum;

                case InputButton.LS_Y:
                    sum += (state.IsKeyDown(Keys.W)) ? 1.0f : 0.0f;
                    sum += (state.IsKeyDown(Keys.S)) ? -1.0f : 0.0f;
                    return sum;

                case InputButton.RS_X:
                    sum += (state.IsKeyDown(Keys.Right)) ? 1.0f : 0.0f;
                    sum += (state.IsKeyDown(Keys.Left)) ? -1.0f : 0.0f;
                    return sum;

                case InputButton.RS_Y:
                    sum += (state.IsKeyDown(Keys.Up)) ? 1.0f : 0.0f;
                    sum += (state.IsKeyDown(Keys.Down)) ? -1.0f : 0.0f;
                    return sum;

                default:
                    return 1.0f;
            }
        }

        internal static bool IsButtonPressed(KeyboardState state, InputButton button)
        {
            switch (button)
            {
                case InputButton.A:
                    return state.IsKeyDown(Keys.K);

                case InputButton.B:
                    return state.IsKeyDown(Keys.I);

                case InputButton.X:
                    return state.IsKeyDown(Keys.J);

                case InputButton.Y:
                    return state.IsKeyDown(Keys.U);


                case InputButton.D_Up:
                    return state.IsKeyDown(Keys.F1);

                case InputButton.D_Down:
                    return state.IsKeyDown(Keys.F3);

                case InputButton.D_Left:
                    return state.IsKeyDown(Keys.F4);

                case InputButton.D_Right:
                    return state.IsKeyDown(Keys.F2);


                case InputButton.LB:
                    return state.IsKeyDown(Keys.D1);

                case InputButton.RB:
                    return state.IsKeyDown(Keys.D2);


                case InputButton.LT:
                    return state.IsKeyDown(Keys.D3);

                case InputButton.RT:
                    return state.IsKeyDown(Keys.D4);


                case InputButton.LS:
                    return state.IsKeyDown(Keys.Q);

                case InputButton.RS:
                    return state.IsKeyDown(Keys.E);


                case InputButton.LS_X:
                    return state.IsKeyDown(Keys.A) || state.IsKeyDown(Keys.D);

                case InputButton.LS_Y:
                    return state.IsKeyDown(Keys.W) || state.IsKeyDown(Keys.S);

                case InputButton.LS_Up:
                    return state.IsKeyDown(Keys.W);

                case InputButton.LS_Down:
                    return state.IsKeyDown(Keys.S);

                case InputButton.LS_Left:
                    return state.IsKeyDown(Keys.A);

                case InputButton.LS_Right:
                    return state.IsKeyDown(Keys.D);


                case InputButton.RS_X:
                    return state.IsKeyDown(Keys.Left) || state.IsKeyDown(Keys.Right);

                case InputButton.RS_Y:
                    return state.IsKeyDown(Keys.Up) || state.IsKeyDown(Keys.Down);

                case InputButton.RS_Up:
                    return state.IsKeyDown(Keys.Up);

                case InputButton.RS_Down:
                    return state.IsKeyDown(Keys.Down);

                case InputButton.RS_Left:
                    return state.IsKeyDown(Keys.Left);

                case InputButton.RS_Right:
                    return state.IsKeyDown(Keys.Right);


                case InputButton.Start:
                    return state.IsKeyDown(Keys.Enter);

                case InputButton.Back:
                    return state.IsKeyDown(Keys.Escape);

                case InputButton.BigButton:
                    return state.IsKeyDown(Keys.Home);

                default:
                    throw new Exception("Enum error");
            }
        }
        #endregion
    }
#endif
}