﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Kinect;

namespace Engine.Input
{
    /// <summary>
    /// A gesture is a combination of movements that triggers an action.
    /// It is somewhat the equivalent of an "InputButton" for the Kinect.
    /// </summary>
    public class KinectGesture
    {
        private JointMovement[] movements;
        public JointMovement[] Movements
        {
            set
            {
                if (value == null) throw new ArgumentNullException("must have movements");
                movements = value;
            }
            get { return movements; }
        }

        public KinectGesture(JointMovement[] movements)
        {
            this.movements = movements;
        }
    }
}
