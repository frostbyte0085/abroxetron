﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Tools;

using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Input
{
    /// <summary>
    /// Real-time movement detection
    /// </summary>
    public class MovementDetection
    {
        #region Read-onlys
        public static readonly float TriggerSpeed = 6f;
        public static readonly float UntriggerSpeed = 2f;
        public static readonly float TriggerDistance = 0.2f;
        #endregion

        #region Properties
        public JointType JointType { set; get; }
        public SkeletonPoint StartingPoint { set; get; }
        public SkeletonPoint Position { set; get; }
        public Vector2 Velocity { set; get; }
        #endregion

        private bool recording = false;

        #region Speed testing
        /// <summary>
        /// Returns true if this joint is moving fast enough to
        /// enter recording mode.
        /// </summary>
        /// <returns>Fast enough on the X axis.</returns>
        private bool FastEnoughX()
        {
            float x = Velocity.X;
            return !(-TriggerSpeed <= x && x <= TriggerSpeed);
        }
        /// <summary>
        /// Returns true if this joint is moving fast enough to
        /// enter recording mode.
        /// </summary>
        /// <returns>Fast enough on the Y axis.</returns>
        private bool FastEnoughY()
        {
            float y = Velocity.Y;
            return !(-TriggerSpeed <= y && y <= TriggerSpeed);
        }
        /// <summary>
        /// Returns true if this joint is moving fast enough to
        /// enter recording mode.
        /// </summary>
        /// <returns>Fast enough on the X OR Y axis.</returns>
        private bool FastEnough()
        {
            return FastEnoughX() || FastEnoughY();
        }

        /// <summary>
        /// Returns true if this joint is moving slow enough on the X axis
        /// to exit recording mode.
        /// </summary>
        /// <returns>Slow enough on the X axis.</returns>
        private bool SlowEnoughX()
        {
            float x = Velocity.X;
            return (-UntriggerSpeed <= x && x <= UntriggerSpeed);
        }
        /// <summary>
        /// Returns true if this joint is moving slow enough on the Y axis
        /// to exit recording mode.
        /// </summary>
        /// <returns>Slow enough on the Y axis.</returns>
        private bool SlowEnoughY()
        {
            float y = Velocity.Y;
            return (-UntriggerSpeed <= y && y <= UntriggerSpeed);
        }
        /// <summary>
        /// Returns true if this joint is moving slow enough on both axes
        /// to exit recording mode.
        /// </summary>
        /// <returns>Slow enough on the X AND Y axis.</returns>
        private bool SlowEnough()
        {
            return SlowEnoughX() && SlowEnoughY();
        }
        #endregion

        #region Updating
        /// <summary>
        /// Updates this MovementDetection.
        /// </summary>
        /// <returns>True if this MovementDetection is exiting a 'fast movement' mode.
        /// (That is, the previous frame FastEnough() == true. In the newer frame
        /// FastEnough() == false.</returns>
        public bool Update(Joint newJoint)
        {
            // Calculate new velocity:
            float dt = GameTiming.TimeElapsed;
            Vector3 vel = Math2.ToVector3(newJoint.Position) - Math2.ToVector3(this.Position);
            vel /= dt;
            this.Velocity = new Vector2(vel.X, vel.Y);

            // Update position
            this.Position = newJoint.Position;

            if (recording)
                return UpdateRecording(newJoint);
            else
                return UpdateNotRecording(newJoint);
        }

        private bool UpdateRecording(Joint newJoint)
        {
            // Is this joint slow enough to exit 'recording' mode?
            bool slow = SlowEnough();
            recording = !slow;
            return slow;
        }

        private bool UpdateNotRecording(Joint newJoint)
        {
            // Is this joint moving fast enough to enter 'recording' mode?
            if (FastEnough())
            {
                recording = true;
                this.StartingPoint = newJoint.Position;
            }
            return false;
        }
        #endregion
    }

    /// <summary>
    /// The primary use of this class is to detect specified gestures.
    /// This class is allow used to access the Kinect directly.
    /// </summary>
    public sealed class KinectDevice
    {
        #region Instance variables
        /// <summary>
        /// Maximum interval (in secs) allowed between two BodyMovements.
        /// </summary>
        public static readonly float BodyMovementInterval = 0.7f;

        private Dictionary<string, KinectAction> hash;

        /// <summary>
        /// A list of the currently tracked body parts. 1 element per joint.
        /// </summary>
        private List<MovementDetection> realtimeMovements;

        /// <summary>
        /// A list of the currently detected body movements. This is used to
        /// construct a KinectGesture instance. The list is cleared if the
        /// device doesn't detect anything for too long.
        /// </summary>
        private List<JointMovement> detectedMovements;
        internal List<JointMovement> DetectedMovements { get { return detectedMovements; } }

        /// <summary>
        /// The last time a valid detection was caught.
        /// </summary>
        private float lastValidDetectionTime = -9999;
        #endregion

        internal KinectDevice()
        {
            hash = new Dictionary<string, KinectAction>();
            detectedMovements = new List<JointMovement>();
            realtimeMovements = new List<MovementDetection>();
        }

        #region Hashtable modification methods
        public void AddAction(string key, KinectGesture whichGesture)
        {
            KinectAction action = new KinectAction(whichGesture);
            hash.Add(key, action);
            InitMovementDetection(whichGesture);
        }
        /// <summary>
        /// Invoked when a body part which wasn't tracked gets added to the
        /// dictionary.
        /// </summary>
        /// <param name="whichGesture"></param>
        private void InitMovementDetection(KinectGesture whichGesture)
        {
            foreach (MovementDetection movement in realtimeMovements)
            {
                // TODO consider several joints in Movements
                if (whichGesture.Movements[0].JointType == movement.JointType)
                    return;
            }

            MovementDetection newMovement = new MovementDetection();
            newMovement.JointType = whichGesture.Movements[0].JointType;
            realtimeMovements.Add(newMovement);
        }
        public void RemoveAction(string key)
        {
            hash.Remove(key);
        }
        public void ClearAll()
        {
            hash.Clear();
            detectedMovements.Clear();
            realtimeMovements.Clear();
        }
        #endregion

        #region Gesture dectection
        internal void Update()
        {
            UpdateMovementDetection();
            UpdateKinectActions();
        }

        private void UpdateMovementDetection()
        {
            Skeleton playerSkel = PlayerSkeleton;
            if (playerSkel == null) return;

            foreach (MovementDetection move in realtimeMovements)
            {
                Joint myJoint = playerSkel.Joints[move.JointType];

                // The joint update is event driven... therefore it's possible that on some frames
                // the joints aren't updated. We cannot make any assumptions on how long it takes
                // to update. However we can assume that each joint moves at least 'a little bit'
                // every iteration.
                if (myJoint.Position == move.Position)
                    continue; // this joint hasn't been updated yet.

                if (move.Update(myJoint))
                {
                    // The Movement detection has stopped. Try to see if this results in a gesture.
                    analyseMovement(move);
                }
            }
        }

        private void analyseMovement(MovementDetection move)
        {
            Vector2 distance2 = Math2.ToVector2(move.Position) - Math2.ToVector2(move.StartingPoint);
            float D = MovementDetection.TriggerDistance;
            if (distance2.LengthSquared() < D * D) 
                return; // Distance is too short

            distance2.Normalize();
            detectedMovements.Add(new JointMovement()
            {
                JointType = move.JointType,
                Direction = distance2
            });

            lastValidDetectionTime = GameTiming.TotalTimeElapsed;
        }

        private void UpdateKinectActions()
        {
            JointMovement[] movements = this.detectedMovements.ToArray();

            foreach (KinectAction action in hash.Values)
            {
                action.Update(movements);
                if (action.IsTriggered)
                {
                    detectedMovements.Clear();
                    break;
                }
            }

            if (lastValidDetectionTime + BodyMovementInterval <= GameTiming.TotalTimeElapsed)
            {
                detectedMovements.Clear();
            }
        }

        // todo remove this:
        public List<MovementDetection> GetMovementDetection()
        {
            return realtimeMovements;
        }
        #endregion

        #region Query methods
        public bool IsTriggered(string key)
        {
            return hash[key].IsTriggered;
        }

        public KinectSensor KinectSensor
        {
            get
            {
                return KinectHandler.KinectSensor;
            }
        }

        public Texture2D RGBVideoStream
        {
            get
            {
                return KinectHandler.RGBVideoStream;
            }
        }

        public Texture2D DepthVideoStream
        {
            get
            {
                return KinectHandler.DepthVideoStream;
            }
        }

        public Skeleton[] Skeletons
        {
            get
            {
                return KinectHandler.Skeletons;
            }
        }

        public Skeleton PlayerSkeleton
        {
            get
            {
                return (from s in KinectHandler.Skeletons
                        where s.TrackingState == SkeletonTrackingState.Tracked
                        select s)
                        .FirstOrDefault();
            }
        }

        public string StatusAsString()
        {
            if (KinectHandler.KinectSensor == null)
                return "No kinect found (null)";

            else return KinectHandler.KinectSensor.Status.ToString();
        }

        public static Point GetScreenPoint(Joint joint, Point screenRes)
        {
            return GetScreenPoint(joint, screenRes.X, screenRes.Y);
        }
        public static Point GetScreenPoint(Joint joint, int screenWidth, int screenHeight)
        {
            Rectangle viewport = new Rectangle(0, 0, screenWidth, screenHeight);
            return GetScreenPoint(joint.Position, viewport);
        }
        public static Point GetScreenPoint(Joint joint, Rectangle viewport)
        {
            return GetScreenPoint(joint.Position, viewport);
        }

        public static Point GetScreenPoint(SkeletonPoint joint, Point screenRes)
        {
            return GetScreenPoint(joint, screenRes.X, screenRes.Y);
        }
        public static Point GetScreenPoint(SkeletonPoint joint, int screenWidth, int screenHeight)
        {
            Rectangle viewport = new Rectangle(0, 0, screenWidth, screenHeight);
            return GetScreenPoint(joint, viewport);
        }
        public static Point GetScreenPoint(SkeletonPoint pos, Rectangle viewport)
        {
            ColorImageFormat format = KinectHandler.COLOR_IMAGE_FORMAT;
            ColorImagePoint colorPoint = KinectHandler.KinectSensor.MapSkeletonPointToColor(pos, format);
            Point cameraRes = GetRGBStreamResolution();

            float xf = colorPoint.X;
            float yf = colorPoint.Y;

            xf = (xf / cameraRes.X) * viewport.Width + viewport.X;
            yf = (yf / cameraRes.Y) * viewport.Height + viewport.Y;

            int x = (int)(xf + 0.5f);
            int y = (int)(yf + 0.5f);

            return new Point(x, y);
        }

        public static Point GetRGBStreamResolution(ColorImageFormat format)
        {
            switch (format)
            {
                case ColorImageFormat.RgbResolution1280x960Fps12:
                    return new Point(1280, 960);
                default:
                    return new Point(640, 480);
            }
        }

        public static Point GetRGBStreamResolution()
        {
            return GetRGBStreamResolution(KinectHandler.COLOR_IMAGE_FORMAT);
        }
        #endregion

    }
}