﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Diagnostics;

using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Input
{
    /// <summary>
    /// Code inspired by Microsoft's Sample:
    /// http://digitalerr0r.wordpress.com/2011/12/13/kinect-fundamentals-4-implementing-skeletal-tracking/
    /// 
    /// This class handles events triggered by the Kinect
    /// </summary>
    internal static class KinectHandler
    {
        public const ColorImageFormat COLOR_IMAGE_FORMAT = ColorImageFormat.RgbResolution640x480Fps30;
        public const DepthImageFormat DEPTH_IMAGE_FORMAT = DepthImageFormat.Resolution320x240Fps30;

        private static GraphicsDevice graphics;

        #region Properties
        private static KinectSensor kinectSensor;
        internal static KinectSensor KinectSensor { get { return kinectSensor; } }

        private static Skeleton[] skeletons;
        internal static Skeleton[] Skeletons { 
            get
            {
                //return kinectSensor == null ? null : skeletons;
                return skeletons;
            }
        }

        private static Texture2D rgbVideoStream;
        internal static Texture2D RGBVideoStream
        {
            get
            {
                return kinectSensor == null ? null : rgbVideoStream;
            }
        }
        internal static ColorImageFrame ColorImageFrame
        {
            get { return colorImageFrame; }
        }

        private static Texture2D depthVideoStream;
        internal static Texture2D DepthVideoStream
        {
            get
            {
                return kinectSensor == null ? null : depthVideoStream;
            }
        }
        internal static DepthImageFrame DepthImageFrame
        {
            get { return depthImageFrame; }
        }
        #endregion

        #region Init / Dispose
        /// <summary>
        /// Initialize the kinect handler
        /// </summary>
        /// <param name="graphics">Graphics device. Needed to render the video 
        /// stream onto a Texture2D.</param>
        internal static void InitializeHandler(GraphicsDevice graphics)
        {
            rgbVideoStream = new Texture2D(graphics, 1, 1);
            depthVideoStream = new Texture2D(graphics, 1, 1);

            KinectSensor.KinectSensors.StatusChanged += new EventHandler<StatusChangedEventArgs>(handle_StatusChanged);
            DiscoverKinectSensor();

            KinectHandler.graphics = graphics;
            skeletons = new Skeleton[0];
        }

        internal static void Dispose()
        {
            if (kinectSensor != null)
            {
                kinectSensor.Stop();
                kinectSensor.Dispose();
            }
        }
        #endregion

        #region Handler methods
        internal static void handle_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            if (kinectSensor == e.Sensor)
            {
                if (e.Status == KinectStatus.Disconnected ||
                    e.Status == KinectStatus.NotPowered)
                {
                    kinectSensor = null;
                    DiscoverKinectSensor();
                }
            }
        }

        internal static void handle_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    if(skeletons.Length != skeletonFrame.SkeletonArrayLength) 
                        skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];

                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }
        }

        private static byte[] pixelsFromFrame = new byte[0];
        private static Color[] colors = new Color[0];
        private static ColorImageFrame colorImageFrame;
        internal static void handle_ColorImageFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (colorImageFrame = e.OpenColorImageFrame())
            {
                if (colorImageFrame != null)
                {
                    // Get Pixels
                    if (pixelsFromFrame.Length != colorImageFrame.PixelDataLength)
                        pixelsFromFrame = new byte[colorImageFrame.PixelDataLength];
                    colorImageFrame.CopyPixelDataTo(pixelsFromFrame);

                    // Init Color array
                    int colorLength = colorImageFrame.Height * colorImageFrame.Width;
                    if (colors.Length != colorLength)
                        colors = new Color[colorLength];

                    // Init Texture:
                    if (rgbVideoStream.Width != colorImageFrame.Width ||
                        rgbVideoStream.Height != colorImageFrame.Height)
                        rgbVideoStream = new Texture2D(graphics, colorImageFrame.Width, colorImageFrame.Height);

                    // Go through each pixel and set the bytes correctly
                    // Remember, each pixel got a Red, Green and Blue
                    int index = 0;
                    for (int y = 0; y < colorImageFrame.Height; y++)
                    {
                        for (int x = 0; x < colorImageFrame.Width; x++, index += 4)
                        {
                            colors[y * colorImageFrame.Width + x] = new Color(pixelsFromFrame[index + 2], pixelsFromFrame[index + 1], pixelsFromFrame[index + 0]);
                        }
                    }

                    // Set pixeldata from the ColorImageFrame to a Texture2D
                    rgbVideoStream.SetData(colors);
                }
            }
        }

        private static short[] depthPixels = new short[0];
        private static Color[] depths = new Color[0];
        private static DepthImageFrame depthImageFrame;
        internal static void handle_DepthImageFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            using (depthImageFrame = e.OpenDepthImageFrame())
            {
                if (depthImageFrame != null)
                {
                    // Get Pixels
                    if (depthPixels.Length != depthImageFrame.PixelDataLength)
                        depthPixels = new short[depthImageFrame.PixelDataLength];
                    depthImageFrame.CopyPixelDataTo(depthPixels);

                    // Init Color array
                    int depthLength = depthImageFrame.Height * depthImageFrame.Width;
                    if (depths.Length != depthLength)
                        depths = new Color[depthLength];

                    // Init Texture:
                    if (depthVideoStream.Width != depthImageFrame.Width ||
                        depthVideoStream.Height != depthImageFrame.Height)
                        depthVideoStream = new Texture2D(graphics, depthImageFrame.Width, depthImageFrame.Height);

                    // Make grayscale texture:
                    for (int i=0; i<depths.Length; i++) {
                        short myDepth = depthPixels[i];
                        //myDepth /= short.MaxValue;
                        short detail1 = (short) (myDepth % 255);
                        short detail2 = (short)(myDepth % (255*255));
                        depths[i] = new Color(myDepth, detail1, detail2);
                    }

                    // Set pixeldata from the DepthImageFrame to a Texture2D
                    depthVideoStream.SetData(depths);
                }
            }
        }
        #endregion

        #region Kinect Connection & Initialization
        private static void DiscoverKinectSensor()
        {
            foreach (KinectSensor sensor in KinectSensor.KinectSensors)
            {
                if (sensor.Status == KinectStatus.Connected)
                {
                    // Found one, set our sensor to this
                    kinectSensor = sensor;
                    break;
                }
            }

            if (kinectSensor == null)
            {
                return;
            }

            // Init the found and connected device
            if (kinectSensor.Status == KinectStatus.Connected)
            {
                InitializeKinect();
            }
        }

        private static bool InitializeKinect()
        {
            // Color streams
            /*
            kinectSensor.ColorStream.Enable(COLOR_IMAGE_FORMAT);
            kinectSensor.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(handle_ColorImageFrameReady);
            // Depth stream
            kinectSensor.DepthStream.Enable(DEPTH_IMAGE_FORMAT);
            kinectSensor.DepthFrameReady += new EventHandler<DepthImageFrameReadyEventArgs>(handle_DepthImageFrameReady);
             */
            // Skeleton Stream
            kinectSensor.SkeletonStream.Enable(new TransformSmoothParameters()
            {
                Smoothing = 0.5f,
                Correction = 0.5f,
                Prediction = 0.5f,
                JitterRadius = 0.05f,
                MaxDeviationRadius = 0.04f
            });
            kinectSensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(handle_SkeletonFrameReady);

            try
            {
                kinectSensor.Start();
            }
            catch
            {
                ConsoleLogService cls = ServiceProvider.GetService<ConsoleLogService>();
                cls.WriteCritical("could not start kinect");
                return false;
            }
            return true;
        }
        #endregion
    }
}
