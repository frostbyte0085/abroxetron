﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Kinect;

namespace Engine.Input
{
    public struct JointMovement
    {
        public static readonly double AngleErrorTolerance = MathHelper.ToRadians(30);

        /// <summary>
        /// Normalized direction
        /// </summary>
        public Vector2 Direction { set; get; }

        /// <summary>
        /// A body part to track
        /// </summary>
        public JointType JointType { set; get; }

        /// <summary>
        /// Compares two JointMovements to check if they have the same Joint and
        /// same Direction. Symmetic binary operator. 
        /// </summary>
        /// <param name="b1">One movement.</param>
        /// <param name="b2">Another movement.</param>
        /// <returns>True if b1.Joint == b2.Joint and the angles of b1 and b2 differ
        /// at most by JointMovement.AngleErrorTolerance in a clockwise or counter-clockwise
        /// direction</returns>
        public static bool operator ==(JointMovement b1, JointMovement b2)
        {
            // Same body part?
            if (b1.JointType != b2.JointType) return false;

            // Get the angles of both vectors in a [-pi,pi] interval:
            double a1 = Math.Atan2(b1.Direction.Y, b1.Direction.X);
            double a2 = Math.Atan2(b2.Direction.Y, b2.Direction.X);
            
            // Bring the angles into [0, 2pi] interval:
            a1 += Math.PI; a2 += Math.PI;

            // Get the minimum difference (either clockwise or counter-clockwise)
            double diff = Math.Abs(a1 - a2); // == | a2 - a1 |
            diff = Math.Min(Math.PI * 2 - diff, diff);

            return diff <= AngleErrorTolerance; // Same angle (almost)?
        }
        public static bool operator !=(JointMovement b1, JointMovement b2)
        {
            return !(b1 == b2);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static readonly Vector2 North = new Vector2(0, 1);
        //public static readonly Vector2 NorthEast = new Vector2((float)Math.Sqrt(2), (float)Math.Sqrt(2));
        public static readonly Vector2 East = new Vector2(1, 0);
        //public static readonly Vector2 SouthEast = new Vector2((float)Math.Sqrt(2), -(float)Math.Sqrt(2));
        public static readonly Vector2 South = new Vector2(0, -1);
        //public static readonly Vector2 SouthWest = new Vector2(-(float)Math.Sqrt(2), -(float)Math.Sqrt(2));
        public static readonly Vector2 West = new Vector2(-1, 0);
        //public static readonly Vector2 NorthWest = new Vector2((float)Math.Sqrt(2), -(float)Math.Sqrt(2));
    }
}
