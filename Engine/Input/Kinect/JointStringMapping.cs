﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Kinect;

namespace Engine.Input
{
    /// <summary>
    /// This class maps kinects joint to character joint names.
    /// You can use null if the character doesn't have a kinect joint.
    /// 
    /// This class is obtimized to obtain a string based on the a JointType.
    /// Doing this opposite is possible, but slow, since it must traverse an array.
    /// 
    /// This class hard-coded based on the JointType enum.
    /// Not great... but not a big deal since we're not changing JointType.
    /// And it's faster.
    /// </summary>
    public class JointStringMapping
    {
        private string[] boneNames;

        private Vector3[] normal;

        public JointStringMapping()
        {
            // use 20... because that's how many joints the kinect detects.
            boneNames = new string[20];
            normal = new Vector3[20];

            for (int i = 0; i < normal.Length; i++)
                normal[i] = Vector3.Up;
        }

        /// <summary>
        /// Get the bone name of this joint. Casts to int.
        /// </summary>
        /// <param name="joint">The kinect joint type.</param>
        /// <returns>The bone name used on the character.</returns>
        public string this[JointType joint]
        {
            get { return this.boneNames[(int)joint]; }
            set { this.boneNames[(int)joint] = value; }
        }

        /// <summary>
        /// Get the bone name of this joint.
        /// </summary>
        /// <param name="joint">The kinect joint type.</param>
        /// <returns>The bone name used on the character.</returns>
        public string this[int joint]
        {
            get { return this.boneNames[joint]; }
            set { this.boneNames[joint] = value; }
        }

        /// <summary>
        /// Length of the array mapping. Equal to the number of elements in the JointType enum.
        /// </summary>
        public int Length { get { return boneNames.Length; } }

        /// <summary>
        /// Get the kinect JointType of a bone name.
        /// Generally... this method should not be used at all.
        /// </summary>
        /// <param name="s">Bone name.</param>
        /// <returns>JointType of this bone.</returns>
        [Obsolete("What you are going is probably useless. Read class spec.")]
        public Nullable<JointType> GetJointType(string s)
        {
            for (int i = 0; i < boneNames.Length; i++)
                if (s == boneNames[i]) return (JointType)i;

            return null;
        }

        public Vector3 GetNormal(JointType joint)
        {
            return this.normal[(int)joint];
        }

        public Vector3 GetNormal(int joint)
        {
            return this.normal[joint];
        }

        public void SetNormal(JointType joint, Vector3 v)
        {
            this.normal[(int)joint] = v;
        }

        public void SetNormal(int joint, Vector3 v)
        {
            this.normal[joint] = v;
        }
    }
}
