﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Input
{
    internal class KinectAction
    {
        private KinectGesture gesture;

        private bool triggered;
        internal bool IsTriggered { get { return triggered; } }

        internal KinectAction(KinectGesture whichGesture)
        {
            this.gesture = whichGesture;
            triggered = false;
        }

        internal void Update(JointMovement[] detectedMovements)
        {
            triggered = false; // Reset trigger flag
            if (detectedMovements.Length == gesture.Movements.Length)
            {
                for (int i = 0; i < detectedMovements.Length; i++)
                {
                    if (detectedMovements[i] != gesture.Movements[i])
                        // one movement doesn't match... break.
                        return;
                }

                // Everything matches
                triggered = true;
            }
            
        }
    }
}
