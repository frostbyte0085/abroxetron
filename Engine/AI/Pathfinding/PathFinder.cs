﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Engine.Tools;

namespace Engine.AI
{
    public class PathFinder
    {
        private int GetDistanceSquared(Point point1, Point point2)
        {
            int dx = point2.X - point1.X;
            int dy = point2.Y - point1.Y;
            return (dx * dx) + (dy * dy);
        }

        /// <summary>
        /// Method that switfly finds the best path from start to end.
        /// </summary>
        /// <returns>The starting PathNode traversable via .next to the end or null if there is no path</returns>        
        public PathNode FindPath(PathGrid grid, Point start, Point end)
        {
            //note we just flip start and end here so you don't have to.            
            return FindPathReversed(grid, end, start);
        }

        /// <summary>
        /// Method that switfly finds the best path from start to end. Doesn't reverse outcome
        /// </summary>
        /// <returns>The end PathNode where each .next is a step back)</returns>
        private PathNode FindPathReversed(PathGrid grid, Point start, Point end)
        {
            int t1 = grid.GridSize.X;
            int t2 = grid.GridSize.Y;

            MinHeap<PathNode> openList = new MinHeap<PathNode>(256);
            PathNode[,] pnGrid = new PathNode[t1+1,t2+2];
            PathNode node;
            Point tmp;
            int cost;
            int diff;

            PathNode current = new PathNode(start, grid);
            current.Cost = 0;

            PathNode finish = new PathNode(end, grid);
            pnGrid[current.GridPosition.X, current.GridPosition.Y] = current;
            openList.Add(current);

            while (openList.Count > 0)
            {
                //Find best item and switch it to the 'closedList'
                current = openList.ExtractFirst();
                current.InClosedList = true;

                //Find neighbours
                for (int i = 0; i < surrounding.Length; i++)
                {
                    int tmpX = current.GridPosition.X + surrounding[i].X;
                    int tmpY = current.GridPosition.Y + surrounding[i].Y;
                    tmp = new Point(tmpX, tmpY);
                    
                    if (!grid.GridPosWithinGrid(tmp))
                        continue;

                    Vector2 world_tmp = grid.GridToWorld(tmp);

                    if (grid.WorldPosWithinGrid(world_tmp) && grid.IsPositionFree(world_tmp))
                    //if (grid.IsPositionFree(tmp) )
                    {
                        //Check if we've already examined a neighbour, if not create a new node for it.
                        if (pnGrid[tmpX, tmpY] == null)
                        {
                            node = new PathNode(tmp, grid);
                            pnGrid[tmpX, tmpY] = node;
                        }
                        else
                        {
                            node = pnGrid[tmpX, tmpY];
                        }

                        //If the node is not on the 'closedList' check it's new score, keep the best
                        if (!node.InClosedList)
                        {
                            diff = 0;
                            if (current.GridPosition.X != node.GridPosition.X)
                            {
                                diff ++;
                            }
                            if (current.GridPosition.Y != node.GridPosition.Y)
                            {
                                diff ++;
                            }

                            cost = current.Cost + diff + GetDistanceSquared(node.GridPosition, end);                              

                            if (cost < node.Cost)
                            {
                                node.Cost = cost;
                                node.Next = current;
                            }

                            //If the node wasn't on the openList yet, add it 
                            if (!node.InOpenList)
                            {
                                //Check to see if we're done
                                if (node.Equals(finish))
                                {
                                    node.Next = current;
                                    return node;
                                }
                                node.InOpenList = true;
                                openList.Add(node);
                            }
                        }
                    }
                }
            }
            return null; //no path found
        }

        private static Point[] surrounding = new Point[]{
            new Point(-1,1), new Point(0,1), new Point(1,1),
            new Point(-1,0), new Point(1,0),
            new Point(-1,-1), new Point(0,-1), new Point(1,-1)
        };
    }
}
