﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using BEPUphysics;
using BEPUphysics.DataStructures;
using BEPUphysics.Collidables;
using BEPUphysics.CollisionShapes.ConvexShapes;

namespace Engine.AI
{
    public static class PathGridMaker
    {
        /// <summary>
        /// Creates a grid that represents the model.
        /// </summary>
        /// <param name="walkableMesh">The mesh that represents where the AI can
        /// position themselves</param>
        /// <param name="offset">The position of the level's scene node</param>
        /// <returns>A new grid.</returns>
        public static PathGrid MakeGrid(Model walkableMesh, Vector3 offset, int cellSize)
        {
            // Get the vertices / indices of the model and create a static-mesh
            Vector3[] verts; int[] inds;
            TriangleMesh.GetVerticesAndIndicesFromModel(walkableMesh, out verts, out inds);
            StaticMesh mesh = new StaticMesh(verts, inds);
            mesh.Sidedness = TriangleSidedness.DoubleSided;

            // Create a new space and add this mesh
            Space space = new Space();
            space.Add(mesh);

            // Get the width/height of the Path-Grid we are calculating
            BoundingBox box = mesh.BoundingBox;
            int X = (int)(box.Max.X - box.Min.X);
            int Y = (int)(box.Max.Z - box.Min.Z);

            // Make the grid:
            PathGrid grid = new PathGrid(X, Y, cellSize);
            
            // Raycast for each cell:
            for (int x = 0; x < grid.GridSize.X; x++)
                for (int y = 0; y < grid.GridSize.Y; y++)
                {
                    // Get the grid point / world point
                    Point p = new Point(x, y);
                    Vector2 world2 = grid.GridToWorld(p);

                    // Prepare the ray
                    Ray ray = new Ray(new Vector3(world2.X, 100, world2.Y), Vector3.Down);
                    RayCastResult raycast;

                    // Check to see if it hits:
                    grid.BlockPosition(p, !space.RayCast(ray, out raycast));
                }
            grid.WorldOffset = offset;

            // Dispose the space, we should. Need it longer, we don't.
            space.Dispose();

            return grid;
        }
    }
}
