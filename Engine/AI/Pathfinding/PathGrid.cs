﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Microsoft.Xna.Framework;

using Engine.Tools;

namespace Engine.AI
{
    public class PathGrid
    {
        private float cellDifferenceX;
        private float cellDifferenceY;

        private int cellSize;
        private int worldWidth;
        private int worldDepth;

        private int gridWidth;
        private int gridDepth;

        private Boolean[,] theGrid;

        private Point worldCenter;

        /// <summary>
        /// Creates a path grid of specific world dimensions. The path is centered in width/2, depth/2 point, so a minimum X position is -width/2, maximum X position is +width/2. Same with depth.
        /// </summary>
        /// <param name="worldWidth">World width</param>
        /// <param name="worldDepth">World length</param>
        /// <param name="cellSize">Cell size (smaller: more precise, but more costy)</param>
        public PathGrid(int worldWidth, int worldDepth, int cellSize)
            : this(worldWidth, worldDepth, cellSize, Vector3.Zero) { }
        /// <summary>
        /// Creates a path grid of specific world dimensions. The path is centered in width/2, depth/2 point, so a minimum X position is -width/2, maximum X position is +width/2. Same with depth.
        /// </summary>
        /// <param name="worldWidth">World width</param>
        /// <param name="worldDepth">World length</param>
        /// <param name="offset">Offset of the world (translation of the scene node)</param>
        /// <param name="cellSize">Cell size (smaller: more precise, but more costy)</param>
        public PathGrid(int worldWidth, int worldDepth, int cellSize, Vector3 offset)
        {
            Debug.Assert(cellSize > 0 && worldWidth > 0 && worldDepth > 0);

            this.worldWidth = worldWidth;
            this.worldDepth = worldDepth;
            this.cellSize = cellSize;

            this.worldCenter = new Point(this.worldWidth / 2, this.worldDepth / 2);
            this.WorldOffset = offset;

            gridWidth = (int)(worldWidth / cellSize);
            gridDepth = (int)(worldDepth / cellSize);

            cellDifferenceX = Math.Abs ((int)(worldWidth / cellSize) - (float)worldWidth / (float)cellSize);
            cellDifferenceY = Math.Abs ((int)(worldDepth / cellSize) - (float)worldDepth / (float)cellSize);

            Debug.Assert(gridWidth > 0 && gridDepth > 0);

            theGrid = new Boolean[gridWidth, gridDepth];
        }


        /// <summary>
        /// World offset (scene node translation)
        /// </summary>
        public Vector3 WorldOffset
        {
            set;
            get;
        }

        /// <summary>
        /// World offset ignoring the Y opponent in the 3D space (up / down)
        /// </summary>
        public Vector2 WorldOffset2
        {
            set { WorldOffset = new Vector3(value.X, 0, value.Y); }
            get { return new Vector2(WorldOffset.X, WorldOffset.Z); }
        }

        public Point WorldCenter
        {
            get
            {
                return worldCenter;
            }
        }

        public Point WorldSize
        {
            get
            {
                return new Point(worldWidth, worldDepth);
            }
        }

        public Point GridSize
        {
            get
            {
                return new Point(gridWidth, gridDepth);
            }
        }

        /// <summary>
        /// Converts a grid coordinate to world coordinate
        /// </summary>
        /// <param name="x">x-coordinate in the array</param>
        /// <param name="y">y-coordinate in the array</param>
        /// <returns>The world coordinate</returns>
        public Vector2 GridToWorld(int x, int y)
        {
            return GridToWorld(new Point(x, y));
        }

        /// <summary>
        /// Converts a grid coordinate to world coordinate
        /// </summary>
        /// <param name="grid">The grid coordinate in the array.</param>
        /// <returns>The world coordinate</returns>
        public Vector2 GridToWorld(Point grid)
        {
            // Check if the input is correct
            Debug.Assert(grid.X >=0 && grid.Y >= 0 && grid.X <= gridWidth && grid.Y <= gridDepth);

            Vector2 correctGrid = new Vector2(grid.X, grid.Y) + new Vector2(cellDifferenceX, cellDifferenceY);

            Vector2 ret = correctGrid * cellSize - new Vector2(worldCenter.X, worldCenter.Y);
            return ret + WorldOffset2;
        }

        /// <summary>
        /// Conerts a world coordinate to grid coordinate
        /// </summary>
        /// <param name="world">The world coordinate</param>
        /// <returns>The grid coordinate.</returns>
        public Point WorldToGrid(Vector2 world)
        {
            world -= WorldOffset2;
            Vector2 trueWorld = world + new Vector2(worldCenter.X, worldCenter.Y);

            //Debug.Assert(trueWorld.X >= 0 && trueWorld.Y >= 0 && trueWorld.X <= worldWidth && trueWorld.Y <= worldDepth);

            Vector2 intWorld = new Vector2((int)trueWorld.X, (int)trueWorld.Y);

            int gridX = (int)(intWorld.X / cellSize);
            int gridY = (int)(intWorld.Y / cellSize);

            return new Point(gridX, gridY);
        }

        /// <summary>
        /// Brings a point which is potentially outside the grid into the grid.
        /// </summary>
        /// <param name="p">Any point.</param>
        /// <returns>The nearest point in the grid to p.</returns>
        public Point CapPoint(Point p)
        {
            if (p.X < 0) p.X = 0;
            else if (p.X >= gridWidth) p.X = gridWidth - 1;

            if (p.Y < 0) p.Y = 0;
            else if (p.Y >= gridDepth) p.Y = gridDepth - 1;

            return p;
        }

        /// <summary>
        /// Brings a point which is potentially outside the grid into the grid.
        /// Equivalent to p = CapPoint(p).
        /// </summary>
        /// <param name="p">Any point.</param>
        public void CapPoint(ref Point p)
        {
            p = CapPoint(p);
        }

        /// <summary>
        /// Auxilary method for ClosestUnblockedGridPoint.
        /// </summary>
        /// <param name="x">x co-ordinate of the point we're handling</param>
        /// <param name="y">y co-ordinate of the point we're handling</param>
        /// <param name="world">World-space position. Used to calculate distance.</param>
        /// <param name="distance">Distance of the neareast unblocked Point amongst the
        /// evaluated points.</param>
        /// <param name="newPoint">Closest evaluated point.</param>
        private void ClosestUnblocked_HandlePoint(int x, int y, ref Vector2 world, ref float distance, ref Point newPoint)
        {
            Point thisPoint = new Point(x, y);

            // Is this point in the grid? 
            if (!GridPosWithinGrid(thisPoint))
                return; ; // no, ignore it.

            if (!theGrid[x, y])
            { // We found an unblocked cell!

                // Is it closer?
                float thisDistance = (GridToWorld(x, y) - world).Length();
                if (thisDistance < distance)
                {
                    // Yes. Overwrite.
                    distance = thisDistance;
                    newPoint = thisPoint;
                }
            }
        }

        /// <summary>
        /// Finds the neareast unblocked cell. Throws an expection if no
        /// solution exists (only occurs if ALL cells are blocked).
        /// </summary>
        /// <param name="world">World position.</param>
        /// <returns>Closest unblocked cell.</returns>
        public Point ClosestUnblockedGridPoint(Vector2 world)
        {
            // Closest point (return value)
            Point p = WorldToGrid(world);
            // Perimeter around p which we search for to find the closest point.
            int level = 0;
            // Distance between world and the closest point.
            float distance = float.MaxValue;
            // Coordinates we are evaluating.
            int x, y;
            // Range of indices to evaluate:
            int From, To;

            // If the world is outside the grid then cap it.
            CapPoint(ref p);

            // If grid[x,y] is blocked. Then try the cells around that point.
            while (theGrid[p.X, p.Y])
            {
                // Still no valid point found (p hasn't changed since the start).
                // Try one extra level around p.
                level++;
                Point newPoint = p;
                if (level > gridDepth && level > gridWidth)
                    throw new Exception("no solution");

                // Evaluate the cells above (including upper-corners)
                From = p.X - level;
                To   = p.X + level;
                y    = p.Y + level;
                for (x = From; x <= To; x++)
                    ClosestUnblocked_HandlePoint(x, y, ref world, ref distance, ref newPoint);

                // Evaluate the cells below (including lower-corners)
                y     = p.Y - level;
                for (x = From; x <= To; x++)
                    ClosestUnblocked_HandlePoint(x, y, ref world, ref distance, ref newPoint);

                // Evaluate the cells on the right (excluding right-corners)
                From = p.Y - (level - 1);
                To   = p.Y + (level - 1);
                x    = p.X + level;
                for (y = From; y <= To; y++)
                    ClosestUnblocked_HandlePoint(x, y, ref world, ref distance, ref newPoint);

                // Evaluate the cells on the left (excluding left-corners)
                x    = p.X - level;
                for (y = From; y <= To; y++)
                    ClosestUnblocked_HandlePoint(x, y, ref world, ref distance, ref newPoint);

                // if no unblocked cells have been found then p == newPoint.
                // if an unblocked cell is found then p != newPoint and the while
                // loop will end.
                p = newPoint;

            } return p;
        }

        /// <summary>
        /// Mark positions in the world as blocked (true) or unblocked (false)
        /// </summary>
        /// <param name="value">use true if you want to block the value</param>
        public void BlockPosition(Point gridPos, bool value)
        {
            theGrid[gridPos.X, gridPos.Y] = value;
        }
        /// <summary>
        /// Mark positions in the world as blocked (true) or unblocked (false)
        /// </summary>
        /// <param name="value">use true if you want to block the value</param>
        public void BlockPosition(Vector2 worldPosition, bool value)
        {
            Point gridPosition = Point.Zero;
            gridPosition = WorldToGrid(worldPosition);

            bool outsideBounds = (gridPosition.X < 0 || gridPosition.Y > gridWidth || gridPosition.Y < 0 || gridPosition.Y > gridDepth);
           // Debug.Assert(gridPosition.X >= 0 && gridPosition.Y <= gridWidth && gridPosition.Y >= 0 && gridPosition.Y <= gridDepth);

            if (!outsideBounds)
            { 
                theGrid[gridPosition.X, gridPosition.Y] = value;
            }
        }

        /// <summary>
        /// Checks if a position is free or blocked
        /// </summary>
        /// <returns>true if the position is blocked</returns>
        public bool IsPositionFree(Vector2 worldPosition)
        {
            Point gridPosition = Point.Zero;

            gridPosition = WorldToGrid(worldPosition);

            return 
                // Is this world position in the grid?
                gridPosition.X >= 0 && gridPosition.X < theGrid.GetLength(0) &&
                gridPosition.Y >= 0 && gridPosition.Y < theGrid.GetLength(1) &&
                // And is it free?
                !theGrid[gridPosition.X, gridPosition.Y];
        }

        public bool IsPositionFree(Point gridPoint)
        {
            return theGrid[gridPoint.X, gridPoint.Y];
        }

        public bool IsPositionFree(int x, int y)
        {
            return theGrid[x, y];
        }

        /// <summary>
        /// Checks if the world position is within the grid borders
        /// </summary>
        /// <param name="worldPosition">Check result</param>
        /// <returns></returns>
        public bool WorldPosWithinGrid(Vector2 worldPosition)
        {
            Point gridPosition = Point.Zero;
            gridPosition = WorldToGrid(worldPosition);

            return GridPosWithinGrid(gridPosition);
        }

        /// <summary>
        /// Checks if the world position is within the grid borders
        /// </summary>
        /// <param name="worldPosition">Check result</param>
        /// <returns></returns>
        public bool GridPosWithinGrid(Point gridPosition)
        {
            return gridPosition.X >= 0 && gridPosition.X < theGrid.GetLength(0) &&
                gridPosition.Y >= 0 && gridPosition.Y < theGrid.GetLength(1);
        }
    }
}
