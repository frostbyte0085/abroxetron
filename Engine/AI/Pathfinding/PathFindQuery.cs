﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace Engine.AI
{
    public class PathFindQuery
    {
        public PathFindQuery(string name, Vector2 worldStart, Vector2 worldEnd)
        {
            Name = name;
            WorldStart = worldStart;
            WorldEnd = worldEnd;
        }

        public string Name
        {
            set;
            get;
        }

        public Vector2 WorldStart
        {
            set;
            get;
        }

        public Vector2 WorldEnd
        {
            set;
            get;
        }
    }
}
