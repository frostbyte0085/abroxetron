﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Engine.AI
{
    public class PathFindResult
    {
        public PathFindResult(string name, List<Vector2> foundPath)
        {
            Name = name;
            FoundPath = foundPath;
        }
        
        public string Name
        {
            internal set;
            get;
        }

        public List<Vector2> FoundPath
        {
            internal set;
            get;
        }
    }
}
