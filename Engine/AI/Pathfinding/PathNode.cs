﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Engine.AI
{
    public class PathNode : IComparable<PathNode>
    {
        private PathGrid grid;
        private Point position;
        
        public PathNode(Point gridPosition, PathGrid grid)
        {
            this.grid = grid;
            position = gridPosition;
            Cost = int.MaxValue;
        }

        public PathNode(Point gridPosition, PathNode parent, PathGrid grid) : this(gridPosition, grid)
        {
            Next = parent;
        }

        public List<Vector2> ToWorldList()
        {
            List<Vector2> lst = new List<Vector2>();

            PathNode current = Next;
            while (current != null)
            {
                lst.Add(grid.GridToWorld(current.GridPosition));

                current = current.Next;
            }

            return lst;
        }

        public Point GridPosition
        {
            get
            {
                return position;
            }
        }

        public Vector2 WorldPosition
        {
            get
            {
                return grid.GridToWorld(GridPosition);
            }
        }

        public int Cost
        {
            internal set;
            get;
        }

        public PathNode Next
        {
            internal set;
            get;
        }

        public bool InClosedList
        {
            internal set;
            get;
        }

        public bool InOpenList
        {
            internal set;
            get;
        }

        public override bool Equals(object obj)
        {
            return (obj is PathNode) && ((PathNode)obj).GridPosition == this.position;
        }

        //Faster Equals for if we know something is a PathNode
        public bool Equals(PathNode other)
        {
            return other.GridPosition == this.position;
        }

        public override int GetHashCode()
        {
            return position.GetHashCode();
        }

        #region IComparable<> interface
        public int CompareTo(PathNode other)
        {
            return Cost.CompareTo(other.Cost);
        }
        #endregion
    }
}
