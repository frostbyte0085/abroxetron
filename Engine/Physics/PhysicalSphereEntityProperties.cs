﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace Engine.Physics
{
    public sealed class PhysicalSphereEntityProperties : IPhysicalEntityProperties
    {
        float radius;

        public PhysicalSphereEntityProperties(float radius)
        {
            Radius = radius;
        }

        public PhysicalSphereEntityProperties()
        {
        }

        public float Radius
        {
            get
            {
                return radius;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                radius = value;
            }
        }


        public PhysicalEntityType EntityType
        {
            get { return PhysicalEntityType.Sphere; }
        }
    }
}
