﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BEPUphysics;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionRuleManagement;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BEPUphysics.MathExtensions;

using System.Diagnostics;

namespace Engine.Physics
{
    public static class CustomCollisionGroupPairs
    {
        private static Dictionary<string, CollisionGroupPair> pairs = new Dictionary<string, CollisionGroupPair>();

        /// <summary>
        /// Add a collision group pair. By default, those two groups will not enter broad phase and will not be tested
        /// for collisions
        /// </summary>
        /// <param name="name">pair name</param>
        /// <param name="group1">group 1</param>
        /// <param name="group2">group 2</param>
        public static void Add(string name, CollisionGroup group1, CollisionGroup group2)
        {
            Add(name, group1, group2, CollisionRule.NoBroadPhase);
        }

        /// <summary>
        /// Add a collision group pair with a custom CollisionRule.
        /// </summary>
        /// <param name="name">pair name</param>
        /// <param name="group1">group 1</param>
        /// <param name="group2">group 2</param>
        /// <param name="ruleOverride">CollisionRule to be used</param>
        public static void Add(string name, CollisionGroup group1, CollisionGroup group2, CollisionRule ruleOverride)
        {
            Debug.Assert(!pairs.ContainsKey(name));

            pairs[name] = new CollisionGroupPair(group1, group2);

            CollisionRules.CollisionGroupRules.Add(pairs[name], ruleOverride);
        }

        public static CollisionGroupPair GetPair(string name)
        {
            Debug.Assert(pairs.ContainsKey(name));
            return pairs[name];
        }

        public static CollisionGroup GetGroup1(string name)
        {
            CollisionGroupPair pair = GetPair(name);
            return pair.A;
        }

        public static CollisionGroup GetGroup2(string name)
        {
            CollisionGroupPair pair = GetPair(name);
            return pair.B;
        }

        /// <summary>
        /// Clear all the rules. Called everytime the game changes state.
        /// </summary>
        public static void Clear()
        {
            foreach (CollisionGroupPair pair in pairs.Values)
            {
                CollisionRules.CollisionGroupRules.Remove(pair);
            }
            pairs.Clear();
        }
    }
}
