﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;

namespace Engine.Physics
{
    public class PhysicalMaterial
    {
        float staticFriction;
        float kineticFriction;
        float bounciness;

        public PhysicalMaterial(float staticFriction, float kineticFriction, float bounciness)
        {
            StaticFriction = staticFriction;
            KineticFriction = kineticFriction;
            Bounciness = bounciness;
        }

        public PhysicalMaterial(float staticFriction, float kineticFriction)
        {
            StaticFriction = staticFriction;
            KineticFriction = kineticFriction;
            Bounciness = 0.0f;
        }

        public float StaticFriction
        {
            get
            {
                return staticFriction;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                staticFriction = value;
            }
        }

        public float KineticFriction
        {
            get
            {
                return kineticFriction;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                kineticFriction = value;
            }
        }

        public float Bounciness
        {
            get
            {
                return bounciness;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                bounciness = value;
            }
        }

    }
}
