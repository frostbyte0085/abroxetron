﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;

namespace Engine.Physics
{
    public sealed class PhysicalBoxEntityProperties : IPhysicalEntityProperties
    {
        float width;
        float height;
        float length;

        public PhysicalBoxEntityProperties(float width, float height, float length)
        {
            Width = width;
            Height = height;
            Length = length;
        }

        public PhysicalBoxEntityProperties()
        {
            
        }

        public PhysicalEntityType EntityType
        {
            get { return PhysicalEntityType.Box; }
        }

        public float Width
        {
            get
            {
                return width;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                width = value;
            }
        }

        public float Height
        {
            get
            {
                return height;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                height = value;
            }
        }

        public float Length
        {
            get
            {
                return length;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                length = value;
            }
        }
    }
}
