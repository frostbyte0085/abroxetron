﻿using System;
using System.Collections.Generic;
using System.Text;

using BEPUphysics;
using BEPUphysics.Threading;
using BEPUphysics.Settings;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;

using Engine.Physics;
using Microsoft.Xna.Framework;

using System.Diagnostics;
using Engine.Core;
using Engine.Scene;
using System.Threading;

namespace Engine.Physics
{
    public sealed class PhysicsService : IDisposable
    {
        #region Variables
        Space space;
        SceneService scene;
        #endregion

        public PhysicsService()
        {
            // create the physical space and set thread count
            space = new Space();

            SetupThreads();
            SetupOptimalSettings();
        }

        internal void SetupThreads()
        {
#if XBOX360
            space.ThreadManager.AddThread(delegate { Thread.CurrentThread.SetProcessorAffinity(new[] { 1 }); }, null);
            //space.ThreadManager.AddThread(delegate { Thread.CurrentThread.SetProcessorAffinity(new[] { 3 }); }, null);
            //space.ThreadManager.AddThread(delegate { Thread.CurrentThread.SetProcessorAffinity(new[] { 5 }); }, null);
#else
            if (Environment.ProcessorCount > 1)
            {
                for (int i = 0; i < Environment.ProcessorCount; i++)
                {
                    space.ThreadManager.AddThread();
                }
            }
#endif
        }

        internal void SetupOptimalSettings()
        {
            MotionSettings.ConserveAngularMomentum = false;
            MotionSettings.DefaultPositionUpdateMode = PositionUpdateMode.Discrete;
            MotionSettings.UseRk4AngularIntegration = false;
            SolverSettings.DefaultMinimumIterations = 1;
            space.Solver.IterationLimit = 10;
            GeneralConvexPairTester.UseSimplexCaching = false;
            MotionSettings.UseExtraExpansionForContinuousBoundingBoxes = false;
            
            space.ForceUpdater.Gravity = new Vector3(0, -9.81f, 0);
        }

        internal void Add(PhysicalEntity entity)
        {
            if (entity.EntityType == PhysicalEntityType.StaticMesh)
            {
                Debug.Assert(entity.BEPUStaticMesh != null);
                space.Add(entity.BEPUStaticMesh);
            }
            else
            {
                Debug.Assert(entity.BEPUEntity != null);
                space.Add(entity.BEPUEntity);
            }
        }

        internal void Add(Entity entity)
        {
            Debug.Assert(entity != null);

            space.Add(entity);
        }

        internal void Remove(PhysicalEntity entity)
        {
            if (entity.EntityType == PhysicalEntityType.StaticMesh)
            {
                Debug.Assert(entity.BEPUStaticMesh != null);
                if (entity.BEPUStaticMesh.Space == space)
                {
                    space.Remove(entity.BEPUStaticMesh);
                }
            }
            else
            {
                Debug.Assert(entity.BEPUEntity != null);
                if (entity.BEPUEntity.Space == space)
                {
                    space.Remove(entity.BEPUEntity);
                }
            }
        }

        internal void Remove(Entity entity)
        {
            Debug.Assert(entity != null);

            space.Remove(entity);
        }

        internal void Update()
        {
            if (scene == null)
                scene = ServiceProvider.GetService<SceneService>();

            space.Update(GameTiming.TimeElapsed);
        }

        public bool Raycast(Ray ray, float distance, out RaycastHit hit)
        {
            hit = null;

            RayCastResult result;
            if (space.RayCast (ray, distance, out result))
            {
                RaycastHit hitOut = new RaycastHit();
                hitOut.Position = result.HitData.Location;
                hitOut.Normal = Vector3.Normalize( result.HitData.Normal );
                hitOut.T = result.HitData.T;
                SceneNode node;
                scene.collidableNodeMappings.TryGetValue((Collidable)result.HitObject, out node);

                hitOut.SceneNode = node;
                hit = hitOut;

                return true;
            }

            return false;
        }

        public bool Raycast(Ray ray, float distance, List<RaycastHit> hits)
        {
            List<RayCastResult> results = new List<RayCastResult>();

            if (space.RayCast(ray, distance, results))
            {
                hits = new List<RaycastHit>();

                for (int i=0; i<results.Count; i++)
                {
                    hits[i] = new RaycastHit();

                    hits[i].Position = results[i].HitData.Location;
                    hits[i].Normal = Vector3.Normalize( results[i].HitData.Normal );
                    hits[i].T = results[i].HitData.T;

                    SceneNode node;
                    scene.collidableNodeMappings.TryGetValue((Collidable)results[i].HitObject, out node);
                    hits[i].SceneNode = node;
                                        
                }
                return true;
            }

            return false;
        }

        public void Dispose()
        {
            space.Dispose();
        }
    }
}
