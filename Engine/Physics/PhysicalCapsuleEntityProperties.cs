﻿using System;
using System.Collections.Generic;
using System.Text;


using System.Diagnostics;

namespace Engine.Physics
{
    public sealed class PhysicalCapsuleEntityProperties : IPhysicalEntityProperties
    {
        float length;
        float radius;

        public PhysicalCapsuleEntityProperties(float length, float radius)
        {
            Length = length;
            Radius = radius;
        }

        public PhysicalCapsuleEntityProperties()
        {

        }

        public PhysicalEntityType EntityType
        {
            get { return PhysicalEntityType.Capsule; }
        }
        
        public float Length
        {
            get
            {
                return length;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                length = value;
            }
        }

        public float Radius
        {
            get
            {
                return radius;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                radius = value;
            }
        }
    }
}
