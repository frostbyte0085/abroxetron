﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Physics
{
    public enum PhysicalEntityType
    {
        None,
        StaticMesh,
        ConvexHull,
        Box,
        Sphere,
        Cylinder,
        Capsule
    }

    public interface IPhysicalEntityProperties
    {
        PhysicalEntityType EntityType { get; }
    }
}
