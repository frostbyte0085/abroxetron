﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Physics
{
    public sealed class PhysicalConvexHullEntityProperties : IPhysicalEntityProperties
    {
        Model model;

        public PhysicalConvexHullEntityProperties(Model model)
        {
            Model = model;
        }

        public PhysicalConvexHullEntityProperties()
        {

        }

        public PhysicalEntityType EntityType
        {
            get { return PhysicalEntityType.ConvexHull; }
        }

        public Model Model
        {
            get
            {
                return model;
            }
            set
            {
                Debug.Assert(value != null);
                model = value;
            }
        }
    }
}
