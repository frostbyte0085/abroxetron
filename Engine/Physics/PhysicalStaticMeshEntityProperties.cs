﻿using System;
using System.Collections.Generic;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BEPUphysics.MathExtensions;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Settings;
using BEPUphysics.Materials;
using BEPUphysics.Constraints.SingleEntity;

using System.Diagnostics;

namespace Engine.Physics
{
    public enum StaticMeshSidedness
    {
        Clockwise = TriangleSidedness.Clockwise,
        CounterClockwise = TriangleSidedness.Counterclockwise,
        DoubleSided = TriangleSidedness.DoubleSided
    }

    public sealed class PhysicalStaticMeshEntityProperties : IPhysicalEntityProperties
    {
        Model model;
        StaticMeshSidedness sidedness;

        public PhysicalStaticMeshEntityProperties(Model model, StaticMeshSidedness sidedness)
        {
            Model = model;
            Sidedness = sidedness;
        }

        public PhysicalStaticMeshEntityProperties(StaticMeshSidedness sidedness)
        {
            Sidedness = sidedness;
        }

        public PhysicalEntityType EntityType
        {
            get { return PhysicalEntityType.StaticMesh; }
        }

        public StaticMeshSidedness Sidedness
        {
            get
            {
                return sidedness;
            }
            set
            {
                sidedness = value;
            }
        }

        public Model Model
        {
            get
            {
                return model;
            }
            set
            {
                Debug.Assert(value != null);
                model = value;
            }
        }
    }
}
