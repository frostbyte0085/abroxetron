﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;

namespace Engine.Physics
{
    public sealed class PhysicalCylinderEntityProperties : IPhysicalEntityProperties
    {
        float height;
        float radius;

        public PhysicalCylinderEntityProperties(float height, float radius)
        {
            Height = height;
            Radius = radius;
        }

        public PhysicalCylinderEntityProperties()
        {

        }

        public PhysicalEntityType EntityType
        {
            get { return PhysicalEntityType.Cylinder; }
        }

        public float Height
        {
            get
            {
                return height;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                height = value;
            }
        }

        public float Radius
        {
            get
            {
                return radius;
            }
            set
            {
                Debug.Assert(value >= 0.0f);
                radius = value;
            }
        }
    }
}
