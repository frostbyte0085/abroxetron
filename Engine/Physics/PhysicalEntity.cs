﻿using System;
using System.Collections.Generic;
using System.Text;

using BEPUphysics;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionRuleManagement;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BEPUphysics.MathExtensions;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Diagnostics;

using Engine.Scene;
using Engine.Graphics;
using Engine.Core;

namespace Engine.Physics
{
    public sealed class PhysicalEntity
    {
        #region Supported collidable entities
        StaticMesh staticMesh;
        Entity entity;


        PhysicalEntityType type;
        #endregion

        internal void RenderDebugEntities()
        {
            DebugRendererService drs = ServiceProvider.GetService<DebugRendererService>();
            Debug.Assert(drs != null);

            switch (type)
            {
                case PhysicalEntityType.Box:
                    
                    Box boxEntity = entity as Box;
                    Vector3 min = new Vector3(-boxEntity.HalfWidth, -boxEntity.HalfHeight, -boxEntity.HalfLength);
                    Vector3 max = new Vector3(boxEntity.HalfWidth, boxEntity.HalfHeight, boxEntity.HalfLength);

                    min += boxEntity.Position;
                    max += boxEntity.Position;

                    drs.AddBox3D(min, max, Color.White, boxEntity.Orientation, Center);
                    break;

                case PhysicalEntityType.Sphere:
                    Sphere sphereEntity = entity as Sphere;

                    drs.AddSphere(sphereEntity.Position, sphereEntity.Radius, Color.White);
                    break;

                case PhysicalEntityType.StaticMesh:
                    Vector3[] copied = new Vector3[BEPUStaticMesh.Mesh.Data.Vertices.Length];
                    Matrix mat = BEPUStaticMesh.WorldTransform.Matrix;
                    Vector3.Transform(BEPUStaticMesh.Mesh.Data.Vertices, ref mat, copied);

                    drs.AddTriSoup(copied, BEPUStaticMesh.Mesh.Data.Indices, Color.White);
                    break;

                case PhysicalEntityType.Capsule:
                    Capsule capsuleEntity = entity as Capsule;

                    drs.AddCapsule(capsuleEntity.Position, capsuleEntity.Radius, capsuleEntity.Length, 24, Color.White);
                    break;
            }
        }

        void CreatePhysicalEntity(SceneNode node, Vector3 center, float mass, IPhysicalEntityProperties properties, bool trigger)
        {
            this.type = properties.EntityType;
            this.trigger = trigger;


            Vector3[] triangleVertices;
            int[] triangleIndices;

            Dictionary<string, object> modelTags = null;
            if (node != null)
            {
                Debug.Assert(node.Model != null);
                modelTags = (Dictionary<string, object>)node.Model.Tag;
            }

            // if node is not null, we'll fetch the dimensions of the entities from the bounding volume of the model, try to create a fitting entity.
            switch (properties.EntityType)
            {
                case PhysicalEntityType.Box:
                    PhysicalBoxEntityProperties boxProperties = properties as PhysicalBoxEntityProperties;
                    if (node != null)
                    {
                        Debug.Assert(node.Model != null);
                        BoundingBox bb = node.BoundingBox;
                        Vector3 bbCenter = (bb.Max + bb.Min) / 2;

                        center = bbCenter;

                        boxProperties.Width = bb.Max.X - bb.Min.X;
                        boxProperties.Height = bb.Max.Y - bb.Min.Y;
                        boxProperties.Length = bb.Max.Z - bb.Min.Z;
                    }

                    if (mass > 0.0f)
                        entity = new Box(Vector3.Zero, boxProperties.Width, boxProperties.Height, boxProperties.Length, mass);
                    else
                        entity = new Box(Vector3.Zero, boxProperties.Width, boxProperties.Height, boxProperties.Length);

                    Center = center;
                    break;

                case PhysicalEntityType.Sphere:
                    PhysicalSphereEntityProperties sphereProperties = properties as PhysicalSphereEntityProperties;
                    if (node != null)
                    {
                        Debug.Assert(node.Model != null);
                        BoundingSphere ss = node.BoundingSphere;

                        center = ss.Center;
                        
                        sphereProperties.Radius = ss.Radius;
                    }

                    if (mass > 0.0f)
                        entity = new Sphere(Vector3.Zero, sphereProperties.Radius, mass);
                    else
                        entity = new Sphere(Vector3.Zero, sphereProperties.Radius);

                    Center = center;
                    break;

                case PhysicalEntityType.Cylinder:
                    PhysicalCylinderEntityProperties cylinderProperties = properties as PhysicalCylinderEntityProperties;
                    if (node != null)
                    {

                    }

                    if (mass > 0.0f)
                        entity = new Cylinder(Vector3.Zero, cylinderProperties.Height, cylinderProperties.Radius, mass);
                    else
                        entity = new Cylinder(Vector3.Zero, cylinderProperties.Height, cylinderProperties.Radius);

                    Center = center;
                    break;

                case PhysicalEntityType.Capsule:
                    PhysicalCapsuleEntityProperties capsuleProperties = properties as PhysicalCapsuleEntityProperties;
                    if (node != null)
                    {
                        Debug.Assert(node.Model != null);
                        Debug.Assert(node.HasBoundingBox);
                        BoundingBox bb = node.BoundingBox;
                        Vector3[] bbCorners = bb.GetCorners();

                        float capsuleHeight = Math.Abs(bbCorners[0].Y - bbCorners[2].Y);
                        float capsuleWidth = Math.Abs(bbCorners[0].X);
                        capsuleProperties.Length = capsuleHeight - capsuleWidth;
                        capsuleProperties.Radius = capsuleWidth / 2;
                        
                        center = (bb.Max + bb.Min) / 2;
                    }

                    if (mass > 0.0f)
                        entity = new Capsule(Vector3.Zero, capsuleProperties.Length, capsuleProperties.Radius, mass);
                    else
                        entity = new Capsule(Vector3.Zero, capsuleProperties.Length, capsuleProperties.Radius);

                    Center = center;
                    break;

                case PhysicalEntityType.StaticMesh:
                    // static meshes make no sense of being triggers
                    Debug.Assert(!trigger);
                    PhysicalStaticMeshEntityProperties staticMeshProperties = properties as PhysicalStaticMeshEntityProperties;

                    if (node != null)
                    {
                        Debug.Assert(node.Model != null);
                        staticMeshProperties.Model = node.Model;

                        center = node.Translation;
                    }

                    TriangleMesh.GetVerticesAndIndicesFromModel(staticMeshProperties.Model, out triangleVertices, out triangleIndices);

                    staticMesh = new StaticMesh(triangleVertices, triangleIndices, new AffineTransform(node.Translation));
                    staticMesh.Sidedness = (TriangleSidedness)staticMeshProperties.Sidedness;

                    break;

                case PhysicalEntityType.ConvexHull:
                    PhysicalConvexHullEntityProperties convexHullProperties = properties as PhysicalConvexHullEntityProperties;
                    if (node != null)
                    {
                        Debug.Assert(node.Model != null);
                        convexHullProperties.Model = node.Model;

                        center = node.Translation;
                    }
                    
                    TriangleMesh.GetVerticesAndIndicesFromModel(convexHullProperties.Model, out triangleVertices, out triangleIndices);
                    if (mass > 0.0f)
                        entity = new ConvexHull(Vector3.Zero, triangleVertices, mass);
                    else
                        entity = new ConvexHull(Vector3.Zero, triangleVertices);

                    Center = center;
                    break;

                default: break;
            }

            
            if (type == PhysicalEntityType.StaticMesh)
            {
                
            }
            else
            {
                // if the entity is a trigger and is not a staticMesh, let's have the personal collision rules to not having collision response.
                // collision events are propagated as normal
                if (IsTrigger)
                    entity.CollisionInformation.CollisionRules.Personal = BEPUphysics.CollisionRuleManagement.CollisionRule.NoSolver;

            }

        }

        public CollisionGroup CollisionGroup
        {
            set
            {
                if (EntityType == PhysicalEntityType.StaticMesh)
                {
                    BEPUStaticMesh.CollisionRules.Group = value;
                }
                else
                {
                    BEPUEntity.CollisionInformation.CollisionRules.Group = value;
                }
            }
            get
            {
                if (EntityType == PhysicalEntityType.StaticMesh)
                {
                    return BEPUStaticMesh.CollisionRules.Group;
                }
                else
                {
                    return BEPUEntity.CollisionInformation.CollisionRules.Group;
                }
            }
            
        }

        public Vector3 Center
        {
            set
            {
                Debug.Assert(BEPUEntity != null);
                BEPUEntity.Tag = value;
            }
            get
            {
                Debug.Assert(BEPUEntity != null);
                return (Vector3)BEPUEntity.Tag;
            }
        }

        public PhysicalMaterial Material
        {
            get
            {
                PhysicalMaterial material = null;
                if (type == PhysicalEntityType.StaticMesh)
                {
                    Debug.Assert(staticMesh != null);
                    material = new PhysicalMaterial(staticMesh.Material.StaticFriction, staticMesh.Material.KineticFriction, staticMesh.Material.Bounciness);
                }
                else
                {
                    Debug.Assert(entity != null);
                    material = new PhysicalMaterial(entity.Material.StaticFriction, entity.Material.KineticFriction, entity.Material.Bounciness);
                }
                return material;
            }
            set
            {
                Debug.Assert(value != null);

                if (type == PhysicalEntityType.StaticMesh)
                {
                    Debug.Assert(staticMesh != null);
                    staticMesh.Material = new BEPUphysics.Materials.Material(value.StaticFriction, value.KineticFriction, value.Bounciness);
                }
                else
                {
                    Debug.Assert(entity != null);
                    entity.Material = new BEPUphysics.Materials.Material(value.StaticFriction, value.KineticFriction, value.Bounciness);
                }
            }
        }

        public PhysicalEntity(IPhysicalEntityProperties properties, float mass, bool trigger)
        {
            
            CreatePhysicalEntity(null, Vector3.Zero, mass, properties, trigger);
        }

        public PhysicalEntity(IPhysicalEntityProperties properties, float mass)
        {
            CreatePhysicalEntity(null, Vector3.Zero, mass, properties, false);
        }

        public PhysicalEntity(Vector3 center, IPhysicalEntityProperties properties, float mass, bool trigger)
        {
            CreatePhysicalEntity(null, center, mass, properties, trigger);
        }

        public PhysicalEntity(Vector3 center, IPhysicalEntityProperties properties, float mass)
        {
            CreatePhysicalEntity(null, center, mass, properties, false);
        }

        public PhysicalEntity(SceneNode node, IPhysicalEntityProperties properties, float mass, bool trigger)
        {
            CreatePhysicalEntity(node, Vector3.Zero, mass, properties, trigger);
        }

        public PhysicalEntity(IPhysicalEntityProperties properties, bool trigger)
        {
            CreatePhysicalEntity(null, Vector3.Zero, 0, properties, trigger);
        }

        public PhysicalEntity(IPhysicalEntityProperties properties)
        {
            CreatePhysicalEntity(null, Vector3.Zero, 0, properties, false);
        }

        public PhysicalEntity(Vector3 center, IPhysicalEntityProperties properties, bool trigger)
        {
            CreatePhysicalEntity(null, center, 0, properties, trigger);
        }

        public PhysicalEntity(Vector3 center, IPhysicalEntityProperties properties)
        {
            CreatePhysicalEntity(null, center, 0, properties, false);
        }

        public PhysicalEntity(SceneNode node, IPhysicalEntityProperties properties, bool trigger)
        {
            CreatePhysicalEntity(node, Vector3.Zero, 0, properties, trigger);
        }


        public PhysicalEntityType EntityType
        {
            get
            {
                return type;
            }
        }
        public Entity BEPUEntity
        {
            get
            {
                return entity;
            }
        }

        public StaticMesh BEPUStaticMesh
        {
            get
            {
                return staticMesh;
            }
        }

        bool trigger;

        void AssertEntity()
        {
            Debug.Assert (entity != null);
        }

        #region Methods exposing Entity functionality
        public void ApplyAngularImpulse(ref Vector3 impulse)
        {
            AssertEntity();
            entity.ApplyAngularImpulse(ref impulse);
        }

        public void ApplyImpulse(Vector3 position, Vector3 impulse)
        {
            AssertEntity();
            entity.ApplyImpulse(position, impulse);
        }

        public void ApplyLinearImpulse(ref Vector3 impulse)
        {
            AssertEntity();
            entity.ApplyLinearImpulse(ref impulse);
        }

        public void ModifyAngularDamping(float d)
        {
            AssertEntity();
            entity.ModifyAngularDamping(d);
        }

        public void ModifyLinearDamping(float d)
        {
            AssertEntity();
            entity.ModifyLinearDamping(d);
        }
        #endregion

        #region Properties exposing Entity functionality

        internal Matrix WorldMatrix
        {
            get
            {
                AssertEntity();
                return entity.WorldTransform;
            }
        }

        public float AngularDamping
        {
            get
            {
                AssertEntity();
                return entity.AngularDamping;
            }
            set
            {
                AssertEntity();
                entity.AngularDamping = value;
            }
        }

        public Vector3 AngularMomentum
        {
            get
            {
                AssertEntity();
                return entity.AngularMomentum;
            }
            set
            {
                AssertEntity();
                entity.AngularMomentum = value;
            }
        }

        public Vector3 AngularVelocity
        {
            get
            {
                AssertEntity();
                return entity.AngularVelocity;
            }
            set
            {
                AssertEntity();
                entity.AngularVelocity = value;
            }
        }

        public float InverseMass
        {
            get
            {
                AssertEntity();
                return entity.InverseMass;
            }
            set
            {
                AssertEntity();
                entity.InverseMass = value;
            }
        }

        public bool IsAffectedByGravity
        {
            get
            {
                AssertEntity();
                return entity.IsAffectedByGravity;
            }
            set
            {
                AssertEntity();
                entity.IsAffectedByGravity = value;
            }
        }

        public bool IsDynamic
        {
            get
            {
                AssertEntity();
                return entity.IsDynamic;
            }
        }

        public float LinearDamping
        {
            get
            {
                AssertEntity();
                return entity.LinearDamping;
            }
            set
            {
                AssertEntity();
                entity.LinearDamping = value;
            }
        }

        public Vector3 LinearMomentum
        {
            get
            {
                AssertEntity();
                return entity.LinearMomentum;
            }
            set
            {
                AssertEntity();
                entity.LinearMomentum = value;
            }
        }

        public Vector3 LinearVelocity
        {
            get
            {
                AssertEntity();
                return entity.LinearVelocity;
            }
            set
            {
                AssertEntity();
                entity.LinearVelocity = value;
            }
        }

        public float Mass
        {
            get
            {
                AssertEntity();
                return entity.Mass;
            }
            set
            {
                AssertEntity();
                entity.Mass = value;
            }
        }

        public Vector3 Translation
        {
            get
            {
                AssertEntity();
                return entity.Position;
            }
            set
            {
                AssertEntity();
                entity.Position = value;
            }
        }

        private Quaternion unpivoted;

        public Quaternion UnpivotedOrientation
        {
            get
            {
                AssertEntity();
                return unpivoted;
            }
            set
            {
                AssertEntity();
                unpivoted = value;
            }
        }

        public Quaternion Orientation
        {
            get
            {
                AssertEntity();
                return entity.Orientation;
            }
            set
            {
                AssertEntity();
                entity.Orientation = value;
            }
        }

        public bool IsTrigger
        {
            get
            {
                return trigger;
            }
        }
        #endregion


    }
}
