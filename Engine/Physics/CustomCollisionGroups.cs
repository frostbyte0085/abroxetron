﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BEPUphysics;
using BEPUphysics.Collidables;
using BEPUphysics.DataStructures;
using BEPUphysics.CollisionShapes.ConvexShapes;
using BEPUphysics.Collidables.MobileCollidables;
using BEPUphysics.Constraints.SingleEntity;
using BEPUphysics.CollisionRuleManagement;
using BEPUphysics.CollisionTests.CollisionAlgorithms;
using BEPUphysics.CollisionTests.CollisionAlgorithms.GJK;
using BEPUphysics.Constraints;
using BEPUphysics.NarrowPhaseSystems.Pairs;
using BEPUphysics.PositionUpdating;
using BEPUphysics.Settings;
using BEPUphysics.Entities;
using BEPUphysics.Entities.Prefabs;
using BEPUphysics.MathExtensions;

using System.Diagnostics;

namespace Engine.Physics
{
    public static class CustomCollisionGroups
    {
        private static Dictionary<string, CollisionGroup> groups = new Dictionary<string,CollisionGroup>();

        /// <summary>
        /// Adds a collision group to the dictionary.
        /// </summary>
        /// <param name="name">The name of the collision group.</param>
        /// <returns>Returns the newly created collision group.</returns>
        public static CollisionGroup Add(string name)
        {
            Debug.Assert(!groups.ContainsKey(name));

            CollisionGroup grp = new CollisionGroup();
            groups[name] = grp;

            return grp;
        }

        public static CollisionGroup Get(string name)
        {
            Debug.Assert(groups.ContainsKey(name));
            return groups[name];
        }

        /// <summary>
        /// Clear all collision groups. Called everytime the game changes state.
        /// </summary>
        public static void Clear()
        {
            groups.Clear();
        }
    }
}
