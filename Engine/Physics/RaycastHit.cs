﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Engine.Scene;

namespace Engine.Physics
{
    public sealed class RaycastHit
    {
        public RaycastHit()
        {
        }

        public Vector3 Position
        {
            internal set;
            get;
        }

        public Vector3 Normal
        {
            internal set;
            get;
        }

        public float T
        {
            internal set;
            get;
        }

        public SceneNode SceneNode
        {
            internal set;
            get;
        }
    }
}
