﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Diagnostics
{

    public enum VerbosityLevel
    {
        Normal,
        Warning,
        Critical
    }

    public class ConsoleLogService
    {
       public ConsoleLogService()
        {
            AllowVerbosityLevel = VerbosityLevel.Normal;
        }

        public VerbosityLevel AllowVerbosityLevel
        {
            set;
            get;
        }

        public void WriteNormal(string text)
        {
            if (AllowVerbosityLevel > VerbosityLevel.Normal)
                return;

            Console.WriteLine(text);
        }

        public void WriteWarning(string text)
        {
            if (AllowVerbosityLevel > VerbosityLevel.Warning)
                return;
            
            Console.WriteLine("(Warning) " + text);
        }

        public void WriteCritical(string text)
        {
            if (AllowVerbosityLevel > VerbosityLevel.Critical)
                return;

            Console.WriteLine("(Critical!)" + text);
        }

        public void Write(string text, VerbosityLevel level)
        {
            if (AllowVerbosityLevel > level)
                return;

            switch (level)
            {
                case VerbosityLevel.Normal:
                    WriteNormal(text);
                    break;
                case VerbosityLevel.Warning:
                    WriteWarning(text);
                    break;
                case VerbosityLevel.Critical:
                    WriteCritical(text);
                    break;
                default: break;
            }
        }
    }
}
