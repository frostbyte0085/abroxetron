﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using Engine.Core;

namespace Engine.Graphics
{
    public class FullScreenQuad
    {
        #region Variables
        VertexBuffer vb;
        IndexBuffer ib;
        GraphicsService gs;

        VertexPositionTexture[] vertices;
        ushort[] indices;
        #endregion

        #region Methods
        public FullScreenQuad()
        {
            gs = ServiceProvider.GetService<GraphicsService>();

            vertices = new VertexPositionTexture[4];
            vertices[0] = new VertexPositionTexture(new Vector3(1, -1, 0), new Vector2(1, 1));
            vertices[1] = new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1));
            vertices[2] = new VertexPositionTexture(new Vector3(-1, 1, 0), new Vector2(0, 0));
            vertices[3] = new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0));

            indices = new ushort[6];
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 2;
            indices[4] = 3;
            indices[5] = 0;

            RestoreData();
        }

        internal void RestoreData()
        {
            vb = new VertexBuffer(gs.GraphicsDevice, VertexPositionTexture.VertexDeclaration, vertices.Length, BufferUsage.None);
            vb.SetData<VertexPositionTexture>(vertices);

            ib = new IndexBuffer(gs.GraphicsDevice, IndexElementSize.SixteenBits, indices.Length, BufferUsage.None);
            ib.SetData<ushort>(indices);
        }

        public void SetBuffers()
        {
            gs.GraphicsDevice.SetVertexBuffer(vb);
            gs.GraphicsDevice.Indices = ib;
        }

        public void Render()
        {
            gs.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
        }
        #endregion
    }

}
