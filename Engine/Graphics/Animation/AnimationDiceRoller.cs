﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Tools;

namespace Engine.Animation
{
    public class AnimationDiceRoller
    {
        private string[] animations = null;
        /// <summary>
        /// Names of the animations we are rolling for.
        /// </summary>
        public string[] AnimationNames
        {
            set
            {
                if (probabilities != null && probabilities.Length != value.Length)
                    throw new ArgumentException("unequal length");

                animations = value;
            }
            get { return animations; }
        }

        private float[] probabilities = null;
        /// <summary>
        /// Probabilities of rolling an animation. Maps directly to
        /// AnimationNames (index-by-index). Use null for uniform law.
        /// </summary>
        public float[] Probabilities
        {
            set
            {
                if (animations != null && animations.Length != value.Length)
                    throw new ArgumentException("unequal length");

                probabilities = value;
            }
            get { return probabilities; }
        }

        public AnimationDiceRoller()
        {
        }

        public string Roll()
        {
            if (AnimationNames == null) return null;

            string newClip = null;

            if (this.Probabilities != null)
            {
                // Use user-defined probability law.

                float p = 0.0f;
                float coin = Random2.RandomF(0, 1);
                for (int i = 0; i < Probabilities.Length; i++)
                {
                    if (coin <= Probabilities[i] + p)
                    {
                        newClip = animations[i];
                        break;
                    }
                    p += Probabilities[i];
                }
            }
            else
            {
                // Use uniform law

                int i = Random2.Random(0, AnimationNames.Length - 1);
                newClip = animations[i];
            }

            return newClip;
        }
    }
}
