#region File Description
//-----------------------------------------------------------------------------
// AnimationPlayer.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Engine.Tools;

// kinect stuff:
using Microsoft.Kinect;
using Engine.Input;
#endregion

namespace Engine.Animation
{
    public enum AnimationPlay
    {
        Once,
        Loop
    }

    public enum AnimationBlend
    {
        None,
        Linear
    }

    /// <summary>
    /// The animation player is in charge of decoding bone position
    /// matrices from an animation clip.
    /// </summary>
    public class AnimationPlayer
    {
        #region Fields
        // Information about the currently playing animation clip.
        AnimationClip currentClipValue;
        float currentTimeValue;
        int currentKeyframe;
        AnimationPlay playType;
        private bool isDone = false;

        // Current animation transform matrices.
        Matrix[] boneTransforms;
        Matrix[] worldTransforms;
        Matrix[] skinTransforms;

        // kinectControlled[i] == true means that boneTransforms[i] has absolute rotation angles.
        bool[] kinectControlled;

        // Information about animation blending.
        Quaternion[] sourceBoneRotation;
        Vector3[] sourceBoneTranslation;
        Vector3[] sourceBoneScale;
        AnimationClip blendTargetClip;
        float blendDuration;

        // Backlink to the bind pose and skeleton hierarchy data.
        SkinningData skinningDataValue;
        #endregion

        /// <summary>
        /// Constructs a new animation player.
        /// </summary>
        public AnimationPlayer(SkinningData skinningData)
        {
            if (skinningData == null)
                throw new ArgumentNullException("skinningData");

            this.playType = AnimationPlay.Loop;
            this.Speed = 1.0f;
            this.Pause = false;
            this.Looped = false;

            this.skinningDataValue = skinningData;

            this.boneTransforms = new Matrix[skinningData.BindPose.Length];
            this.worldTransforms = new Matrix[skinningData.BindPose.Length];
            this.skinTransforms = new Matrix[skinningData.BindPose.Length];
            this.kinectControlled = new bool[skinningData.BindPose.Length];

            // Transform used when blending:
            this.sourceBoneRotation = new Quaternion[skinningData.BindPose.Length];
            this.sourceBoneTranslation = new Vector3[skinningData.BindPose.Length];
            this.sourceBoneScale = new Vector3[skinningData.BindPose.Length];

            // Init Transforms
            for (int i = 0; i < skinTransforms.Length; i++)
            {
                this.boneTransforms[i] = skinningData.BindPose[i];
            }
            UpdateWorldTransforms(Matrix.Identity);
            UpdateSkinTransforms();
        }

        #region Playing animation clips

        public AnimationClip this[string name]
        {
            get
            {
                return skinningDataValue.AnimationClips[name];
            }
        }

        /// <summary>
        /// Stop any animation immediately.
        /// </summary>
        public void Stop()
        {
        }

        /// <summary>
        /// Play a clip.
        /// </summary>
        /// <param name="clipname">Name of the clip to play.</param>
        /// <param name="playType">Loop animation or play it once then stop.</param>
        /// <param name="blendType">Type of blending to use.</param>
        /// <param name="blendDur">How long the blending should last for. Ignored if blendType = None</param>
        public void Play(string clipname, AnimationPlay playType, AnimationBlend blendType, float blendDur)
        {
            Play(this[clipname], playType, blendType, blendDur);
        }
        /// <summary>
        /// Play a clip.
        /// </summary>
        /// <param name="clip">Clip to play.</param>
        /// <param name="playType">Loop animation or play it once then stop.</param>
        /// <param name="blendType">Type of blending to use.</param>
        /// <param name="blendDur">How long the blending should last for. Ignored if blendType = None</param>
        public void Play(AnimationClip clip, AnimationPlay playType, AnimationBlend blendType, float blendDur)
        {
            if (blendType == AnimationBlend.None)
                StartClip(clip, playType);
            else if (blendType == AnimationBlend.Linear)
                BlendInto(clip, playType, blendDur);
        }

        private void initPlay(AnimationClip clip, AnimationPlay playtype)
        {
            /// don't use clip to set anything. We don't know if we're setting
            /// the blending target or the current animation. However, do check 
            /// that the value is not null.
            if (clip == null)
                throw new ArgumentNullException("clip");
            this.isDone = false;
            this.playType = playtype;
        }
        /// <summary>
        /// Starts decoding the specified animation clip.
        /// Transition directly into the animation without blending.
        /// <param name="clipname">Name of the clip to play</param>
        /// <param name="play">Loop animation or play it once then stop.</param>
        /// </summary>
        public void StartClip(string clipname, AnimationPlay play)
        {
            StartClip(this[clipname], play);
        }
        /// <summary>
        /// Starts decoding the specified animation clip.
        /// Transition directly into the animation without blending.
        /// <param name="clip">The clip to play</param>
        /// <param name="play">Loop animation or play it once then stop.</param>
        /// </summary>
        public void StartClip(AnimationClip clip, AnimationPlay play)
        {
            StartClip(clip, play, Matrix.Identity);
        }
        /// <summary>
        /// Starts decoding the specified animation clip.
        /// Transition directly into the animation without blending.
        /// <param name="clip">The clip to play</param>
        /// <param name="play">Loop animation or play it once then stop.</param>
        /// <param name="rootTransform">Root transform used to call the Update method.</param>
        /// </summary>
        public void StartClip(AnimationClip clip, AnimationPlay play, Matrix rootTransform)
        {
            initPlay(clip, play);

            // Set current animation values:
            currentClipValue = clip;
            currentTimeValue = 0.0f;
            currentKeyframe = 0;

            // Set blending animation values (no blending):
            blendTargetClip = null;
            blendDuration = 0.0f;

            // Initialize bone transforms to the bind pose.
            skinningDataValue.BindPose.CopyTo(boneTransforms, 0);

            Update(0, false, rootTransform);
        }

        /// <summary>
        /// Play an animation by blending the current animation into
        /// the new one over a time span.
        /// </summary>
        /// <param name="clipname">Name of the clip to blend into. non-null</param>
        /// <param name="play">Loop animation or play it once then stop.</param>
        /// <param name="duration">Duration of the blending in milliseconds.</param>
        public void BlendInto(string clipname, AnimationPlay play, float duration)
        {
            BlendInto(this[clipname], play, duration);
        }
        /// <summary>
        /// Play an animation by blending the current animation into
        /// the new one over a time span.
        /// </summary>
        /// <param name="clip">Clip to blend into. non-null</param>
        /// <param name="play">Loop animation or play it once then stop.</param>
        /// <param name="duration">Duration of the blending in milliseconds.</param>
        public void BlendInto(AnimationClip clip, AnimationPlay play, float duration)
        {
            initPlay(clip, play);

            // Set blending animation values:
            blendTargetClip = clip;
            blendDuration = duration;
            currentTimeValue = 0.0f;
            Looped = false;

            // Initialize bone transforms to the bind pose.
            for (int i = 0; i < boneTransforms.Length; i++)
            {
                boneTransforms[i].Decompose(out sourceBoneScale[i], out sourceBoneRotation[i], out sourceBoneTranslation[i]);
            }
        }

        public void PlayKinectStance(Skeleton skeleton, JointStringMapping mapping)
        {
            // Reset kinectControlled array:
            for (int k = 0; k < kinectControlled.Length; k++)
                kinectControlled[k] = false;

            // Calculate Absolute angles:
            for (int i = 0; i < mapping.Length; i++)
                ComputeKinectJoint(skeleton, mapping, (JointType)i);

            UpdateKinectWorldTransforms();
            UpdateSkinTransforms();
        }

        private void ComputeKinectJoint(Skeleton skeleton, JointStringMapping map, JointType joint)
        {
            // Get bone name and check if it's valid:
            string boneName = map[joint];
            if (boneName == null) return; // ignore this joint.

            // Get target joint:
            JointType? target = null;
            switch (joint)
            {
                // Right side of the body:

                case JointType.ShoulderRight:
                    target = JointType.ElbowRight;
                    break;

                case JointType.ElbowRight:
                    target = JointType.WristRight;
                    break;

                case JointType.HipRight:
                    target = JointType.KneeRight;
                    break;

                case JointType.KneeRight:
                    target = JointType.AnkleRight;
                    break;

                // Left side of the body:

                case JointType.ShoulderLeft:
                    target = JointType.ElbowLeft;
                    break;

                case JointType.ElbowLeft:
                    target = JointType.WristLeft;
                    break;

                case JointType.HipLeft:
                    target = JointType.KneeLeft;
                    break;

                case JointType.KneeLeft:
                    target = JointType.AnkleLeft;
                    break;

                default: break;
            }

            // Compute bone transform:
            if (target.HasValue)
            {
                int index = GetBoneIndex(boneName);

                Vector3 kinectAngles = ComputeBoneAbsoluteAngles(skeleton, joint, target.Value);
                Vector3 defaultAngles = map.GetNormal(joint);

                /*boneTransforms[index] = Matrix.CreateFromYawPitchRoll(
                    kinectAngles.Y - defaultAngles.Y,
                    0,
                    kinectAngles.X);*/
                boneTransforms[index] = Matrix.CreateFromYawPitchRoll(kinectAngles.Y - defaultAngles.Y, 0, kinectAngles.X);

                kinectControlled[index] = true;
            }
        }

        private Vector3 ComputeBoneAbsoluteAngles(Skeleton skeleton, JointType source, JointType target)
        {
            // Get the positions of the joints as Vector3s
            Vector3 sourcePos = Math2.ToVector3(skeleton.Joints[source].Position);
            Vector3 targetPos = Math2.ToVector3(skeleton.Joints[target].Position);

            // Get the pitch / yaw. (ignore roll. Cannot be detected)
            return Math2.AngleTo(sourcePos, targetPos);
        }

        #endregion

        #region Updating

        /// <summary>
        /// Advances the current animation position.
        /// <param name="time">Current time / time difference in milliseconds.</param>
        /// <param name="relativeToCurrentTime">If true, adds currentTime to time.</param>
        /// <param name="rootTransform">Transform of the character in the game.</param>
        /// <returns>False when the animation is done. Always true when PlayType = Loop,
        /// if no animation is playing, or when blending.</returns>
        /// </summary>
        public bool Update(float time, bool relativeToCurrentTime,
                           Matrix rootTransform)
        {
            // No animation:
            if (currentClipValue == null || isDone || Pause)
            {
                return true;
            }

            // No blending
            else if (blendTargetClip == null)
            {
                UpdateBoneTransforms(time, relativeToCurrentTime);
                UpdateWorldTransforms(rootTransform);
                UpdateSkinTransforms();

                return isDone;
            }

            // Blending:
            else
            {
                UpdateBlendedBoneTransforms(time, relativeToCurrentTime);
                UpdateWorldTransforms(rootTransform);
                UpdateSkinTransforms();
                return true;
            }
        }

        /// <summary>
        /// Update the blending animation
        /// </summary>
        /// <param name="time">Time in millis</param>
        /// <param name="relativeToCurrentTime">relative or not</param>
        internal void UpdateBlendedBoneTransforms(float time, bool relativeToCurrentTime)
        {
            if (relativeToCurrentTime) time += currentTimeValue;
            currentTimeValue = time;

            Looped = currentTimeValue > blendDuration;

            if (currentTimeValue > blendDuration)
            {
                // Done blending. Play the target clip.
                currentClipValue = blendTargetClip;
                blendTargetClip = null;

                while (currentTimeValue > blendDuration)
                    currentTimeValue -= blendDuration;

                UpdateBoneTransforms(0.0f, false);
                return;
            }

            // Still blending

            // Calc normalized time;
            float dur = blendDuration;
            float normalizedTime = (currentTimeValue / dur);

            // For debugging (remove this)
            if (normalizedTime < 0.0f || normalizedTime > 1.0f)
                throw new ArgumentException("normalizedTime");

            // Interpolate the source transforms with the target transforms:
            Quaternion r;
            Vector3 t, s;
            for (int i = 0; i < skinningDataValue.BindPose.Length; i++)
            {
                r = blendTargetClip.Keyframes[0].Parts[i].Orientation; // Target rotation
                t = blendTargetClip.Keyframes[0].Parts[i].Translation; // Target translation.
                s = blendTargetClip.Keyframes[0].Parts[i].Scale;       // Target scale.

                r = Quaternion.Slerp(sourceBoneRotation[i], r, normalizedTime);
                t = Vector3.Lerp(sourceBoneTranslation[i], t, normalizedTime);
                s = Vector3.Lerp(sourceBoneScale[i], s, normalizedTime);

                boneTransforms[i] =
                    Matrix.CreateScale(s) *
                    Matrix.CreateFromQuaternion(r) *
                    Matrix.CreateTranslation(t);
            }
        }

        /// <summary>
        /// Helper used by the Update method to refresh the BoneTransforms data.
        /// </summary>
        internal void UpdateBoneTransforms(float time, bool relativeToCurrentTime)
        {
            // Calculate the time:
            if (relativeToCurrentTime)
            {
                time += currentTimeValue;
            }

            Looped = time >= currentClipDuration;

            // If we reached the end. Stop!
            if (time >= currentClipDuration && playType == AnimationPlay.Once)
            {
                time = 0.0f;
                isDone = true;
                return;
            }

            // If we reached the end, loop back to the start.
            else while (time >= currentClipDuration)
                    time -= currentClipDuration;

            if ((time < 0.0f) || (time >= currentClipDuration))
                throw new ArgumentOutOfRangeException("time");

            currentTimeValue = time;

            // Get all keyframes
            IList<Keyframe> keyframes = currentClipValue.Keyframes;

            // Get keyframe index:
            float dur = currentClipDuration;
            float normalizedTime = (currentTimeValue / dur);
            int i = (int) (keyframes.Count * normalizedTime);

            // Normalize the time in the time span of [key1,key2]
            float split01 = 1.0f / keyframes.Count;
            float normalizedNormalized = (normalizedTime - i * split01) / split01;

            // Float approximations can cause normalizedNormalized to be <0 or >1
            // when normalizedTime and (i * split01) are suppose to be equal but aren't:
            if (normalizedNormalized < 0.0f) normalizedNormalized = 0.0f;
            if (normalizedNormalized > 1.0f) normalizedNormalized = 1.0f;

            if (i >= keyframes.Count - 1)
            {
                // Wrap i back to 0 to avoid an index exception:
                if (playType == AnimationPlay.Loop) i = 0;
                else { ApplyKeyframe(keyframes[i]); return; }
            }

            // Get the 2 keyframes that are currently begin interpolated:
            currentKeyframe = i;
            Keyframe key1 = keyframes[i];
            Keyframe key2 = keyframes[i + 1];

            // Interpolate the 2 keyframes:
            lerp(key1, key2, normalizedNormalized);
        }

        private void ApplyKeyframe(Keyframe whichFrame)
        {
            lerp(whichFrame, whichFrame, 0);
        }

        #region Lerping 2 keyframes
        private KeyframePart GetNextKeyframePart(int whichBone, Keyframe whichFrame)
        {
            int difference;
            return GetNextKeyframePart(whichBone, whichFrame, out difference);
        }
        private KeyframePart GetNextKeyframePart(int whichBone, Keyframe whichFrame, out int difference)
        {
            IList<Keyframe> keyframes = currentClipValue.Keyframes;

            int i = keyframes.IndexOf(whichFrame);

            difference = 1;
            int j = i + 1;
            while (true)
            {
                if(j == keyframes.Count) j = 0;
                
                Keyframe nextKey = keyframes[j];
                foreach (KeyframePart part in nextKey.Parts)
                {
                    if (part.Bone == whichBone)
                        return part;
                }
                difference++;
                j++;
            }
        }

        private KeyframePart GetPreviousKeyframePart(int whichBone, Keyframe whichFrame)
        {
            int difference = 1;
            return GetPreviousKeyframePart(whichBone, whichFrame, out difference);
        }
        private KeyframePart GetPreviousKeyframePart(int whichBone, Keyframe whichFrame, out int difference)
        {
            IList<Keyframe> keyframes = currentClipValue.Keyframes;

            int i = keyframes.IndexOf(whichFrame);

            difference = 1;
            int j = i - 1;
            while (true)
            {
                if (j == keyframes.Count) j = 0;

                Keyframe nextKey = keyframes[j];
                foreach (KeyframePart part in nextKey.Parts) 
                {
                    if (part.Bone == whichBone)
                        return part;
                }
                difference++;
                j--;
            }
        }

        private void lerp(Keyframe key1, Keyframe key2, float time)
        {
            KeyframePart p1, p2;

            // Indices for bones of key 1 and 2 respectively:
            int a = 0, b = 0;

            // Time for specific bone. This changes only if a bone is not found.
            float timeForThisBone;
            for (int i = 0;
                i < boneTransforms.Length && a < key1.Parts.Length && b < key2.Parts.Length;
                i++)
            {
                timeForThisBone = time;
                p1 = key1.Parts[a];
                p2 = key2.Parts[b];

                if (p1.Bone > p2.Bone)       // frame 1 has a missing bone. ditch this bone
                {
                    int diff;
                    p1 = GetPreviousKeyframePart(p2.Bone, key1, out diff);
                    float split01 = 1.0f / (diff + 1);
                    timeForThisBone *= split01;
                    timeForThisBone += diff * split01;

                    b++;
                }
                else if (p2.Bone > p1.Bone) // frame 2 has a missing bone. ditch this bone
                {
                    int diff;
                    p2 = GetNextKeyframePart(p1.Bone, key2, out diff);
                    float split01 = 1.0f / (diff + 1);
                    timeForThisBone *= split01;

                    a++;
                }
                else if (p2.Bone != i) // both frames have a missing bone! :O
                {
                    a++; b++;

                    int diff, diff1, diff2;
                    p1 = GetPreviousKeyframePart(i, key1, out diff1);
                    p2 = GetNextKeyframePart(i, key2, out diff2);
                    diff = diff1 + diff2;

                    float split01 = 1.0f / (diff + 1);
                    timeForThisBone *= split01;
                    timeForThisBone += diff1 * split01;

                    // Don't increment a and b. We need to come back to
                    // these bones later on.
                     
                }
                else 
                {
                    a++;b++;
                }

                // At this stage we have found 2 key frames for bone.

                // Validation (for debugging purposes).
                if (i != p1.Bone || i != p2.Bone)
                    throw new ArgumentException("interpolating the wrong bone");
                if (timeForThisBone < 0.0f || timeForThisBone > 1.0f)
                    throw new ArgumentException("time");

                Quaternion rot = Quaternion.Slerp(p1.Orientation, p2.Orientation, timeForThisBone);
                Vector3 pos = Vector3.Lerp(p1.Translation, p2.Translation, timeForThisBone);
                Vector3 scale = Vector3.Lerp(p1.Scale, p2.Scale, timeForThisBone);

                boneTransforms[i] =
                    Matrix.CreateScale(scale) *
                    Matrix.CreateFromQuaternion(rot) *
                    Matrix.CreateTranslation(pos);
            }
        }
        #endregion

        /// <summary>
        /// Helper used by the Update method to refresh the WorldTransforms data.
        /// </summary>
        internal void UpdateWorldTransforms(Matrix rootTransform)
        {
            // Root bone.
            worldTransforms[0] = boneTransforms[0] * rootTransform;

            // Child bones.
            for (int bone = 1; bone < worldTransforms.Length; bone++)
            {
                int parentBone = skinningDataValue.SkeletonHierarchy[bone];

                worldTransforms[bone] = boneTransforms[bone] *
                                             worldTransforms[parentBone];
            }
        }

        /// <summary>
        /// Similar to UpdateWorldTransforms, but only considers translations
        /// (Assumes boneTransforms rotation and scales are absolute).
        /// 
        /// Useful for PlayKinectStance() that calculates absolute rotations.
        /// </summary>
        private void UpdateKinectWorldTransforms()
        {
            // Root bone:
            worldTransforms[0] = boneTransforms[0];

            // Child bones:
            for (int bone = 1; bone < worldTransforms.Length; bone++)
            {
                int parentBone = skinningDataValue.SkeletonHierarchy[bone];

                if (kinectControlled[bone])
                {
                    Vector3 parentTranslation = worldTransforms[parentBone].Translation;
                    Vector3 thisTranslation = skinningDataValue.BindPose[bone].Translation;
                    Matrix translation = Matrix.CreateTranslation(parentTranslation + thisTranslation);
                    worldTransforms[bone] = boneTransforms[bone] * translation;
                }
                else
                {
                    worldTransforms[bone] = boneTransforms[bone] * worldTransforms[parentBone];
                }
            }
        }

        /// <summary>
        /// Helper used by the Update method to refresh the SkinTransforms data.
        /// </summary>
        internal void UpdateSkinTransforms()
        {
            for (int bone = 0; bone < skinTransforms.Length; bone++)
            {
                skinTransforms[bone] = skinningDataValue.InverseBindPose[bone] *
                                            worldTransforms[bone];
            }
        }

        #endregion

        public int GetBoneIndex(string boneName)
        {
            return skinningDataValue.BoneIndices[boneName];
        }

        /// <summary>
        /// Gets the current bone transform matrices, relative to their parent bones.
        /// </summary>
        public Matrix[] BoneTransforms
        {
            get
            {
                return boneTransforms;
            }
        }


        /// <summary>
        /// Gets the current bone transform matrices, in absolute format.
        /// </summary>
        public Matrix[] WorldTransforms
        {
            get
            {
                return worldTransforms;
            }
        }


        /// <summary>
        /// Gets the current bone transform matrices,
        /// relative to the skinning bind pose.
        /// </summary>
        public Matrix[] SkinTransforms
        {
            get
            {
                return skinTransforms;
            }
        }


        /// <summary>
        /// Gets the clip currently being decoded.
        /// </summary>
        public AnimationClip CurrentClip
        {
            get { return currentClipValue; }
        }

        /// <summary>
        /// Clip duration relative to the speed.
        /// </summary>
        private float currentClipDuration
        {
            get
            {
                return blendTargetClip != null ? 
                    blendDuration : 
                    currentClipValue.Duration / Speed;
            }
        }

        /// <summary>
        /// Returns a list of all the animations for this player.
        /// </summary>
        public Dictionary<string, AnimationClip> AnimationClips
        {
            get
            {
                if (skinningDataValue == null)
                    return null;

                return skinningDataValue.AnimationClips;
            }
        }

        /// <summary>
        /// Gets the current play position.
        /// </summary>
        public float CurrentTime
        {
            get { return currentTimeValue; }
        }

        /// <summary>
        /// Play speed of this Animation player (affects all animations).
        /// Does not affect the blending time.
        /// </summary>
        public float Speed { set; get; }

        /// <summary>
        /// Pauses animation update.
        /// </summary>
        public bool Pause { set; get; }

        /// <summary>
        /// Returns true whenever this frame loops back to frame 0 (or stops).
        /// </summary>
        public bool Looped { private set; get; }
    }
}
