﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;
using Engine.Tools;

namespace Engine.Graphics
{
    public abstract class PostProcessEffect : IDisposable
    {
        string name;

        public PostProcessEffect(string name)
        {
            Enabled = true;
            this.name = name;
        }

        public string Name { get { return name; } }

        public bool Enabled { set; get; }

        internal RenderTarget2D SceneRenderTarget { set; get; }

        internal abstract void Render(Texture2D screenTexture);

        public RenderTargetBinding DiffuseBinding { set; get; }
        public RenderTargetBinding DepthBinding { set; get; }
        public RenderTargetBinding NormalBinding { set; get; }

        internal virtual void RecreateRenderTargets()
        {

        }

        public virtual void Dispose()
        {
            
        }
    }
}
