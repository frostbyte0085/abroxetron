﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Engine.Core;
using Engine.Tools;
using Engine.Graphics;

using System.Diagnostics;

using Microsoft.Xna.Framework.Input;

namespace Engine.Graphics
{
    public sealed class PostProcessService : IDisposable
    {
        #region Variables
        GraphicsService gs;
        ContentService cs;

        RenderTarget2D targetRenderTo;
        RenderTarget2D targetRenderTo2;

        int width, height;

        //Dictionary<string, PostProcessEffect> effects;
        List<PostProcessEffect> effects;

        EmptyPostProcessEffect emptyEffect;

        #endregion

        internal PostProcessService()
        {
            gs = ServiceProvider.GetService<GraphicsService>();
            Debug.Assert(gs != null);

            cs = ServiceProvider.GetService<ContentService>();
            Debug.Assert(cs != null);

            effects = new List<PostProcessEffect>();

            gs.DeviceResetting += new EventHandler<EventArgs>(DeviceResetting);
            gs.DeviceReset += new EventHandler<EventArgs>(DeviceReset);

            emptyEffect = new EmptyPostProcessEffect("empty");

            AddPostProcessEffect(new GlowPostProcessEffect("glow"));
            
            //AddPostProcessEffect(new BloomPostProcessEffect("bloom", BloomPreset.Subtle));

            AddPostProcessEffect(new FXAAPostProcessEffect("fxaa"));
            //AddPostProcessEffect(new SepiaPostProcessEffect("sepia"));

            CreateRenderTarget();
        }

        void CreateRenderTarget()
        {
            PresentationParameters pp = gs.GraphicsDevice.PresentationParameters;
            width = pp.BackBufferWidth;
            height = pp.BackBufferHeight;

            SurfaceFormat format = pp.BackBufferFormat;
            targetRenderTo = new RenderTarget2D(gs.GraphicsDevice, width, height, false, format, pp.DepthStencilFormat);
            targetRenderTo2 = new RenderTarget2D(gs.GraphicsDevice, width, height, false, format, pp.DepthStencilFormat);

            foreach (PostProcessEffect efx in effects)
            {
                Debug.Assert(efx != null);
                efx.RecreateRenderTargets();
            }
        }

        public int Count
        {
            get
            {
                return effects.Count;
            }
        }

        void DeviceReset(object sender, EventArgs e)
        {
            CreateRenderTarget();
        }

        void DeviceResetting(object sender, EventArgs e)
        {
            
        }

        public void Clear()
        {
            foreach (PostProcessEffect efx in effects)
            {
                efx.Dispose();
            }
            effects.Clear();
        }

        public bool ContainsEffect(string name)
        {
            foreach (PostProcessEffect eff in effects)
            {
                if (name == eff.Name)
                    return true;
            }
            return false;
        }

        public bool ContainsEffect(PostProcessEffect effect)
        {
            return effects.Contains(effect);
        }

        public void AddPostProcessEffect(PostProcessEffect pfx)
        {
            Debug.Assert (ContainsEffect(pfx.Name) == false);

            effects.Add(pfx);
        }

        public void RemovePostProcessEffect(string name)
        {
            Debug.Assert(ContainsEffect(name));

            effects.Remove(GetEffect(name));
        }

        public PostProcessEffect GetEffect(string name)
        {
            PostProcessEffect e = null;
            foreach (PostProcessEffect eff in effects)
            {
                if (name == eff.Name)
                {
                    e = eff;
                    break;
                }   
            }
            return e;
        }

        public PostProcessEffect this[string name]
        {
            get
            {
                return GetEffect(name);
            }
        }

        internal void Begin()
        {
            gs.GraphicsDevice.SetRenderTarget(targetRenderTo);
            gs.GraphicsDevice.Clear(Color.Transparent);
        }

        internal void PostProcess(RenderTargetBinding diffuseBinding, RenderTargetBinding depthBinding, RenderTargetBinding normalBinding)
        {
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.None;

            int i = 0;
            foreach (PostProcessEffect effect in effects)
            {
                Texture2D textureRenderTo = null;

                gs.GraphicsDevice.SetRenderTarget(null);

                if (i % 2 == 0)
                {
                    textureRenderTo = targetRenderTo;
                }
                else
                {
                    textureRenderTo = targetRenderTo2;
                }

                if (i == effects.Count - 1)
                {
                    effect.SceneRenderTarget = null;
                }
                else
                {
                    if (i % 2 == 0)
                    {
                        effect.SceneRenderTarget = targetRenderTo2;
                    }
                    else
                    {
                        effect.SceneRenderTarget = targetRenderTo;
                    }
                }
                gs.GraphicsDevice.SetRenderTarget(effect.SceneRenderTarget);
                gs.GraphicsDevice.Clear(Color.Transparent);

                if (effect.Enabled)
                {
                    effect.DiffuseBinding = diffuseBinding;
                    effect.DepthBinding = depthBinding;
                    effect.NormalBinding = normalBinding;

                    effect.Render(textureRenderTo);

                }
                else
                    emptyEffect.Render(textureRenderTo);

                i++;
            }

            gs.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            
        }

        public void Dispose()
        {
            Clear();

            targetRenderTo.Dispose();
            targetRenderTo2.Dispose();
        }
    }
}
