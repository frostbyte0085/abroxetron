﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;
using Engine.Tools;

using System.Diagnostics;

namespace Engine.Graphics
{
    public enum BloomPreset
    {
        Default,
        Soft,
        Subtle,
        Saturated,
        Desaturated
    }

    public class BloomPostProcessEffect : PostProcessEffect
    {
        ContentService cs;
        GraphicsService gs;

        Effect bloomExtractEffect;
        Effect gaussianBlurEffect;
        Effect bloomCombineEffect;

        RenderTarget2D rt1, rt2;

        #region Bloom settings

        public float BloomThreshold { set; get; }
        public float BlurAmount { set; get; }
        public float BloomIntensity { set; get; }
        public float BaseIntensity { set; get; }
        public float BloomSaturation { set; get; }
        public float BaseSaturation { set; get; }

        #endregion

        public BloomPostProcessEffect(string name)
            : this(name, BloomPreset.Default)
        {
           
        }

        public BloomPostProcessEffect(string name, BloomPreset preset)
            : base(name)
        {
            cs = ServiceProvider.GetService<ContentService>();
            Debug.Assert(cs != null);

            gs = ServiceProvider.GetService<GraphicsService>();
            Debug.Assert(gs != null);

            bloomExtractEffect = cs["engine"].Load<Effect>("Effects/PostProcess/BloomExtract");
            bloomExtractEffect.CurrentTechnique = bloomExtractEffect.Techniques[0];
            gaussianBlurEffect = cs["engine"].Load<Effect>("Effects/PostProcess/GaussianBlur");
            gaussianBlurEffect.CurrentTechnique = gaussianBlurEffect.Techniques[0];
            bloomCombineEffect = cs["engine"].Load<Effect>("Effects/PostProcess/BloomCombine");
            bloomCombineEffect.CurrentTechnique = bloomCombineEffect.Techniques[0];

            RecreateRenderTargets();

            switch (preset)
            {
                case BloomPreset.Default:
                    SetDefaultPreset();
                    break;

                case BloomPreset.Desaturated:
                    SetDesaturatedPreset();
                    break;

                case BloomPreset.Saturated:
                    SetSaturatedPreset();
                    break;

                case BloomPreset.Soft:
                    SetSoftPreset();
                    break;

                case BloomPreset.Subtle:
                    SetSubtlePreset();
                    break;

                default: break;
            }
        }

        public void SetDefaultPreset()
        {
            BloomThreshold = 0.25f;
            BlurAmount = 4;
            BloomIntensity = 1.25f;
            BaseIntensity = 1.0f;
            BloomSaturation = 1.0f;
            BaseSaturation = 1.0f;
        }

        public void SetSoftPreset()
        {
            BloomThreshold = 0;
            BlurAmount = 3;
            BloomIntensity = 1;
            BaseIntensity = 1;
            BloomSaturation = 1;
            BaseSaturation = 1;
        }

        public void SetDesaturatedPreset()
        {
            BloomThreshold = 0.5f;
            BlurAmount = 8;
            BloomIntensity = 2;
            BaseIntensity = 1;
            BloomSaturation = 0;
            BaseSaturation = 1;
        }

        public void SetSaturatedPreset()
        {
            BloomThreshold = 0.25f;
            BlurAmount = 4;
            BloomIntensity = 2;
            BaseIntensity = 1;
            BloomSaturation = 2;
            BaseSaturation = 0;
        }

        public void SetSubtlePreset()
        {
            BloomThreshold = 0.5f;
            BlurAmount = 2;
            BloomIntensity = 1;
            BaseIntensity = 1;
            BloomSaturation = 1;
            BaseSaturation = 1;
        }

        internal override void RecreateRenderTargets()
        {
            PresentationParameters pp = gs.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth/2;
            int height = pp.BackBufferHeight/2;
            
            rt1 = new RenderTarget2D(gs.GraphicsDevice, width, height, false, pp.BackBufferFormat, DepthFormat.None);
            rt2 = new RenderTarget2D(gs.GraphicsDevice, width, height, false, pp.BackBufferFormat, DepthFormat.None);
        }

        internal override void Render(Texture2D screenTexture)
        {
            gs.GraphicsDevice.SamplerStates[1] = SamplerState.LinearClamp;
            // Pass 1: draw the scene into rendertarget 1, using a
            // shader that extracts only the brightest parts of the image.
            bloomExtractEffect.Parameters["BloomThreshold"].SetValue(BloomThreshold);

            SpriteHelper.Draw(screenTexture, rt1, bloomExtractEffect);

            // Pass 2: draw from rendertarget 1 into rendertarget 2,
            // using a shader to apply a horizontal gaussian blur filter.
            SetBlurEffectParameters(1.0f / (float)rt1.Width, 0);

            SpriteHelper.Draw(rt1, rt2, gaussianBlurEffect);

            // Pass 3: draw from rendertarget 2 back into rendertarget 1,
            // using a shader to apply a vertical gaussian blur filter.
            SetBlurEffectParameters(0, 1.0f / (float)rt1.Height);

            SpriteHelper.Draw(rt2, rt1, gaussianBlurEffect);

            // Pass 4: draw both rendertarget 1 and the original scene
            // image back into the main backbuffer, using a shader that
            // combines them to produce the final bloomed result.
            gs.GraphicsDevice.SetRenderTarget(SceneRenderTarget);

            EffectParameterCollection parameters = bloomCombineEffect.Parameters;

            parameters["BloomIntensity"].SetValue(BloomIntensity);
            parameters["BaseIntensity"].SetValue(BaseIntensity);
            parameters["BloomSaturation"].SetValue(BloomSaturation);
            parameters["BaseSaturation"].SetValue(BaseSaturation);

            gs.GraphicsDevice.Textures[1] = screenTexture;
            Viewport viewport = gs.GraphicsDevice.Viewport;

            SpriteHelper.Draw(rt1, viewport.Width, viewport.Height, bloomCombineEffect);

        }

        public override void Dispose()
        {
            rt1.Dispose();
            rt2.Dispose();

            base.Dispose();
        }

        /// <summary>
        /// Computes sample weightings and texture coordinate offsets
        /// for one pass of a separable gaussian blur filter.
        /// </summary>
        void SetBlurEffectParameters(float dx, float dy)
        {
            // Look up the sample weight and offset effect parameters.
            EffectParameter weightsParameter, offsetsParameter;

            weightsParameter = gaussianBlurEffect.Parameters["SampleWeights"];
            offsetsParameter = gaussianBlurEffect.Parameters["SampleOffsets"];

            // Look up how many samples our gaussian blur effect supports.
            int sampleCount = weightsParameter.Elements.Count;

            // Create temporary arrays for computing our filter settings.
            float[] sampleWeights = new float[sampleCount];
            Vector2[] sampleOffsets = new Vector2[sampleCount];

            // The first sample always has a zero offset.
            sampleWeights[0] = ComputeGaussian(0);
            sampleOffsets[0] = new Vector2(0);

            // Maintain a sum of all the weighting values.
            float totalWeights = sampleWeights[0];

            // Add pairs of additional sample taps, positioned
            // along a line in both directions from the center.
            for (int i = 0; i < sampleCount / 2; i++)
            {
                // Store weights for the positive and negative taps.
                float weight = ComputeGaussian(i + 1);

                sampleWeights[i * 2 + 1] = weight;
                sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                // To get the maximum amount of blurring from a limited number of
                // pixel shader samples, we take advantage of the bilinear filtering
                // hardware inside the texture fetch unit. If we position our texture
                // coordinates exactly halfway between two texels, the filtering unit
                // will average them for us, giving two samples for the price of one.
                // This allows us to step in units of two texels per sample, rather
                // than just one at a time. The 1.5 offset kicks things off by
                // positioning us nicely in between two texels.
                float sampleOffset = i * 2 + 1.5f;

                Vector2 delta = new Vector2(dx, dy) * sampleOffset;

                // Store texture coordinate offsets for the positive and negative taps.
                sampleOffsets[i * 2 + 1] = delta;
                sampleOffsets[i * 2 + 2] = -delta;
            }

            // Normalize the list of sample weightings, so they will always sum to one.
            for (int i = 0; i < sampleWeights.Length; i++)
            {
                sampleWeights[i] /= totalWeights;
            }

            // Tell the effect about our new filter settings.
            weightsParameter.SetValue(sampleWeights);
            offsetsParameter.SetValue(sampleOffsets);
        }


        /// <summary>
        /// Evaluates a single point on the gaussian falloff curve.
        /// Used for setting up the blur filter weightings.
        /// </summary>
        float ComputeGaussian(float n)
        {
            float theta = BlurAmount;

            return (float)((1.0 / Math.Sqrt(2 * Math.PI * theta)) *
                           Math.Exp(-(n * n) / (2 * theta * theta)));
        }
    }
}
