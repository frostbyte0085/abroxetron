﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;
using Engine.Tools;

using System.Diagnostics;

namespace Engine.Graphics
{
    public class GlowPostProcessEffect : PostProcessEffect
    {
        ContentService cs;
        GraphicsService gs;

        Effect gaussianBlurEffect;
        Effect glowCombineEffect;
        Effect glowDownsampleEffect;

        RenderTarget2D rt1, rt2;

        public GlowPostProcessEffect(string name)
            : base(name)
        {
            cs = ServiceProvider.GetService<ContentService>();
            Debug.Assert(cs != null);

            gs = ServiceProvider.GetService<GraphicsService>();
            Debug.Assert(gs != null);

            glowDownsampleEffect = cs["engine"].Load<Effect>("Effects/PostProcess/GlowDownsample");
            glowDownsampleEffect.CurrentTechnique = glowDownsampleEffect.Techniques[0];
            gaussianBlurEffect = cs["engine"].Load<Effect>("Effects/PostProcess/GaussianBlur");
            gaussianBlurEffect.CurrentTechnique = gaussianBlurEffect.Techniques[0];
            glowCombineEffect = cs["engine"].Load<Effect>("Effects/PostProcess/GlowCombine");
            glowCombineEffect.CurrentTechnique = glowCombineEffect.Techniques[0];

            RecreateRenderTargets();
        }

        internal override void RecreateRenderTargets()
        {
            PresentationParameters pp = gs.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth/4;
            int height = pp.BackBufferHeight/4;
            
            rt1 = new RenderTarget2D(gs.GraphicsDevice, width, height, false, pp.BackBufferFormat, DepthFormat.None);
            rt2 = new RenderTarget2D(gs.GraphicsDevice, width, height, false, pp.BackBufferFormat, DepthFormat.None);
        }

        internal override void Render(Texture2D screenTexture)
        {
            gs.GraphicsDevice.Textures[2] = DiffuseBinding.RenderTarget;

            gs.GraphicsDevice.SamplerStates[2] = SamplerState.PointClamp;
            gs.GraphicsDevice.SamplerStates[1] = SamplerState.LinearClamp;

            // 1: downsample
            SpriteHelper.Draw(screenTexture, rt1, glowDownsampleEffect);

            SetBlurEffectParameters(1.0f / (float)rt1.Width, 0);

            // 2: horizontal blur
            SpriteHelper.Draw(rt1, rt2, gaussianBlurEffect);

            SetBlurEffectParameters(0, 1.0f / (float)rt1.Height);

            // 3: vertical blur
            SpriteHelper.Draw(rt2, rt1, gaussianBlurEffect);

            gs.GraphicsDevice.SetRenderTarget(SceneRenderTarget);

            EffectParameterCollection parameters = glowCombineEffect.Parameters;

            gs.GraphicsDevice.Textures[1] = screenTexture;
            Viewport viewport = gs.GraphicsDevice.Viewport;

            SpriteHelper.Draw(rt1, viewport.Width, viewport.Height, glowCombineEffect);

        }

        public override void Dispose()
        {
            rt1.Dispose();
            rt2.Dispose();

            base.Dispose();
        }

        /// <summary>
        /// Computes sample weightings and texture coordinate offsets
        /// for one pass of a separable gaussian blur filter.
        /// </summary>
        void SetBlurEffectParameters(float dx, float dy)
        {
            // Look up the sample weight and offset effect parameters.
            EffectParameter weightsParameter, offsetsParameter;

            weightsParameter = gaussianBlurEffect.Parameters["SampleWeights"];
            offsetsParameter = gaussianBlurEffect.Parameters["SampleOffsets"];

            // Look up how many samples our gaussian blur effect supports.
            int sampleCount = weightsParameter.Elements.Count;

            // Create temporary arrays for computing our filter settings.
            float[] sampleWeights = new float[sampleCount];
            Vector2[] sampleOffsets = new Vector2[sampleCount];

            // The first sample always has a zero offset.
            sampleWeights[0] = ComputeGaussian(0);
            sampleOffsets[0] = new Vector2(0);

            // Maintain a sum of all the weighting values.
            float totalWeights = sampleWeights[0];

            // Add pairs of additional sample taps, positioned
            // along a line in both directions from the center.
            for (int i = 0; i < sampleCount / 2; i++)
            {
                // Store weights for the positive and negative taps.
                float weight = ComputeGaussian(i + 1);

                sampleWeights[i * 2 + 1] = weight;
                sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                // To get the maximum amount of blurring from a limited number of
                // pixel shader samples, we take advantage of the bilinear filtering
                // hardware inside the texture fetch unit. If we position our texture
                // coordinates exactly halfway between two texels, the filtering unit
                // will average them for us, giving two samples for the price of one.
                // This allows us to step in units of two texels per sample, rather
                // than just one at a time. The 1.5 offset kicks things off by
                // positioning us nicely in between two texels.
                float sampleOffset = i * 2 + 1.5f;

                Vector2 delta = new Vector2(dx, dy) * sampleOffset;

                // Store texture coordinate offsets for the positive and negative taps.
                sampleOffsets[i * 2 + 1] = delta;
                sampleOffsets[i * 2 + 2] = -delta;
            }

            // Normalize the list of sample weightings, so they will always sum to one.
            for (int i = 0; i < sampleWeights.Length; i++)
            {
                sampleWeights[i] /= totalWeights;
            }

            // Tell the effect about our new filter settings.
            weightsParameter.SetValue(sampleWeights);
            offsetsParameter.SetValue(sampleOffsets);
        }


        /// <summary>
        /// Evaluates a single point on the gaussian falloff curve.
        /// Used for setting up the blur filter weightings.
        /// </summary>
        float ComputeGaussian(float n)
        {
            float theta = 0.45f;

            return (float)((1.0 / Math.Sqrt(2 * Math.PI * theta)) *
                           Math.Exp(-(n * n) / (2 * theta * theta)));
        }
    }
}
