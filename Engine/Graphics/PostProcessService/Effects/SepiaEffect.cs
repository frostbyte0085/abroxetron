﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;
using Engine.Tools;

using System.Diagnostics;

namespace Engine.Graphics
{
    public class SepiaPostProcessEffect : PostProcessEffect
    {
        ContentService cs;
        GraphicsService gs;
        Effect effect;

        public SepiaPostProcessEffect(string name) : base(name)
        {
            cs = ServiceProvider.GetService<ContentService>();
            Debug.Assert(cs != null);

            gs = ServiceProvider.GetService<GraphicsService>();
            Debug.Assert(gs != null);

            effect = cs["engine"].Load<Effect>("Effects/PostProcess/Sepia");
            effect.CurrentTechnique = effect.Techniques[0];
        }
        
        internal override void Render(Texture2D screenTexture)
        {
            SpriteHelper.Draw(screenTexture, effect);
        }
    }
}
