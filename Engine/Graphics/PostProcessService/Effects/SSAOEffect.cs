﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;
using Engine.Tools;
using Engine.Scene;
using Engine.Core;

using System.Diagnostics;

namespace Engine.Graphics
{
    public class SSAOPostProcessEffect : PostProcessEffect
    {
        ContentService cs;
        GraphicsService gs;
        SceneService ss;

        Effect ssaoEffect;
        Effect ssaoBlurEffect;
        Effect ssaoCombineEffect;

        RenderTarget2D ssaoRT, blurRT;
        Texture2D randomNormalsTexture;

        FullScreenQuad quad;

        public float SampleRadius { set; get; }

        public float DistanceScale { set; get; }

        public SSAOPostProcessEffect()
            : base()
        {
            cs = ServiceProvider.GetService<ContentService>();
            Debug.Assert(cs != null);

            gs = ServiceProvider.GetService<GraphicsService>();
            Debug.Assert(gs != null);

            ss = ServiceProvider.GetService<SceneService>();
            Debug.Assert(ss != null);

            ssaoEffect = cs["engine"].Load<Effect>("Effects/PostProcess/SSAO");
            ssaoEffect.CurrentTechnique = ssaoEffect.Techniques[0];

            ssaoBlurEffect = cs["engine"].Load<Effect>("Effects/PostProcess/SSAOBlur");
            ssaoBlurEffect.CurrentTechnique = ssaoBlurEffect.Techniques[0];

            ssaoCombineEffect = cs["engine"].Load<Effect>("Effects/PostProcess/SSAOCombine");
            ssaoCombineEffect.CurrentTechnique = ssaoCombineEffect.Techniques[0];

            randomNormalsTexture = cs["engine"].Load<Texture2D>("Textures/RandomNormals");

            quad = new FullScreenQuad();

            RecreateRenderTargets();

            DistanceScale = 30000;
            SampleRadius = 7;
        }

        internal override void RecreateRenderTargets()
        {
            PresentationParameters pp = gs.GraphicsDevice.PresentationParameters;
            int width = pp.BackBufferWidth/2;
            int height = pp.BackBufferHeight/2;

            ssaoRT = new RenderTarget2D(gs.GraphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None);
            blurRT = new RenderTarget2D(gs.GraphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None);
        }

        internal override void Render(SpriteBatch batch, Texture2D screenTexture)
        {
            quad.SetBuffers();

            Camera camera = ss.ActiveCameraNode.Camera;

            gs.GraphicsDevice.SetRenderTarget(ssaoRT);

            //Clear
            gs.GraphicsDevice.Clear(Color.White);

            // SSAO
            //Set Samplers
            gs.GraphicsDevice.Textures[1] = NormalBinding.RenderTarget;
            gs.GraphicsDevice.SamplerStates[1] = SamplerState.LinearClamp;

            gs.GraphicsDevice.Textures[2] = NormalBinding.RenderTarget;
            gs.GraphicsDevice.SamplerStates[2] = SamplerState.PointClamp;

            gs.GraphicsDevice.Textures[3] = randomNormalsTexture;
            gs.GraphicsDevice.SamplerStates[3] = SamplerState.LinearWrap;

            //Calculate Frustum Corner of the Camera
            Vector3 cornerFrustum = Vector3.Zero;
            cornerFrustum.Y = (float)Math.Tan(Math.PI / 3.0 / 2.0) * camera.ClipPlanes.Y;
            cornerFrustum.X = cornerFrustum.Y * camera.AspectRatio;
            cornerFrustum.Z = camera.ClipPlanes.Y;

            //Set SSAO parameters
            ssaoEffect.Parameters["Projection"].SetValue(camera.ProjectionMatrix);
            ssaoEffect.Parameters["cornerFustrum"].SetValue(cornerFrustum);
            ssaoEffect.Parameters["sampleRadius"].SetValue(SampleRadius);
            ssaoEffect.Parameters["distanceScale"].SetValue(DistanceScale);
            ssaoEffect.Parameters["GBufferTextureSize"].SetValue(new Vector2(ssaoRT.Width, ssaoRT.Height));


            ssaoEffect.CurrentTechnique.Passes[0].Apply();
            quad.Render();

            // BLUR
            gs.GraphicsDevice.SetRenderTarget(blurRT);

            //Clear
            gs.GraphicsDevice.Clear(Color.White);

            //Set Samplers, GBuffer was set before so no need to reset...
            gs.GraphicsDevice.Textures[3] = ssaoRT;
            gs.GraphicsDevice.SamplerStates[3] = SamplerState.LinearClamp;

            //Set SSAO parameters
            ssaoBlurEffect.Parameters["blurDirection"].SetValue(Vector2.One);
            ssaoBlurEffect.Parameters["targetSize"].SetValue(new Vector2(ssaoRT.Width, ssaoRT.Height));

            ssaoBlurEffect.CurrentTechnique.Passes[0].Apply();

            quad.Render();

            // COMBINE
            gs.GraphicsDevice.SetRenderTarget(SceneRenderTarget);
            gs.GraphicsDevice.Clear(Color.Transparent);

            //Set Samplers
            gs.GraphicsDevice.Textures[0] = screenTexture;
            gs.GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            bool useBlurredSSAO = true;

            if (useBlurredSSAO)
                gs.GraphicsDevice.Textures[1] = blurRT;
            else 
                gs.GraphicsDevice.Textures[1] = ssaoRT;
            gs.GraphicsDevice.SamplerStates[1] = SamplerState.LinearClamp;

            //Set Effect Parameters
            ssaoCombineEffect.Parameters["targetSize"].SetValue(new Vector2(ssaoRT.Width, ssaoRT.Height));

            //Apply
            ssaoCombineEffect.CurrentTechnique.Passes[0].Apply();

            quad.Render();

        }
    }
}
