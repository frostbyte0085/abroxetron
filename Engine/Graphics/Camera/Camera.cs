﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    public enum CameraProjection
    {
        Orthographic,
        Perspective
    }

    public sealed class Camera
    {
        #region Variables
        Matrix projectionMatrix;

        #endregion

        public Camera()
        {

        }

        public void SetupPerspective(float fieldOfViewDegrees, float aspectRatio, float nearClipPlane, float farClipPlane)
        {
            ProjectionType = CameraProjection.Perspective;

            FieldOfView = fieldOfViewDegrees;
            AspectRatio = aspectRatio;
            ClipPlanes = new Vector2(nearClipPlane, farClipPlane);

            MakeProjectionMatrix();
        }

        public void SetupOrthographic(float width, float height, float nearClipPlane, float farClipPlane)
        {
            ProjectionType = CameraProjection.Orthographic;

            Width = width;
            Height = height;
            ClipPlanes = new Vector2(nearClipPlane, farClipPlane);

            MakeProjectionMatrix();
        }

        public void MakeProjectionMatrix()
        {
            if (ProjectionType == CameraProjection.Orthographic)
            {
                Matrix.CreateOrthographic(Width, Height, ClipPlanes.X, ClipPlanes.Y, out projectionMatrix);
            }
            else
            {
                Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(FieldOfView),
                        AspectRatio, ClipPlanes.X, ClipPlanes.Y, out projectionMatrix);
            }
        }

        public CameraProjection ProjectionType
        {
            set;
            get;
        }

        public Matrix ProjectionMatrix
        {
            get
            {
                return projectionMatrix;
            }
        }

        public float FieldOfView
        {
            set;
            get;
        }

        public float AspectRatio
        {
            set;
            get;
        }

        public Vector2 ClipPlanes
        {
            set;
            get;
        }

        public float Width
        {
            set;
            get;
        }

        public float Height
        {
            set;
            get;
        }
    }
}
