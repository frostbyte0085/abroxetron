﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    internal struct PointSpriteVertex
    {
        /// <summary>
        /// Center of the point. Assure that this value is the same for each vertex
        /// of the point sprite.
        /// </summary>
        internal Vector3 Position;
        /// <summary>
        /// Texture coord of the vertex. This value is used along with the camera pos
        /// & view to calculate the actual position of the triangle
        /// </summary>
        internal Vector2 TexCoord;
        /// <summary>
        /// Current color of the particle.
        /// </summary>
        internal Vector4 Color;
        /// <summary>
        /// x: Scaling value
        /// y: Rotation angle (along to camera view axis)
        /// </summary>
        internal Vector2 Transform;

        internal PointSpriteVertex(Vector3 pos, Vector2 texco, Color c, float scale, float rot)
        {
            Position = pos;
            TexCoord = texco;
            Color = c.ToVector4();
            Transform = new Vector2(scale, rot);
        }

        internal readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * 3, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(sizeof(float) * 5, VertexElementFormat.Vector4, VertexElementUsage.Color, 0),
            new VertexElement(sizeof(float) * 9, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1)
        );
    }
}
