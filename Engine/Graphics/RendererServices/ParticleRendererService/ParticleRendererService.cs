﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Engine.Graphics;

namespace Engine.Graphics
{
    public sealed class ParticleRendererService : IRendererService, IDisposable
    {
        #region Variables

        private SceneService ss;
        private GraphicsService gs;
        private BlendState blendState;
        private Effect effect;

        private Matrix world;
        private Matrix view;
        private Matrix projection;
        private Vector3 campos;
        private Vector3 camup;

        private List<ParticleSystem> particleSystems;
        #endregion

        #region Contructors
        internal ParticleRendererService()
        {
            ThreadSafeContentManager cm = ServiceProvider.GetService<ContentService>()["engine"];
            gs = ServiceProvider.GetService<GraphicsService>();
            ss = ServiceProvider.GetService<SceneService>();

            blendState = BlendState.Additive;

            effect = cm.Load<Effect>("Effects/Particle");
            effect.CurrentTechnique = effect.Techniques[0];

            particleSystems = new List<ParticleSystem>();
        }
        #endregion

        #region IRendererService
        public ParticleSystem MyTestSystem
        {
            set
            {
                particleSystems.Clear();
                particleSystems.Add(value);
            }
            get
            {
                return particleSystems[0];
            }
        }

        public void Fetch() 
        {
            // Update global matrices and vectors
            SceneNode c = ss.ActiveCameraNode;
            world = c.WorldMatrix;
            view = Matrix.Invert(world);
            projection = c.Camera.ProjectionMatrix;
            campos = c.Translation;
            camup = c.Up;

            List<SceneNode> nodes = ss.GetSceneNodesWithParticleSystems();
            particleSystems.Clear();

            foreach (SceneNode node in nodes)
                foreach (ParticleSystem ps in node.ParticleSystems)
                    if(!ps.FrustumCulled)
                        particleSystems.Add(ps);

            /*
            if (particleSystems.Count == 0)
                return;

            // this.particleSystems = whatever
            if(MyTestSystem.IsEmitting) 
                MyTestSystem.Update();
             */
        }

        public void Render() 
        {
            // get the depth buffer from the deferred pass
            ModelRendererService mdr = ServiceProvider.GetService<ModelRendererService>();
            DeferredProcessor defProc = mdr.DeferredProcessor;

            PushRendererState();

            Fetch();

            //VertexPositionTexture[] verts;
            PointSpriteVertex[] verts;
            int[] indices;

            foreach (ParticleSystem ps in particleSystems)
            {
                int nparts = ps.Particles.Count;
                if (nparts == 0) continue;

                ParticleAnimation pa = ps.Animation;
                verts = Particle.GetVertexArray(nparts * 4);
                indices = new int[nparts * 6];
                int i = 0;
                int j = 0;
                int indexBase = 0;

                foreach (Particle p in ps.Particles)
                {
                    float sz = p.Size;
                    float now = GameTiming.TotalTimeElapsed;

                    Vector3 myPos = ps.GetParticleGlobalPosition(p);

                    PointSpriteVertex[] pverts = p.Vertices;

                    verts[i++] = pverts[0];
                    verts[i++] = pverts[1];
                    verts[i++] = pverts[2];
                    verts[i++] = pverts[3];

                    indices[j++] = (indexBase * 4) + 0;
                    indices[j++] = (indexBase * 4) + 1;
                    indices[j++] = (indexBase * 4) + 2;
                    indices[j++] = (indexBase * 4) + 2;
                    indices[j++] = (indexBase * 4) + 3;
                    indices[j++] = (indexBase * 4) + 0;

                    indexBase++;
                }

                effect.CurrentTechnique = effect.Techniques["Default"];
                effect.Parameters["xWorld"].SetValue(Matrix.Identity);
                effect.Parameters["xProjection"].SetValue(projection);
                effect.Parameters["xView"].SetValue(view);
                effect.Parameters["xCamPos"].SetValue(campos);
                effect.Parameters["xTexture"].SetValue(pa.Texture);
                effect.Parameters["xCamUp"].SetValue(camup);
                effect.Parameters["xDepthBufferSize"].SetValue(defProc.GBufferSize);
                effect.Parameters["xDepth"].SetValue(defProc.DepthBufferBinding.RenderTarget as Texture2D);
                effect.CurrentTechnique.Passes[0].Apply();

                gs.GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verts, 0, nparts*4,
                    indices, 0, nparts*2, PointSpriteVertex.VertexDeclaration);
            }
        }

        public void PushRendererState()
        {
            gs.GraphicsDevice.BlendState = blendState;//BlendState.Additive;
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            gs.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
        }

        public void PopRendererState()
        {
            gs.GraphicsDevice.BlendState = BlendState.Opaque;
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        }

        #endregion

        #region IDisposable
        public void Dispose() { }
        #endregion
    }
}
