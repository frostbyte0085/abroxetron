﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    /// <summary>
    /// This class is what converts virtual coordinates into physical coords.
    /// </summary>
    internal class UIPainter
    {
        #region Properties
        internal SpriteBatch SpriteBatch { set; get; }
        internal Point VirtualResolution { set; get; }
        internal UIAtlas UIAtlas { set; get; }
        
        internal float TextScale { 
            get 
            { 
                return ((float) this.SpriteBatch.GraphicsDevice.Viewport.Width)
                           / ((float) VirtualResolution.X); 
            } 
        }
        #endregion

        #region Drawing
        /// <summary>
        /// Draw from the UIAtlas used
        /// </summary>
        /// <param name="key">Which element to draw</param>
        /// <param name="destination">Destination on the screen in virtual coords</param>
        internal void DrawFromAtlas(string key, Rectangle destination)
        {
            // TODO transformation
            UIElementPainter painter = this.UIAtlas.GetPainter(key);
            painter.DrawTexture(this, this.UIAtlas.Texture, ToPhysicalCoordinates(destination));
        }

        /// <summary>
        /// Draw text with rotation 0.
        /// </summary>
        /// <param name="font">Font to use</param>
        /// <param name="Text">Text to display</param>
        /// <param name="color">Color of the text</param>
        /// <param name="destination">The rectangle of the widget (in virtual coords)</param>
        internal void DrawText(SpriteFont font, string text, Color color, Rectangle destination)
        {
            DrawText(font, text, color, 0.0f, destination);
        }

        /// <summary>
        /// Draw text.
        /// </summary>
        /// <param name="font">Font to use</param>
        /// <param name="Text">Text to display</param>
        /// <param name="color">Color of the text</param>
        /// <param name="rotation">Rotation of the text in radians</param>
        /// <param name="destination">The rectangle of the widget (in virtual coords)</param>
        internal void DrawText(SpriteFont font, string Text, Color color, float rotation, Rectangle destination)
        {
            // Calculate center of this widget and length of the string.
            Vector2 length = font.MeasureString(Text);
            Point center = destination.Center;

            // Calculate the rectangle of the string.
            Vector2 topLeft = new Vector2();
            topLeft.X = center.X - (int)Math.Round(length.X / 2.0f);
            topLeft.Y = center.Y - (int)Math.Round(length.Y / 2.0f);

            // Paint:
            topLeft = ToPhysicalCoordinates(topLeft);
            this.SpriteBatch.DrawString(font, Text, topLeft, color, rotation, Vector2.Zero, TextScale, SpriteEffects.None, 0.0f);
        }

        /// <summary>
        /// Draw a texture.
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="destination">Where to draw in on the screen (virtual res)</param>
        /// <param name="color">Tinting color. Use Color.White for no tinting.</param>
        internal void DrawTexture(Texture2D texture, Rectangle destination, Color color)
        {
            destination = ToPhysicalCoordinates(destination);
            this.SpriteBatch.Draw(texture, destination, color);
        }

        /// <summary>
        /// Draw a texture.
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="destination">Where to draw in on the screen (virtual res)</param>
        /// <param name="source">Which part of the texture to render.</param>
        /// <param name="color">Tinting color. Use Color.White for no tinting.</param>
        internal void DrawTexture(Texture2D texture, Rectangle destination, Rectangle source, Color color)
        {
            destination = ToPhysicalCoordinates(destination);
            this.SpriteBatch.Draw(texture, destination, source, color);
        }

        /// <summary>
        /// Draw a texture.
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="destination">Where to draw in on the screen (virtual res)</param>
        /// <param name="source">Which part of the texture to render.</param>
        /// <param name="color">Tinting color. Use Color.White for no tinting.</param>
        /// <param name="rotation">Rotation in radians about center.</param>
        internal void DrawTexture(Texture2D texture, Rectangle destination, Rectangle source, Color color, float rotation)
        {
            destination = ToPhysicalCoordinates(destination);

            // Calc origin:
            float x = (texture.Width / 2.0f);
            float y = (texture.Height / 2.0f);
            Vector2 o = new Vector2(x, y);

            // Calc offset (based on x,y scale):
            float kx = ((float)destination.Width) / ((float)texture.Width);
            float ky = ((float)destination.Height) / ((float)texture.Height);
            destination.X += (int)(x * kx + 0.5f);
            destination.Y += (int)(y * ky + 0.5f);

            // Draw:
            this.SpriteBatch.Draw(texture, destination, source, color, rotation, o, SpriteEffects.None, 0);
        }
        #endregion

        #region Virtual / Physical coordinates transformation
        private Rectangle scaleRect(ref Rectangle rect, int nw, int dw, int nh, int dh)
        {
            float cw = (float)nw / (float)dw;
            float ch = (float)nh / (float)dh;

            rect.X = (int)(rect.X * cw);
            rect.Width = (int)(rect.Width * cw);

            rect.Y = (int)(rect.Y * ch);
            rect.Height = (int)(rect.Height * ch);

            return rect;
        }

        private Vector2 ToPhysicalCoordinates(Vector2 virtualPoint)
        {
            int pW = SpriteBatch.GraphicsDevice.Viewport.Width;
            int pH = SpriteBatch.GraphicsDevice.Viewport.Height;

            int vW = VirtualResolution.X;
            int vH = VirtualResolution.Y;

            return new Vector2(virtualPoint.X * pW / vW, virtualPoint.Y * pH / vH);
        }

        private Rectangle ToPhysicalCoordinates(Rectangle virtualCoordinate)
        {
            int pW = SpriteBatch.GraphicsDevice.Viewport.Width;
            int pH = SpriteBatch.GraphicsDevice.Viewport.Height;

            int vW = VirtualResolution.X;
            int vH = VirtualResolution.Y;

            return scaleRect(ref virtualCoordinate, pW, vW, pH, vH);
        }

        private Rectangle ToVirtualCoordinates(Rectangle physicalCoordinates)
        {
            int pW = SpriteBatch.GraphicsDevice.Viewport.Width;
            int pH = SpriteBatch.GraphicsDevice.Viewport.Height;

            int vW = VirtualResolution.X;
            int vH = VirtualResolution.Y;

            return scaleRect(ref physicalCoordinates, vW, pW, vH, pH);
        }

        private Vector2 ToVirtualCoordinates(Vector2 physicalPoint)
        {
            int pW = SpriteBatch.GraphicsDevice.Viewport.Width;
            int pH = SpriteBatch.GraphicsDevice.Viewport.Height;

            int vW = VirtualResolution.X;
            int vH = VirtualResolution.Y;

            return new Vector2(physicalPoint.X * vW / pW, physicalPoint.Y * vH / pH);
        }
        #endregion
    }
}
