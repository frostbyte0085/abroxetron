﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.UI;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    public sealed class UIRendererService : IRendererService, IDisposable
    {
        #region Variables
        private UILayer layer;
        private UIPainter painter;
        #endregion

        #region Properties
        public UILayer CurrentLayer { set { layer = value; } get { return layer; } }
        public Point VirtualResolution 
        { 
            set { painter.VirtualResolution = value; } 
            get { return painter.VirtualResolution; } 
        }
        public Rectangle VirtualResolutionRectangle
        {
            get
            {
                Point p = VirtualResolution;
                return new Rectangle(0, 0, p.X, p.Y);
            }
        }
        #endregion

        #region Contructors
        internal UIRendererService()
        {
            this.layer = null;
            this.painter = new UIPainter();
            ResetGraphicsDevice();
            ResetVirtualResolution();
        }
        #endregion

        public void ResetGraphicsDevice()
        {
            if(this.painter.SpriteBatch!=null) this.painter.SpriteBatch.Dispose();
            GraphicsService gs = ServiceProvider.GetService<GraphicsService>();
            SpriteBatch sb = new SpriteBatch(gs.GraphicsDevice);
            this.painter.SpriteBatch = sb;
        }
        public void ResetVirtualResolution()
        {
            this.painter.VirtualResolution = new Point(1280, 720);
        }

        public void Update()
        {
            if (layer != null)
            {
                layer.Update();
            }
        }

        public void Fetch() {}

        public void Render() 
        {
            if (layer != null)
            {
                painter.UIAtlas = layer.Atlas;
                painter.SpriteBatch.Begin();
                layer.Draw(painter);
                painter.SpriteBatch.End();
            }
        }

        public void PushRendererState() {}
        public void PopRendererState() {}

        #region IDisposable
        public void Dispose() { }
        #endregion
    }
}
