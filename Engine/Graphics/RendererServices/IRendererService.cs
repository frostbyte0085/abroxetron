﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Graphics
{
    interface IRendererService
    {
        void Fetch();
        void Render();
        void PushRendererState();
        void PopRendererState();
    }
}
