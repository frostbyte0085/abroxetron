﻿using System;
using System.Collections.Generic;
using System.Text;


using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Engine.Graphics
{
    internal struct ModelRendererVariables
    {
        public Matrix ProjectionMatrix
        {
            set;
            get;
        }

        public Matrix ViewMatrix
        {
            set;
            get;
        }

        public Vector3 EyePosition
        {
            set;
            get;
        }
    }
}
