﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Scene;
using Engine.Diagnostics;
using Engine.Core;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Engine.Graphics
{

    public sealed class DeferredProcessor
    {
        #region Variables
        Effect clear;
        Effect gbuffer;
        Effect gbufferSkinned;
        Effect directionalLight;
        Effect pointLight;
        Effect compose;

        BlendState lightmapBlendState;
        RenderTargetBinding[] gbufferTargets;
        Vector2 gbufferSize;
        RenderTarget2D lightmap;

        Model pointLightGeometry;

        FullScreenQuad quad;

        GraphicsService gs;
        Matrix view, projection;
        SpriteBatch spriteBatch;
        Matrix[] pointLightTransforms;

        #endregion

        public RenderTargetBinding[] GBuffer
        {
            get
            {
                return gbufferTargets;
            }
        }

        public DeferredProcessor(GraphicsService gs)
        {
            this.gs = gs;
            // create the deferred renderer
            ThreadSafeContentManager cm = ServiceProvider.GetService<ContentService>()["engine"];
            clear = cm.Load<Effect>("Effects/Deferred/Clear");
            clear.CurrentTechnique = clear.Techniques[0];

            gbuffer = cm.Load<Effect>("Effects/Deferred/RenderGBuffer");
            gbuffer.CurrentTechnique = gbuffer.Techniques[0];

            gbufferSkinned = cm.Load<Effect>("Effects/Deferred/RenderGBufferSkinned");
            gbufferSkinned.CurrentTechnique = gbufferSkinned.Techniques[0];

            compose = cm.Load<Effect>("Effects/Deferred/Composition");
            compose.CurrentTechnique = compose.Techniques[0];

            directionalLight = cm.Load<Effect>("Effects/Deferred/DirectionalLight");
            directionalLight.CurrentTechnique = directionalLight.Techniques[0];

            pointLight = cm.Load<Effect>("Effects/Deferred/PointLight");
            pointLight.CurrentTechnique = pointLight.Techniques[0];

            pointLightGeometry = cm.Load<Model>("Models/PointLightSphere");
            pointLightTransforms = new Matrix[pointLightGeometry.Bones.Count];
            pointLightGeometry.CopyAbsoluteBoneTransformsTo(pointLightTransforms);

            lightmapBlendState = new BlendState();
            lightmapBlendState.ColorSourceBlend = Blend.One;
            lightmapBlendState.ColorDestinationBlend = Blend.One;
            lightmapBlendState.ColorBlendFunction = BlendFunction.Add;
            lightmapBlendState.AlphaSourceBlend = Blend.One;
            lightmapBlendState.AlphaDestinationBlend = Blend.One;
            lightmapBlendState.AlphaBlendFunction = BlendFunction.Add;

            gbufferTargets = new RenderTargetBinding[3];

            CreateRenderTargets();

            quad = new FullScreenQuad();

            spriteBatch = new SpriteBatch(gs.GraphicsDevice);

            gs.DeviceResetting += new EventHandler<EventArgs>(DeviceResetting);
            gs.DeviceReset += new EventHandler<EventArgs>(DeviceReset);
        }

        void CreateRenderTargets()
        {
            int deferredWidth = gs.PresentationParameters.BackBufferWidth;
            int deferredHeight = gs.PresentationParameters.BackBufferHeight;

            gbufferSize = new Vector2(deferredWidth, deferredHeight);

            gbufferTargets[0] = new RenderTargetBinding(new RenderTarget2D(gs.GraphicsDevice,
                               deferredWidth, deferredHeight, false, SurfaceFormat.Color, DepthFormat.Depth24));
            gbufferTargets[1] = new RenderTargetBinding(new RenderTarget2D(gs.GraphicsDevice,
                               deferredWidth, deferredHeight, false, SurfaceFormat.Rgba1010102, DepthFormat.None));
            gbufferTargets[2] = new RenderTargetBinding(new RenderTarget2D(gs.GraphicsDevice,
                               deferredWidth, deferredHeight, false, SurfaceFormat.Single, DepthFormat.None));

            lightmap = new RenderTarget2D(gs.GraphicsDevice, deferredWidth, deferredHeight, false, SurfaceFormat.Color, DepthFormat.None);
        }

        public Vector2 GBufferSize
        {
            get
            {
                return gbufferSize;
            }
        }

        public RenderTargetBinding DepthBufferBinding
        {
            get
            {
                return gbufferTargets[2];
            }
        }

        public RenderTargetBinding NormalBufferBinding
        {
            get
            {
                return gbufferTargets[1];
            }
        }

        public RenderTargetBinding DiffuseBufferBinding
        {
            get
            {
                return gbufferTargets[0];
            }
        }
        
        void DisposeRenderTargets()
        {
            
        }

        void DeviceReset(object sender, EventArgs e)
        {
            CreateRenderTargets();
        }

        void DeviceResetting(object sender, EventArgs e)
        {
            
        }

        public void Debug()
        {
            //Begin SpriteBatch
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.PointClamp, null, null);

            //Width + Height
            int width = 208;
            int height = 128;

            //Set up Drawing Rectangle
            Rectangle rect = new Rectangle(0, 0, width, height);

            //Draw GBuffer 0
            spriteBatch.Draw((Texture2D)gbufferTargets[0].RenderTarget, rect, Color.White);

            //Draw GBuffer 1
            rect.X += width;
            spriteBatch.Draw((Texture2D)gbufferTargets[1].RenderTarget, rect, Color.White);

            //Draw GBuffer 2
            rect.X += width;
            spriteBatch.Draw((Texture2D)gbufferTargets[2].RenderTarget, rect, Color.White);

            // Lightmap
            rect.X += width;
            spriteBatch.Draw((Texture2D)lightmap, rect, Color.White);

            //End SpriteBatch
            spriteBatch.End();
        }

        public void Clear()
        {
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            gs.GraphicsDevice.SetRenderTargets(gbufferTargets);
            clear.CurrentTechnique.Passes[0].Apply();

            
            quad.SetBuffers();
            quad.Render();
        }

        public void Prepare(Matrix view, Matrix projection)
        {
            this.view = view;
            this.projection = projection;

            gs.GraphicsDevice.BlendState = BlendState.Opaque;
            gs.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            gbuffer.Parameters["View"].SetValue(view);
            gbuffer.Parameters["Projection"].SetValue(projection);

            gbuffer.Parameters["elapsedTime"].SetValue(GameTiming.TimeElapsed);
            gbuffer.Parameters["currentTime"].SetValue(GameTiming.TotalTimeElapsed);

            gbufferSkinned.Parameters["View"].SetValue(view);
            gbufferSkinned.Parameters["Projection"].SetValue(projection);
        }

        private void ProcessModel(Effect gbufferEffect, Matrix world, Effect effect, Matrix[] transforms, 
            Vector3 meshColor, bool liquid, Vector3 tintColor, float tintAmount, float emissiveAdd)
        {
            gbufferEffect.Parameters["World"].SetValue(world);
            gbufferEffect.Parameters["WorldViewIT"].SetValue(Matrix.Transpose(Matrix.Invert(world * view)));
            gbufferEffect.Parameters["colorOverride"].SetValue(meshColor);

            gbufferEffect.Parameters["tint"].SetValue(tintColor);
            gbufferEffect.Parameters["tintTime"].SetValue(tintAmount);
            gbufferEffect.Parameters["EmissiveAdditive"].SetValue(emissiveAdd);

            Texture2D normalMapTexture = effect.Parameters["Bump"].GetValueTexture2D();
            Texture2D albedoMapTexture = effect.Parameters["Texture"].GetValueTexture2D();
            Texture2D specularMapTexture = effect.Parameters["Specular"].GetValueTexture2D();
            Texture2D emissiveMapTexture = effect.Parameters["Emissive"].GetValueTexture2D();
            Texture2D parallaxMapTexture = effect.Parameters["Parallax"].GetValueTexture2D();
            
            if (albedoMapTexture != null)
                gbufferEffect.Parameters["Texture"].SetValue(albedoMapTexture);
            if (normalMapTexture != null)
                gbufferEffect.Parameters["Bump"].SetValue(normalMapTexture);
            if (specularMapTexture != null)
                gbufferEffect.Parameters["Specular"].SetValue(specularMapTexture);
            if (emissiveMapTexture != null)
                gbufferEffect.Parameters["Emissive"].SetValue(emissiveMapTexture);
            if (parallaxMapTexture != null)
                gbufferEffect.Parameters["Parallax"].SetValue(parallaxMapTexture);

            gbufferEffect.Parameters["hasTextureMap"].SetValue(albedoMapTexture != null);
            gbufferEffect.Parameters["hasNormalMap"].SetValue(normalMapTexture != null);
            gbufferEffect.Parameters["hasSpecularMap"].SetValue(specularMapTexture != null);
            gbufferEffect.Parameters["hasEmissiveMap"].SetValue(emissiveMapTexture != null);
            gbufferEffect.Parameters["hasParallaxMap"].SetValue(parallaxMapTexture != null);

            gbuffer.Parameters["isLiquid"].SetValue(liquid);

            // parallax test
            gbufferEffect.Parameters["hasParallaxMap"].SetValue(false);

            if (transforms != null)
                gbufferEffect.Parameters["bones"].SetValue(transforms);

            gbufferEffect.CurrentTechnique.Passes[0].Apply();
        }

        public void ProcessModel(Matrix world, Effect effect, Vector3 meshColor,
            bool liquid, Vector3 tintColor, float tintAmount, float emissiveAdd)
        {
            ProcessModel(gbuffer, world, effect, null, meshColor, liquid, tintColor, tintAmount, emissiveAdd);
        }

        public void ProcessModelSkinned(Matrix world, Effect effect, Matrix[] transforms, Vector3 meshColor,
            Vector3 tintColor, float tintAmount, float emissiveAdd)
        {
            ProcessModel(gbufferSkinned, world, effect, transforms, meshColor, false, tintColor, tintAmount, emissiveAdd);
        }
        
        public void ProcessLights(ref List<SceneNode> directionalLights, ref List<SceneNode> pointLights)
        {
            gs.GraphicsDevice.SetRenderTarget(lightmap);
            gs.GraphicsDevice.Clear(Color.Transparent);

            gs.GraphicsDevice.BlendState = lightmapBlendState;
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;

            #region global samplers
            //GBuffer 1 Sampler
            gs.GraphicsDevice.Textures[0] = gbufferTargets[0].RenderTarget;
            gs.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            //GBuffer 2 Sampler
            gs.GraphicsDevice.Textures[1] = gbufferTargets[1].RenderTarget;
            gs.GraphicsDevice.SamplerStates[1] = SamplerState.PointClamp;
            //GBuffer 3 Sampler
            gs.GraphicsDevice.Textures[2] = gbufferTargets[2].RenderTarget;
            gs.GraphicsDevice.SamplerStates[2] = SamplerState.PointClamp;
            #endregion

            Matrix inverseView = Matrix.Invert(view);
            Matrix inverseViewProjection = Matrix.Invert(view * projection);

            directionalLight.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
            directionalLight.Parameters["inverseView"].SetValue(inverseView);
            directionalLight.Parameters["CameraPosition"].SetValue(inverseView.Translation);
            directionalLight.Parameters["GBufferTextureSize"].SetValue(gbufferSize);

            quad.SetBuffers();

            // directional light rendering
            foreach (SceneNode n in directionalLights)
            {
                DirectionalLight light = n.Light as DirectionalLight;

                Vector3 direction = Vector3.Normalize(Matrix.CreateFromQuaternion(n.Orientation).Forward);
                directionalLight.Parameters["L"].SetValue(direction);
                directionalLight.Parameters["LightColor"].SetValue(light.Color);
                directionalLight.Parameters["LightIntensity"].SetValue(light.Intensity);
                //Apply
                directionalLight.CurrentTechnique.Passes[0].Apply();
                //Draw
                quad.Render();
            }

            //Set Point Lights Globals
            pointLight.Parameters["inverseView"].SetValue(inverseView);
            pointLight.Parameters["View"].SetValue(view);
            pointLight.Parameters["Projection"].SetValue(projection);
            pointLight.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
            pointLight.Parameters["CameraPosition"].SetValue(inverseView.Translation);
            pointLight.Parameters["GBufferTextureSize"].SetValue(gbufferSize);

            ModelMeshPart part = pointLightGeometry.Meshes[0].MeshParts[0];
            gs.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
            gs.GraphicsDevice.Indices = part.IndexBuffer;

            foreach (SceneNode n in pointLights)
            {
                PointLight light = n.Light as PointLight;

                Vector3 position = n.Translation;
                float radius2 = light.Radius;

                Matrix translationMatrix = Matrix.CreateTranslation(position);
                Matrix scaleMatrix = Matrix.CreateScale(radius2);
                
                //Set Point Light Parameters                
                pointLight.Parameters["World"].SetValue (pointLightTransforms[0] * (scaleMatrix * translationMatrix));
                pointLight.Parameters["LightPosition"].SetValue(position);
                pointLight.Parameters["LightRadius"].SetValue(radius2);
                pointLight.Parameters["LightColor"].SetValue(light.Color);
                pointLight.Parameters["LightIntensity"].SetValue(light.Intensity);
                
                //Set Cull Mode
                float cameraToLight = Vector3.Dot(inverseView.Translation, position);
                //If the Camera is in the light, render the backfaces, if it's out of the light, render the frontfaces
                if (cameraToLight < radius2)
                    gs.GraphicsDevice.RasterizerState = RasterizerState.CullClockwise;
                else
                    gs.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                
                //Apply
                pointLight.CurrentTechnique.Passes[0].Apply();

                gs.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                                part.NumVertices,
                                part.StartIndex,
                                part.PrimitiveCount);  
            }

            gs.GraphicsDevice.BlendState = BlendState.Opaque;
            gs.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            gs.GraphicsDevice.SetRenderTargets(null);
        }
       
        public void Combine()
        {
            // the composition shader also writes back the depth, so we can use forward rendering after this.
            // see composition.fx for details.

            //Clear
            gs.GraphicsDevice.Clear(Color.Transparent);

            //Set Textures
            // the albedo also provides the emissive intensity through the alpha channel
            gs.GraphicsDevice.Textures[0] = gbufferTargets[0].RenderTarget;
            gs.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            // lightmap, for our light accumulation.
            gs.GraphicsDevice.Textures[1] = lightmap;
            gs.GraphicsDevice.SamplerStates[1] = SamplerState.PointClamp;
            // pass the depth gbuffer, we use it to reconstruct the lost depth values.
            gs.GraphicsDevice.Textures[2] = gbufferTargets[2].RenderTarget;
            gs.GraphicsDevice.SamplerStates[2] = SamplerState.PointClamp;
            
            //Set Effect Parameters
            compose.Parameters["GBufferTextureSize"].SetValue(gbufferSize);
            //Apply
            compose.CurrentTechnique.Passes[0].Apply();
            //Draw
            quad.SetBuffers();
            quad.Render();

            //Debug();
            /*
            SceneService ss = ServiceProvider.GetService<SceneService>();

            List<SceneNode> l = ss.GetSceneNodesWithLight(LightQuery.Point);
            SceneNode cm = ss.ActiveCameraNode;
            
            foreach (SceneNode n in l)
            {
                PointLight ll = n.Light as PointLight;

                Matrix view = Matrix.Invert(cm.WorldMatrix);
                Matrix proj = cm.Camera.ProjectionMatrix;
                Matrix world = Matrix.CreateScale(ll.Radius) * Matrix.CreateTranslation(n.Translation);

                Matrix[] transforms = new Matrix[pointLightGeometry.Bones.Count];
                pointLightGeometry.CopyAbsoluteBoneTransformsTo(transforms);

                // Draw the model. A model can have multiple meshes, so loop.
                foreach (ModelMesh mesh in pointLightGeometry.Meshes)
                {
                    // This is where the mesh orientation is set, as well 
                    // as our camera and projection.
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.World = transforms[mesh.ParentBone.Index] * world;
                        effect.View = view;
                        effect.Projection = proj;
                    }
                    // Draw the mesh, using the effects set above.
                    mesh.Draw();
                }
                
            }
            l.Clear();
             * */

        }
    }
}
