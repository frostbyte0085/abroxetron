﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Scene;
using Engine.Diagnostics;
using Engine.Core;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Engine.Animation;

namespace Engine.Graphics
{
    internal sealed class ModelRendererService : IRendererService, IDisposable
    {
        #region Variables
        ModelRendererVariables variables;
        SceneService ss;
        GraphicsService gs;

        DeferredProcessor defProcessor;
        List<RenderableModel> models;
        List<Vector3> meshColors;

        List<SceneNode> directionalLights;
        List<SceneNode> pointLights;

        #endregion

        public DeferredProcessor DeferredProcessor
        {
            get
            {
                return defProcessor;
            }
        }

        public ModelRendererService()
        {
            variables = new ModelRendererVariables();
            models = new List<RenderableModel>();
            meshColors = new List<Vector3>();

            directionalLights = new List<SceneNode>();
            pointLights = new List<SceneNode>();

            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            
            defProcessor = new DeferredProcessor(gs);

            ThreadSafeContentManager cm = ServiceProvider.GetService<ContentService>()["engine"];
        }

        #region IRendererService
        public void Fetch()
        {
            models.Clear();
            meshColors.Clear();

            Camera camera = ss.ActiveCameraNode.Camera;

            variables.EyePosition = ss.ActiveCameraNode.Translation;
            variables.ProjectionMatrix = camera.ProjectionMatrix;
            variables.ViewMatrix = Matrix.Invert(ss.ActiveCameraNode.WorldMatrix);
            
            List<SceneNode> modelNodes = ss.GetSceneNodesWithModel();

            foreach (SceneNode node in modelNodes)
            {
                if (node.FrustumCulled)
                    continue;

                RenderableModel model = new RenderableModel();
                model.Model = node.Model;
                model.WorldMatrix = node.WorldMatrix;
                model.Transforms = new Matrix[model.Model.Bones.Count];
                model.Model.CopyAbsoluteBoneTransformsTo(model.Transforms);

                if (node.Animation != null)
                {
                    model.BoneTransforms = node.Animation.SkinTransforms;
                }

                model.TintAmount = node.TintAmount;
                model.TintColor = node.TintColor;
                model.EmissiveAdditive = node.EmissiveAdditive;

                models.Add(model);
                meshColors.Add(node.MeshColor);
            }

            modelNodes.Clear();

            directionalLights = ss.GetSceneNodesWithLight(LightQuery.Directional);
            pointLights = ss.GetSceneNodesWithLight(LightQuery.Point);

        }

        public void RenderNormally()
        {
            gs.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            IEnumerator<Vector3> cols = meshColors.GetEnumerator();
            
            foreach (RenderableModel rm in models)
            {
                Model model = rm.Model;
                cols.MoveNext(); Vector3 col = cols.Current;

                foreach (ModelMesh mesh in model.Meshes)
                {
                    BoundingSphere transformedSphere = mesh.BoundingSphere.Transform(rm.Transforms[mesh.ParentBone.Index] * rm.WorldMatrix);

                    ContainmentType currentContainmentType = ContainmentType.Disjoint;
                    currentContainmentType = ss.Frustum.Contains(transformedSphere);
                    if (currentContainmentType == ContainmentType.Disjoint)
                        continue;

                    bool liquid = false;
                    Dictionary<string, object> modelTags = model.Tag as Dictionary<string, object>;

                    if (modelTags != null && modelTags.ContainsKey("isLiquid"))
                    {
                        liquid = (bool)modelTags["isLiquid"];
                    }

                    foreach (ModelMeshPart part in mesh.MeshParts)
                    {
                        gs.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);

                        gs.GraphicsDevice.Indices = part.IndexBuffer;

                        if (rm.BoneTransforms != null)
                            defProcessor.ProcessModelSkinned(
                                rm.Transforms[mesh.ParentBone.Index] * rm.WorldMatrix,
                                part.Effect,
                                rm.BoneTransforms,
                                col, rm.TintColor, rm.TintAmount,
                                rm.EmissiveAdditive);
                        else
                            defProcessor.ProcessModel(
                                rm.Transforms[mesh.ParentBone.Index] * rm.WorldMatrix,
                                part.Effect,
                                col, liquid, rm.TintColor, rm.TintAmount,
                                rm.EmissiveAdditive);

                        gs.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                    }
                }
            }
        }

        public void Render()
        {
            PushRendererState();
            
            Fetch();

            defProcessor.Clear();
            defProcessor.Prepare(variables.ViewMatrix, variables.ProjectionMatrix);

            RenderNormally();

            gs.GraphicsDevice.SetRenderTargets(null);
            defProcessor.ProcessLights(ref directionalLights, ref pointLights);
        }

        public void RenderFinal()
        {
            defProcessor.Combine();

            PopRendererState();
        }

        public void PushRendererState()
        {
            gs.GraphicsDevice.BlendState = BlendState.Opaque;
            gs.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            gs.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
        }

        public void PopRendererState()
        {
            
        }

        #endregion

        public void Dispose()
        {
            directionalLights.Clear();
            pointLights.Clear();

            models.Clear();
        }
    }
}
