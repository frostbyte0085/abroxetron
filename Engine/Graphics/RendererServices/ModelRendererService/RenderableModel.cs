﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Engine.Graphics
{
    internal struct RenderableModel
    {
        public Matrix WorldMatrix
        {
            set;
            get;
        }

        public Model Model
        {
            set;
            get;
        }

        public Matrix[] BoneTransforms
        {
            get;
            set;
        }

        public Matrix[] Transforms
        {
            get;
            set;
        }

        public Vector3 TintColor
        {
            set;
            get;
        }

        public float TintAmount
        {
            set;
            get;
        }

        public float EmissiveAdditive
        {
            set;
            get;
        }
    }
}
