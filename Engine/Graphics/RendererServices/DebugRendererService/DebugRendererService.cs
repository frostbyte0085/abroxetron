﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.AI;
using Engine.Core;
using Engine.Scene;
using Engine.Input;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Kinect;

namespace Engine.Graphics
{
    public sealed class DebugRendererService : IDisposable
    {
        #region Variables
        // list of 3d objs
        private RasterizerState rs;
        private ContentService cs;
        private GraphicsDevice gd;
        private BlendState myBlendState;

        private List<VertexPositionColor> lineVertices;
        private List<int> lineIndices;
        private DynamicVertexBuffer lineVBO;
        private DynamicIndexBuffer lineIBO;

        private List<VertexPositionColor> triVertices;
        private List<int> triIndices;
        private DynamicVertexBuffer triVBO;
        private DynamicIndexBuffer triIBO;

        private Effect myeffect;

        // used for the debug overlay text
        private SpriteBatch sprite;
        private SpriteFont font;

        #endregion

        #region Properties
        public RasterizerState RasterizerState { set { rs = value; } get { return rs; } }

        public int SphereDivisions { set; get; }

        public bool ShowFrameStatistics { set; get; }

        #endregion

        #region Constructors
        internal DebugRendererService()
        {
            // Init rasterizer state
            rs = new RasterizerState();
            rs.FillMode = FillMode.WireFrame;
            rs.CullMode = CullMode.None;

            // Init Sphere poly count
            SphereDivisions = 10;

            // The content service
            cs = ServiceProvider.GetService<ContentService>();

            // Init Graphics device
            gd = ServiceProvider.GetService<GraphicsService>().GraphicsDevice;
            myBlendState = BlendState.Opaque;

            // Init Effect
            ThreadSafeContentManager cm = ServiceProvider.GetService<ContentService>()["engine"];
            myeffect = cm.Load<Effect>("Effects/DebugRender");
            myeffect.CurrentTechnique = myeffect.Techniques[0];

            // Init vertex / index lists
            lineVertices = new List<VertexPositionColor>();
            lineIndices = new List<int>();
            triVertices = new List<VertexPositionColor>();
            triIndices = new List<int>();

            // Init buffers
            lineVBO = new DynamicVertexBuffer(gd, VertexPositionColor.VertexDeclaration,
                1000, BufferUsage.WriteOnly);
            lineIBO = new DynamicIndexBuffer(gd, typeof(int),
                1000, BufferUsage.WriteOnly);

            triVBO = new DynamicVertexBuffer(gd, VertexPositionColor.VertexDeclaration,
                1000, BufferUsage.WriteOnly);
            triIBO = new DynamicIndexBuffer(gd, typeof(int),
                1000, BufferUsage.WriteOnly);


            sprite = new SpriteBatch(gd);
            font = cs["engine"].Load<SpriteFont>("Fonts/System");

            InitGestureTexture();
        }
        #endregion

        #region DebugRendererService

        /// <summary>
        /// Add a triangle soup mesh in the 3D space.
        /// </summary>
        /// <param name="vertices">Mesh vertices</param>
        /// <param name="indices">Mesh indices</param>
        public void AddTriSoup(Vector3[] vertices, int[] indices, Color color)
        {
            foreach (Vector3 v in vertices)
            {
                triVertices.Add(new VertexPositionColor(v, color));
            }

            foreach (int i in indices)
            {
                triIndices.Add(i);
            }
        }

        public void AddCapsule(Vector3 center, float radius, float length, int numSides, Color color)
        {
            var n = new Vector3();
            var offset = new Vector3(0, length / 2, 0);
            float angleBetweenFacets = MathHelper.TwoPi / numSides;

            int previousCount = triVertices.Count;

            //Create the vertex list
            //Top
            triVertices.Add(new VertexPositionColor(new Vector3(0, radius + length / 2, 0)+center, color));
            //Upper hemisphere
            for (int i = 1; i <= numSides / 4; i++)
            {
                float phi = MathHelper.PiOver2 - i * angleBetweenFacets;
                var sinPhi = (float)Math.Sin(phi);
                var cosPhi = (float)Math.Cos(phi);

                for (int j = 0; j < numSides; j++)
                {
                    float theta = j * angleBetweenFacets;

                    n.X = (float)Math.Cos(theta) * cosPhi;
                    n.Y = sinPhi;
                    n.Z = (float)Math.Sin(theta) * cosPhi;

                    triVertices.Add(new VertexPositionColor(n * radius + offset+center, color));
                }
            }
            //Lower hemisphere
            for (int i = numSides / 4; i < numSides / 2; i++)
            {
                float phi = MathHelper.PiOver2 - i * angleBetweenFacets;
                var sinPhi = (float)Math.Sin(phi);
                var cosPhi = (float)Math.Cos(phi);

                for (int j = 0; j < numSides; j++)
                {
                    float theta = j * angleBetweenFacets;

                    n.X = (float)Math.Cos(theta) * cosPhi;
                    n.Y = sinPhi;
                    n.Z = (float)Math.Sin(theta) * cosPhi;

                    triVertices.Add(new VertexPositionColor(n * radius - offset+center, color));
                }
            }
            //Bottom
            triVertices.Add(new VertexPositionColor(new Vector3(0, -radius - length / 2, 0)+center, color));

            int currentCount = triVertices.Count;

            int difCount = currentCount - previousCount;

            int baseIndex = 0;//
            if (triIndices.Count > 0)
                baseIndex = triIndices[triIndices.Count - 1];

            //Create the index list
            for (int i = 0; i < numSides; i++)
            {
                triIndices.Add((int)(difCount - 1) + baseIndex);
                triIndices.Add((int)(difCount - 2 - i) + baseIndex);
                triIndices.Add(((int)(difCount - 2 - (i + 1) % numSides)) + baseIndex);
            }

            for (int i = 0; i < numSides / 2 - 1; i++)
            {
                for (int j = 0; j < numSides; j++)
                {
                    int nextColumn = (j + 1) % numSides;

                    triIndices.Add((int)(i * numSides + nextColumn + 1) + baseIndex);
                    triIndices.Add((int)(i * numSides + j + 1) + baseIndex);
                    triIndices.Add((int)((i + 1) * numSides + j + 1) + baseIndex);

                    triIndices.Add((int)((i + 1) * numSides + nextColumn + 1) + baseIndex);
                    triIndices.Add((int)(i * numSides + nextColumn + 1) + baseIndex);
                    triIndices.Add((int)((i + 1) * numSides + j + 1) + baseIndex);
                }
            }

            for (int i = 0; i < numSides; i++)
            {
                triIndices.Add(baseIndex);
                triIndices.Add((int)(i + 1) + baseIndex);
                triIndices.Add(((int)((i + 1) % numSides + 1)) + baseIndex);
            }
        }

        /// <summary>
        /// Draw the grid that the AI uses to navigate.
        /// </summary>
        /// <param name="grid">The Grid</param>
        /// <param name="cOn">Color of vertices when grid.IsPositionFree is true.</param>
        /// <param name="cOff">Color of vertices when grid.IsPositionFree is false.</param>
        /// <param name="heightOff">Height of the plane.</param>
        public void AddGrid3D(PathGrid grid, Color cOn, Color cOff, float heightOff)
        {
            Point size = grid.GridSize;
            int i = triVertices.Count;

            for (int x = 0; x < size.X; x++)
                for (int y = 0; y < size.Y; y++)
                {
                    Color c = grid.IsPositionFree(x, y) ? cOn : cOff;
                    Vector2 w = grid.GridToWorld(x,y);
                    Vector3 vertex = new Vector3(w.X, heightOff, w.Y);
                    triVertices.Add(new VertexPositionColor(vertex, c));
                }

            for (int x = 0; x < size.X - 1; x++)
                for (int y = 0; y < size.Y - 1; y++)
                {
                    int off = i + (size.Y * x) + y;

                    triIndices.Add(off + 1);
                    triIndices.Add(off);
                    triIndices.Add(off + size.Y);

                    triIndices.Add(off + size.Y + 1);
                    triIndices.Add(off + 1);
                    triIndices.Add(off + size.Y);
                }

            return;
        }

        public void AddGrid3D(Vector3 position, int width, int height, int cellSize, Color color)
        {
            int columns = width / cellSize;
            int rows = height / cellSize;

            int xdiff = width / columns;
            int zdiff = height / rows;

            float xdiff_f = (float)width / (float)columns;
            float zdiff_f = (float)height / (float)rows;

            float xbase = position.X - width / 2f;
            float zbase = position.Z - height / 2f;
            float ybase = position.Y;

            for (int i = 0; i <= rows; i++)
            {
                Vector3 start = new Vector3(xbase + i * xdiff, ybase, zbase);
                Vector3 end = new Vector3(xbase + i * xdiff, ybase, zbase + height);
                AddLine3D(start, end, color);
            }

            for (int i = 0; i <= columns; i++)
            {
                Vector3 start = new Vector3(xbase, ybase, zbase + i * zdiff);
                Vector3 end = new Vector3(xbase + width, ybase, zbase + i * zdiff);
                AddLine3D(start, end, color);
            }
        }

        /// <summary>
        /// Add a line in the 3D space.
        /// </summary>
        /// <param name="start">Start vertex</param>
        /// <param name="end">End vertex</param>
        /// <param name="col">Color of the line</param>
        public void AddLine3D(Vector3 start, Vector3 end, Color col)
        {
            lineIndices.Add(lineVertices.Count);
            lineVertices.Add(new VertexPositionColor(start, col));

            lineIndices.Add(lineVertices.Count);
            lineVertices.Add(new VertexPositionColor(end, col));
        }

        /// <summary>
        /// Adds a series of lines linked by common vertices. (Creates
        /// a path that crosses all the vertices in order).
        /// </summary>
        /// <param name="vertices">List of vertices used to draw lines 
        /// <param name="col">Color of the whole line</param>
        /// (minimum length 2)</param>
        public void AddLinkedLines3D(Vector3[] vertices, Color col)
        {
            // Check args
            if(vertices.Length < 2) throw new ArgumentException("Have atleast 2 vertices");

            // INIT
            lineIndices.Add(lineVertices.Count);
            lineVertices.Add(new VertexPositionColor(vertices[0], col));

            // ITER
            for (int i=1; i<vertices.Length; i++)
            {
                lineIndices.Add(lineVertices.Count);
                lineVertices.Add(new VertexPositionColor(vertices[i], col));
            }
        }

        /// <summary>
        /// Add a sphere in the 3D space.
        /// </summary>
        /// <param name="position">Position of the center of the sphere</param>
        /// <param name="radius">Radius of the sphere</param>
        /// <param name="col">Color of the whole line</param>
        public void AddSphere(Vector3 position, float radius, Color col)
        {
            Vector3[] V = CalcSphereVertices(SphereDivisions, SphereDivisions);
            int[] I = CalcSphereIndices(SphereDivisions, SphereDivisions, triVertices.Count);

            foreach (Vector3 v in V)
                triVertices.Add(new VertexPositionColor(v * radius + position, col));

            foreach (int i in I)
                triIndices.Add(i);
        }

        /// <summary>
        /// Add a sphere in the 3D space.
        /// </summary>
        /// <param name="s">Bounding sphere</param>
        /// <param name="col">Color of the whole line</param>
        public void AddSphere(BoundingSphere s, Color col)
        {
            AddSphere(s.Center, s.Radius, col);
        }

        /// <summary>
        /// Add a box (cuboid) in the 3D space.
        /// </summary>
        /// <param name="box">Any XNA bounding box</param>
        /// <param name="col">Color of the whole line</param>
        public void AddBox3D(BoundingBox box, Color col) { AddBox3D(box.Min, box.Max, col); }
        /// <summary>
        /// Add a box (cuboid) in the 3D space.
        /// </summary>
        /// <param name="box">Any XNA bounding box</param>
        /// <param name="col">Color of the whole line</param>
        /// <param name="q">Rotation to apply</param>
        public void AddBox3D(BoundingBox box, Color col, Quaternion q) { AddBox3D(box.Min, box.Max, col, q); }

        /// <summary>
        /// Add a box (cuboid) in the 3D space.
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        /// <param name="col">color of the box</param>
        public void AddBox3D(Vector3 minCorner, Vector3 maxCorner, Color col)
        {
            Vector3[] V = CalcCudeVertices(minCorner, maxCorner);
            int[] I = CalcCudeIndices(triVertices.Count);

            foreach (Vector3 v in V)
                triVertices.Add(new VertexPositionColor(v, col));

            foreach (int i in I)
                triIndices.Add(i);
        }

        /// Add a box (cuboid) in the 3D space.
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        /// <param name="col">Color of the box</param>
        /// <param name="q">Rotation to apply</param>
        /// <param name="pivot">Pivot point</param>
        public void AddBox3D(Vector3 minCorner, Vector3 maxCorner, Color col, Quaternion q, Vector3 pivot)
        {
            Vector3[] V = CalcCudeVertices(minCorner, maxCorner, q, pivot);
            int[] I = CalcCudeIndices(triVertices.Count);

            foreach (Vector3 v in V)
                triVertices.Add(new VertexPositionColor(v, col));

            foreach (int i in I)
                triIndices.Add(i);
        }

        /// Add a box (cuboid) in the 3D space.
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        /// <param name="col">Color of the box</param>
        /// <param name="q">Rotation to apply</param>
        public void AddBox3D(Vector3 minCorner, Vector3 maxCorner, Color col, Quaternion q)
        {
            AddBox3D(minCorner, maxCorner, col, q, Vector3.Zero);
        }

        /// <summary>
        /// Add a box (cuboid) where each vertex has different color! :O OMG!!!
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        /// <param name="colors">colors of the corner (length 8)</param>
        public void AddBox3D(Vector3 minCorner, Vector3 maxCorner, Color[] colors)
        {
            Vector3[] V = CalcCudeVertices(minCorner, maxCorner);
            int[] I = CalcCudeIndices(triVertices.Count);

            for (int i = 0; i < V.Length; i++)
                triVertices.Add(new VertexPositionColor(V[i], colors[i]));

            foreach (int i in I)
                triIndices.Add(i);
        }

        /// <summary>
        /// Add a box (cuboid) where each vertex has different color! :O OMG!!!
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        /// <param name="colors">colors of the corner (length 8)</param>
        /// <param name="q">rotation</param>
        /// <param name="pivot">pivot point"</param>
        public void AddBox3D(Vector3 minCorner, Vector3 maxCorner, Color[] colors, Quaternion q, Vector3 pivot)
        {
            Vector3[] V = CalcCudeVertices(minCorner, maxCorner, q, pivot);
            int[] I = CalcCudeIndices(triVertices.Count);

            for (int i = 0; i < V.Length; i++)
                triVertices.Add(new VertexPositionColor(V[i], colors[i]));

            foreach (int i in I)
                triIndices.Add(i);
        }

        /// <summary>
        /// Add a box (cuboid) where each vertex has different color! :O OMG!!!
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        /// <param name="colors">colors of the corner (length 8)</param>
        /// <param name="q">rotation</param>
        public void AddBox3D(Vector3 minCorner, Vector3 maxCorner, Color[] colors, Quaternion q)
        {
            AddBox3D(minCorner, maxCorner, colors, q, Vector3.Zero);
        }

        /// <summary>
        /// Add a box (cuboid) where each vertex has a predefined different color! :O OMG!!!
        /// </summary>
        /// <param name="minCorner">Minimum corner</param>
        /// <param name="maxCorner">Maximum corner</param>
        public void AddMagicBox3D(Vector3 minCorner, Vector3 maxCorner)
        {
            Color[] colors = new Color[8];
            colors[0] = Color.Red;
            colors[1] = Color.Orange;
            colors[2] = Color.Yellow;
            colors[3] = Color.Green;
            colors[4] = Color.Cyan;
            colors[5] = Color.Blue;
            colors[6] = Color.Purple;
            colors[7] = Color.Pink;
            AddBox3D(minCorner, maxCorner, colors);
        }

        private Skeleton skel = null;
        private Texture2D jointTexture;
        private int jointRadius;
        private bool drawBones;
        public void SetSkeleton(Skeleton skel, int jointRadius, bool drawBones, Color jointCol, Color boneCol)
        {
            this.skel = skel;

            this.jointTexture = new Texture2D(sprite.GraphicsDevice, 1, 1);
            Color[] cols = {jointCol};
            jointTexture.SetData(cols);

            this.jointRadius = jointRadius;

            this.drawBones = drawBones;

        }

        public void Clear()
        {
            lineVertices.Clear();
            lineIndices.Clear();
            triVertices.Clear();
            triIndices.Clear();
        }
        #endregion

        #region Vertex and Index calculation for a cuboid
        private static Vector3[] CalcCudeVertices(Vector3 min, Vector3 max, Quaternion q, Vector3 pivot)
        {
            // Calc verts with box centered at 0,0,0
            Vector3 center = (min + max) / 2.0f - pivot;
            Vector3[] V = CalcCudeVertices(min - center, max - center);


            // Rotate around 0,0,0
            //Vector3.Transform(
            Vector3.Transform(V, ref q, V);

            // Shift back
            for (int i = 0; i < V.Length; i++) V[i] = V[i] + center;

            return V;
        }

        private static Vector3[] CalcCudeVertices(Vector3 min, Vector3 max)
        {
            float x = Math.Abs(max.X - min.X);
            float y = Math.Abs(max.Y - min.Y);
            float z = Math.Abs(max.Z - min.Z);

            Vector3 X = new Vector3(x, 0, 0);
            Vector3 Y = new Vector3(0, y, 0);
            Vector3 XY = new Vector3(x, y, 0);

            Vector3[] vs = new Vector3[8];

            vs[0] = min;
            vs[1] = min + X;
            vs[2] = min + XY;
            vs[3] = min + Y;

            vs[4] = max - XY;
            vs[5] = max - Y;
            vs[6] = max - X;
            vs[7] = max;

            /**
             * Cudoid viewed from BACK:
             * 3 -- 2
             * |    |   y ^
             * |    |     |
             * 0 -- 1      --> x
             * 
             * Cudoid viewed from FRONT:
             * 7 -- 6
             * |    |        ^ y
             * |    |        |
             * 5 -- 4   x <-- 
             */

            return vs;
        }

        private static int[] CalcCudeIndices(int vertexoff)
        {
            // (6 sides, 2 tri. per side, 3 verts per tri)
            int[] I = new int[6 * 3 * 2];
            int i = 0;
            /*
            i = SetIndices1Side(I, i, vertexoff, 1, 0, 3, 2);
            i = SetIndices1Side(I, i, vertexoff, 5, 1, 2, 7);
            i = SetIndices1Side(I, i, vertexoff, 4, 5, 7, 6);
            i = SetIndices1Side(I, i, vertexoff, 0, 4, 6, 3);
            i = SetIndices1Side(I, i, vertexoff, 3, 6, 7, 2);
            i = SetIndices1Side(I, i, vertexoff, 5, 4, 0, 1);
             */

            i = SetIndices1Side(I, i, vertexoff, 0, 1, 2, 3);
            i = SetIndices1Side(I, i, vertexoff, 1, 5, 7, 2);
            i = SetIndices1Side(I, i, vertexoff, 5, 4, 6, 7);
            i = SetIndices1Side(I, i, vertexoff, 4, 0, 3, 6);
            i = SetIndices1Side(I, i, vertexoff, 6, 3, 2, 7);
            i = SetIndices1Side(I, i, vertexoff, 4, 5, 1, 0);

            return I;
        }

        /// <summary>
        /// Set the indices for 1 side of a cudoid
        /// </summary>
        /// <param name="indices">Indices array</param>
        /// <param name="off">Offset in indices (where to start writing)</param>
        /// <param name="vertexoff">Value to add to the vertex#</param>
        /// <param name="bl">Bottom-left index</param>
        /// <param name="br">Bottom-right index</param>
        /// <param name="tr">Top-right index</param>
        /// <param name="tl">Top-left index</param>
        /// <returns>New offset</returns>
        private static int SetIndices1Side(
            int[] indices, 
            int off, int vertexoff, 
            int bl, int br, int tr, int tl)
        {
            indices[off++] = tr + vertexoff;
            indices[off++] = bl + vertexoff;
            indices[off++] = br + vertexoff;
            indices[off++] = tl + vertexoff;
            indices[off++] = bl + vertexoff;
            indices[off++] = tr + vertexoff;

            return off;
        }
        #endregion

        #region Vertex and Index calculation for a sphere
        private static Vector3[] CalcSphereVertices(int PointRows, int PointsPerRow)
        {
            Vector3[] verts = new Vector3[PointRows * PointsPerRow];

            for (int i = 1; i < (PointRows-1); i++)
	        {
		        for (int j = 0; j < PointsPerRow; j++)
		        {
                    float fi = (float)i;
                    float fj = (float)j;
                    float pi2 = (float)Math.PI * 2.0f;

			        float y = 1.0f - fi / (PointRows-1) * 2.0f ;
			        float r = (float) Math.Sin (Math.Acos(y));  //radius of the row

			        float x = r * (float) Math.Sin(fj / PointsPerRow * pi2);
			        float z = r * (float) Math.Cos(fj / PointsPerRow * pi2);

                    verts[(i - 1) * PointsPerRow + j] = new Vector3(x, y, z);
		        }
	        }

            //The highest and deepest vertices:
            int off = (PointRows - 2) * PointsPerRow;
            verts[off] = new Vector3(0, 1, 0);
            verts[off + 1] = new Vector3(0, -1, 0);

            return verts;
        }

        private static int[] CalcSphereIndices(int PointRows, int PointsPerRow, int voff)
        {
            List<int> IndexVect = new List<int>();
            int i, j;

            //create the index array:
            for (i = 1; i < (PointRows - 2); i++)
            {
                for (j = 0; j < (PointsPerRow - 1); j++)
                {
                    IndexVect.Add(voff + (i - 1) * PointsPerRow + j);
                    IndexVect.Add(voff + (i - 1) * PointsPerRow + j + 1);
                    IndexVect.Add(voff + (i) * PointsPerRow + j);

                    IndexVect.Add(voff + (i - 1) * PointsPerRow + j + 1);
                    IndexVect.Add(voff + (i) * PointsPerRow + j + 1);
                    IndexVect.Add(voff + (i) * PointsPerRow + j);
                }

                IndexVect.Add(voff + (i - 1) * PointsPerRow + PointsPerRow - 1);
                IndexVect.Add(voff + (i - 1) * PointsPerRow);
                IndexVect.Add(voff + (i) * PointsPerRow + j);

                IndexVect.Add(voff + (i) * PointsPerRow);
                IndexVect.Add(voff + (i) * PointsPerRow + j);
                IndexVect.Add(voff + (i - 1) * PointsPerRow);
            }

            //The triangles to the highest and deepest vertices:
            for (j = 0; j < (PointsPerRow - 1); j++)
            {
                IndexVect.Add(voff + j);
                IndexVect.Add(voff + (PointRows - 2) * PointsPerRow);
                IndexVect.Add(voff + j + 1);
            }
            IndexVect.Add(voff + j);
            IndexVect.Add(voff + (PointRows - 2) * PointsPerRow);
            IndexVect.Add(voff + 0);

            for (j = 0; j < (PointsPerRow - 1); j++)
            {
                IndexVect.Add(voff + (PointRows - 3) * PointsPerRow + j);
                IndexVect.Add(voff + (PointRows - 3) * PointsPerRow + j + 1);
                IndexVect.Add(voff + (PointRows - 2) * PointsPerRow + 1);
            }
            IndexVect.Add(voff + (PointRows - 3) * PointsPerRow + j);
            IndexVect.Add(voff + (PointRows - 3) * PointsPerRow);
            IndexVect.Add(voff + (PointRows - 2) * PointsPerRow + 1);

            //copy the indices into the array:
            return IndexVect.ToArray();
        }
        #endregion

        #region Rendering
        /// <summary>
        /// Render the debug objects
        /// </summary>
        internal void Render(SceneNode activeCam)
        {
            try
            {
                gd.BlendState = myBlendState;
                gd.DepthStencilState = DepthStencilState.Default;

                Matrix view = Matrix.Invert(activeCam.WorldMatrix);
                Matrix proj = activeCam.Camera.ProjectionMatrix;
                Matrix world = Matrix.Identity;

                // Draw the lines:
                if(lineVertices.Count != 0)
                    Render(view, proj, world, lineVBO, lineIBO, PrimitiveType.LineList);

                // Draw the triangles:
                if(triVertices.Count != 0)
                    Render(view, proj, world, triVBO, triIBO, PrimitiveType.TriangleList);

                Clear();
            }
            catch (Exception e) // TODO : chuck this into the bin.
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void Render(Matrix view, Matrix proj, Matrix world,
            DynamicVertexBuffer vbo, 
            DynamicIndexBuffer ibo, 
            PrimitiveType type)
        {
            gd.DepthStencilState = DepthStencilState.DepthRead;

            // set effect params
            myeffect.Parameters["View"].SetValue(view);
            myeffect.Parameters["Projection"].SetValue(proj);
            myeffect.Parameters["World"].SetValue(world);

            // get lists:
            List<VertexPositionColor> listv = null;
            List<int> listi = null;
            if (type == PrimitiveType.LineList)
            {
                listv = lineVertices;
                listi = lineIndices;
            }
            else if (type == PrimitiveType.TriangleList)
            {
                listv = triVertices;
                listi = triIndices;
            }
            else throw new Exception("Invalid argument for 'type': " + type );

            // set up buffers:
            /*
            VertexPositionColor[] vertices = listv.ToArray();
            int[] indices = listi.ToArray();
            vbo.SetData(vertices, 0, vertices.Length, SetDataOptions.Discard);
            ibo.SetData(indices, 0, vertices.Length, SetDataOptions.Discard);
            gd.SetVertexBuffer(vbo);
            gd.Indices = ibo;

            // draw:
            myeffect.CurrentTechnique.Passes[0].Apply();

            int indicesPerShape = (type == PrimitiveType.LineList) ? 2 : 3;

            gd.RasterizerState = rs;
            gd.DrawIndexedPrimitives(type, 0, 0, vertices.Length, 0, indices.Length / indicesPerShape);
            */

            // testing code (which works)
            VertexPositionColor[] vertices = listv.ToArray();
            int[] indices = listi.ToArray();
            int indicesPerShape = (type == PrimitiveType.LineList) ? 2 : 3;
            int nprimitives = indices.Length / indicesPerShape;
            gd.RasterizerState = rs;
            gd.DepthStencilState = DepthStencilState.Default;

            foreach (EffectPass pass in myeffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                gd.DrawUserIndexedPrimitives(
                    type,
                    vertices, 
                    0, 
                    vertices.Length, 
                    indices, 
                    0,
                    nprimitives,
                    VertexPositionColor.VertexDeclaration);
            }
        }

        internal void RenderDebugOverlay()
        {
            sprite.Begin();
            if (ShowFrameStatistics)
            {
                sprite.DrawString(font, "Framerate: " + PersistentTiming.FPS, new Vector2(0, 0), Color.Pink);
                sprite.DrawString(font, "Total frame time: " + Math.Round(PersistentTiming.FrameTime, 2) + "ms", new Vector2(0, 20), Color.Pink);
                sprite.DrawString(font, "Update time: " + Math.Round(PersistentTiming.UpdateTime, 2) + "ms", new Vector2(0, 40), Color.Pink);
                sprite.DrawString(font, "Render time: " + Math.Round(PersistentTiming.TotalRenderTime, 2) + "ms", new Vector2(0, 60), Color.Pink);
                sprite.DrawString(font, "Model render time: " + Math.Round(PersistentTiming.ModelRenderTime, 2) + "ms", new Vector2(0, 80), Color.Pink);
                sprite.DrawString(font, "Particle render time: " + Math.Round(PersistentTiming.ParticleRenderTime, 2) + "ms", new Vector2(0, 100), Color.Pink);
                sprite.DrawString(font, "UI render time: " + Math.Round(PersistentTiming.UIRenderTime, 2) + "ms", new Vector2(0, 120), Color.Pink);
                sprite.DrawString(font, "Post-Process render time: " + Math.Round(PersistentTiming.PostProcessRenderTime, 2) + "ms", new Vector2(0, 140), Color.Pink);
            }

            RenderKinectSkeleton();
            RenderKinectGestures();

            sprite.End();
        }

        private void RenderKinectSkeleton()
        {
            if (skel == null) return;

            int x = sprite.GraphicsDevice.Viewport.Width;
            int y = sprite.GraphicsDevice.Viewport.Height;

            foreach (Joint j in skel.Joints)
            {
                Point point = KinectDevice.GetScreenPoint(j, x, y);
                Rectangle rect = new Rectangle(point.X, point.Y, jointRadius, jointRadius);

                //Vector2 pos = Math2.GetScreenPoint(j, x, y);
                //Rectangle rect = new Rectangle((int)(pos.X + 0.5f), (int)(pos.Y + 0.5f), jointRadius, jointRadius);

                sprite.Draw(jointTexture, rect, Color.White);
            }
        }

        private Texture2D GestureTexture = null;
        private void InitGestureTexture()
        {
            GestureTexture = new Texture2D(sprite.GraphicsDevice, 1, 3);
            Color[] cols = { Color.Red, Color.Yellow, Color.Green };
            GestureTexture.SetData(cols);
        }

        private bool showkinectGestures = false;
        public bool ShowKinectGestures { set { showkinectGestures = value; } get { return showkinectGestures; } }

        private void RenderKinectGestures()
        {
            if (!showkinectGestures) return;

            InputService inp = ServiceProvider.GetService<InputService>();
            KinectDevice kinect = inp.GetKinect();

            Vector2 pos = new Vector2(250, 50);
            Vector2 origin = new Vector2(0,1);
            foreach (JointMovement move in kinect.DetectedMovements)
            {
                Vector2 direction = move.Direction;
                float angle = (float)Math.Atan2(direction.X, direction.Y);

                sprite.Draw(
                    GestureTexture,
                    pos,
                    null,
                    Color.White,
                    angle,
                    origin,
                    7.0f,
                    SpriteEffects.None, 0);

                pos.X = pos.X + 32;
            }
        }
        #endregion

        #region IDisposable
        public void Dispose()
        {
            rs.Dispose();
        }
        #endregion
    }
}
