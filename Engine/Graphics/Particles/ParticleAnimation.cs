﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Linq;
using System.IO;

using Engine.Core;
using Engine.Tools;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    /// <summary>
    /// Particle animation description. Contains data on how to
    /// update the particle system and also contains functions
    /// to help calculate stuff.
    /// 
    /// This class only hold information and help calculate more.
    /// It doesn't actually perform the animations, that's done
    /// in ParticleSystem.cs
    /// </summary>
    public class ParticleAnimation
    {
        /// <summary>
        /// Equal to 1/(colors.Length-1). This value is used to calculate the color.
        /// It is kept here so it doesn't have to be recalculated everytime.
        /// </summary>
        private float split01;

        #region Properties
        private Color[] colors;
        public Color[] Colors { 
            set
            {
                split01 = 1.0f / (value.Length - 1);
                colors = value;
            } 
            get{return colors;} 
        }
        public Texture2D Texture { set; get; }
        public string TextureFileName { set; get; }

        // scaling
        public float GrowSize { set; get; }
        public float MinSize { set; get; }
        public float MaxSize { set; get; }

        // Energy (lifetime)
        public float MinEnergy { set; get; }
        public float MaxEnergy { set; get; }

        // # of particles spawning
        public int MinAmount { set; get; }
        public int MaxAmount { set; get; }

        // movement
        public float MinSpeed { set; get; }
        public float MaxSpeed { set; get; }
        public Vector3 Gravity { set; get; }

        // vector angle randomisation:
        public float MinBearing { set; get; }
        public float MaxBearing { set; get; }
        public float MinElevation { set; get; }
        public float MaxElevation { set; get; }

        // looping
        public float Interval { set; get; } //interval between emissions. Useless if Loop=false;
        public bool Loop { set; get; }
        #endregion

        #region Constructors
        public ParticleAnimation()
        {
            MinBearing = MinElevation = 0;
            MaxBearing = MaxElevation = (float) Math.PI * 2.0f;
        }
        #endregion

        #region Factory methods
        public Color GetColor(float start, float now, float end)
        {
            float duration = end - start;

            // bring now, fadein, fadeout into [0,1] time scale
            now = (now - start) / duration;
            if (now <= 0.0f) return colors[0];
            if (now >= 1.0f) return colors[colors.Length - 1];

            // get the 2 involved colors
            int i = (int) (now / split01);
            Color c1 = Colors[i];
            Color c2 = Colors[i + 1];

            // mix the colors:
            float nowc = (now - i * split01) / split01;
            Color c = Color.Lerp(c1, c2, nowc);

            //Console.WriteLine(now + "   "+ nowc + "   "+c.ToString());
            /*
            // add alpha effect (fade in/out);
            c.A = (byte) (c.A * OpacityFunction(now));
             */
            return c;
        }

        public float GetNormalizedLifeTimeInColorSegment(float start, float now, float end)
        {
            float duration = end - start;

            // bring now, fadein, fadeout into [0,1] time scale
            now = (now - start) / duration;
            if (now <= 0.0f) return 0;
            if (now >= 1.0f) return 1;

            // bring now into [0,1] for this color segment;
            int n = (int)(now / split01);
            now = n - n * split01;
            return now / split01;
        }

        /// <summary>
        /// Creates a particle at specific location
        /// </summary>
        /// <param name="position">spawning point</param>
        /// <returns>A random particle point</returns>
        internal Particle CreateRandomParticle(Vector3 position)
        {
            return Particle.GetParticle(
                GameTiming.TotalTimeElapsed,
                GameTiming.TotalTimeElapsed + Random2.RandomF(MinEnergy, MaxEnergy),
                Random2.RandomF(MinSize, MaxSize),
                Random2.RandomVector3(MinSpeed, MaxSpeed, MinBearing, MaxBearing, MinElevation, MaxElevation),
                position,
                colors[0],
                Random2.RandomAngle(),
                Random2.RandomF(MinSpeed, MaxSpeed) * Random2.RandomSign());
        }

        /// <summary>
        /// Do not use this on the XBox!!
        /// <param name="anim">Animation to save.</param>
        /// 
        /// <param name="outfile">File to save it to.</param>
        /// </summary>
        public static void SaveToXmlFile(ParticleAnimation anim, string texture, string outfile)
        {
            StreamWriter outstm = new StreamWriter(outfile);
            XDocument xdoc = new XDocument();

            XElement material = new XElement("Material",
                new XElement("Texture", texture));

            XElement[] colors = new XElement[anim.Colors.Length];
            for (int i = 0; i < colors.Length; i++)
                colors[i] = new XElement("Color", Parser.UnparseColor(anim.Colors[i]));

            XElement animation = new XElement("Animation", new XElement[] {
                new XElement("Colors", colors),
                new XElement("GrowSize", anim.GrowSize),
                new XElement("MinSize", anim.MinSize),
                new XElement("MaxSize", anim.MaxSize),
                new XElement("MinAmount", anim.MinAmount),
                new XElement("MaxAmount", anim.MaxAmount),
                new XElement("MinEnergy", anim.MinEnergy),
                new XElement("MaxEnergy", anim.MaxEnergy),
                new XElement("MinSpeed", anim.MinSpeed),
                new XElement("MaxSpeed", anim.MaxSpeed),
                new XElement("Gravity", Parser.UnparseVector3(anim.Gravity)),
                new XElement("Interval", anim.Interval),
                new XElement("Loop", anim.Loop ? "1": "0")});

            xdoc.Add(new XElement("ParticleSystem", new object[]{
                material,
                animation}));

            outstm.Write(xdoc.ToString());
            outstm.Close();
        }

        /// <summary>
        /// Creates a particle system from an XML file
        /// </summary>
        /// <returns></returns>
        public static ParticleAnimation CreateParticleAnimation(Stream s)
        {
            XDocument particleXml = XDocument.Load(s);

            string tfn = "";

            // Load Texture
            Texture2D texture =
                (from material in particleXml.Descendants("Material")
                 select LoadTexture(material.Element("Texture").Value, out tfn)).ToArray()[0];

            // Load Colors
            Color[] colors =
                (from color 
                     in particleXml.Descendants("Animation").First()
                     .Descendants("Colors").First()
                     .Descendants("Color")
                 select new Color(Parser.ParseVector4(color.Value))).ToArray();

            // Create particle animation
            ParticleAnimation pa =
                (from anim in particleXml.Descendants("Animation")
                 select new ParticleAnimation() 
                 {
                     Colors = colors,
                     Texture = texture,

                     GrowSize = float.Parse(anim.Element("GrowSize").Value),
                     MinSize = float.Parse(anim.Element("MinSize").Value),
                     MaxSize = float.Parse(anim.Element("MaxSize").Value),

                     MinEnergy = float.Parse(anim.Element("MinEnergy").Value),
                     MaxEnergy = float.Parse(anim.Element("MaxEnergy").Value),

                     MinAmount = int.Parse(anim.Element("MinAmount").Value),
                     MaxAmount = int.Parse(anim.Element("MaxAmount").Value),

                     MinSpeed = float.Parse(anim.Element("MinSpeed").Value),
                     MaxSpeed = float.Parse(anim.Element("MaxSpeed").Value),
                     Gravity = Parser.ParseVector3(anim.Element("Gravity").Value),

                     Interval = float.Parse(anim.Element("Interval").Value),
                     Loop = anim.Element("Loop").Value != "0"
                 }).ToArray()[0];

            pa.TextureFileName = tfn;

            // Try parse optional elements:
            XElement animationNode = particleXml.Descendants("Animation").First();

            // angles:
            if (animationNode.Descendants("MinBearing").Count() != 0)
                pa.MinBearing = float.Parse(animationNode.Descendants("MinBearing").First().Value);
            if (animationNode.Descendants("MaxBearing").Count() != 0)
                pa.MaxBearing = float.Parse(animationNode.Descendants("MaxBearing").First().Value);
            if (animationNode.Descendants("MinElevation").Count() != 0)
                pa.MinElevation = float.Parse(animationNode.Descendants("MinElevation").First().Value);
            if (animationNode.Descendants("MaxElevation").Count() != 0)
                pa.MaxElevation = float.Parse(animationNode.Descendants("MaxElevation").First().Value);

            return pa;
        }

        public static ParticleAnimation CreateParticleAnimation(string filename)
        {
            Stream particleXmlStream = TitleContainer.OpenStream(filename);
            if (particleXmlStream == null) throw new IOException("file not found");

            return CreateParticleAnimation(particleXmlStream);
        }


        private static Texture2D LoadTexture(string s, out string tfn)
        {
            ContentService cs = ServiceProvider.GetService<ContentService>();
            tfn = s;
            return cs["game"].Load<Texture2D>(s);
        }

        #endregion
    }
}
