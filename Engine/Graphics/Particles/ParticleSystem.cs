﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Tools;
using Engine.Scene;

using Microsoft.Xna.Framework;

namespace Engine.Graphics
{
    public enum ParticleSystemSpaceMode
    {
        LocalSpace,
        WorldSpace
    }

    public class ParticleSystem
    {
        #region Variables
        private List<Particle> deadParts;
        private ParticleAnimation anim;
        private List<Particle> parts;
        private Random rand;

        private bool emitting;
        private float stopAt;
        private float startedAt;
        private float interval;
        #endregion

        #region Properties
        public bool IsEmitting { get { return emitting; } }

        /// <summary>
        /// Returns true if this particle system has at least 1 live particle
        /// </summary>
        public bool IsAlive { get { return Particles.Count > 0; } }

        private bool culled = false;
        public bool FrustumCulled { get { return culled; } internal set { culled = value; } }

        // the translation to apply to all the particles
        public ParticleSystemSpaceMode ParticleSystemSpaceMode {get;set;}

        /**
         * The variables are used to insure that the particle culling doesn't hide particles
         * if they are moving in the WorldSpace mode. The old_old_ variable is used to measure the
         * (maximum) difference between the current center that the center of the oldest possible
         * particle. 
         * 
         * old_ is updated every MAX_ENERGY seconds.
         * 
         * old_old_ is updated with old_ when we are guaranteed that no particles are hidden.
         */
        private Vector3 old_AttachementTranslation;
        private Vector3 old_old_AttachementTranslation;
        private float flyTime;

        internal string AttachmentPoint { set; get; }
        internal Vector3 AttachementTranslation { set; get; }
        internal void InitOldAttachementTranslation(Vector3 pos)
        {
            this.old_AttachementTranslation = pos;
            this.old_old_AttachementTranslation = pos;
        }

        internal List<Particle> Particles { get { return parts; } }

        public ParticleAnimation Animation { get { return anim; } }
        #endregion

        public ParticleSystem(ParticleAnimation anim)
            : this(anim, ParticleSystemSpaceMode.LocalSpace)
        { }

        public ParticleSystem(ParticleAnimation anim, ParticleSystemSpaceMode mode)
        {
            deadParts = new List<Particle>();
            this.rand = new Random();
            this.anim = anim;
            this.parts = new List<Particle>();
            this.emitting = false;
            this.startedAt = this.stopAt = this.interval = 0.0f;
            this.ParticleSystemSpaceMode = mode;
        }

        /// <summary>
        /// Start emitting particles:
        /// </summary>
        public void StartEmitting()
        {
            emitting = true;

            startedAt = GameTiming.TimeElapsed;
            if (!anim.Loop) stopAt = GameTiming.TimeElapsed + anim.MaxEnergy;

            Emit();
        }

        private void Emit()
        {
            int nparts = Random2.Random(anim.MinAmount, anim.MaxAmount);

            for (int i = 0; i < nparts; i++)
            {
                Vector3 spawn = (ParticleSystemSpaceMode == ParticleSystemSpaceMode.LocalSpace) ?
                    Vector3.Zero :
                    AttachementTranslation;
                parts.Add(anim.CreateRandomParticle(spawn));
            }
        }

        /// <summary>
        /// Stop emitting particles. Particles which have already been emitted and are
        /// still alive will still render.
        /// </summary>
        public void StopEmitting()
        {
            emitting = false;
        }

        /// <summary>
        /// Clear all the emitted particles immediately.
        /// </summary>
        public void Clear()
        {
            parts.Clear();
        }

        /// <summary>
        /// Stops emitting particles and removes the ones which are still alive.
        /// </summary>
        public void StopAndClear()
        {
            StopEmitting();
            Clear();
        }

        public BoundingSphere GetBoundingSphere()
        {
            // calc max distance
            float maxSize = anim.GrowSize > 0.0f ?
                anim.MaxSize + anim.GrowSize * anim.MaxEnergy :
                anim.MaxSize;

            float maxDist 
                = anim.MaxSpeed * anim.MaxEnergy 
                + anim.Gravity.Length() * anim.MaxEnergy * anim.MaxEnergy / 2.0f;

            float radius = maxDist + maxSize;

            if (this.ParticleSystemSpaceMode == ParticleSystemSpaceMode.WorldSpace)
            {
                radius += (this.old_old_AttachementTranslation - this.AttachementTranslation).Length();
            }

            return new BoundingSphere(AttachementTranslation, radius);
        }

        internal Vector3 GetParticleGlobalPosition(Particle p)
        {
            switch (this.ParticleSystemSpaceMode)
            {
                case ParticleSystemSpaceMode.LocalSpace:
                    return p.Position + this.AttachementTranslation;
                case ParticleSystemSpaceMode.WorldSpace:
                    return p.Position;
                default:
                    throw new Exception("Unknown ParticleSystemSpaceMode");
            }
        }
                
        /// <summary>
        /// Update this particle emitter
        /// </summary>
        /// <returns>true if not complete. false if the animation is done.</returns>
        internal bool Update()
        {
            float dt = GameTiming.TimeElapsed;
            float now = GameTiming.TotalTimeElapsed;

            deadParts.Clear();

            // Update culling data:
            if (this.ParticleSystemSpaceMode == ParticleSystemSpaceMode.WorldSpace)
            {
                flyTime += dt;

                if (flyTime >= anim.MaxEnergy * 2)
                {
                    old_old_AttachementTranslation = old_AttachementTranslation;
                }
                else if (flyTime >= anim.MaxEnergy)
                {
                    old_AttachementTranslation = AttachementTranslation;
                }
            }

            // Update each particle
            foreach (Particle p in parts)
            {
                if (p.IsDead)
                    deadParts.Add(p);
                else
                {
                    p.Size += anim.GrowSize * dt;
                    if (p.Size < 0.0f) p.Size = 0;
                    p.Velocity += anim.Gravity * dt;

                    p.Position += p.Velocity * dt;
                    p.RefreshVertices( GetParticleGlobalPosition(p) );

                    p.Rotation += p.RotationSpeed * dt;
                    p.CurrentColor = anim.GetColor(p.BirthTime, now, p.DeathTime);
                }
            }

            // Remove unneeded particles
            foreach (Particle p in deadParts)
            {
                parts.Remove(p);
                Particle.CacheParticle(p); // save a "new" operation
            }

            // Keep emitting:
            if (anim.Loop && IsEmitting)
            {
                interval += dt;
                if (interval >= anim.Interval)
                {
                    Emit();
                    interval -= anim.Interval;
                }
                return true;
            }
           
            // Are we done?
            else
                return IsAlive;
        }
    }
}
