﻿/**
 * @author: Olivier "SanitySlayer" Legat
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;

using Microsoft.Xna.Framework;

namespace Engine.Graphics
{
    /// <summary>
    /// This class represents data of a single sprite point.
    /// </summary>
    internal class Particle
    {
        #region Variables
        /* Life variables: */
        private float birth;
        private float death;

        /* Animation variables: */
        private float size;
        private float rot;
        private float rotSpeed;
        private Vector3 vel;
        private Vector3 pos;
        private Color col;

        private PointSpriteVertex[] vertex;
        #endregion

        #region Properties
        public float BirthTime { get { return birth; } }
        public float DeathTime { get { return death; } }
        public float Life { get { return death-birth; } }

        public bool IsDead { get { return GameTiming.TotalTimeElapsed > DeathTime; } }

        public PointSpriteVertex[] Vertices { get { return vertex; } }

        public float Size { 
            set 
            {
                size = value;
                vertex[0].Transform.X = size;
                vertex[1].Transform.X = size;
                vertex[2].Transform.X = size;
                vertex[3].Transform.X = size;
            } 
            get { return size; } 
        }
        public Vector3 Velocity { set { vel = value; } get { return vel; } }
        public Vector3 Position { 
            set 
            { 
                pos = value;
                vertex[0].Position = pos;
                vertex[1].Position = pos;
                vertex[2].Position = pos;
                vertex[3].Position = pos;
            } get { return pos; } 
        }

        public Color CurrentColor { 
            set 
            { 
                col = value;
                vertex[0].Color = col.ToVector4();
                vertex[1].Color = col.ToVector4();
                vertex[2].Color = col.ToVector4();
                vertex[3].Color = col.ToVector4();
            } 
            get { return col; } 
        }

        public float Rotation { 
            set 
            { 
                rot = value;
                float pi2 = (float)Math.PI * 2;
                if (rot < 0.0f) rot += pi2;
                if (rot > pi2) rot -= pi2;

                vertex[0].Transform.Y = rot;
                vertex[1].Transform.Y = rot;
                vertex[2].Transform.Y = rot;
                vertex[3].Transform.Y = rot;
            } 
            get { return rot; } 
        }
        public float RotationSpeed { set { rotSpeed = value; } get { return rotSpeed; } }

        internal float Distance { set; get; }
        #endregion

        #region Constructors
        private Particle()
            : this(1.0f, new Vector3(0), new Vector3(0), Color.White)
        { }

        private Particle(float size, Vector3 vel, Vector3 pos, Color col)
            : this(
            GameTiming.TotalTimeElapsed, 
            GameTiming.TotalTimeElapsed + 1, 
            size, vel, pos, col)
        { }

        private Particle(float birth, float death, float size, Vector3 vel, Vector3 pos, Color col)
            : this(birth, death, size, vel, pos, col, 0.0f, 0.0f)
        { }

        //private static int partcount = 0;
        private Particle(float birth, float death, float size, Vector3 vel, Vector3 pos, Color col, float rot, float rotSpeed)
        {
            //Console.WriteLine("create at:"+ GameTiming.TotalTimeElapsed+"  count:"+(++partcount));
            this.birth = birth;
            this.death = death;
            this.size = size;
            this.vel = vel;
            this.pos = pos;
            this.col = col;
            this.rot = rot;
            this.rotSpeed = rotSpeed;
            this.vertex = new PointSpriteVertex[4];
            this.vertex[0] = new PointSpriteVertex(pos, new Vector2(0, 0), col, size, rot);
            this.vertex[1] = new PointSpriteVertex(pos, new Vector2(1, 0), col, size, rot);
            this.vertex[2] = new PointSpriteVertex(pos, new Vector2(1, 1), col, size, rot);
            this.vertex[3] = new PointSpriteVertex(pos, new Vector2(0, 1), col, size, rot);
        }
        #endregion

        #region Methods
        internal void RefreshVertices(Vector3 pos)
        {
            vertex[0].Position = pos;
            vertex[1].Position = pos;
            vertex[2].Position = pos;
            vertex[3].Position = pos;
        }
        #endregion

        #region Static methods (particle caching)
        private static List<Particle> cache = new List<Particle>();

        /// <summary>
        /// Maximum amount of particles to cache. 0 = unlimited
        /// </summary>
        private static int CacheSize { set; get; }

        /// <summary>
        /// Return return if CacheParticle(Particle) will accept more
        /// particles
        /// </summary>
        private static bool IsCacheFull
        {
            get
            {
                return (CacheSize != 0) && (cache.Count < CacheSize);
            }
        }

        /// <summary>
        /// Returns true if there are no particles in the cache.
        /// </summary>
        private static bool IsCacheEmpty
        {
            get
            {
                return cache.Count == 0;
            }
        }

        /// <summary>
        /// Add this particle to the cache.
        /// </summary>
        /// <param name="p">Any particle</param>
        /// <returns>true if the particle was added to the cache. false if 
        /// the cache is full</returns>
        internal static bool CacheParticle(Particle p)
        {
            if (IsCacheFull) return false;
            cache.Add(p);
            return true;
        }

        internal static Particle GetParticle(
            float birth, float death, float size, 
            Vector3 vel, Vector3 pos, Color col, 
            float rot, float rotSpeed)
        {
            // No cached particle. Create a new one.
            if (IsCacheEmpty)
                return new Particle(birth, death, size, vel, pos, col, rot, rotSpeed);

            // Pop a cached particle.
            Particle p = cache[0];
            cache.RemoveAt(0);

            // Set the data.
            p.birth = birth;
            p.death = death;
            p.size = size;
            p.vel = vel;
            p.pos = pos;
            p.col = col;
            p.rot = rot;
            p.rotSpeed = rotSpeed;
            for (int i = 0; i < 4; i++)
            {
                p.vertex[i].Position = pos;
                p.vertex[i].Color = col.ToVector4();
                p.vertex[i].Transform.X = size;
                p.vertex[i].Transform.Y = rot;
            }

            return p;
        }

        private static PointSpriteVertex[] vertexArray = new PointSpriteVertex[20000];
        /// <summary>
        /// Get a vertex array with ATLEAST "length" elements. This function returns
        /// a cached array if return of elements required is less than or equal to
        /// to amount stored in the cache.
        /// </summary>
        /// <param name="length">Number of elements to contain ATLEAST</param>
        /// <returns>An array with atleast "length". Note that there is
        /// no guarantee that the array will be exactly the specified length.</returns>
        internal static PointSpriteVertex[] GetVertexArray(int length)
        {
            if (length > vertexArray.Length)
                vertexArray = new PointSpriteVertex[length];

            return vertexArray;
        }

        #endregion
    }

    internal class FurthestFirst : IComparer<Particle>
    {
        private Vector3 campos;
        public FurthestFirst(Vector3 campos)
        {
            this.campos = campos;
        }

        public int Compare(Particle x, Particle y) 
        {
            if (x.Distance > y.Distance) 
                return -1;

            else if (x.Distance < y.Distance) 
                return 1;

            else
                return 0;
        }
    }
}
