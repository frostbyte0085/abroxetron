﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Engine.Tools;
using Engine.Core;

#if WINDOWS
using System.Windows.Forms;

#endif

namespace Engine.Graphics
{
    public class GraphicsService : IGraphicsDeviceService
    {
        GraphicsDevice device;
        PresentationParameters parameters;
        List<DisplayMode> modes;
        IntPtr windowHandle;

        static GraphicsService singletonInstance;
        // Keep track of how many controls are sharing the singletonInstance.
        static int referenceCount;

        private GraphicsService(IntPtr windowHandle,
                                           int width, int height, bool fullscreen, bool vsync)
        {
            this.windowHandle = windowHandle;

            EnumerateDisplayModes();
            CreateDevice(windowHandle, width, height, fullscreen, vsync);
        }

        internal static GraphicsService AddRef(IntPtr windowHandle,
                                           int width, int height, bool fullscreen, bool vsync)
        {
            // Increment the "how many controls sharing the device" reference count.
            if (Interlocked.Increment(ref referenceCount) == 1)
            {
                // If this is the first control to start using the
                // device, we must create the singleton instance.
                singletonInstance = new GraphicsService(windowHandle,
                                                              width, height, fullscreen, vsync);
            }

            return singletonInstance;
        }

        public bool IsModeSupported(int width, int height)
        {
            for (int i = 0; i < modes.Count; i++)
            {
                DisplayMode mode = modes[i];
     
               if (mode.Width == width && mode.Height == height)
                    return true;
            }
            return false;
        }

        private void EnumerateDisplayModes()
        {
            modes = new List<DisplayMode>();
            foreach (DisplayMode dm in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes)
            {
                modes.Add(dm);
            }
        }

        public void SetViewport(int x, int y, int width, int height)
        {
            Viewport viewport = new Viewport();

            viewport.X = x;
            viewport.Y = y;

            viewport.Width = width;
            viewport.Height = height;

            viewport.MinDepth = 0;
            viewport.MaxDepth = 1;

            device.Viewport = viewport;
        }

        internal void CreateDevice(IntPtr windowHandle, int width, int height, bool fullscreen, bool vsync)
        {
            if (fullscreen && !IsModeSupported(width, height))
            {
                throw new NoSuitableGraphicsDeviceException("Display mode " + width.ToString() + "x" + height.ToString() + " is not supported, cannot create the device!");
            }

            parameters = new PresentationParameters();

            parameters.BackBufferWidth = Math.Max(width, 1);
            parameters.BackBufferHeight = Math.Max(height, 1);
            parameters.BackBufferFormat = SurfaceFormat.Color;
            parameters.DepthStencilFormat = DepthFormat.Depth24Stencil8;
            parameters.DeviceWindowHandle = windowHandle;
            if (!vsync)
                parameters.PresentationInterval = PresentInterval.Immediate;
            else
                parameters.PresentationInterval = PresentInterval.One;

            parameters.IsFullScreen = fullscreen;

            device = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, parameters);

            if (DeviceCreated != null)
                DeviceCreated(this, EventArgs.Empty);
        }

        internal void ResetDevice(int width, int height)
        {
            ResetDevice(width, height, parameters.IsFullScreen, parameters.PresentationInterval == PresentInterval.One);
        }

        public void ResetDevice(int width, int height, bool fullscreen, bool vsync)
        {
#if WINDOWS
            if (!fullscreen)
            {
                parameters.BackBufferWidth = width;
                parameters.BackBufferHeight = height;
                parameters.IsFullScreen = false;
                if (!vsync)
                    parameters.PresentationInterval = PresentInterval.Immediate;
                else
                    parameters.PresentationInterval = PresentInterval.One;
            }
            else
            {
                if (IsModeSupported(width, height))
                {
                    parameters.BackBufferWidth = width;
                    parameters.BackBufferHeight = height;
                    parameters.IsFullScreen = true;
                    if (!vsync)
                        parameters.PresentationInterval = PresentInterval.Immediate;
                    else
                        parameters.PresentationInterval = PresentInterval.One;
                }
                else
                {
                    throw new NoSuitableGraphicsDeviceException("Display mode " + width.ToString() + "x" + height.ToString() + " is not supported, cannot re-create the device!");
                }
            }

            GameTiming.Paused = true;
            IsResetting = true;

            if (DeviceResetting != null)
                DeviceResetting(this, EventArgs.Empty);

            device.Reset(parameters);

            Control renderForm = Control.FromHandle(windowHandle);
            renderForm.Width = parameters.BackBufferWidth;
            renderForm.Height = parameters.BackBufferHeight;
            
            if (DeviceReset != null)
                DeviceReset(this, EventArgs.Empty);

            IsResetting = false;
            GameTiming.Paused = false;
#endif
        }

        public bool IsResetting
        {
            private set;
            get;
        }

        public Vector2 MaximumResolution
        {
            get
            {
                return new Vector2(modes[modes.Count - 1].Width, modes[modes.Count-1].Height);
            }
        }
        internal void Release(bool disposing)
        {
            // Decrement the "how many controls sharing the device" reference count.
            if (Interlocked.Decrement(ref referenceCount) == 0)
            {
                // If this is the last control to finish using the
                // device, we should dispose the singleton instance.
                if (disposing)
                {
                    if (DeviceDisposing != null)
                        DeviceDisposing(this, EventArgs.Empty);

                    device.Dispose();
                }

                device = null;
            }
        }

        public GraphicsDevice GraphicsDevice
        {
            get { return device; }
        }

        public PresentationParameters PresentationParameters
        {
            get { return parameters; }
        }

        #region IGraphicsDeviceService
        public event EventHandler<EventArgs> DeviceCreated;
        public event EventHandler<EventArgs> DeviceDisposing;
        public event EventHandler<EventArgs> DeviceReset;
        public event EventHandler<EventArgs> DeviceResetting;
        #endregion
    }
}
