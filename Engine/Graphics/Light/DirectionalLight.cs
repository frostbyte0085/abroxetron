﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    public sealed class DirectionalLight : ILight
    {
        public DirectionalLight(Vector4 color, float intensity)
        {
            Color = color;
            Intensity = intensity;
        }

        public Vector4 Color
        {
            set;
            get;
        }

        public float Intensity
        {
            set;
            get;
        }


        public LightType LightType
        {
            get
            {
                return LightType.Directional;
            }
        }

        public float Radius
        {
            set
            {
            }
            get
            {
                return 0.0f;
            }
        }
    }
}
