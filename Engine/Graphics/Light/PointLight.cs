﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Engine.Graphics;
using Engine.Core;

namespace Engine.Graphics
{
    public sealed class PointLight : ILight
    {
        GraphicsService gs;

        public PointLight(Vector4 color, float intensity, float radius)
        {
            gs = ServiceProvider.GetService<GraphicsService>();

            Color = color;
            Intensity = intensity;
            Radius = radius;
        }

        public Vector4 Color
        {
            set;
            get;
        }

        public float Intensity
        {
            set;
            get;
        }

        public LightType LightType
        {
            get
            {
                return LightType.Point;
            }
        }

        public float Radius
        {
            set;
            get;
        }
    }
}
