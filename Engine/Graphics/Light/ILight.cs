﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.Graphics
{
    public enum LightType
    {
        Directional,
        Point
    }

    public interface ILight
    {
        Vector4 Color
        {
            set;
            get;
        }

        float Intensity
        {
            set;
            get;
        }

        LightType LightType
        {
            get;
        }

        float Radius
        {
            set;
            get;
        }
    }
}
