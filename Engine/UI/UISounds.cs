﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework.Audio;
using Engine.Audio;

namespace Engine.UI
{
    /// <summary>
    /// Small container to hold the SoundEffects to be played when certain
    /// UI events are fired.
    /// </summary>
    public static class UISounds
    {
        /// <summary>
        /// The sound played when the "back" button is clicked
        /// </summary>
        public static SoundEffect BackSound { set; get; }

        /// <summary>
        /// The sound played when a button is clicked
        /// </summary>
        public static SoundEffect ClickSound { set; get; }

        /// <summary>
        /// The sound played when a menu item is selected
        /// </summary>
        public static SoundEffect ItemChangedSound { set; get; }
    }
}
