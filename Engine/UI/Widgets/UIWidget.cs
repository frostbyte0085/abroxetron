﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// Abstract widget class. A widget is either interactable or simply decorative.
    /// If the widget is interactable it's adviced to use the UISelectable class.
    /// </summary>
    public abstract class UIWidget
    {
        private Rectangle rect;
        public virtual Rectangle Rectangle { set { rect = value; } get { return rect; } }

        /// <summary>
        /// Color of the widget. (only affects some widgets)
        /// </summary>
        public Color Color { set; get; }

        public UIWidget()
        {
            Color = Color.White;
        }

        public virtual int Width { set { rect.Width = value; } get { return rect.Width; } }
        public virtual int Height { set { rect.Height = value; } get { return rect.Height; } }

        public virtual int X { set { rect.X = value; } get { return rect.X; } }
        public virtual int Y { set { rect.Y = value; } get { return rect.Y; } }

        public virtual Point Position
        {
            set
            {
                this.X = Position.X;
                this.Y = Position.Y;
            }
            get { return new Point(X,Y); }
        }

        public abstract bool IsSelected { get; }

        public bool Enabled { set; get; }

        public virtual void CenterX(Rectangle region) 
        {
            int theCenter = region.X + (int)(region.Width / 2.0f + 0.5f);
            this.X = theCenter - (int)(this.Width / 2.0f + 0.5f);
        }
        public virtual void CenterY(Rectangle region)
        {
            int theCenter = region.Y + (int)(region.Height / 2.0f + 0.5f);
            this.Y = theCenter - (int)(this.Height / 2.0f + 0.5f);
        }
        public virtual void Center(Rectangle region)
        {
            CenterX(region);
            CenterY(region);
        }

        internal abstract void Draw(UIPainter painter);
    }
}
