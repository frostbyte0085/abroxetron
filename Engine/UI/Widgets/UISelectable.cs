﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// A widget which can be selected by an XBox 360 controller
    /// </summary>
    public abstract class UISelectable : UIWidget
    {
        protected bool isSelected;
        public override bool IsSelected { get { return isSelected; } }
        internal protected void SetSelected(bool selected) { isSelected = selected; }
    }
}