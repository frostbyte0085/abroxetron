﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// The label widget. Plain colored text with no background (yet)
    /// </summary>
    public class UILabel : UIWidget
    {
        #region Properties
        /// <summary>
        /// The font of the text
        /// </summary>
        public SpriteFont Font { set; get; }

        /// <summary>
        /// Text to display
        /// </summary>
        public string Text { set; get; }
        #endregion

        #region Constructor
        public UILabel(string text, SpriteFont font, Color textColor)
            : this(text, font, textColor, Rectangle.Empty) { }
        public UILabel(string text, SpriteFont font, Color textColor, Rectangle rect)
        {
            this.Text = text;
            this.Font = font;
            this.Color = textColor;
            this.Rectangle = rect;
        }
        #endregion

        #region UIWidget override
        public override bool IsSelected { get { return false; } }

        internal override void Draw(UIPainter painter)
        {
            painter.DrawText(Font, Text, this.Color, this.Rectangle);
        }
        #endregion
    }
}