﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// States how to handle newly added widgets
    /// </summary>
    public enum DialogAdjustmentMode
    {
        Automatic = 0,
        Off = 1
    }

    /// <summary>
    /// Parameters used in auto-adjusting mode
    /// </summary>
    public struct DialogAutoAdjust
    {
        /// <summary>
        /// Height of each widget (in pxl)
        /// </summary>
        public int WidgetHeight;

        /// <summary>
        /// Gap in between each widget (in pxl)
        /// </summary>
        public int GapWidget;

        /// <summary>
        /// Gap in between the right side of the widgets and the left of the dialog (in pxl)
        /// </summary>
        public int GapLeft;

        /// <summary>
        /// Gap in between the left side of the widgets and the right of the dialog (in pxl)
        /// </summary>
        public int GapRight;

        /// <summary>
        /// Gap in between the top of the first widge and the top of the dialog (in pxl)
        /// </summary>
        public int GapTop;

        /// <summary>
        /// Returns a redefined auto-adjust based on the width / height of the dialog
        /// </summary>
        /// <param name="dialogWidth"></param>
        /// <param name="dialogHeight"></param>
        /// <returns></returns>
        public static DialogAutoAdjust GetDefaultAutoAdjust(int dialogWidth, int dialogHeight)
        {
            DialogAutoAdjust adjust = new DialogAutoAdjust();
            adjust.WidgetHeight = (int)(dialogHeight * 0.1f);
            adjust.GapRight = adjust.GapLeft = adjust.GapTop = adjust.GapWidget = (int)(adjust.WidgetHeight * 0.2f);

            return adjust;
        }
    }

    /// <summary>
    /// The Dialog with a background and with widgets on top.
    /// 
    /// Uses the "DIA" tag in the atlas.
    /// </summary>
    public class UIDialog : UIContainer
    {
        public DialogAdjustmentMode DialogAdjustmentMode { set; get; }

        public DialogAutoAdjust DialogAutoAdjust { set; get; }

        public UIDialog()
            : this(new Rectangle(0, 0, 100, 200))
        { }
        public UIDialog(Rectangle rect)
            : this(DialogAdjustmentMode.Automatic, rect)
        { }
        public UIDialog(DialogAdjustmentMode mode, Rectangle rect)
            : this(mode, rect, DialogAutoAdjust.GetDefaultAutoAdjust(rect.Width, rect.Height))
        { }
        public UIDialog(DialogAdjustmentMode mode, Rectangle rect, DialogAutoAdjust adjust)
        {
            this.DialogAdjustmentMode = mode;
            this.DialogAutoAdjust = adjust;
            this.Rectangle = rect;
        }

        private void autoAdjust(UIWidget widget)
        {
            widget.Width = this.Width - this.DialogAutoAdjust.GapLeft - this.DialogAutoAdjust.GapRight;
            widget.Height = this.DialogAutoAdjust.WidgetHeight;
            widget.X = this.X + this.DialogAutoAdjust.GapLeft;
            widget.Y = this.Y + this.DialogAutoAdjust.GapTop;
            widget.Y += children.Count * (this.DialogAutoAdjust.GapWidget + this.DialogAutoAdjust.WidgetHeight);
        }

        public override bool IsSelected { get { return false; } }

        public override void Add(UIWidget widget)
        {
            base.Add(widget);

            switch (this.DialogAdjustmentMode)
            {
                case DialogAdjustmentMode.Automatic:
                    autoAdjust(widget);
                    break;

                default:
                    break;
            }
        }

        internal override void Draw(UIPainter painter)
        {
            painter.DrawFromAtlas("DIA", this.Rectangle);
            base.Draw(painter);
        }
    }
}
