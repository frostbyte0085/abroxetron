﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// A basic static Texture
    /// </summary>
    public class UITexture : UIWidget
    {
        #region Properties
        /// <summary>
        /// Texture used.
        /// </summary>
        public Texture2D Texture { set; get; }

        public float Rotation { set; get; }

        public override bool IsSelected { get { return false; } }
        #endregion

        #region Constructors
        public UITexture()
            : this(Rectangle.Empty) { }

        public UITexture(Rectangle rectangle)
            : this(null, rectangle) { }

        public UITexture(Texture2D texture, Rectangle rectangle)
        {
            this.Texture = texture;
            this.Rectangle = rectangle;
            this.Rotation = 0.0f;
        }
        #endregion

        #region Updating
        internal override void Draw(UIPainter painter)
        {
            if (Texture == null) return;

            if (this.Rotation != 0.0f)
            {
                Rectangle src = new Rectangle(0, 0, Texture.Width, Texture.Height);
                painter.DrawTexture(Texture, Rectangle, src, this.Color, this.Rotation);
            }
            else painter.DrawTexture(Texture, Rectangle, this.Color);

        }
        #endregion
    }
}
