﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

using Engine.Core;
using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    public struct UITileData
    {
        /// <summary>
        /// Width of the tiles in pixels
        /// </summary>
        public int TileWidth { set; get; }

        /// <summary>
        /// Height of the tiles in pixels
        /// </summary>
        public int TileHeight { set; get; }

        /// <summary>
        /// Timing interval between the tiles
        /// </summary>
        public float Interval { set; get; }

        public static UITileData GetDefaultData()
        {
            return new UITileData()
            {
                TileWidth = 16,
                TileHeight = 16,
                Interval = 0.1f
            };
        }

        public static UITileData LoadFromXml(string xmlFile)
        {
            Stream xmlStream = TitleContainer.OpenStream(xmlFile);
            XDocument xml = XDocument.Load(xmlStream);

            UITileData data = new UITileData();

            IEnumerator<XElement> e = xml.Descendants("TileWidth").GetEnumerator();
            e.MoveNext();
            data.TileWidth = int.Parse(e.Current.Value);

            e = xml.Descendants("TileHeight").GetEnumerator();
            e.MoveNext();
            data.TileHeight = int.Parse(e.Current.Value);

            e = xml.Descendants("Interval").GetEnumerator();
            e.MoveNext();
            data.Interval = float.Parse(e.Current.Value);

            return data;
        }
    };

    /// <summary>
    /// An tiled-animated texture.
    /// </summary>
    public class UITiledTexture : UIWidget
    {
        #region Properties
        /// <summary>
        /// Texture used.
        /// </summary>
        public Texture2D Texture { set; get; }

        public override bool IsSelected { get { return false; } }

        public UITileData TileData { set; get; }
        #endregion

        #region Constructors
        public UITiledTexture()
            : this(Rectangle.Empty) { }

        public UITiledTexture(Rectangle rectangle)
            : this(null, rectangle, UITileData.GetDefaultData()) { }

        public UITiledTexture(Texture2D texture, Rectangle rectangle, UITileData data)
        {
            this.Texture = texture;
            this.Rectangle = rectangle;
            this.TileData = data;
        }
        #endregion

        #region Updating
        private int xOff, yOff;
        private float elapsedTime;

        public void ResetAnimation()
        {
            elapsedTime = xOff = yOff = 0;
        }

        private void shiftOffsets()
        {
            // Update timing
            elapsedTime += GameTiming.TimeElapsed;
            
            // Must we switch to a new tile?
            if (elapsedTime >= this.TileData.Interval)
                elapsedTime -= TileData.Interval; // yes!
            else return; // no

            // Update offsets:
            xOff += TileData.TileWidth;

            // Does the new source rectangle go outside the texture x-coord?
            if (xOff + TileData.TileWidth > Texture.Width)
            {
                // Wrap the offsets down:
                xOff = 0;
                yOff += TileData.TileHeight;

                // Does the new new source rectangle go outside the texture.. again..? (y-coord)
                if (yOff + TileData.TileHeight > Texture.Height)
                {
                    // Return to the original position:
                    yOff = 0;
                }
            }
        }

        internal override void Draw(UIPainter painter)
        {
            if(Texture == null) return;

            Rectangle source = new Rectangle(xOff, yOff, TileData.TileWidth, TileData.TileHeight);
            shiftOffsets();

            painter.DrawTexture(Texture, Rectangle, source, this.Color);
        }
        #endregion
    }
}
