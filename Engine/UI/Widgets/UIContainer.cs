﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// A widget containing more widget.
    /// </summary>
    public abstract class UIContainer : UIWidget
    {
        /// <summary>
        /// List of the children binded to this widget
        /// </summary>
        protected List<UIWidget> children;

        public UIContainer()
        {
            children = new List<UIWidget>();
        }

        public virtual void Add(UIWidget widget)
        {
            children.Add(widget);
        }

        internal override void Draw(UIPainter painter)
        {
            foreach (UIWidget widget in children)
                widget.Draw(painter);
        }
    }
}
