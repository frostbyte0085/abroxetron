﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// A button with a label.
    /// 
    /// Uses the "BTN-select" and "BTN-normal" in the atlas.
    /// 
    /// -- TODO... add a picture to the BUTTON?
    /// </summary>
    public class UIButton : UISelectable
    {
        #region Properties
        private UILabel label;
        /// <summary>
        /// A UILabel on top of the button. The rectangle of this label is updated
        /// whenever the rectangle of this button is changed. However this label
        /// can still be access and the rectangle can be offsetted if desired.
        /// </summary>
        public UILabel Label
        {
            set
            {
                label = value;
                if (label != null)
                    label.Rectangle = this.Rectangle;
            }
            get { return label; }
        }
        #endregion

        #region Rectangle manipulation
        public override Rectangle Rectangle
        {
            get { return base.Rectangle; }
            set
            {
                if (Label != null) Label.Rectangle = value;
                base.Rectangle = value;
            }
        }
        public override int X
        {
            get { return base.X; }
            set
            {
                if (Label != null) Label.X = value;
                base.X = value;
            }
        }
        public override int Y
        {
            get { return base.Y; }
            set
            {
                if (Label != null) Label.Y = value;
                base.Y = value;
            }
        }
        public override int Width
        {
            get { return base.Width; }
            set
            {
                if (Label != null) Label.Width = value;
                base.Width = value;
            }
        }
        public override int Height
        {
            get { return base.Height; }
            set
            {
                if (Label != null) Label.Height = value;
                base.Height = value;
            }
        }
        #endregion

        #region Constructors
        public UIButton()
            : this(null, "") { }

        public UIButton(SpriteFont font, string text)
            : this(font, text, Color.Black) { }

        public UIButton(SpriteFont font, string text, Color color)
            : this(font, text, color, new Rectangle(0, 0, 0, 0)) { }

        public UIButton(SpriteFont font, string text, Color color, Rectangle rectangle)
        {
            if (text == null || font == null) Label = null;
            else Label = new UILabel(text, font, color, rectangle);

            this.Rectangle = rectangle;
        }
        #endregion

        #region Updating
        internal override void Draw(UIPainter painter)
        {
            painter.DrawFromAtlas(IsSelected ? "BTN-select" : "BTN-normal", this.Rectangle);
            if (Label != null) 
                Label.Draw(painter);
        }
        #endregion
    }
}
