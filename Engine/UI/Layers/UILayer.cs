﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Graphics;

using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// Abstract UILayer class. Set an instance of this class as the CurrentLayer
    /// to view it.
    /// </summary>
    public abstract class UILayer
    {
        public UIAtlas Atlas { set; get; }

        public abstract void Add(UIWidget widget);

        public abstract void Remove(UIWidget widget);

        /// <summary>
        /// Selector. Can be null, because some UIs layers might not depend
        /// on user input. Use with caution.
        /// </summary>
        public UISelector Selector { set; get; }

        public virtual void Update()
        {
            if (Selector != null)
                Selector.Update();
        }

        internal abstract void Draw(UIPainter painter);

        public void SetAsCurrent()
        {
            UIRendererService urs = ServiceProvider.GetService<UIRendererService>();
            urs.CurrentLayer = this;
        }
    }
}
