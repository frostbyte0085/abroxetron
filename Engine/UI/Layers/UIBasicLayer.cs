﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

using Engine.Graphics;

using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// A simple layer implementation. A list of widgets rendered in order
    /// </summary>
    public class UIBasicLayer : UILayer
    {
        #region Variables
        private List<UIWidget> widgets;
        #endregion

        #region Constructors
        public UIBasicLayer(UIAtlas atlas)
        {
            widgets = new List<UIWidget>();
            this.Atlas = atlas;
        }
        #endregion

        #region UILayer override
        public override void Add(UIWidget widget) { widgets.Add(widget); }
        public override void Remove(UIWidget widget) { widgets.Remove(widget); }

        internal override void Draw(UIPainter painter)
        {
            Debug.Assert(Atlas != null);

            foreach (UIWidget widget in widgets)
                widget.Draw(painter);
        }
        #endregion
    }
}
