﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Graphics;
using Engine.Input;
using Engine.Audio;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Engine.UI
{
    /// <summary>
    /// A UILayer that supports children and changes the current state in the 
    /// UIServiceManager dynamically.
    /// </summary>
    public class UITreeLayer : UIBasicLayer
    {
        #region Variables
        /// <summary>
        /// A child layer is binded to a selectable widget and an input key.
        /// Currently the UI system doesn't support more than 1 input key
        /// binding
        /// </summary>
        private class ChildLayer
        {
            /// <summary>
            /// The child layer
            /// </summary>
            public UITreeLayer target;
            /// <summary>
            /// The button that the use can click to access the target
            /// </summary>
            public UISelectable invoker;
            /// <summary>
            /// The input key mapping used to click.
            /// </summary>
            public string inputKey;
        }

        private List<ChildLayer> children;
        #endregion

        #region Properties
        /// <summary>
        /// Parent Layer. When the BackKey is pressed this layer is set to 
        /// this layer (unless null). If null, then this is a root node.
        /// </summary>
        public UITreeLayer Parent { set; get; }

        /// <summary>
        /// Input key binding that returns to the Parent layer.
        /// </summary>
        public string BackKey { set; get; }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new root layer.
        /// </summary>
        /// <param name="atlas"></param>
        public UITreeLayer(UIAtlas atlas)
            : this(atlas, null) { }
        /// <summary>
        /// Creates a new layer with a parent
        /// </summary>
        /// <param name="atlas"></param>
        /// <param name="parent"></param>
        public UITreeLayer(UIAtlas atlas, UITreeLayer parent)
            : base(atlas) 
        {
            BackKey = "B";
            Parent = parent;
            children = new List<ChildLayer>();
        }
        #endregion

        #region Add / remove / modify child
        /// <summary>
        /// Get the UILayer targetted by a selectable widget
        /// </summary>
        /// <param name="invoker">Invoke and changes layer.</param>
        /// <returns>The target, or null if the param isn't an invoker</returns>
        public UITreeLayer GetInvokerTarget(UISelectable invoker)
        {
            ChildLayer child = GetInvokerChild(invoker);
            return child == null ? null : child.target;
        }

        /// <summary>
        /// Set the UILayer targetted by a selectable widget. Creates a new child if
        /// invoker is not recognised as an existing invoker. Otherwise it overwrites
        /// the data for that invoker.
        /// </summary>
        /// <param name="invoker">Invoke and changes layer.</param>
        /// <param name="target">Which layer to change to</param>
        /// <param name="key">Which input key mapping to listen to</param>
        public void SetInvokerTarget(UISelectable invoker, UITreeLayer target, string key)
        {
            ChildLayer child = GetInvokerChild(invoker);
            if (child == null)
            {
                child = new ChildLayer();
                children.Add(child);
            }
            child.target = target;
            child.invoker = invoker;
            child.inputKey = key;

            if (target.Parent == null) target.Parent = this;
        }

        /// <summary>
        /// Get the child targetted by a selectable widget
        /// </summary>
        /// <param name="invoker">Invoke and changes layer.</param>
        /// <returns>The target, or null if the param isn't an invoker</returns>
        private ChildLayer GetInvokerChild(UISelectable invoker)
        {
            foreach (ChildLayer child in children)
            {
                if (child.invoker.Equals(invoker)) return child;
            }
            return null;
        }
        #endregion

        #region Updating the UI
        /// <summary>
        /// 
        /// </summary>
        /// <returns>The layer that a button triggered. Or null if nothing is pressed.</returns>
        internal UITreeLayer GetNewState()
        {
            if (Selector != null)
            {
                UISelectable selected = Selector.Selected;
                if (selected != null)
                {
                    ChildLayer child = GetInvokerChild(selected);
                    if (child != null && Selector.InputDevice.IsClicked(child.inputKey))
                    {
                        Console.WriteLine("clicked");
                        return child.target;
                    }
                }
            }
            return null;
        }

        public override void Update()
        {
            base.Update();
            UITreeLayer newLayer = GetNewState();
            if (newLayer != null && newLayer.Selector != null)
            {
                newLayer.Selector.ResetSelection();
                UIRendererService serv = ServiceProvider.GetService<UIRendererService>();
                serv.CurrentLayer = newLayer;
                // play a click sound
                if (UISounds.ClickSound != null)
                {
                    UISounds.ClickSound.Play();
                }
            }
            else if (Parent != null 
                && Selector != null 
                && !string.IsNullOrEmpty(BackKey) 
                && Selector.InputDevice.IsClicked(BackKey))
            {
                UIRendererService serv = ServiceProvider.GetService<UIRendererService>();
                serv.CurrentLayer = Parent;
                // play a back sound
                if (UISounds.BackSound != null)
                {
                    UISounds.BackSound.Play();
                }
            }
        }
        #endregion
    }
}
