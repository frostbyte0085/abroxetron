﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Input;
using Engine.Audio;

using Microsoft.Xna.Framework.Audio;

namespace Engine.UI
{
    /// <summary>
    /// A directional edge.
    /// </summary>
    internal class GraphEdge
    {
        /// <summary>
        /// Top / left node
        /// </summary>
        internal GraphNode Node1 { set; get; }
        /// <summary>
        /// Bottom / right node
        /// </summary>
        internal GraphNode Node2 { set; get; }
        /// <summary>
        /// Axis used to toggle between nodes
        /// </summary>
        internal InputAxis Axis { set; get; }

        internal GraphEdge(GraphNode node1, GraphNode node2, InputAxis axis)
        {
            this.Node1 = node1;
            this.Node2 = node2;
            this.Axis = axis;
        }
    }

    /// <summary>
    /// A graph node
    /// </summary>
    internal class GraphNode
    {
        /// <summary>
        /// Edges this node is linked to
        /// </summary>
        internal List<GraphEdge> edges;
        /// <summary>
        /// Widget in the graph
        /// </summary>
        internal UISelectable element;

        internal GraphNode()
            : this(null) { }
        internal GraphNode(UISelectable element)
        {
            this.edges = new List<GraphEdge>();
            this.element = element;
        }
    }

    public class UISelector
    {
        #region Variable
        /// <summary>
        /// The graph. Note that this class doesn't need to hold an edge list,
        /// so it doesn't.
        /// </summary>
        private List<GraphNode> nodes;

        /// <summary>
        /// Currently selected node
        /// </summary>
        private GraphNode selectedNode;

        /// <summary>
        /// Node to select on Reset;
        /// </summary>
        private GraphNode DefaultNode { set; get; }

        #endregion

        #region Properties
        /// <summary>
        /// The input device to listen to. Currently, only 1 device can be listened to.
        /// </summary>
        public InputDevice InputDevice { set; get; }

        /// <summary>
        /// The widget that is currently selected
        /// </summary>
        public UISelectable Selected
        {
            set
            {
                GraphNode node = find(value);
                if (node == null) throw new KeyNotFoundException("This selectable widget isn't in the graph. Forgot to add it?");

                Select(node);
            }
            get
            {
                return selectedNode == null ? null : selectedNode.element;
            }
        }

        /// <summary>
        /// The widget that is selected when the selection is reset.
        /// </summary>
        public UISelectable DefaultSelect
        {
            set
            {
                GraphNode node = find(value);
                if (node == null) throw new KeyNotFoundException("This selectable widget isn't in the graph. Forgot to add it?");

                DefaultNode = node;
            }
            get
            {
                return DefaultNode.element;
            }
        }
        #endregion

        #region Constructor
        public UISelector()
        {
            this.nodes = new List<GraphNode>();
            this.selectedNode = null;
            this.InputDevice = ServiceProvider.GetService<InputService>().GetDevice(0);
        }
        #endregion

        #region Node management
        /// <summary>
        /// Find the node that holds a widget
        /// </summary>
        /// <param name="widget">Widget to search for</param>
        /// <returns>The GraphNode holding widget or null if widget was
        /// not found.</returns>
        private GraphNode find(UISelectable widget)
        {
            foreach (GraphNode node in nodes)
            {
                if (node.element.Equals(widget)) return node;
            }

            return null;
        }
        /// <summary>
        /// Creates a new node and adds it to the list. Also set selectedNode and DefaultNode
        /// if $widget is the first widget.
        /// </summary>
        /// <param name="widget">Widget to add</param>
        /// <returns>The node that holds the widget</returns>
        private GraphNode newNode(UISelectable widget)
        {
            GraphNode node = new GraphNode(widget);
            if (nodes.Count == 0)
            {
                selectedNode = node;
                DefaultNode = node;
                widget.SetSelected(true);
            }
            nodes.Add(node);
            return node;
        }
        /// <summary>
        /// Sets the selection of a node
        /// </summary>
        /// <param name="select"></param>
        private void Select(GraphNode select)
        {
            selectedNode.element.SetSelected(false);
            selectedNode = select;
            selectedNode.element.SetSelected(true);

            if (UISounds.ItemChangedSound != null)
            {
                UISounds.ItemChangedSound.Play();
            }
        }

        public void ResetSelection()
        {
            if(DefaultNode!=null)
                Select(DefaultNode);
        }

        /// <summary>
        /// Add an input link.
        /// </summary>
        /// <param name="node1">Top / left node</param>
        /// <param name="node2">Bottom / right node</param>
        /// <param name="axis">Input axis that allows the user to pass from one to
        /// another node.</param>
        public void AddLink(UISelectable node1, UISelectable node2, InputAxis axis)
        {
            GraphNode gnode1 = find(node1);
            GraphNode gnode2 = find(node2);

            if (gnode1 == null) gnode1 = newNode(node1);
            if (gnode2 == null) gnode2 = newNode(node2);

            GraphEdge edge = new GraphEdge(gnode1, gnode2, axis);

            gnode1.edges.Add(edge);
            gnode2.edges.Add(edge);
        }
        #endregion

        #region Updating the selection
        private bool tryToggleSelect1(GraphEdge e, string inputKey)
        {
            if (InputDevice.IsClicked(inputKey))
            {
                Select(e.Node1);
                return true;
            }
            else return false;
        }
        private bool tryToggleSelect2(GraphEdge e, string inputKey)
        {
            if (InputDevice.IsClicked(inputKey))
            {
                Select(e.Node2);
                return true;
            }
            else return false;
        }

        public void Update()
        {
            // TODO get input 
            if(selectedNode == null) return;

            foreach (GraphEdge e in selectedNode.edges)
            {
                if (e.Node1.Equals(selectedNode))
                {
                    // Selected node is on the top / left
                    switch (e.Axis)
                    {
                        case InputAxis.D_X: if (tryToggleSelect2(e, "D_Right")) return; break;
                        case InputAxis.D_Y: if (tryToggleSelect2(e, "D_Down")) return; break;
                        case InputAxis.LS_X: if (tryToggleSelect2(e, "LS_Right")) return; break;
                        case InputAxis.LS_Y: if (tryToggleSelect2(e, "LS_Down")) return; break;
                        case InputAxis.RS_X: if (tryToggleSelect2(e, "RS_Right")) return; break;
                        case InputAxis.RS_Y: if (tryToggleSelect2(e, "RS_Down")) return; break;
                        case InputAxis.Bumpers: if (tryToggleSelect2(e, "RB")) return; break;
                        case InputAxis.Triggers: if (tryToggleSelect2(e, "RT")) return; break;

                        default:
                            break;
                    }
                }
                else
                {
                    // Selected node is on the bottom / right
                    switch (e.Axis)
                    {
                        case InputAxis.D_X: if (tryToggleSelect1(e, "D_Left")) return; break;
                        case InputAxis.D_Y: if (tryToggleSelect1(e, "D_Up")) return; break;
                        case InputAxis.LS_X: if (tryToggleSelect1(e, "LS_Left")) return; break;
                        case InputAxis.LS_Y: if (tryToggleSelect1(e, "LS_Up")) return; break;
                        case InputAxis.RS_X: if (tryToggleSelect1(e, "RS_Left")) return; break;
                        case InputAxis.RS_Y: if (tryToggleSelect1(e, "RS_Up")) return; break;
                        case InputAxis.Bumpers: if (tryToggleSelect1(e, "LB")) return; break;
                        case InputAxis.Triggers: if (tryToggleSelect1(e, "LT")) return; break;

                        default:
                            break;
                    }
                }
            }
        }
        #endregion
    }
}
