﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Linq;
using System.IO;

using System.Diagnostics;

using Engine.Core;
using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Engine.UI
{
    /// <summary>
    /// This a class which represents a mapping to a specific component
    /// on the atlas. It draws the component using a Sprite batch and
    /// 9 Draw() calls (4 borders, 4 corner, 1 center)
    /// </summary>
    internal class UIElementPainter
    {
        #region Stored properties
        internal int X { set; get; }
        internal int Y { set; get; }
        internal int Width { set; get; }
        internal int Height { set; get; }
        internal int BorderWidth { set; get; }
        internal int BorderHeight { set; get; }
        #endregion

        #region Rectangle Calcutions

        internal Rectangle CenterRectangle(int x, int y, int w, int h)
        {
            return new Rectangle(
                x + BorderWidth, 
                y + BorderHeight, 
                w - BorderWidth * 2,
                h - BorderHeight * 2);
        }

        internal Rectangle TopBorder(int x, int y, int w, int h)
        {
            return new Rectangle(
                x + BorderWidth,
                y,
                w - BorderWidth * 2,
                BorderHeight);
        }
        internal Rectangle BottomBorder(int x, int y, int w, int h)
        {
            return new Rectangle(
                x + BorderWidth, 
                y + h - BorderHeight, 
                w - BorderWidth * 2, 
                BorderHeight);
        }
        internal Rectangle LeftBorder(int x, int y, int w, int h)
        {
            return new Rectangle(
                x, 
                y + BorderHeight,
                BorderWidth,
                h - BorderHeight * 2);
        }
        internal Rectangle RightBorder(int x, int y, int w, int h)
        {
            return new Rectangle(
                x + w - BorderWidth,
                y + BorderHeight,
                BorderWidth,
                h - BorderHeight * 2);
        }

        internal Rectangle TopLeftCorner(int x, int y, int w, int h)
        {
            return new Rectangle(
                x, 
                y, 
                BorderWidth, 
                BorderHeight);
        }
        internal Rectangle TopRightCorner(int x, int y, int w, int h)
        {
            return new Rectangle(
                x + w - BorderWidth,
                y,
                BorderWidth,
                BorderHeight);
        }
        internal Rectangle BottomLeftCorner(int x, int y, int w, int h)
        {
            return new Rectangle(
                x,
                y + h - BorderHeight, 
                BorderWidth,
                BorderHeight);
        }
        internal Rectangle BottomRightCorner(int x, int y, int w, int h)
        {
            return new Rectangle(
                x + w - BorderWidth,
                y + h - BorderHeight,
                BorderWidth,
                BorderHeight);
        }
        #endregion

        /// <summary>
        /// Paint with this element painter
        /// </summary>
        /// <param name="painter">Painter used</param>
        /// <param name="source">Texture to sample</param>
        /// <param name="destination">Destination in physical coords</param>
        internal void DrawTexture(UIPainter painter, Texture2D source, Rectangle destination)
        {
            SpriteBatch batch = painter.SpriteBatch;

            int dx = destination.X;
            int dy = destination.Y;
            int dw = destination.Width;
            int dh = destination.Height;

            // Draw the center
            batch.Draw(source, 
                CenterRectangle(dx, dy, dw, dh), 
                CenterRectangle(X, Y, Width, Height),
                Color.White);

            // Draw the top / bottom borders:
            batch.Draw(source,
                TopBorder(dx, dy, dw, dh),
                TopBorder(X, Y, Width, Height),
                Color.White);
            batch.Draw(source,
                BottomBorder(dx, dy, dw, dh),
                BottomBorder(X, Y, Width, Height),
                Color.White);

            // Draw the left and right borders:
            batch.Draw(source,
                LeftBorder(dx, dy, dw, dh),
                LeftBorder(X, Y, Width, Height),
                Color.White);
            batch.Draw(source,
                RightBorder(dx, dy, dw, dh),
                RightBorder(X, Y, Width, Height),
                Color.White);

            // Draw the corners:
            batch.Draw(source,
                TopLeftCorner(dx, dy, dw, dh),
                TopLeftCorner(X, Y, Width, Height),
                Color.White);
            batch.Draw(source,
                TopRightCorner(dx, dy, dw, dh),
                TopRightCorner(X, Y, Width, Height),
                Color.White);
            batch.Draw(source,
                BottomLeftCorner(dx, dy, dw, dh),
                BottomLeftCorner(X, Y, Width, Height),
                Color.White);
            batch.Draw(source,
                BottomRightCorner(dx, dy, dw, dh),
                BottomRightCorner(X, Y, Width, Height),
                Color.White);
        }
    }

    public class UIAtlas
    {
        #region Variables
        private Texture2D wholeTexture;
        private Dictionary<string, UIElementPainter> hash;
        #endregion

        #region Constructors
        internal UIAtlas(Texture2D texture, Dictionary<string, UIElementPainter> hash)
        {
            wholeTexture = texture;
            this.hash = hash;
        }
        #endregion

        internal Texture2D Texture { get { return wholeTexture; } }

        internal UIElementPainter GetPainter(string key) { return hash[key]; }

        /// <summary>
        /// Load a UIAtlas from an XML file and a texture object
        /// </summary>
        /// <param name="texture">Texture to map to</param>
        /// <param name="atlasMappingFile">XML file that describes the mapping</param>
        /// <returns></returns>
        public static UIAtlas LoadAtlas(Texture2D texture, string atlasMappingFile)
        {
            Stream xmlStream = TitleContainer.OpenStream(atlasMappingFile);
            Debug.Assert(xmlStream != null);

            XDocument xml = XDocument.Load(xmlStream);
            Debug.Assert(xml != null);

            
            UIElementPainter[] elements = 
                (from elem in xml.Descendants("SubTexture")
                 select new UIElementPainter()
                 {
                     X = int.Parse(elem.Attribute("x").Value),
                     Y = int.Parse(elem.Attribute("y").Value),
                     Width = int.Parse(elem.Attribute("width").Value),
                     Height = int.Parse(elem.Attribute("height").Value),
                     BorderWidth = int.Parse(elem.Attribute("border-width").Value),
                     BorderHeight = int.Parse(elem.Attribute("border-height").Value)
                 }).ToArray();

            string[] keys =
                (from elem in xml.Descendants("SubTexture")
                 select elem.Attribute("name").Value).ToArray();

            xmlStream.Close();

            if(elements.Length != keys.Length) throw new Exception("keys/values length mismatch");

            Dictionary<string, UIElementPainter> hash = new Dictionary<string, UIElementPainter>();
            for (int i = 0; i < elements.Length; i++)
                hash.Add(keys[i], elements[i]);

            UIAtlas ret = new UIAtlas(texture, hash);
            return ret;
        }
    }
}
