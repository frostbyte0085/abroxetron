﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;

using System.Diagnostics;

namespace Engine.Tools
{
    public static class SpriteHelper
    {
        private static GraphicsService gs;

        public static SpriteBatch SpriteBatch { internal set; get; }

        private static Matrix projectionHalfPixel;

        public static Matrix Projection
        {
            internal set
            {
                projectionHalfPixel = value;
            }
            get
            {
                return projectionHalfPixel;
            }
        }

        internal static void PrepareProjection()
        {
            if (gs == null)
            {
                gs = ServiceProvider.GetService<GraphicsService>();
                Debug.Assert(gs != null);
            }

            if (SpriteBatch == null)
                SpriteBatch = new Microsoft.Xna.Framework.Graphics.SpriteBatch(gs.GraphicsDevice);

            Projection = Matrix.CreateTranslation(-0.5f, -0.5f, 0) * Matrix.CreateOrthographicOffCenter(0, gs.GraphicsDevice.Viewport.Width, gs.GraphicsDevice.Viewport.Height, 0, 0, 1);
        }

        public static void Draw(Texture2D texture, int width, int height, Effect effect)
        {
            effect.Parameters["MatrixTransform"].SetValue(SpriteHelper.Projection);
            effect.Parameters["TextureSize"].SetValue(new Vector2(texture.Width, texture.Height));

            SpriteBatch.Begin(0, BlendState.Opaque, null, null, null, effect);

            SpriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
            
            SpriteBatch.End();
        }

        public static void Draw(Texture2D texture, RenderTarget2D renderTarget, Effect effect)
        {
            PresentationParameters pp = gs.GraphicsDevice.PresentationParameters;
            gs.GraphicsDevice.SetRenderTarget(renderTarget);

            //Draw(texture, renderTarget.Width, renderTarget.Height, effect);
            Draw(texture, pp.BackBufferWidth, pp.BackBufferHeight, effect);
        }

        public static void Draw(Texture2D texture, Effect effect)
        {
            PresentationParameters pp = gs.GraphicsDevice.PresentationParameters;

            Draw(texture, pp.BackBufferWidth, pp.BackBufferHeight, effect);
        }
    }
}
