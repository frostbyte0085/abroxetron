﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Engine.Tools
{
    public sealed class Parser
    {
        public static Vector3 ParseVector3(string s)
        {
            string[] strings = s.Split(',');
            return new Vector3(
                float.Parse(strings[0]),
                float.Parse(strings[1]),
                float.Parse(strings[2]));
        }

        public static string UnparseVector3(Vector3 v)
        {
            return v.X + "," + v.Y + "," + v.Z;
        }

        public static Vector4 ParseVector4(string s)
        {
            string[] strings = s.Split(',');
            return new Vector4(
                float.Parse(strings[0]),
                float.Parse(strings[1]),
                float.Parse(strings[2]),
                float.Parse(strings[3]));
        }

        public static string UnparseColor(Color c)
        {
            return
            (c.R / 255.0f) + "," +
            (c.G / 255.0f) + "," +
            (c.B / 255.0f) + "," +
            (c.A / 255.0f);
        }

        public static Quaternion ParseQuaternion(string s)
        {
            string[] strings = s.Split(',');
            return new Quaternion(
                float.Parse(strings[0]),
                float.Parse(strings[1]),
                float.Parse(strings[2]),
                float.Parse(strings[3]));
        }
    }
}
