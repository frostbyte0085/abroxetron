﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;

namespace Engine.Tools
{
    public class BlockingQueue<T>
    {
        private Queue<T> q = new Queue<T>();
        private Semaphore sem = new Semaphore(0, Int32.MaxValue);

        public void Enqueue(T element)
        {
            lock (q)
            {
                q.Enqueue(element);
            }
            sem.Release();
        }

        public T Get()
        {
            sem.WaitOne();
            lock (q)
            {
                return q.Dequeue();
            }
        }

        public bool IsEmpty
        {
            get
            {
                bool ret = false;
                lock (q)
                {
                    ret = (q.Count == 0);
                }
                return ret;
            }
        }
    }
}
