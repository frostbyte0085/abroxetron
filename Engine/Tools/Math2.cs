﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Kinect;

namespace Engine.Tools
{
    /**
     * Quaternion to Euler vector conversion:
     * http://forums.create.msdn.com/forums/p/4574/23988.aspx#23988
     */
    public static class Math2
    {
        //In a 2D grid, returns the angle to a specified point from the +X axis
        public static float ArcTanAngle(float X, float Y)
        {
            if (X == 0)
            {
                if (Y == 1)
                    return (float)MathHelper.PiOver2;
                else
                    return (float)-MathHelper.PiOver2;
            }
            else if (X > 0)
                return (float)Math.Atan(Y / X);
            else if (X < 0)
            {
                if (Y > 0)
                    return (float)Math.Atan(Y / X) + MathHelper.Pi;
                else
                    return (float)Math.Atan(Y / X) - MathHelper.Pi;
            }
            else
                return 0;
        }

        //returns Euler angles that point from one point to another
        public static Vector3 AngleTo(Vector3 from, Vector3 location)
        {
            Vector3 angle = new Vector3();
            Vector3 v3 = Vector3.Normalize(location - from);
            angle.X = (float)Math.Asin(v3.Y);
            angle.Y = ArcTanAngle(-v3.Z, -v3.X);
            return angle;
        }

        //converts a Quaternion to Euler angles (X = pitch, Y = yaw, Z = roll)
        public static Vector3 QuaternionToEuler(Quaternion rotation)
        {
            Vector3 rotationaxes = new Vector3();

            Vector3 forward = Vector3.Transform(Vector3.Forward, rotation);
            Vector3 up = Vector3.Transform(Vector3.Up, rotation);
            rotationaxes = AngleTo(new Vector3(), forward);
            if (rotationaxes.X == MathHelper.PiOver2)
            {
                rotationaxes.Y = ArcTanAngle(up.Z, up.X);
                rotationaxes.Z = 0;
            }
            else if (rotationaxes.X == -MathHelper.PiOver2)
            {
                rotationaxes.Y = ArcTanAngle(-up.Z, -up.X);
                rotationaxes.Z = 0;
            }
            else
            {
                up = Vector3.Transform(up, Matrix.CreateRotationY(-rotationaxes.Y));
                up = Vector3.Transform(up, Matrix.CreateRotationX(-rotationaxes.X));
                rotationaxes.Z = ArcTanAngle(up.Y, -up.X);
            }
            return rotationaxes;
        }

        public static Quaternion RotateAroundPivot(Vector3 pivotPoint, Quaternion quat)
        {
            Quaternion retQuat = Quaternion.Identity;

            Matrix minusOffset = Matrix.CreateTranslation(-pivotPoint);
            Matrix rotMatrix = Matrix.CreateFromQuaternion(quat);
            Matrix positiveOffset = Matrix.CreateTranslation(pivotPoint);

            Matrix finalMatrix = minusOffset * rotMatrix * positiveOffset;
            Vector3 ss, tt;
            finalMatrix.Decompose(out ss, out retQuat, out tt);
            return retQuat;
        }

        public static bool IsPointInSphere(BoundingSphere sphere, Vector3 point)
        {
            Vector3 diffVect = point - sphere.Center;

            return (diffVect.LengthSquared() < (float)Math.Abs(sphere.Radius * sphere.Radius));
        }

        #region Kinect functionality
        public static Vector2 ToVector2(SkeletonPoint point)
        {
            return new Vector2(point.X, point.Y);
        }

        public static Vector3 ToVector3(SkeletonPoint point)
        {
            return new Vector3(point.X, point.Y, point.Z);
        }

        public static SkeletonPoint ToSkeletonPoint(Vector3 v)
        {
            SkeletonPoint p = new SkeletonPoint();
            p.X = v.X;
            p.Y = v.Y;
            p.Z = v.Z;
            return p;
        }

        public static Vector2 GetOrthographicScreenPoint(Joint joint, int screenW, int screenH)
        {
            return new Vector2(
                ((0.5f * joint.Position.X) + 0.5f) * (screenW),
                ((-0.5f * joint.Position.Y) + 0.5f) * (screenH) );
        }
        #endregion
    }
}
