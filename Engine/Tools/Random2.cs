﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Engine.Tools
{
    /// <summary>
    /// A class that provides tools for generating random numbers.
    /// </summary>
    public sealed class Random2
    {
        private static readonly Random rand = new Random();

        private Random2() { }

        /// <summary>
        /// Returns a random number in the [min,max] interval.
        /// </summary>
        /// <param name="min">Smallest value possible (inclusive)</param>
        /// <param name="max">Largest value possible (inclusive)</param>
        /// <returns>Random int</returns>
        public static int Random(int min, int max)
        {
            return rand.Next(max - min + 1) + min;
        }

        /// <summary>
        /// Returns a random number in the [min,max] interval
        /// </summary>
        /// <param name="min">Smallest value possible (inclusive)</param>
        /// <param name="max">Largest value possible (inclusive)</param>
        /// <returns>Random float</returns>
        public static float RandomF(float min, float max)
        {
            float randomf = min + (float)rand.NextDouble() * (max - min);
            return randomf;
        }

        /// <summary>
        /// Returns a random angle (in radians) in the [0,pi*2] interval
        /// </summary>
        /// <returns>Random float</returns>
        public static float RandomAngle() { return RandomF(0, (float)Math.PI * 2); }


        public static int RandomSign()
        {
            return Random(0, 1) == 0 ? -1 : 1;
        }

        /// <summary>
        /// Return a vector pointing in any random direction
        /// </summary>
        /// <param name="minLength">Minimum length</param>
        /// <param name="maxLength">Maximum length</param>
        /// <returns>A vector of a random length between [minLength, maxLength]</returns>
        public static Vector3 RandomVector3(float minLength, float maxLength)
        {
            float len = RandomF(minLength, maxLength);
            float bearing = RandomAngle();
            float elevation = RandomAngle();

            Vector3 v = new Vector3(0, 0, len);
            v = Vector3.Transform(v, Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), elevation));
            v = Vector3.Transform(v, Quaternion.CreateFromAxisAngle(new Vector3(0, 1, 0), bearing));

            return v;
        }

        /// <summary>
        /// Return a vector pointing in any random direction
        /// </summary>
        /// <param name="minLength">Minimum length</param>
        /// <param name="maxLength">Maximum length</param>
        /// <param name="minB">Minimum bearing angle</param>
        /// <param name="maxB">Maximum bearing angle</param>
        /// <param name="minE">Minimum elevation angle</param>
        /// <param name="maxE">Maximum elevation angle</param>
        /// <returns>A vector of a random length between [minLength, maxLength]</returns>
        public static Vector3 RandomVector3(float minLength, float maxLength, float minB, float maxB, float minE, float maxE)
        {
            float len = RandomF(minLength, maxLength);
            float bearing = RandomF(minB, maxB);
            float elevation = RandomF(minE, maxE);

            Vector3 v = new Vector3(0, 0, len);
            v = Vector3.Transform(v, Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), elevation));
            v = Vector3.Transform(v, Quaternion.CreateFromAxisAngle(new Vector3(0, 1, 0), bearing));

            return v;
        }
    }
}