﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine;
using Engine.Core;
using Engine.Diagnostics;
using Engine.Tools;

namespace Ghast
{
    public class GhastApplication : EngineApplication
    {
        static GhastApplication _app;

        public GhastApplication()
            : base()
        {

        }

        public static GhastApplication App
        {
            get
            {
                if (_app == null)
                    _app = new GhastApplication();
                return _app;
            }
        }

        public override void Initialize(EngineConfiguration configuration)
        {
            base.Initialize(configuration);
        }

        public override void SetupStates()
        {
            AddState("GhastEditor", new GhastEditorState());
            SetState("GhastEditor");

            base.SetupStates();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
