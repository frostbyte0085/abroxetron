﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Engine.Core;
using Engine.Graphics;


namespace Ghast
{
    public static class ParticleSystemData
    {
        #region animation
        private static Color[] _colors;
        private static Vector3 _gravity;
        private static float _growSize;
        private static float _interval;
        private static bool _loop;
        private static int _maxAmount;
        private static int _minAmount;
        private static float _maxEnergy;
        private static float _minEnergy;
        private static float _maxSize;
        private static float _minSize;
        private static float _maxSpeed;
        private static float _minSpeed;
        private static string _textureFileName;


        private static ParticleSystemSpaceMode _spaceUpdateMode;

        public static ParticleSystemSpaceMode SpaceUpdateMode
        {
            set
            {
                StateNeedsUpdate = true;
                _spaceUpdateMode = value;
            }
            get
            {
                return _spaceUpdateMode;
            }
        }

        public static Color[] Colors
        {
            set
            {
                StateNeedsUpdate = true;
                _colors = value;
            }
            get
            {
                return _colors;
            }
        }

        public static Vector3 Gravity
        {
            set
            {
                StateNeedsUpdate = true;
                _gravity = value;
            }
            get
            {
                return _gravity;
            }
        }

        public static float GrowSize
        {
            set
            {
                StateNeedsUpdate = true;
                _growSize = value;
            }
            get
            {
                return _growSize;
            }
        }

        public static float Interval
        {
            set
            {
                StateNeedsUpdate = true;
                _interval = value;
            }
            get
            {
                return _interval;
            }
        }

        public static bool Loop
        {
            set
            {
                StateNeedsUpdate = true;
                _loop = value;
            }
            get
            {
                return _loop;
            }
        }

        public static int MaxAmount
        {
            set
            {
                StateNeedsUpdate = true;
                _maxAmount = value;
            }
            get
            {
                return _maxAmount;
            }
        }

        public static int MinAmount
        {
            set
            {
                StateNeedsUpdate = true;
                _minAmount = value;
            }
            get
            {
                return _minAmount;
            }
        }

        public static float MaxEnergy
        {
            set
            {
                StateNeedsUpdate = true;
                _maxEnergy = value;
            }
            get
            {
                return _maxEnergy;
            }
        }

        public static float MinEnergy
        {
            set
            {
                StateNeedsUpdate = true;
                _minEnergy = value;
            }
            get
            {
                return _minEnergy;
            }
        }

        public static float MaxSize
        {
            set
            {
                StateNeedsUpdate = true;
                _maxSize = value;
            }
            get
            {
                return _maxSize;
            }
        }

        public static float MinSize
        {
            set
            {
                StateNeedsUpdate = true;
                _minSize = value;
            }
            get
            {
                return _minSize;
            }
        }

        public static float MaxSpeed
        {
            set
            {
                StateNeedsUpdate = true;
                _maxSpeed = value;
            }
            get
            {
                return _maxSpeed;
            }
        }

        public static float MinSpeed
        {
            set
            {
                StateNeedsUpdate = true;
                _minSpeed = value;
            }
            get
            {
                return _minSpeed;
            }
        }

        public static string TextureFileName
        {
            set
            {
                StateNeedsUpdate = true;
                _textureFileName = value;
            }
            get
            {
                return _textureFileName;
            }
        }
        
        public static void Reset()
        {
            Colors = new Color[4];
            Colors[0] = Color.Transparent;
            Colors[1] = Color.White;
            Colors[2] = Color.DarkGray;
            Colors[3] = Color.Transparent;
            Gravity = new Vector3(0, 0, 0);
            GrowSize = 0.0f;
            Interval = 0.1f;
            Loop = true;
            MaxAmount = 15;
            MinAmount = 15;
            MaxEnergy = 1.0f;
            MinEnergy = 1.0f;
            MaxSize = 0.5f;
            MinSize = 0.5f;
            MaxSpeed = 1.0f;
            MinSpeed = 1.0f;
            TextureFileName = "Particles/Textures/Fire";

            StateNeedsReset = true;
        }

        public static void FromParticleAnimation(ParticleAnimation animation)
        {
            Colors = animation.Colors;
            Gravity = animation.Gravity;
            GrowSize = animation.GrowSize;
            Interval = animation.Interval;
            Loop = animation.Loop;
            MaxAmount = animation.MaxAmount;
            MinAmount = animation.MinAmount;
            MaxEnergy = animation.MaxEnergy;
            MinEnergy = animation.MinEnergy;
            MaxSize = animation.MaxSize;
            MinSize = animation.MinSize;
            MaxSpeed = animation.MaxSpeed;
            MinSpeed = animation.MinSpeed;
            TextureFileName = animation.TextureFileName;

            StateNeedsReset = true;
        }
        #endregion

        #region Messages to the state

        public static bool StateNeedsReset;
        public static bool StateNeedsUpdate;

        #endregion
    }
}
