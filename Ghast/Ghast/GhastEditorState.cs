﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Engine.Core;
using Engine.Scene;
using Engine.Tools;
using Engine.Graphics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Ghast
{
    public class GhastEditorState : State
    {
        SceneNode cameraNode;
        SceneNode particleSystemNode;

        SceneService ss;
        GraphicsService gs;
        ContentService cs;

        ParticleSystem particleSystem;
        ParticleAnimation particleAnimation;

        
        public GhastEditorState()
        {
            ss = ServiceProvider.GetService<SceneService>();
            gs = ServiceProvider.GetService<GraphicsService>();
            cs = ServiceProvider.GetService<ContentService>();
                        
        }

        private void Reset()
        {
            ss.Clear();

            cameraNode = ss.AddSceneNode("camera");
            cameraNode.Camera = new Camera();
            cameraNode.Camera.SetupPerspective(45.0f, gs.GraphicsDevice.Viewport.AspectRatio, 0.1f, 1000.0f);
            cameraNode.Translation = new Vector3(0, 0, 10);
            ss.ActiveCameraNode = cameraNode;

            particleSystemNode = ss.AddSceneNode("particleSystem");

            particleAnimation = new ParticleAnimation();
            UpdateParticleData();

            particleSystem = new ParticleSystem(particleAnimation, ParticleSystemSpaceMode.WorldSpace);
            particleSystemNode.ParticleAttachMode = ParticleAttachMode.Default;
            particleSystem.StartEmitting();

            particleSystemNode.Attach("node", particleSystem);
        }

        private void UpdateParticleData()
        {
            if (ParticleSystemData.StateNeedsUpdate)
            {
                particleAnimation.Colors = ParticleSystemData.Colors;
                particleAnimation.Gravity = ParticleSystemData.Gravity;
                particleAnimation.GrowSize = ParticleSystemData.GrowSize;
                particleAnimation.Interval = ParticleSystemData.Interval;
                particleAnimation.Loop = ParticleSystemData.Loop;
                particleAnimation.MaxAmount = ParticleSystemData.MaxAmount;
                particleAnimation.MinAmount = ParticleSystemData.MinAmount;
                particleAnimation.MaxEnergy = ParticleSystemData.MaxEnergy;
                particleAnimation.MinEnergy = ParticleSystemData.MinEnergy;
                particleAnimation.MaxSize = ParticleSystemData.MaxSize;
                particleAnimation.MinSize = ParticleSystemData.MinSize;
                particleAnimation.MaxSpeed = ParticleSystemData.MaxSpeed;
                particleAnimation.MinSpeed = ParticleSystemData.MinSpeed;
                particleAnimation.Texture = cs["game"].Load<Texture2D>(ParticleSystemData.TextureFileName);

                ParticleSystemData.StateNeedsUpdate = false;

                if (particleSystem != null)
                    particleSystem.ParticleSystemSpaceMode = ParticleSystemData.SpaceUpdateMode;
            }
        }

        private void ResetParticleData()
        {
            if (ParticleSystemData.StateNeedsReset)
            {

                Reset();

                ParticleSystemData.StateNeedsReset = false;
            }
        }

        private void UpdateInput()
        {
            
            KeyboardState ks = Keyboard.GetState();
            // camera movement
            if (ks.IsKeyDown(Keys.Up))
            {
                cameraNode.Translate(new Vector3(0, 1, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.Down))
            {
                cameraNode.Translate(new Vector3(0, -1, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.Left))
            {
                cameraNode.Translate(new Vector3(-1, 0, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.Right))
            {
                cameraNode.Translate(new Vector3(1, 0, 0) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.W))
            {
                cameraNode.Translate(new Vector3(0, 0, -1) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.S))
            {
                cameraNode.Translate(new Vector3(0, 0, 1) * GameTiming.TimeElapsed * 10);
            }
            if (ks.IsKeyDown(Keys.C))
            {
                cameraNode.Translation = new Vector3(0, 0, 10);
            }
            // particle system movement
            if (ks.IsKeyDown(Keys.Y))
            {
                particleSystemNode.Translate(new Vector3(0, 1, 0) * GameTiming.TimeElapsed * 7);
            }
            if (ks.IsKeyDown(Keys.H))
            {
                particleSystemNode.Translate(new Vector3(0, -1, 0) * GameTiming.TimeElapsed * 7);
            }
            if (ks.IsKeyDown(Keys.G))
            {
                particleSystemNode.Translate(new Vector3(-1, 0, 0) * GameTiming.TimeElapsed * 7);
            }
            if (ks.IsKeyDown(Keys.J))
            {
                particleSystemNode.Translate(new Vector3(1, 0, 0) * GameTiming.TimeElapsed * 7);
            }
        }

        public override bool OnEnter()
        {
            return base.OnEnter();
        }

        public override bool OnUpdate()
        {
            UpdateInput();
            ResetParticleData();
            UpdateParticleData();

            return base.OnUpdate();
        }

        public override bool OnExit()
        {

            return base.OnExit();
        }
    }
}
