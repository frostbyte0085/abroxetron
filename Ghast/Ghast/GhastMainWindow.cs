﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Engine;
using Engine.Tools;
using Engine.Core;
using Engine.Scene;
using Engine.Graphics;

namespace Ghast
{
    public partial class GhastMainWindow : Form
    {
        GhastApplication app;
        string previousFilename;

        public GhastMainWindow()
        {
            InitializeComponent();
        }

        private void GhastMainWindow_Load(object sender, EventArgs e)
        {
            EngineConfiguration configuration = new EngineConfiguration();
            configuration.Handle = xnaTarget.Handle;
            configuration.TargetWidth = xnaTarget.Width;
            configuration.TargetHeight = xnaTarget.Height;
            configuration.Fullscreen = false;
            configuration.VSync = false;
            configuration.Name = "Ghast Particle Editor";

            app = GhastApplication.App;
            app.Initialize(configuration);

            Application.Idle += delegate { Invalidate(); };
            Application.ApplicationExit += delegate { app.Dispose(); };

            radioWorldSpace.Select();
            Reset();
        }

        ParticleSystemPropertyGrid gridProperties = new ParticleSystemPropertyGrid();

        private void Reset()
        {
            ParticleSystemData.Reset();

            gridProperties.UpdateFromData();
            propertyGrid1.SelectedObject = gridProperties;
            propertyGrid1.Refresh();

            saveToolStripMenuItem.Enabled = false;
            previousFilename = "";
        }

        private void GhastMainWindow_Paint(object sender, PaintEventArgs e)
        {
            if (app.ExitRequested)
                Application.Exit();

            app.OneFrame();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void aboutGhastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = ofn.ShowDialog(this);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader reader = new StreamReader(ofn.FileName);
                ParticleAnimation loadedAnim = ParticleAnimation.CreateParticleAnimation(reader.BaseStream);

                ParticleSystemData.FromParticleAnimation(loadedAnim);
                gridProperties.UpdateFromData();
                propertyGrid1.Refresh();

                previousFilename = ofn.FileName;
                saveToolStripMenuItem.Enabled = true;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = sfn.ShowDialog(this);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                previousFilename = sfn.FileName;
                
                saveToolStripMenuItem_Click(sender, e);
                                
                saveToolStripMenuItem.Enabled = true;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reset();
        }

        // change the particle update mode
        private void radioWorldSpace_CheckedChanged(object sender, EventArgs e)
        {
            ParticleSystemData.SpaceUpdateMode = ParticleSystemSpaceMode.WorldSpace;
        }

        private void radioLocalSpace_CheckedChanged(object sender, EventArgs e)
        {
            ParticleSystemData.SpaceUpdateMode = ParticleSystemSpaceMode.LocalSpace;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SceneNode pnode = ServiceProvider.GetService<SceneService>()["particleSystem"];
            ParticleSystem psystem = null;
            pnode.GetAttachment("node", out psystem);
            if (psystem != null)
                ParticleAnimation.SaveToXmlFile(psystem.Animation, ParticleSystemData.TextureFileName, previousFilename);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SceneNode pnode = ServiceProvider.GetService<SceneService>()["particleSystem"];
            ParticleSystem psystem = null;
            pnode.GetAttachment("node", out psystem);
            if (psystem != null)
                psystem.StartEmitting();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SceneNode pnode = ServiceProvider.GetService<SceneService>()["particleSystem"];
            ParticleSystem psystem = null;
            pnode.GetAttachment("node", out psystem);
            if (psystem != null)
                psystem.StopEmitting();
        }

        private void GhastMainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            app.ExitRequested = true;
        }
    }
}
