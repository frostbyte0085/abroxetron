﻿using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Engine.Core;
using Engine.Graphics;


namespace Ghast
{

    public class ParticleSystemPropertyGrid
    {
        #region Animation
        private System.Drawing.Color[] _colors;
        private Vector3 _gravity;
        private float _growSize;
        private float _interval;
        private bool _loop;
        private int _maxAmount;
        private int _minAmount;
        private float _maxEnergy;
        private float _minEnergy;
        private float _maxSize;
        private float _minSize;
        private float _maxSpeed;
        private float _minSpeed;
        private string _textureFileName;
        #endregion

        #region Emitter
        
        #endregion
        [CategoryAttribute("Animation"), DescriptionAttribute("Particle texture")]
        public string Texture
        {
            set
            {
                _textureFileName = value;
                ParticleSystemData.TextureFileName = _textureFileName;
            }
            get
            {
                return _textureFileName;
            }
        }

        [CategoryAttribute("Animation"), DescriptionAttribute("Gravity force")]
        public Vector3 Gravity
        {
            set
            {
                _gravity = value;
                ParticleSystemData.Gravity = _gravity;
            }
            get
            {
                return _gravity;
            }
        }

        [CategoryAttribute("Animation"), DescriptionAttribute("The particle colors")]
        public System.Drawing.Color[] Colors
        {
            set
            {
                _colors = value;
                ParticleSystemData.Colors = new Color[_colors.Length];
                for (int i = 0; i < _colors.Length; i++)
                {
                    ParticleSystemData.Colors[i] = new Color(_colors[i].R, _colors[i].G, _colors[i].B, _colors[i].A);
                }
            }
            get
            {
                ParticleSystemData.Colors = new Color[_colors.Length];
                for (int i = 0; i < _colors.Length; i++)
                {
                    ParticleSystemData.Colors[i] = new Color(_colors[i].R, _colors[i].G, _colors[i].B, _colors[i].A);
                }
                return _colors;
            }
        }

        [CategoryAttribute("Size"), DescriptionAttribute("How much the particle will grow or shrink in time")]
        public float GrowSize
        {
            set
            {
                _growSize = value;
                ParticleSystemData.GrowSize = _growSize;
            }
            get
            {
                return _growSize;
            }
        }

        [CategoryAttribute("Emitting"), DescriptionAttribute("How often will the system emit new particles, measured in miliseconds")]
        public float Interval
        {
            set
            {
                _interval = value;
                ParticleSystemData.Interval = _interval;
            }
            get
            {
                return _interval;
            }
        }

        [CategoryAttribute("Emitting"), DescriptionAttribute("Is the particle system looping?")]
        public bool Loop
        {
            set
            {
                _loop = value;
                ParticleSystemData.Loop = _loop;
            }
            get
            {
                return _loop;
            }
        }

        [CategoryAttribute("Emitting"), DescriptionAttribute("Maximum amount of particles emited")]
        public int MaxAmount
        {
            set
            {
                _maxAmount = value;
                ParticleSystemData.MaxAmount = _maxAmount;
            }
            get
            {
                return _maxAmount;
            }
        }

        [CategoryAttribute("Emitting"), DescriptionAttribute("Minimum amount of particles emited")]
        public int MinAmount
        {
            set
            {
                _minAmount = value;
                ParticleSystemData.MinAmount = _minAmount;
            }
            get
            {
                return _minAmount;
            }
        }

        [CategoryAttribute("Energy"), DescriptionAttribute("Maximum lifetime of the particle, measured in seconds")]
        public float MaxEnergy
        {
            set
            {
                _maxEnergy = value;
                ParticleSystemData.MaxEnergy = _maxEnergy;
            }
            get
            {
                return _maxEnergy;
            }
        }

        [CategoryAttribute("Energy"), DescriptionAttribute("Minimum lifetime of the particle, measured in seconds")]
        public float MinEnergy
        {
            set
            {
                _minEnergy = value;
                ParticleSystemData.MinEnergy = _minEnergy;
            }
            get
            {
                return _minEnergy;
            }
        }

        [CategoryAttribute("Size"), DescriptionAttribute("Maximum particle size during emit stage")]
        public float MaxSize
        {
            set
            {
                _maxSize = value;
                ParticleSystemData.MaxSize = _maxSize;
            }
            get
            {
                return _maxSize;
            }
        }

        [CategoryAttribute("Size"), DescriptionAttribute("Minimum particle size during emit stage")]
        public float MinSize
        {
            set
            {
                _minSize = value;
                ParticleSystemData.MinSize = _minSize;
            }
            get
            {
                return _minSize;
            }
        }

        [CategoryAttribute("Speed"), DescriptionAttribute("Maximum particle speed, measured in meters/second")]
        public float MaxSpeed
        {
            set
            {
                _maxSpeed = value;
                ParticleSystemData.MaxSpeed = _maxSpeed;
            }
            get
            {
                return _maxSpeed;
            }
        }

        [CategoryAttribute("Speed"), DescriptionAttribute("Minimum particle speed, measured in meters/second")]
        public float MinSpeed
        {
            set
            {
                _minSpeed = value;
                ParticleSystemData.MinSpeed = _minSpeed;
            }
            get
            {
                return _minSpeed;
            }
        }

        //
        public void UpdateFromData()
        {
            MaxSpeed = ParticleSystemData.MaxSpeed;
            MinSpeed = ParticleSystemData.MinSpeed;
            MaxAmount = ParticleSystemData.MaxAmount;
            MinAmount = ParticleSystemData.MinAmount;
            MaxEnergy = ParticleSystemData.MaxEnergy;
            MinEnergy = ParticleSystemData.MinEnergy;
            Gravity = ParticleSystemData.Gravity;
            MinSize = ParticleSystemData.MinSize;
            MaxSize = ParticleSystemData.MaxSize;
            Loop = ParticleSystemData.Loop;
            Interval = ParticleSystemData.Interval;
            GrowSize = ParticleSystemData.GrowSize;
            Texture = ParticleSystemData.TextureFileName;

            _colors = new System.Drawing.Color[ParticleSystemData.Colors.Length];
            for (int i = 0; i < _colors.Length; i++)
            {
                _colors[i] = new System.Drawing.Color();

                _colors[i] = System.Drawing.Color.FromArgb(ParticleSystemData.Colors[i].A,
                                               ParticleSystemData.Colors[i].R,
                                               ParticleSystemData.Colors[i].G,
                                                ParticleSystemData.Colors[i].B);

            }
        }
    }
}
